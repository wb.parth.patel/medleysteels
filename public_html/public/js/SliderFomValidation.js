$(document).ready(function () {
    var ext=['image/png','image/jpg','image/jpeg'];  
     
    // $('.m-form__help').html('');
    var file_length_flag=true;  
    var email_length_flag=true;

    var form = $('#content_form');
    // $('.m-form__help').html('');

    form.submit(function(e) { 
        
        $('.help-block').html('');        
        // $('.m-form__help').html('');
        $('.user_image').html('');
        
        if($("input[name='_method']").val()=='post' || $("input[name='_method']").val()=='POST'){
                if($('#slider_image').get(0).files.length>0 && $('#slider_image')[0].files[0].size>=2097152){
                    file_length_flag=false;                
                    $('.user_image').html('File size is greater than 2MB');
                    $('.user_image').show();                
                }else if(($('#slider_image').get(0).files.length<=0)){
                    file_length_flag=false;                
                    $('.user_image').html('Please select slider image');
                    $('.user_image').show();                
                }else if($.inArray($('#slider_image').get(0).files[0].type,ext)<0 || $('#slider_image').get(0).files[0].type==''){
                    file_length_flag=false;
                    $('.user_image').html('The slider image must be a file of type: jpeg, jpg, png.');
                    $('.user_image').show();                             
                }else{
                    $('.user_image').html('');
                    file_length_flag=true;
                }
        }else{
                if($('#slider_image').get(0).files.length>0){
                    if($('#slider_image').get(0).files.length>0 && $('#slider_image')[0].files[0].size>=2097152){
                        file_length_flag=false;                
                        $('.user_image').html('File size is greater than 2MB');
                        $('.user_image').show();                
                    }else if(($('#slider_image').get(0).files.length<=0)){
                        file_length_flag=false;                
                        $('.user_image').html('Please select slider image');
                        $('.user_image').show();                
                    }else if($.inArray($('#slider_image').get(0).files[0].type,ext)<0 || $('#slider_image').get(0).files[0].type==''){
                        file_length_flag=false;
                        $('.user_image').html('The slider image must be a file of type: jpeg, jpg, png.');
                        $('.user_image').show();                             
                    }else{
                        $('.user_image').html('');
                        file_length_flag=true;
                    }
                } else{
                        file_length_flag=true;
                }
        }
        
        $.ajax({
            url     : form.attr('action'),
            type    : form.attr('method'),
            data    : form.serialize(),
            dataType: 'json',
            async:false,
            success : function ( json )
            {      
                if(file_length_flag==false){
                    
                     e.preventDefault();
                }else{
                    return true;                     
                }             
            },
            error: function( json )
            {           
                if(json.status === 422) {
                    e.preventDefault();
                    var errors_ = json.responseJSON;
                    $.each(errors_.errors, function (key, value) {
                        
                        $('.'+key).html(value);                       
                    });
                } else if(file_length_flag==false){
                    
                    e.preventDefault();
                }
            }
        });
    });

 
    $('.file-drop-zone-title').html('Drag & drop file here...');
});