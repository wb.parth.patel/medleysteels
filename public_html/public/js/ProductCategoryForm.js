$(document).ready(function () {
    var ext=['image/png','image/jpg','image/jpeg'];  
     
    $('.m-form__help').html('');
    var file_length_flag=true;  
    var email_length_flag=true;

    var form = $('#content_form');
    $('.m-form__help').html('');

    form.submit(function(e) { 
        
        $('.help-block').html('');        
        $('.m-form__help').html('');
        $('.category_img').html('');
        
        if($("input[name='_method']").val()=='post' || $("input[name='_method']").val()=='POST'){
                if($('#category_img').get(0).files.length>0 && $('#category_img')[0].files[0].size>=2097152){
                    file_length_flag=false;                
                    $('.category_img').html('File size is greater than 2MB');
                    $('.category_img').show();                
                }else if(($('#category_img').get(0).files.length<=0)){
                    file_length_flag=false;                
                    $('.category_img').html('Please select image');
                    $('.category_img').show();                
                }else if($.inArray($('#category_img').get(0).files[0].type,ext)<0 || $('#file-1').get(0).files[0].type==''){
                    file_length_flag=false;
                    $('.category_img').html('The slider image must be a file of type: jpeg, jpg, png.');
                    $('.category_img').show();                             
                }else{
                    $('.category_img').html('');
                    file_length_flag=true;
                }
        }else{
                if($('#category_img').get(0).files.length>0){
                    if($('#category_img').get(0).files.length>0 && $('#category_img')[0].files[0].size>=2097152){
                        file_length_flag=false;                
                        $('.category_img').html('File size is greater than 2MB');
                        $('.category_img').show();                
                    }else if(($('#category_img').get(0).files.length<=0)){
                        file_length_flag=false;                
                        $('.category_img').html('Please select slider image');
                        $('.category_img').show();                
                    }else if($.inArray($('#category_img').get(0).files[0].type,ext)<0 || $('#category_img').get(0).files[0].type==''){
                        file_length_flag=false;
                        $('.category_img').html('The slider image must be a file of type: jpeg, jpg, png.');
                        $('.category_img').show();                             
                    }else{
                        $('.category_img').html('');
                        file_length_flag=true;
                    }
                } else{
                        file_length_flag=true;
                }
        }
        
        $.ajax({
            url     : form.attr('action'),
            type    : form.attr('method'),
            data    : form.serialize(),
            dataType: 'json',
            async:false,
            success : function ( json )
            {      
                if(file_length_flag==false){
                    
                     e.preventDefault();
                }else{
                    return true;                     
                }             
            },
            error: function( json )
            {           
                if(json.status === 422) {
                    e.preventDefault();
                    var errors_ = json.responseJSON;
                    $.each(errors_.errors, function (key, value) {
                        
                        $('.'+key).html(value);                       
                    });
                } else if(file_length_flag==false){
                    
                    e.preventDefault();
                }
            }
        });
    });

    $('#m_table_2').on('click', '#delete_btn', function (e) {
        if(confirm('Are you sure you want to delete this Record?')==false){
            e.preventDefault();
        }
    });

    $('#m_table_2').on('click', '#is_active', function (e) {
        status=$(this).data('status');
        if(status=='Y'){
            message='Are you sure you want to inactivate this Record?';
        }else{
            message='Are you sure you want to activate this Record?';
        }
        if(confirm(message)==false){
            e.preventDefault();
        }
    });

    $('#m_table_2').on('click', '#approved_btn', function (e) {
        approve=$(this).data('approve');
        if(approve=='Y'){
            message='Are you sure you want to block this Record?';
        }else{
            message='Are you sure you want to unblock this Record?';
        }
        if(confirm(message)==false){
            e.preventDefault();
        }
    });
    $('.file-drop-zone-title').html('Drag & drop file here...');
});