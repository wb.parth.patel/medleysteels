<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::GET('/', function () {
    return redirect('index');
});

Auth::routes();

Route::group(['middleware'=>['auth','checkUserRole']],function (){

		Route::get('/home', 'HomeController@index')->name('home');

		Route::GET('/home', 'DashboardController@index')->name('home');
		Route::GET('/dashboard', 'DashboardController@index')->name('dashboard');
		Route::GET('/dashboard', 'DashboardController@index')->name('dashboard');
		Route::GET('/dashboard/contact', 'DashboardController@contact')->name('dashboard.contact');
		Route::GET('/dashboard/about', 'DashboardController@about')->name('dashboard.about');
		Route::GET('/dashboard/daily', 'DashboardController@daily')->name('dashboard.daily');
		Route::GET('/dashboard/weekly', 'DashboardController@weekly')->name('dashboard.weekly');
		Route::GET('/dashboard/monthly', 'DashboardController@monthly')->name('dashboard.monthly');
		Route::GET('/dashboard/yearly', 'DashboardController@yearly')->name('dashboard.yearly');
		Route::GET('/dashboard/all', 'DashboardController@all')->name('dashboard.all');



		Route::group(['namespace' => '\App\Http\Controllers\Admin'], function(){
		//chage password
		Route::get('/changePassword','ChangePasswordController@index')->name('changePassword');
		Route::POST('/changePassword/store','ChangePasswordController@store')->name('changePassword/store');

		Route::GET('/edit-profile','MyProfileController@index')->name('edit-profile.index');
		Route::GET('/edit-profile/edit','MyProfileController@edit')->name('edit-profile.edit');
		Route::PATCH('/edit-profile/update/{id}','MyProfileController@update')->name('edit-profile.update');

		//Module Route
		Route::GET('/module/index','ModuleController@index')->name('module.index');
		Route::GET('/module/data/{filter_data?}','ModuleController@data')->name('module.data');
		Route::GET('/module/create','ModuleController@create')->name('module.create');
		Route::POST('/module/store','ModuleController@store')->name('module.store');
		Route::GET('/module/edit/{id}','ModuleController@edit')->name('module.edit');
		Route::PATCH('/module/update/{id}','ModuleController@update')->name('module.update');
		Route::GET('/module/status/{id}','ModuleController@status')->name('module.status');
		Route::GET('/module/delete/{id}','ModuleController@destroy')->name('module.delete');

		//Module Action
		Route::GET('/moduleaction/index','ModuleactionController@index')->name('moduleaction.index');
		Route::GET('/moduleaction/data','ModuleactionController@data')->name('moduleaction.data');
		Route::GET('/moduleaction/create','ModuleactionController@create')->name('moduleaction.create');
		Route::POST('/moduleaction/store','ModuleactionController@store')->name('moduleaction.store');

		//Module Action
		Route::GET('/userrole/index','UserroleController@index')->name('userrole.index');
		Route::GET('/userrole/data/{filter_data?}','UserroleController@data')->name('userrole.data');
		Route::GET('/userrole/create','UserroleController@create')->name('userrole.create');
		Route::POST('/userrole/store','UserroleController@store')->name('userrole.store');
		Route::GET('/userrole/edit/{id}','UserroleController@edit')->name('userrole.edit');
		Route::PATCH('/userrole/update/{id}','UserroleController@update')->name('userrole.update');
		Route::GET('/userrole/status/{id}','UserroleController@status')->name('userrole.status');
		Route::GET('/userrole/delete/{id}','UserroleController@destroy')->name('userrole.delete');
		Route::GET('/userrole/view/{id}','UserroleController@view')->name('userrole.view');


		//Country routes
		Route::GET('/country/index','CountryManagementController@index')->name('country.index');
		Route::GET('/country/data/{filter_data?}','CountryManagementController@data')->name('country.data');
		Route::GET('/country/create','CountryManagementController@create')->name('country.create');
		Route::POST('/country/store','CountryManagementController@store')->name('country.store');
		Route::GET('/country/edit/{id}','CountryManagementController@edit')->name('country.edit');
		Route::PATCH('/country/update/{id}','CountryManagementController@update')->name('country.update');
		Route::GET('/country/status/{id}','CountryManagementController@status')->name('country.status');
		Route::GET('/country/delete/{id}','CountryManagementController@destroy')->name('country.delete');

		//City routes
		Route::GET('/city/index','CityManagementController@index')->name('city.index');
		Route::GET('/city/data/{filter_data?}','CityManagementController@data')->name('city.data');
		Route::GET('/city/create','CityManagementController@create')->name('city.create');
		Route::POST('/city/store','CityManagementController@store')->name('city.store');
		Route::GET('/city/edit/{id}','CityManagementController@edit')->name('city.edit');
		Route::PATCH('/city/update/{id}','CityManagementController@update')->name('city.update');
		Route::GET('/city/status/{id}','CityManagementController@status')->name('city.status');
		Route::GET('/city/delete/{id}','CityManagementController@destroy')->name('city.delete');

		//State routes
		Route::GET('/state/index','StateManagementController@index')->name('state.index');
		Route::GET('/state/data/{filter_data?}','StateManagementController@data')->name('state.data');
		Route::GET('/state/create','StateManagementController@create')->name('state.create');
		Route::POST('/state/store','StateManagementController@store')->name('state.store');
		Route::GET('/state/edit/{id}','StateManagementController@edit')->name('state.edit');
		Route::PATCH('/state/update/{id}','StateManagementController@update')->name('state.update');
		Route::GET('/state/status/{id}','StateManagementController@status')->name('state.status');
		Route::GET('/state/delete/{id}','StateManagementController@destroy')->name('state.delete');

		Route::get('/city/get-state-list/{id}','CityManagementController@getStateList')->name('city/get-state-list');
			Route::get('/city/get-level-list/{id}','CityManagementController@getLevelList')->name('city/get-level-list');

		//Faq Route

		//CMS Action
		Route::GET('/content-management/index','ContentManagementController@index')->name('content-management.index');

		Route::GET('/content-management/data/{filter_data?}','ContentManagementController@data')->name('content-management.data');
		Route::GET('/content-management/create','ContentManagementController@create')->name('content-management.create');
		Route::POST('/content-management/store','ContentManagementController@store')->name('content-management.store');
		Route::GET('/content-management/show/{id}','ContentManagementController@show')->name('content-management.show');
		Route::GET('/content-management/edit/{id}','ContentManagementController@edit')->name('content-management.edit');
		Route::PATCH('/content-management/update/{id}','ContentManagementController@update')->name('content-management.update');
		Route::GET('/content-management/status/{id}','ContentManagementController@status')->name('content-management.status');
		Route::GET('/content-management/delete/{id}','ContentManagementController@destroy')->name('content-management.delete');
		Route::GET('/content-management/view/{id}','ContentManagementController@view')->name('content-management.view');

		//site configuration
			Route::GET('/site-configuration/index','SiteConfigurationController@index')->name('site-configuration.index');
			Route::GET('/site-configuration/data/{filter_data?}','SiteConfigurationController@data')->name('site-configuration.data');
			Route::GET('/site-configuration/create','SiteConfigurationController@create')->name('site-configuration.create');
			Route::POST('/site-configuration/store','SiteConfigurationController@store')->name('site-configuration.store');
			Route::GET('/site-configuration/edit/{id}','SiteConfigurationController@edit')->name('site-configuration.edit');
			Route::PATCH('/site-configuration/update/{id}','SiteConfigurationController@update')->name('site-configuration.update');
			Route::GET('/site-configuration/status/{id}','SiteConfigurationController@status')->name('site-configuration.status');
			Route::GET('/site-configuration/delete/{id}','SiteConfigurationController@destroy')->name('site-configuration.delete');
			Route::GET('/site-configuration/view/{id}','SiteConfigurationController@view')->name('site-configuration.view');

			// Payment-Management Route
			Route::GET('/payment-management/index','PaymentManagementController@index')->name('payment-management.index');
			Route::GET('/payment-management/data/{filter_data?}','PaymentManagementController@data')->name('payment-management.data');
			Route::GET('/payment-management/view/{id}','PaymentManagementController@view')->name('payment-management.view');

			// Wallet-Management Route
			Route::GET('/wallet-management/index','WalletManagementController@index')->name('wallet-management.index');
			Route::GET('/wallet-management/data/{filter_data?}','WalletManagementController@data')->name('wallet-management.data');
			Route::GET('/wallet-management/view/{id}','WalletManagementController@view')->name('wallet-management.view');

			// Buy-Order-Managements Route
			Route::GET('/buy-order-managements/index','BuyOrderManagementController@index')->name('buy-order-managements.index');
			Route::GET('/buy-order-managements/data/{filter_data?}','BuyOrderManagementController@data')->name('buy-order-managements.data');
			Route::GET('/buy-order-managements/view/{id}','BuyOrderManagementController@view')->name('buy-order-managements.view');

			//User-Subscribe-Management Route
			Route::GET('/user-subscribe-management/index','UserSubscribeManagementController@index')->name('user-subscribe-management.index');
			Route::GET('/user-subscribe-management/data/{filter_data?}','UserSubscribeManagementController@data')->name('user-subscribe-management.data');
			Route::GET('/user-subscribe-management/view/{id}','UserSubscribeManagementController@view')->name('user-subscribe-management.view');

			// Gift-Management Route
			Route::GET('/gift-management/index','GiftManagementController@index')->name('gift-management.index');
			Route::GET('/gift-management/data/{filter_data?}','GiftManagementController@data')->name('gift-management.data');
			Route::GET('/gift-management/view/{id}','GiftManagementController@view')->name('gift-management.view');

			// User Route
			Route::GET('/user/index','UserController@index')->name('user.index');
			Route::GET('/user/data/{filter_data?}','UserController@data')->name('user.data');
			Route::GET('/user/create','UserController@create')->name('user.create');
			Route::POST('/user/store','UserController@store')->name('user.store');
			Route::GET('/user/edit/{id}','UserController@edit')->name('user.edit');
			Route::GET('/user/show/{id}','UserController@show')->name('user.show');
			Route::GET('/user/status/{id}','UserController@status')->name('user.status');
			Route::PATCH('/user/update/{id}','UserController@update')->name('user.update');
			Route::GET('/user/view/{id}','UserController@view')->name('user.view');
			Route::GET('/user/delete/{id}','UserController@destroy')->name('user.delete');
			Route::GET('/user/approved/{id}','UserController@approved')->name('user.approved');
			Route::GET('/user/subuser/{id}','UserController@subuser')->name('user.subuser');

			// Notification-Management Route
			Route::GET('/notification-management/index','NotificationManagementController@index')->name('notification-management.index');
			Route::GET('/notification-management/data/{filter_data?}','NotificationManagementController@data')->name('notification-management.data
				');
			Route::GET('/notification-management/create','NotificationManagementController@create')->name('notification-management.create');
			Route::POST('/notification-management/store','NotificationManagementController@store')->name('notification-management.store');
			Route::GET('/notification-management/edit/{id}','NotificationManagementController@edit')->name('notification-management.edit');
			Route::PATCH('/notification-management/update/{id}','NotificationManagementController@update')->name('notification-management.update');
			Route::GET('/notification-management/delete/{id}','NotificationManagementController@destroy')->name('notification-management.delete');
			Route::GET('/notification-management/view/{id}','NotificationManagementController@view')->name('notification-management.view');

			//facillities route
			Route::GET('/facilities-master/index','FacilitiesMasterController@index')->name('facilities-master.index');
			Route::GET('/facilities-master/data/{filter_data?}','FacilitiesMasterController@data')->name('facilities-master.data');
			Route::GET('/facilities-master/create','FacilitiesMasterController@create')->name('facilities-master.create');
			Route::POST('/facilities-master/store','FacilitiesMasterController@store')->name('facilities-master.store');
			Route::GET('/facilities-master/edit/{id}','FacilitiesMasterController@edit')->name('facilities-master.edit');
			Route::GET('/facilities-master/show/{id}','FacilitiesMasterController@show')->name('facilities-master.show');
			Route::GET('/facilities-master/status/{id}','FacilitiesMasterController@status')->name('facilities-master.status');
			Route::PATCH('/facilities-master/update/{id}','FacilitiesMasterController@update')->name('facilities-master.update');
			Route::GET('/facilities-master/view/{id}','FacilitiesMasterController@view')->name('facilities-master.view');
			Route::GET('/facilities-master/delete/{id}','FacilitiesMasterController@destroy')->name('facilities-master.delete');

			//Room Type routes
			Route::GET('/room-type/index','RoomTypeController@index')->name('room-type.index');
			Route::GET('/room-type/data/{filter_data?}','RoomTypeController@data')->name('room-type.data');
			Route::GET('/room-type/create','RoomTypeController@create')->name('room-type.create');
			Route::POST('/room-type/store','RoomTypeController@store')->name('room-type.store');
			Route::GET('/room-type/edit/{id}','RoomTypeController@edit')->name('room-type.edit');
			Route::PATCH('/room-type/update/{id}','RoomTypeController@update')->name('room-type.update');
			Route::GET('/room-type/status/{id}','RoomTypeController@status')->name('room-type.status');
			Route::GET('/room-type/delete/{id}','RoomTypeController@destroy')->name('room-type.delete');

			//Hotel other details routes
			Route::GET('/hotel-other-details/data/{filter_data?}','HotelOtherDetailsController@data')->name('hotel-other-details.data');
			Route::GET('/hotel-other-details/create','HotelOtherDetailsController@create')->name('hotel-other-details.create');
			Route::POST('/hotel-other-details/store','HotelOtherDetailsController@store')->name('hotel-other-details.store');
			Route::GET('/hotel-other-details/edit/{id}','HotelOtherDetailsController@edit')->name('hotel-other-details.edit');
			Route::PATCH('/hotel-other-details/update/{id}','HotelOtherDetailsController@update')->name('hotel-other-details.update');
			Route::GET('/hotel-other-details/status/{id}','HotelOtherDetailsController@status')->name('hotel-other-details.status');
			Route::GET('/hotel-other-details/delete/{id}','HotelOtherDetailsController@destroy')->name('hotel-other-details.delete');
			Route::GET('/hotel-other-details/view/{id}','HotelOtherDetailsController@view')->name('hotel-other-details.view');


			//Hotel Management routes
			Route::GET('/hotel-management/index','HotelManagementController@index')->name('hotel-management.index');
			Route::GET('/hotel-management/data/{filter_data?}','HotelManagementController@data')->name('hotel-management.data');
			Route::GET('/hotel-management/create','HotelManagementController@create')->name('hotel-management.create');
			Route::POST('/hotel-management/store','HotelManagementController@store')->name('hotel-management.store');
			Route::GET('/hotel-management/edit/{id}','HotelManagementController@edit')->name('hotel-management.edit');
			Route::GET('/hotel-management/show/{id}','HotelManagementController@show')->name('hotel-management.show');
			Route::GET('/hotel-management/hotel_blackout_create','HotelManagementController@hotel_blackout_create')->name('hotel-management.hotel_blackout_create');
			Route::GET('/hotel-management/hotel_blackout/{id}','HotelManagementController@hotel_blackout')->name('hotel-management.hotel_blackout');
			Route::GET('/hotel-management/hotel_blackout_data/{filter_data?}','HotelManagementController@hotel_blackout_data')->name('hotel-management.hotel_blackout_data');
			Route::POST('/hotel-management/hotel_blackout_store','HotelManagementController@hotel_blackout_store')->name('hotel-management.hotel_blackout_store');
			Route::GET('/hotel-management/hotel_blackout_delete/{id}','HotelManagementController@hotel_blackout_destroy')->name('hotel-management.hotel_blackout_delete');
			Route::GET('/hotel-management/status/{id}','HotelManagementController@status')->name('hotel-management.status');
			Route::PATCH('/hotel-management/update/{id}','HotelManagementController@update')->name('hotel-management.update');
			Route::GET('/hotel-management/view/{id}','HotelManagementController@view')->name('hotel-management.view');
			Route::GET('/hotel-management/delete/{id}','HotelManagementController@destroy')->name('hotel-management.delete');

			//Deal Management routes
			Route::GET('/deal-management/index','DealManagementController@index')->name('deal-management.index');
			Route::GET('/deal-management/data/{filter_data?}','DealManagementController@data')->name('deal-management.data');
			Route::GET('/deal-management/create','DealManagementController@create')->name('deal-management.create');
			Route::POST('/deal-management/store','DealManagementController@store')->name('deal-management.store');
			Route::GET('/deal-management/edit/{id}','DealManagementController@edit')->name('deal-management.edit');
			Route::GET('/deal-management/show/{id}','DealManagementController@show')->name('deal-management.show');
			Route::GET('/deal-management/status/{id}','DealManagementController@status')->name('deal-management.status');
			Route::PATCH('/deal-management/update/{id}','DealManagementController@update')->name('deal-management.update');
			Route::GET('/deal-management/view/{id}','DealManagementController@view')->name('deal-management.view');
			Route::GET('/deal-management/delete/{id}','DealManagementController@destroy')->name('deal-management.delete');

			Route::get('/deal-management/get-state-list/{id}','DealManagementController@getStateList')->name('deal-management/get-state-list');
			Route::get('/deal-management/get-city-list/{id}','DealManagementController@getCityList')->name('deal-management/get-city-list');

			//Blog Management routes
			Route::GET('/blog-management/index','BlogManagementController@index')->name('blog-management.index');
			Route::GET('/blog-management/data/{filter_data?}','BlogManagementController@data')->name('blog-management.data');
			Route::GET('/blog-management/create','BlogManagementController@create')->name('blog-management.create');
			Route::POST('/blog-management/store','BlogManagementController@store')->name('blog-management.store');
			Route::GET('/blog-management/edit/{id}','BlogManagementController@edit')->name('blog-management.edit');
			Route::GET('/blog-management/show/{id}','BlogManagementController@show')->name('blog-management.show');
			Route::GET('/blog-management/status/{id}','BlogManagementController@status')->name('blog-management.status');
			Route::PATCH('/blog-management/update/{id}','BlogManagementController@update')->name('blog-management.update');
			Route::GET('/blog-management/view/{id}','BlogManagementController@view')->name('blog-management.view');
			Route::GET('/blog-management/delete/{id}','BlogManagementController@destroy')->name('blog-management.delete');

			//Blog Category Master routes
			Route::GET('/blog-category-master/index','BlogCategoryMasterController@index')->name('blog-category-master.index');
			Route::GET('/blog-category-master/data/{filter_data?}','BlogCategoryMasterController@data')->name('blog-category-master.data');
			Route::GET('/blog-category-master/create','BlogCategoryMasterController@create')->name('blog-category-master.create');
			Route::POST('/blog-category-master/store','BlogCategoryMasterController@store')->name('blog-category-master.store');
			Route::GET('/blog-category-master/edit/{id}','BlogCategoryMasterController@edit')->name('blog-category-master.edit');
			Route::GET('/blog-category-master/show/{id}','BlogCategoryMasterController@show')->name('blog-category-master.show');
			Route::GET('/blog-category-master/status/{id}','BlogCategoryMasterController@status')->name('blog-category-master.status');
			Route::PATCH('/blog-category-master/update/{id}','BlogCategoryMasterController@update')->name('blog-category-master.update');
			Route::GET('/blog-category-master/view/{id}','BlogCategoryMasterController@view')->name('blog-category-master.view');
			Route::GET('/blog-category-master/delete/{id}','BlogCategoryMasterController@destroy')->name('blog-category-master.delete');

			//Faq Master Route
			Route::GET('/faq-master/index','FaqMasterController@index')->name('faq-master.index');
			Route::GET('/faq-master/data/{filter_data?}','FaqMasterController@data')->name('faq-master.data');
			Route::GET('/faq-master/create','FaqMasterController@create')->name('faq-master.create');
			Route::POST('/faq-master/store','FaqMasterController@store')->name('faq-master.store');
			Route::GET('/faq-master/edit/{id}','FaqMasterController@edit')->name('faq-master.edit');
			Route::GET('/faq-master/show/{id}','FaqMasterController@show')->name('faq-master.show');
			Route::GET('/faq-master/status/{id}','FaqMasterController@status')->name('faq-master.status');
			Route::PATCH('/faq-master/update/{id}','FaqMasterController@update')->name('faq-master.update');
			Route::GET('/faq-master/view/{id}','FaqMasterController@view')->name('faq-master.view');
			Route::GET('/faq-master/delete/{id}','FaqMasterController@destroy')->name('faq-master.delete');

			//Promo Code Master Route
			Route::GET('/promo-code-master/index','PromoCodeMasterController@index')->name('promo-code-master.index');
			Route::GET('/promo-code-master/data/{filter_data?}','PromoCodeMasterController@data')->name('promo-code-master.data');
			Route::GET('/promo-code-master/create','PromoCodeMasterController@create')->name('promo-code-master.create');
			Route::POST('/promo-code-master/store','PromoCodeMasterController@store')->name('promo-code-master.store');
			Route::GET('/promo-code-master/edit/{id}','PromoCodeMasterController@edit')->name('promo-code-master.edit');
			Route::GET('/promo-code-master/show/{id}','PromoCodeMasterController@show')->name('promo-code-master.show');
			Route::GET('/promo-code-master/status/{id}','PromoCodeMasterController@status')->name('promo-code-master.status');
			Route::PATCH('/promo-code-master/update/{id}','PromoCodeMasterController@update')->name('promo-code-master.update');
			Route::GET('/promo-code-master/view/{id}','PromoCodeMasterController@view')->name('promo-code-master.view');
			Route::GET('/promo-code-master/delete/{id}','PromoCodeMasterController@destroy')->name('promo-code-master.delete');
			Route::get('export', 'PromoCodeMasterController@export')->name('export');
			Route::get('importExportView', 'PromoCodeMasterController@importExportView');
			Route::post('import', 'PromoCodeMasterController@import')->name('import');

			//Surcharge Master Route
			Route::GET('/surcharge-master/index','SurchargeMasterController@index')->name('surcharge-master.index');
			Route::GET('/surcharge-master/data/{filter_data?}','SurchargeMasterController@data')->name('surcharge-master.data');
			Route::GET('/surcharge-master/create','SurchargeMasterController@create')->name('surcharge-master.create');
			Route::POST('/surcharge-master/store','SurchargeMasterController@store')->name('surcharge-master.store');
			Route::GET('/surcharge-master/edit/{id}','SurchargeMasterController@edit')->name('surcharge-master.edit');
			Route::GET('/surcharge-master/show/{id}','SurchargeMasterController@show')->name('surcharge-master.show');
			Route::GET('/surcharge-master/status/{id}','SurchargeMasterController@status')->name('surcharge-master.status');
			Route::PATCH('/surcharge-master/update/{id}','SurchargeMasterController@update')->name('surcharge-master.update');
			Route::GET('/surcharge-master/view/{id}','SurchargeMasterController@view')->name('surcharge-master.view');
			Route::GET('/surcharge-master/delete/{id}','SurchargeMasterController@destroy')->name('surcharge-master.delete');

			//Faq Master Route
			Route::GET('/wishlist-master/index','WishlistMasterController@index')->name('wishlist-master.index');
			Route::GET('/wishlist-master/data/{filter_data?}','WishlistMasterController@data')->name('wishlist-master.data');
			Route::GET('/wishlist-master/create','WishlistMasterController@create')->name('wishlist-master.create');
			Route::POST('/wishlist-master/store','WishlistMasterController@store')->name('wishlist-master.store');
			Route::GET('/wishlist-master/edit/{id}','WishlistMasterController@edit')->name('wishlist-master.edit');
			Route::GET('/wishlist-master/show/{id}','WishlistMasterController@show')->name('wishlist-master.show');
			Route::GET('/wishlist-master/status/{id}','WishlistMasterController@status')->name('wishlist-master.status');
			Route::PATCH('/wishlist-master/update/{id}','WishlistMasterController@update')->name('wishlist-master.update');
			Route::GET('/wishlist-master/view/{id}','WishlistMasterController@view')->name('wishlist-master.view');
			Route::GET('/wishlist-master/delete/{id}','WishlistMasterController@destroy')->name('wishlist-master.delete');

			//Contact Inquiry Routes
			Route::GET('/contact-inquiry/index','ContactInquiryController@index')->name('contact-inquiry.index');
			Route::GET('/contact-inquiry/data/{filter_data?}','ContactInquiryController@data')->name('contact-inquiry.data');
			Route::GET('/contact-inquiry/show/{id}','ContactInquiryController@show')->name('contact-inquiry.show');
			Route::GET('/contact-inquiry/status/{id}','ContactInquiryController@status')->name('contact-inquiry.status');
			Route::GET('/contact-inquiry/view/{id}','ContactInquiryController@view')->name('contact-inquiry.view');
			Route::GET('/contact-inquiry/delete/{id}','ContactInquiryController@destroy')->name('contact-inquiry.delete');

			//Testimonials Routes
			Route::GET('/testimonials/index','TestimonialsController@index')->name('testimonials.index');
			Route::GET('/testimonials/data/{filter_data?}','TestimonialsController@data')->name('testimonials.data');
			Route::GET('/testimonials/create','TestimonialsController@create')->name('testimonials.create');
			Route::POST('/testimonials/store','TestimonialsController@store')->name('testimonials.store');
			Route::GET('/testimonials/edit/{id}','TestimonialsController@edit')->name('testimonials.edit');
			Route::GET('/testimonials/show/{id}','TestimonialsController@view')->name('testimonials.show');
			Route::GET('/testimonials/status/{id}','TestimonialsController@status')->name('testimonials.status');
			Route::PATCH('/testominials/update/{id}','TestimonialsController@update')->name('testimonials.update');
			Route::GET('/testimonials/view/{id}','TestimonialsController@view')->name('testimonials.view');
			Route::GET('/testimonials/delete/{id}','TestimonialsController@destroy')->name('testimonials.delete');

			//Order Management Routes
			Route::GET('/order-management/index','OrderManagementController@index')->name('order-management.index');
			Route::GET('/order-management/data/{filter_data?}','OrderManagementController@data')->name('order-management.data');
			Route::GET('/order-management/create','OrderManagementController@create')->name('order-management.create');
			Route::POST('/order-management/store','OrderManagementController@store')->name('order-management.store');
			Route::GET('/order-management/edit/{id}','OrderManagementController@edit')->name('order-management.edit');
			Route::GET('/order-management/show/{id}','OrderManagementController@show')->name('order-management.show');
			Route::GET('/order-management/status/{id}','OrderManagementController@status')->name('order-management.status');
			Route::PATCH('/order-management/update/{id}','OrderManagementController@update')->name('order-management.update');
			Route::GET('/order-management/view/{id}','OrderManagementController@view')->name('order-management.view');
			Route::GET('/order-management/delete/{id}','OrderManagementController@destroy')->name('order-management.delete');

			//Comment_Rating Routes
			Route::GET('/comment-rating/index','CommentRatingController@index')->name('comment-rating.index');
			Route::GET('/comment-rating/data/{filter_data?}','CommentRatingController@data')->name('comment-rating.data');
			Route::GET('/comment-rating/show/{id}','CommentRatingController@show')->name('comment-rating.show');
			Route::GET('/comment-rating/status/{id}','CommentRatingController@status')->name('comment-rating.status');
			Route::GET('/comment-rating/view/{id}','CommentRatingController@view')->name('comment-rating.view');
			Route::GET('/comment-rating/delete/{id}','CommentRatingController@destroy')->name('comment-rating.delete');

			//User Group Routes
			Route::GET('/user-group/index','UserGroupController@index')->name('user-group.index');
			Route::GET('/user-group/data/{filter_data?}','UserGroupController@data')->name('user-group.data');
			Route::GET('/user-group/create','UserGroupController@create')->name('user-group.create');
			Route::POST('/user-group/store','UserGroupController@store')->name('user-group.store');
			Route::GET('/user-group/edit/{id}','UserGroupController@edit')->name('user-group.edit');
			Route::GET('/user-group/show/{id}','UserGroupController@show')->name('user-group.show');
			Route::GET('/user-group/status/{id}','UserGroupController@status')->name('user-group.status');
			Route::PATCH('/user-group/update/{id}','UserGroupController@update')->name('user-group.update');
			Route::GET('/user-group/view/{id}','UserGroupController@view')->name('user-group.view');
			Route::GET('/user-group/delete/{id}','UserGroupController@destroy')->name('user-group.delete');

			//Package Managment route
			Route::GET('/package-management/index','PackageManagementController@index')->name('package-management.index');
			Route::GET('/package-management/data/{filter_data?}','PackageManagementController@data')->name('package-management.data');
			Route::GET('/package-management/create','PackageManagementController@create')->name('package-management.create');
			Route::POST('/package-management/store','PackageManagementController@store')->name('package-management.store');
			Route::GET('/package-management/edit/{id}','PackageManagementController@edit')->name('package-management.edit');
			Route::PATCH('/package-management/update/{id}','PackageManagementController@update')->name('package-management.update');
			Route::GET('/package-management/show/{id}','PackageManagementController@show')->name('package-management.show');
			Route::GET('/package-management/view/{id}','PackageManagementController@view')->name('package-management.view');
			Route::GET('/package-management/delete/{id}','PackageManagementController@destroy')->name('package-management.delete');
			Route::GET('/package-management/status/{id}','PackageManagementController@status')->name('package-management.status');

			Route::GET('/package-management/package_blackout_create','PackageManagementController@package_blackout_create')->name('package-management.package_blackout_create');
			Route::GET('/package-management/package_blackout/{id}','PackageManagementController@package_blackout')->name('package-management.package_blackout');
			Route::GET('/package-management/package_blackout_data/{filter_data?}','PackageManagementController@package_blackout_data')->name('package-management.package_blackout_data');
			Route::POST('/package-management/package_blackout_store','PackageManagementController@package_blackout_store')->name('package-management.package_blackout_store');
			Route::GET('/package-management/package_blackout_delete/{id}','PackageManagementController@package_blackout_destroy')->name('package-management.package_blackout_delete');

			//Group Management Routes
			Route::GET('/group-management/index','GroupManagementController@index')->name('group-management.index');
			Route::GET('/group-management/create','GroupManagementController@create')->name('group-management.create');
			Route::POST('/group-management/store','GroupManagementController@store')->name('group-management.store');
			Route::GET('/group-management/data/{filter_data?}','GroupManagementController@data')->name('group-management.data');
			Route::GET('/group-management/edit/{id}','GroupManagementController@edit')->name('group-management.edit');
			Route::PATCH('/group-management/update/{id}','GroupManagementController@update')->name('group-management.update');
			Route::GET('/group-management/show/{id}','GroupManagementontrColler@show')->name('group-management.show');
			Route::GET('/group-management/status/{id}','GroupManagementController@status')->name('group-management.status');
			Route::GET('/group-management/view/{id}','GroupManagementController@view')->name('group-management.view');
			Route::GET('/group-management/delete/{id}','GroupManagementController@destroy')->name('group-management.delete');

			//Customer Management Route
			Route::GET('/customer-management/index','CustomerManagementController@index')->name('customer-management.index');
			Route::GET('/customer-management/data/{filter_data?}','CustomerManagementController@data')->name('customer-management.data');
			Route::GET('/customer-management/create','CustomerManagementController@create')->name('customer-management.create');
			Route::POST('/customer-management/store','CustomerManagementController@store')->name('customer-management.store');
			Route::GET('/customer-management/edit/{id}','CustomerManagementController@edit')->name('customer-management.edit');
			Route::GET('/customer-management/show/{id}','CustomerManagementController@show')->name('customer-management.show');
			Route::GET('/customer-management/status/{id}','CustomerManagementController@status')->name('customer-management.status');
			Route::PATCH('/customer-management/update/{id}','CustomerManagementController@update')->name('customer-management.update');
			Route::GET('/customer-management/view/{id}','CustomerManagementController@view')->name('customer-management.view');
			Route::GET('/customer-management/delete/{id}','CustomerManagementController@destroy')->name('customer-management.delete');

			//newslatter routes
			Route::GET('/newslatter-master/index','NewslatterController@index')->name('newslatter-master.index');
			Route::GET('/newslatter-master/data/{filter_data?}','NewslatterController@data')->name('newslatter-master.data');

			//ServiceManagement route
			Route::GET('/service-management/index','ServiceManagementController@index')->name('service-management.index');
			Route::GET('/service-management/data/{filter_data?}','ServiceManagementController@data')->name('service-management.data');
			Route::GET('/service-management/create','ServiceManagementController@create')->name('service-management.create');
			Route::POST('/service-management/store','ServiceManagementController@store')->name('service-management.store');
			Route::GET('/service-management/edit/{id}','ServiceManagementController@edit')->name('service-management.edit');
			Route::PATCH('/service-management/update/{id}','ServiceManagementController@update')->name('service-management.update');
			Route::GET('/service-management/status/{id}','ServiceManagementController@status')->name('service-management.status');
			Route::GET('/service-management/delete/{id}','ServiceManagementController@destroy')->name('service-management.delete');
			Route::GET('/service-management/view/{id}','ServiceManagementController@show')->name('user.view');


			//Product Management routes
			Route::GET('/product-management/index','ProductManagementController@index')->name('product-management.index');

			Route::GET('/product-management/data/{filter_data?}','ProductManagementController@data')->name('product-management.data');
			Route::GET('/product-management/create','ProductManagementController@create')->name('product-management.create');
			Route::POST('/product-management/store','ProductManagementController@store')->name('product-management.store');
			Route::GET('/product-management/edit/{id}','ProductManagementController@edit')->name('product-management.edit');
			Route::PATCH('/product-management/update/{id}','ProductManagementController@update')->name('product-management.update');
			Route::GET('/product-management/status/{id}','ProductManagementController@status')->name('product-management.status');
			Route::GET('/product-management/delete/{id}','ProductManagementController@destroy')->name('product-management.delete');
			Route::GET('/product-management/view/{id}','ProductManagementController@show')->name('user.view');


			//Product Category Management routes
			Route::GET('/product-category/index','ProductCategoryController@index')->name('product-category.index');
			Route::GET('/product-category/data/{filter_data?}','ProductCategoryController@data')->name('product-category.data');
			Route::GET('/product-category/create','ProductCategoryController@create')->name('product-category.create');
			Route::POST('/product-category/store','ProductCategoryController@store')->name('product-category.store');
			Route::GET('/product-category/edit/{id}','ProductCategoryController@edit')->name('product-category.edit');
			Route::PATCH('/product-category/update/{id}','ProductCategoryController@update')->name('product-category.update');
			Route::GET('/product-category/status/{id}','ProductCategoryController@status')->name('product-category.status');
			Route::GET('/product-category/delete/{id}','ProductCategoryController@destroy')->name('product-category.delete');
			Route::GET('/product-category/view/{id}','ProductCategoryController@show')->name('user.view');



			Route::GET('/slider-management/index','SliderManagementController@index')->name('slider-management.index');
			Route::GET('/slider-management/data/{filter_data?}','SliderManagementController@data')->name('slider-management.data');
			Route::GET('/slider-management/create','SliderManagementController@create')->name('slider-management.create');
			Route::POST('/slider-management/store','SliderManagementController@store')->name('slider-management.store');
			Route::GET('/slider-management/edit/{id}','SliderManagementController@edit')->name('slider-management.edit');
			Route::PATCH('/slider-management/update/{id}','SliderManagementController@update')->name('slider-management.update');
			Route::GET('/slider-management/status/{id}','SliderManagementController@status')->name('slider-management.status');
			Route::GET('/slider-management/delete/{id}','SliderManagementController@destroy')->name('slider-management.delete');
			Route::GET('/slider-management/view/{id}','SliderManagementController@show')->name('user.view');

			//Menu Management routes
			Route::GET('/menu-management/index','MenuManagementController@index')->name('menu-management.index');
			Route::GET('/menu-management/data/{filter_data?}','MenuManagementController@data')->name('menu-management.data');
			Route::GET('/menu-management/create','MenuManagementController@create')->name('menu-management.create');
			Route::POST('/menu-management/store','MenuManagementController@store')->name('menu-management.store');
			Route::GET('/menu-management/edit/{id}','MenuManagementController@edit')->name('menu-management.edit');
			Route::GET('/menu-management/show/{id}','MenuManagementController@show')->name('menu-management.show');
			Route::GET('/menu-management/status/{id}','MenuManagementController@status')->name('menu-management.status');
			Route::PATCH('/menu-management/update/{id}','MenuManagementController@update')->name('menu-management.update');
			Route::GET('/menu-management/view/{id}','MenuManagementController@view')->name('menu-management.view');
			Route::GET('/menu-management/delete/{id}','MenuManagementController@destroy')->name('menu-management.delete');

		Route::fallback(function(){
			// Route::GET('/dashboard', 'DashboardController@index')->name('dashboard');
		});

	});






});

/*============================== frontend Route ==============================*/
Route::group(['namespace' => '\App\Http\Controllers\frontend'], function(){

	Route::GET('/index','DashboardController@index')->name('index');

	Route::GET('/contact','ContactusController@index')->name('contact');
	Route::POST('/contact/store','ContactusController@store')->name('contact.store');


	Route::POST('/newsletter/store','ContactusController@subscribe')->name('newsletter.store');


	Route::GET('/blog','BlogController@index')->name('blog');

    Route::GET('/privacy-policy','PrivacypolicyController@index')->name('privacy-policy');
    Route::GET('/career','PrivacypolicyController@careerdata')->name('career');
    Route::GET('/affliate','PrivacypolicyController@affliatedata')->name('affliate');
     Route::GET('/disclaimer','PrivacypolicyController@disclaimerdata')->name('disclaimer');




	//about-us
	Route::GET('/about-us','BlogController@aboutus')->name('about-us');
	Route::GET('/blogdetails/{id}','BlogController@details');

	Route::GET('/service','ServiceController@index')->name('service');

	Route::GET('/servicedetail/{id}', 'ServiceController@detail')->name('servicedetail');

	Route::GET('/product/','ProductController@index')->name('product');
	Route::GET('/productdetails/','ProductController@details')->name('productdetails');

	Route::GET('/testimonial/','TestimonialController@index')->name('testimonial');
	Route::GET('/export/','DashboardController@export')->name('export');
	Route::GET('/whoweare/','DashboardController@whoweare')->name('whoweare');
	Route::POST('/subscribe','DashboardController@subscribe')->name('subscribe');
	Route::GET('/co-founder/','DashboardController@cofounder')->name('co-founder');

});
Route::fallback(function(){
	return redirect('index');
});

