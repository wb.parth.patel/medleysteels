@extends('layouts_frontend.masters',['title'=>'Everglades'])
@section('content')
        <!-- Page Title -->
        <div class="page-title-area">
            <div class="d-table">
                <div class="d-table-cell">
                    <div class="container">
                        <div class="title-item">
                            <h2>{{$blog->blog_title}}</h2>
                            <ul>
                                <li>
                                    <a href="index.html">Home</a>
                                </li>
                                <li>
                                    <span>/</span>
                                </li>
                                <li>
                                    Blog Details
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Page Title -->

        <!-- Blog Details -->
        <div class="blog-details-area ptb-100">
            <div class="container">
                <div class="row">
                    <div class="col-lg-9">
                        <div class="blog-details-item">
                            <div class="blog-details-name">
                                <div class="details-user">
                                    <ul>
                                        <!-- <li>
                                            <span>by</span> 
                                            <a href="#">John Doe</a>
                                        </li> -->
                                        <li>
                                            <span>{{date('j F Y', strtotime($blog->created_date))}}</span>
                                        </li>
                                        <li>
                                            <span>by</span> 
                                            <a href="#">{{$blog->category_name}}</a>
                                        </li>
                                    </ul>
                                </div>
                                <h2>{{$blog->blog_title}}</h2>
                                <p>{!!$blog->blog_description!!} </p>
                                <div class="row align-items-center">
                                    <div class="col-sm-6 col-lg-6">
                                        <img src="{{url('/media/blog',$blog['image'])}}" alt="Blog Details">
                                    </div>
                                    
                                </div>
                                
                            </div>
                            
                            <div class="blog-area blog-area-two blog-area-three">
                                <div class="section-title">
                                    <h2>Other Post</h2>
                                </div>
                                <div class="container">
                                    <div class="row">
                                        @foreach($related_blog as $blog)
                                        <div class=" col-lg-6">
                                            <div class="blog-item">
                                                <a href="{{url('/blogdetails',$blog['id'])}}">
                                                    <img src="{{url('/media/blog',$blog['image'])}}" alt="Blog">
                                                </a>
                                                <span>{{date('j F Y', strtotime($blog->created_date))}}</span>
                                                <div class="blog-inner">
                                                    <h3>
                                                        <a href="{{url('/blogdetails',$blog['id'])}}">{{$blog->blog_title}} </a>
                                                    </h3>
                                                    <p>{!! substr($blog->blog_description, 0, "150").'...'!!}</p>
                                                    <a class="blog-link" href="{{url('/blogdetails',$blog['id'])}}">
                                                        Read More
                                                        <i class='bx bx-plus'></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            
            </div>
        </div>
        <!-- End Blog Details -->


@endsection