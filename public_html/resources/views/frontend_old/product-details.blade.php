@extends('layouts_frontend.masters',['title'=>'Everglades'])
@section('content')

    <!-- Page Title -->
    <div class="page-title-area">
        <div class="d-table">
            <div class="d-table-cell">
                <div class="container">
                    <div class="title-item">
                        <h2>Product Details</h2>
                        <ul>
                            <li>
                                <a href="{{route('index')}}">Home</a>
                            </li>
                            <li>
                                <span>/</span>
                            </li>
                            <li>
                                Product Details
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Page Title -->

    <!-- Project Details -->
    <div class="project-details-area pt-100 pb-70">
        <div class="container">
            <div class="row">
                <div class="col-lg-9">
                    <div class="project-details-item">
                        <div class="project-details-img">
                            <img src="{{asset('assets/img/project-details1.jpg')}}" alt="Project">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic.</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic.</p>
                            <div class="row">
                                <div class="col-sm-6 col-lg-6">
                                    <ul>
                                        <li>
                                            <i class='bx bx-check-square'></i>
                                            Industrial Technology
                                        </li>
                                        <li>
                                            <i class='bx bx-check-square'></i>
                                            Unique Technology
                                        </li>
                                        <li>
                                            <i class='bx bx-check-square'></i>
                                            Modern Technology
                                        </li>
                                        <li>
                                            <i class='bx bx-check-square'></i>
                                            Automation
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-sm-6 col-lg-6">
                                    <ul>
                                        <li>
                                            <i class='bx bx-check-square'></i>
                                            Construction & Demolition
                                        </li>
                                        <li>
                                            <i class='bx bx-check-square'></i>
                                            Manufacture
                                        </li>
                                        <li>
                                            <i class='bx bx-check-square'></i>
                                            OIl & Gas
                                        </li>
                                        <li>
                                            <i class='bx bx-check-square'></i>
                                            Improved Monitoring
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="project-details-content">
                            <div class="row">
                                <div class="col-sm-6 col-lg-6">
                                    <img src="{{asset('assets/img/project-details2.jpg')}}" alt="Project Details">
                                </div>
                                <div class="col-sm-6 col-lg-6">
                                    <img src="{{asset('assets/img/project-details3.jpg')}}" alt="Project Details">
                                </div>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown specimen book. It has survived not only centuries, but printer took a galley of type and scrambled it to make a type specimen book. It has survived not only centuries, but also the leap into electronic Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic.</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic.</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic.</p>
                            <div class="video-wrap">
                                <img src="{{asset('assets/img/project-details4.jpg')}}" alt="Video">
                                <a href="https://www.youtube.com/watch?v=QcBeDbs8n9k" class="popup-youtube">
                                    <i class='bx bx-play'></i>
                                </a>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic.</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="project-details-item ">
                        <div class="project-details-mec ">
                            <h3>Product</h3>
                           <ul>
                                <li>
                                    <a href="#">
                                        Merchant Bars
                                        <i class='bx bx-chevron-right'></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                       Beams /Structural
                                        <i class='bx bx-chevron-right'></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Pipes & Tubes
                                        <i class='bx bx-chevron-right'></i>
                                    </a>
                                </li>

                                <li>
                                    <a href="#">
                                        Sheet & Plate
                                        <i class='bx bx-chevron-right'></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                       Industrial
                                        <i class='bx bx-chevron-right'></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="project-details-form">
                            <h3>Ask Here</h3>
                            <form>
                                <div class="form-group">
                                    <label>
                                        <i class='bx bx-user'></i>
                                    </label>
                                    <input type="text" class="form-control" placeholder="Ente your name">
                                </div>
                                <div class="form-group">
                                    <label>
                                        <i class='bx bx-mail-send'></i>
                                    </label>
                                    <input type="text" class="form-control" placeholder="Ente your name">
                                </div>
                                <div class="form-group">
                                    <label>
                                        <i class='bx bx-notepad'></i>
                                    </label>
                                    <textarea id="your_message" rows="8" class="form-control" placeholder="Enter your message"></textarea>
                                </div>
                                <button type="submit" class="btn project-form-btn">Submit Message</button>
                            </form>
                        </div>
                        <!-- <div class="project-details-click">
                            <img src="{{asset('assets/img/project-details5.jpg')}}" alt="Project Details">
                            <a href="#">Click Here</a>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Project Details -->

@endsection