@extends('layouts_frontend.masters',['title'=>'Everglades'])
@section('content')
<!-- Page Title -->
<div class="page-title-area">
    <div class="d-table">
        <div class="d-table-cell">
            <div class="container">
                <div class="title-item">
                    <h2>Founder</h2>
                    <ul>
                        <li>
                            <a href="{{url('index')}}">Home</a>
                        </li>
                        <li>
                            <span>/</span>
                        </li>
                        <li>
                            Founder
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Page Title -->
        
 <!-- Founder -->
        <section class="about-area ptb-100">
            <div class="container">
                <div class="row align-iems-center">
                    <div class="col-lg-6">
                        <div class="section-title">
                            <span class="sub-title">Founder</span>
                            <h2>OUR FOUNDER</h2>
                        </div>
                        <div class="about-content">
                            {!!$ourfounder->content!!}
                            <!-- <img src="assets/img/home-one/about-man.png" alt="About">
                            <img src="assets/img/home-one/about-signature.png" alt="About"> -->
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="about-img-wrap">
                            <div class="about-img-slider owl-theme owl-carousel">
                                <div class="about-img-item">
                                    <img src="assets/img/orlando.png" height="500px" alt="About">
                                </div>
                            </div>
                            <div class="about-shape">
                                <img src="assets/img/home-one/about2.png" alt="About">   
                                <img src="assets/img/home-one/about4.png" alt="About">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
 <!-- End Founder -->

 @endsection