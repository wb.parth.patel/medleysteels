    @extends('layouts_frontend.masters',['title'=>'Everglades'])
    @section('content')
    <!-- Page Title -->
    <!-- Page Title -->
    
        <div class="page-title-area">
            <div class="d-table">
                <div class="d-table-cell">
                    <div class="container">
                        <div class="title-item">
                            <h2>Who we are?</h2>
                            <ul>
                                <li>
                                    <a href="index.html">Home</a>
                                </li>
                                <li>
                                    <span>/</span>
                                </li>
                                <li>
                                   Who we are?
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Page Title -->

      <!-- Privacy -->
        <section class="privacy-area pt-100">
            <div class="container">
                <div class="privacy-item">
                     {!!$aboutus->content!!}
                </div>
                
                <div class="privacy-item">
                    <h3>Our Located Place</h3>
                    <div class="col-lg-6">
                    <p>{{strip_tags($contact_us->content)}}</p>
                    <br>
                    <div class="contact-item contact-left">
                        
                        
                        <ul>
                            <li>
                                <i class='bx bx-location-plus'></i>
                                {{App\Http\Controllers\Controller::getSiteconfigValueByKey('Address')['value']}}
                            </li>
                            <li>
                                <i class='bx bx-mail-send'></i>
                                <a href="mailto:info@robtic.com">
                                    {{App\Http\Controllers\Controller::getSiteconfigValueByKey('Email')['value']}}
                                </a>
                            </li>
                            <li>
                                <i class='bx bx-phone-call'></i>
                                <a href="tel:+8562-65516-595">
                                    {{App\Http\Controllers\Controller::getSiteconfigValueByKey('CONTACT_NUMBER')['value']}}
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                </div>
            </div>
        </section>
        <!-- End Privacy -->


 @endsection