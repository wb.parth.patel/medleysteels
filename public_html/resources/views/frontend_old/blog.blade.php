    @extends('layouts_frontend.masters',['title'=>'Everglades'])
    @section('content')
    <!-- Page Title -->
        <div class="page-title-area">
            <div class="d-table">
                <div class="d-table-cell">
                    <div class="container">
                        <div class="title-item">
                            <h2>Blog</h2>
                            <ul>
                                <li>
                                    <a href="{{url('index')}}">Home</a>
                                </li>
                                <li>
                                    <span>/</span>
                                </li>
                                <li>
                                    Blog
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Page Title -->
        <!-- Blog -->
        
        <section class="blog-area blog-area-two pt-100 pb-70">
            
            <div class="container">
                <div class="row">
                    @foreach($blog as $blog)
                    <div class="col-sm-6 col-lg-4">
                        <div class="blog-item">
                            <a href="{{url('/blogdetails',$blog['id'])}}">
                                <img src="{{url('/media/blog',$blog['image'])}}" alt="Blog">
                            </a>
                            <span>{{date('j F Y', strtotime($blog->created_date))}}</span>
                            <div class="blog-inner">

                                <h3>

                            <div class="share-btn dropdown">
                <div class="share-icon dropdown-toggle" data-toggle="dropdown">
                    Share
                </div>
                <div class="dropdown-menu">
                    <a class="dropdown-item facebook" onClick="window.open('http://www.facebook.com/sharer.php?u=http://everglades.xceltec.in:8080/blogdetails/{{$blog->id}}', 'sharer', 'toolbar=0,status=0,width=548,height=325');" target="_parent" href="javascript: void(0)"><i class="bx bxl-facebook" aria-hidden="true"></i>&nbsp Facebook</a>

                    <a class="dropdown-item twitter" onClick="window.open('https://twitter.com/intent/tweet?text=my share text&amp;url=http://everglades.xceltec.in:8080/blogdetails/{{$blog->id}}', 'sharer', 'toolbar=0,status=0,width=548,height=325');" target="_parent" href="javascript: void(0)"><i class="bx bxl-twitter" aria-hidden="true"></i>&nbspTwitter</a>

                    <a class="dropdown-item whatsapp" onClick="window.open('https://api.whatsapp.com/send?text=http://everglades.xceltec.in:8080/blogdetails/{{$blog->id}}', 'sharer', 'toolbar=0,status=0,width=548,height=325');" target="_parent" href="javascript: void(0)"><i class="bx bxl-whatsapp" aria-hidden="true"></i>&nbspWhatsapp</a>

                    <a class="dropdown-item envelope" onClick="window.open('mailto:?subject=&amp;body=http://everglades.xceltec.in:8080/blogdetails/{{$blog->id}}', 'sharer', 'toolbar=0,status=0,width=548,height=325');" target="_parent" href="javascript: void(0)"><i class="bx bx-mail-send" aria-hidden="true"></i>&nbspMail</a>
                </div>
            </div>
                                    <a href="{{url('/blogdetails',$blog['id'])}}">{{$blog->blog_title}} </a>
                                </h3>
                                <a class="blog-link" href="{{url('/blogdetails',$blog['id'])}}">
                                    Read More
                                    <i class='bx bx-plus'></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </section>
        <!-- End Blog -->


 @endsection