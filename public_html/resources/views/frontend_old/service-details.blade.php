@extends('layouts_frontend.masters',['title'=>'Everglades'])
@section('content')
        <!-- Page Title -->
        <div class="page-title-area">
            <div class="d-table">
                <div class="d-table-cell">
                    <div class="container">
                        <div class="title-item">
                            <h2>Service Details</h2>
                            <ul>
                                <li>
                                    <a href="index.html">Home</a>
                                </li>
                                <li>
                                    <span>/</span>
                                </li>
                                <li>
                                    Service Details
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Page Title -->

           <!-- Service Details -->
        <div class="service-details-area pt-100 pb-70">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8">
                        <div class="service-details-item">
                            <div class="service-details-img">
                                <h2>{{$id_service->service_name}}</h2>
                                <p>{!!$id_service->service_desctiprion!!}</p>
                                <p>{{$id_service->service_detail}}</p>
                                @if ($id_service['service_image'] != null) 
                                <img src="{{url('/media/service',$id_service['service_image'])}}" alt="Service Details">
                                @else
                                <img src="{{url('/media/no-image.png')}}" alt="Service Details">
                                @endif
                            </div>
                           
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="service-details-item">
                            <div class="service-details-cat">
                                <h3>Services</h3>
                                <!-- <ul> -->
                                    @foreach($services as $service)
                                    <!-- <li> -->
                                        <div class="offer-bottom">                 
                                            <a class="cmn-btn" href="{{url('/servicedetails',$service['id'])}}">{{$service->service_name}}
                                                <i class='bx bx-plus'></i>
                                            </a>
                                        </div>
                                        <!-- <a href="{{url('/servicedetails',$service['id'])}}">{{$service->service_name}}</a> -->
                                    <!-- </li> -->
                                    
                                    @endforeach
                                <!-- </ul> -->
                            </div>
                          
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Service Details -->
       


@endsection