@extends('layouts_frontend.masters',['title'=>'Everglades'])
@section('content')
<!-- Page Title -->
<!-- Page Title -->
<div class="page-title-area">
    <div class="d-table">
        <div class="d-table-cell">
            <div class="container">
                <div class="title-item">
                    <h2>Service</h2>
                    <ul>
                        <li>
                            <a href="index.html">Home</a>
                        </li>
                        <li>
                            <span>/</span>
                        </li>
                        <li>
                            Service
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Page Title -->

<!-- Blog -->
<section class="service-area-two service-area-three pt-100 pb-70">
    <div class="container">
        <div class="row">
            @foreach ($services as $service)
            <div class="col-sm-6 col-lg-4">
                <!-- <div class="service-item"> -->
                @if($service->service_image != null)
                <div class="service-item" 
                style="background-image: url({{ url('/media/service/'. $service->service_image) }})">
                @else
                <div class="service-item" style="background-image: url('../img/home-three/service-bg.jpg');">
                @endif
                    <!-- <i class="{{$service['service_icon']}}"></i> -->
                    {!!$service->service_icon!!}
                    <h3>
                        <a href="{{url('/servicedetails',$service['id'])}}">{{$service->service_name}}</a>
                    </h3>
                    <div class="servie-text"> 
                        <!-- <p>{!!$service->service_desctiprion!!}</p>  -->
                        <p>{!!$service->service_detail!!}</p> 
                    </div>
                    <a class="service-link" href="{{url('/servicedetails',$service['id'])}}">Read More</a>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>
<!-- End Blog -->

@endsection
