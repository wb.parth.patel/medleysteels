    @extends('layouts_frontend.masters',['title'=>'Everglades'])
    @section('content')
       <!-- Page Title -->
    <div class="page-title-area">
        <div class="d-table">
            <div class="d-table-cell">
                <div class="container">
                    <div class="title-item">
                        <h2>Contact</h2>
                        <ul>
                            <li>
                                <a href="{{ url('/index')}}">Home</a>
                            </li>
                            <li>
                                <span>/</span>
                            </li>
                            <li>
                                Contact
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Page Title -->

    <!-- Contact -->
    <section class="contact-area pt-100 pb-70">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="contact-item contact-left">
                        <h3>Our Located Place</h3>
                        <p>{{strip_tags($contact_us->content)}}</p>
                        <ul>
                            <li>
                                <i class='bx bx-location-plus'></i>
                                {{App\Http\Controllers\Controller::getSiteconfigValueByKey('Address')['value']}}
                            </li>
                            <li>
                                <i class='bx bx-mail-send'></i>
                                <a href="mailto:{{App\Http\Controllers\Controller::getSiteconfigValueByKey('Email')['value']}}">
                                    {{App\Http\Controllers\Controller::getSiteconfigValueByKey('Email')['value']}}
                                </a>
                            </li>
                            <li>
                                <i class='bx bx-phone-call'></i>
                                <a href="tel:{{App\Http\Controllers\Controller::getSiteconfigValueByKey('CONTACT_NUMBER')['value']}}">
                                    {{App\Http\Controllers\Controller::getSiteconfigValueByKey('CONTACT_NUMBER')['value']}}
                                </a>
                            </li>
                            <li>
                                <i class='bx bx-alarm'></i>
                                <a href="tel:{{App\Http\Controllers\Controller::getSiteconfigValueByKey('CONTACT_NUMBER')['value']}}">
                                    {{App\Http\Controllers\Controller::getSiteconfigValueByKey('MONDAY_TO_FRIDAY')['value']}}
                                </a>
                            </li>
                            <li>
                                <i class='bx bx-alarm'></i>
                                <a href="tel:{{App\Http\Controllers\Controller::getSiteconfigValueByKey('CONTACT_NUMBER')['value']}}">
                                    {{ App\Http\Controllers\Controller::getSiteconfigValueByKey('SATURDAY')['value'] }}
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="contact-item contact-right">
                        <h3>Get In Touch</h3>
                        <form  action="{{ url('/contact/store')}}" id="changepassword_form" method="post">
                            @csrf
                            @method('POST')
                            <div class="row">
                                <div class="col-sm-6 col-lg-6">
                                    <div class="form-group">
                                        <input type="text" name="name" id="name" class="form-control txtOnly"  data-error="Please enter your name" placeholder="Name *">
                                       <span style="color:red" class="kt-form__help error name"></span>
                                       <span style="color:red;">{{ $errors->first('name') }}</span>
                                    </div>
                                </div>
    
                                <div class="col-sm-6 col-lg-6">
                                    <div class="form-group">
                                        <input type="email" name="email" id="email" class="form-control"  data-error="Please enter your email" placeholder="Email *">
                                        <span style="color:red" class="kt-form__help error email"></span>
                                       <span style="color:red;">{{ $errors->first('email') }}</span>

                                    </div>
                                </div>

                                <div class="col-sm-6 col-lg-6">
                                    <div class="form-group">
                                        <input type="text" name="phone_number" id="phone_number"  data-error="Please enter your number" class="form-control" placeholder="Phone *">
                                        <span style="color:red" class="kt-form__help error phone_number"></span>
                                       <span style="color:red;">{{ $errors->first('phone_number') }}</span>
                                    </div>
                                </div>
        
                                <div class="col-sm-6 col-lg-6">
                                    <div class="form-group">
                                        <input type="text" name="msg_subject" id="msg_subject" class="form-control" max="130"  data-error="Please enter your subject" placeholder="Subject">
                                        <span style="color:red" class="kt-form__help error msg_subject"></span>
                                       <span style="color:red;">{{ $errors->first('msg_subject') }}</span>
                                       
                                    </div>
                                </div>
    
                                <div class="col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <textarea name="message" class="form-control" id="message" cols="30" rows="8"  data-error="Write your message" placeholder="Message"></textarea>
                                       <span style="color:red;">{{ $errors->first('message') }}</span>
                                       
                                    </div>
                                </div>
    
                                <div class="col-md-12 col-lg-12">
                                    <button type="submit" class="contact-btn btn">
                                        Send Message
                                    </button>
                                    <div id="msgSubmit" class="h3 text-center hidden"></div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Contact -->

    <!-- Map -->
    <div class="map-area">
        <!-- <iframe id="map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d59791272.948469065!2d62.57506029141487!3d23.84100284788009!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3663f18a24cbe857%3A0xa9416bfcd3a0f459!2sAsia!5e0!3m2!1sen!2sbd!4v1594975629033!5m2!1sen!2sbd" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe> -->
        <iframe  id="map" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3591.253224759006!2d-80.317685924!3d25.82794!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xff65a0ed75cce4fd!2sEverglades+Steel+Corporation!5e0!3m2!1ses-419!2sve!4v1481684508051"  allowfullscreen  tabindex="0"></iframe>
      
    </div>
     <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="{{asset('js/ChangePasswordFormValidation.js')}}"></script>
    <script type="text/javascript">
        
        $(document).ready(function () {

            $('.txtOnly').bind('keyup blur',function(){ 
                    var node = $(this);
                    node.val(node.val().replace(/[^A-Za-z_\s]/,'') ); }
                );
        });
    </script>
 @endsection