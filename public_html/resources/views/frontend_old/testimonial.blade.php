    @extends('layouts_frontend.masters',['title'=>'Everglades'])
    @section('content')

        <!-- Page Title -->
        <div class="page-title-area">
            <div class="d-table">
                <div class="d-table-cell">
                    <div class="container">
                        <div class="title-item">
                            <h2>Testimonial</h2>
                            <ul>
                                <li>
                                    <a href="index.html">Home</a>
                                </li>
                                <li>
                                    <span>/</span>
                                </li>
                                <li>
                                    Testimonial
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Page Title -->

        <!-- Testimonial -->
        <section class="testimonial-area ptb-100">
            <div class="testimonial-shape">
                <img src="assets/img/home-two/testimonial-bg.png" alt="Testimonial">
            </div>
            <div class="container">
                <div class="section-title">
                    <h2>What Our Clients Say</h2>
                </div>
                <div class="testimonial-slider owl-theme owl-carousel">
                    @foreach($Testimonial as $Testimonial)
                    <div class="testimonial-item">
                        <div class="testimonial-top">
                            <i class="flaticon-right-quotation-sign"></i>
                            <p>{!!$Testimonial->description!!}</p>
                        </div>
                        <ul class="align-items-center">
                            <li>
                                @if($Testimonial->image)
                                <img src="{{url('media/testimonials',$Testimonial['image'])}}" >
                                 @else
                                <img src="{{ url('media/default.jpg
                                  ')}}" alt="default" />
                                @endif
                            </li>
                            <li>
                                <h4>{!!$Testimonial->author_name!!}</h4>
                            <!--     <span>Developer</span> -->
                            </li>
                        </ul>
                    </div>
                    @endforeach
                </div>
            </div>
        </section>
        <!-- End Testimonial -->


 @endsection