    @extends('layouts_frontend.masters',['title'=>'Everglades'])
    @section('content')
       <!-- Page Title -->
     <!-- Page Title -->
        <div class="page-title-area">
            <div class="d-table">
                <div class="d-table-cell">
                    <div class="container">
                        <div class="title-item">
                            <h2>Export</h2>
                            <ul>
                                <li>
                                    <a href="index.html">Home</a>
                                </li>
                                <li>
                                    <span>/</span>
                                </li>
                                <li>
                                    Export
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Page Title -->

        <!-- Benefit -->
    <section class="benefit-area pb-100" >
        <div class="container">
            <div class="benefit-content">
                <!-- <div class="section-title">
                    <h2>Get Benefits of Using Latest Artificial Intelligence Technologies.</h2>
                </div> -->
                {!!$exportsales->content!!}
                <!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.</p> -->
                <!-- <div class="row">
                    <div class="col-sm-6 col-lg-6">
                        <div class="benefit-inner">
                            <i class="flaticon-darts"></i>
                            <h4>Special Packaging for Export</h4>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-6">
                        <div class="benefit-inner">
                            <i class="flaticon-customer"></i>
                            <h4>In house container and Flat Rack loading</h4>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-6">
                        <div class="benefit-inner">
                            <i class="flaticon-security-purposes"></i>
                            <h4>Material Consolidation</h4>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-6">
                        <div class="benefit-inner">
                            <i class="flaticon-artificial-intelligence"></i>
                            <h4>Deliveries to the Port on our own fleet</h4>
                        </div>
                    </div>
                </div> -->
               
                <div class="benefit-shape">
                    <img src="{{asset('assets/img/home-one/benefit-shape.png')}}" alt="Benefit">
                </div>
            </div>
        </div>
    </section>
    <!-- End Benefit -->


 @endsection