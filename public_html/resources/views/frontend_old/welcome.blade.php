@extends('layouts_frontend.masters',['title'=>'Everglades'])
@section('content')
<!-- Banner -->
<link rel="stylesheet" href="{{asset('assets/frontend_css/swiper.min.css')}}">
<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6/jquery.min.js" type="text/javascript"></script>
   <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script> -->
@if(Session::has('success'))        
<script>
   $(function() {
       $('#myModal').modal('show');
   }); 
   
   
   setTimeout(function() {
   $('#myModal').modal('hide');
   }, 8000);
   
</script>
@endif  

<div id="myModal" class="modal fade thankyou-modal" tabindex="-1">
   <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
         <div class="modal-header justify-content-center">
            <div class="icon-box">
               <i class="bx bx-check"></i>
            </div>
         </div>
         <div class="modal-body text-center">
            <h4>{{ (Session::has('success') ? Session::get('success') : '' ) }}</h4>
         </div>
      </div>
   </div>
</div>
<section class="new-slider">
   <div class="swiper-container slideshow">
      <div class="swiper-wrapper">
        @foreach($expert_team as $slider)
            <?php
                $url = $slider['url'];
                $splitUrl = explode('/public/', $url);
                $urlback = end($splitUrl);
                $currentUrl = Request::url();
                $explodeCurrentUrl = explode('/public/', $currentUrl);
                $firstCurrentUrl = $explodeCurrentUrl[0];
                $sliderUrl = $firstCurrentUrl.'/public/'.$urlback;
            ?>
         <div class="swiper-slide slide"> 
            @if($slider->image)
            <div class="slide-image" style="background-image: url({{url('media/slider',$slider['image'])}})"></div>
            @else
            <div class="slide-image" style="background-image: url({{asset('assets/img/banner-bg.jpg')}})"></div>
            @endif
            <span class="slide-title">{{$slider->title}}</span>  
            <p class="slide-title-small">{{strip_tags($slider['description'])}}</p>
            <a class="cmn-btn" href="{{$slider['url']}}">
               Discover More
               <i class="bx bx-right-arrow-alt"></i>
               </a>
         </div>
         @endforeach
      </div>
      <div class="slideshow-pagination"></div>
      <div class="slideshow-navigation">
         <div class="slideshow-navigation-button prev"><span class="bx bx-chevron-left"></span></div>
         <div class="slideshow-navigation-button next"><span class="bx bx-chevron-right"></span></div>
      </div>
   </div>
</section>


<!-- About -->
    <section class="about-area ptb-100" id="aboutus">
        <div class="container">
            <div class="row align-iems-center">
                <div class="col-lg-6">
                    <div class="section-title">
                        <span class="sub-title">About Us</span>
                    </div>
                    <div class="about-content">
                        {!!$aboutus->content!!}
                        <!-- <img src="{{asset('assets/img/home-one/about-man.png')}}" alt="About"> -->
                        <img src="{{asset('assets/img/home-one/about-signature.png')}}" alt="About">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="about-img-wrap">
                        <div class="about-img-slider owl-theme owl-carousel">
                            <div class="about-img-item">
                                <img src="{{asset('assets/img/home-one/truck.jpg')}}">
                                <!-- <img src="{{asset('assets/img/home-one/truck.jpg')}}" alt="About"> -->
                            </div>
                            <div class="about-img-item">
                                <img src="{{asset('assets/img/home-one/fork2.jpg')}}">
                                <!-- <img src="{{asset('assets/img/home-one/fork2.jpg')}}" alt="About"> -->
                            </div>
                            <div class="about-img-item">
                                <img src="{{asset('assets/img/home-one/Plasma.jpg')}}">
                                <!-- <img src="{{asset('assets/img/home-one/Plasma.jpg')}}" alt="About"> -->
                            </div>
                            <!-- <div class="about-img-item">
                                <img src="{{asset('assets/img/home-one/about5.jpg')}}" alt="About">
                            </div> -->
                        </div>
                        <div class="about-shape">
                            <img src="{{asset('assets/img/home-one/about2.png')}}" alt="About">
                            <img src="{{asset('assets/img/home-one/building.JPG')}}" id="building" style="width: 290px;">
                            <!-- <img src="{{asset('assets/img/home-one/about3.jpg')}}" alt="About"> -->
                            <img src="{{asset('assets/img/home-one/about4.png')}}" alt="About">
                            <img src="{{asset('assets/img/home-one/about5.png')}}" alt="About">
                        </div>
                        <div class="about-year">
                            <h2>35 <span>Years</span></h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End About -->

    <!-- Service -->
<!--
    <section class="service-area  pt-100 pb-70" id="services">
 -->
    <section class="homepage-service service-area-two service-area-three pt-100 pb-70" id="services">

        <div class="container">
            <div class="section-title">
                <span class="sub-title">Services</span>
                <h2>Services We Offer You From Our<span>Company</span></h2>
            </div>
            <div class="row">
                
                @foreach ($services as $service)
                <div class="col-sm-6 col-lg-4">
                    <div class="service-item">
                        <img src="{{asset('assets/img/home-one/service-shape.png')}}" alt="Service">
                        @if($service->service_image != null)
                        <img src="{{ url('/media/service/'. $service->service_image) }}" alt="Service">
                        
                        @else
                         <img src="{{asset('assets/img/home-one/service-shape2.png')}}" alt="Service">
                          
                        @endif
                        <!-- <i class="{{$service->service_icon}}"></i> -->
                        {!! $service->service_icon !!}
                        <h3>
                            <a href="{{url('/servicedetails',$service['id'])}}">{{$service->service_name}}</a>
                        </h3>
                        <div class="servie-text"><p>{{$service->service_detail}}</p></div>
                        <a class="service-link" href="{{url('/servicedetails',$service['id'])}}">Read More</a>
                    </div>
                </div>
                @endforeach
                  
            </div>
             <div class="section-title">
                <a href="{{url('/service')}}">
                     <button type="button" value="Veiw All" class="cmn-btn border-0">View All</button>
                </a>
            </div>
        </div>
    </section>
    <!-- End Service -->

    <!-- Counter -->
    <!-- <section class="counter-area pt-100" id="product">
        <div class="container">
            <div class="row align-iems-center">
                <div class="col-lg-5">
                    <div class="counter-text">
                        <h2>We have Completed Some Foreign Clients Project</h2>
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="row">
                        <div class="col-sm-4 col-lg-4">
                            <div class="counter-item">
                                <h3>
                                    <span class="odometer" data-count="1226">00</span> 
                                </h3>
                                <p>HAPPY CLIENTS</p>
                            </div>
                        </div>
                        <div class="col-sm-4 col-lg-4">
                            <div class="counter-item">
                                <h3>
                                    <span class="odometer" data-count="1552">00</span> 
                                </h3>
                                <p>WORKERS</p>
                            </div>
                        </div>
                        <div class="col-sm-4 col-lg-4">
                            <div class="counter-item">
                                <h3>
                                    <span class="odometer" data-count="1000">00</span> 
                                </h3>
                                <p>EXPERT</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> -->
    <!-- End Counter -->

        <!-- Project -->
    <section class="offer-area offer-area-four pt-100 pb-70">
        <div class="container">
            <div class="section-title">
                <span class="sub-title" style="color: black">Products</span>
               
            </div>
            <div class="row">
                <div class="col-sm-6 col-lg-4">
                    <div class="offer-item">
                        <div class="offer-top">
                            <img src="{{asset('media/productspdf/image 2.jpeg')}}" alt="Offer">
                            <i class="flaticon-3d-print"></i>
                        </div>
                        <div class="offer-bottom">
                            <h3>
                                <!-- <a href="{{route('productdetails')}}">Merchant Bars</a> -->
                                <a href="javascript:void(0)">Merchant Bars</a>
                            </h3>
                            <a class="cmn-btn" href="{{url('media/productspdf/merchant bars.pdf')}}" target="_blank">Download Pdf
                                <i class='bx bx-plus'></i>
                            </a>
                        </div>
                        <div class="offer-shape">
                            <img src="{{asset('assets/img/home-two/offer-shape.png')}}" alt="Offer">
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <div class="offer-item">
                        <div class="offer-top">
                            <img src="{{asset('media/productspdf/image 5.jpeg')}}" alt="Offer">
                            <i class="flaticon-robotic-arm"></i>
                        </div>
                        <div class="offer-bottom">
                            <h3>
                                <!-- <a href="{{route('productdetails')}}">Beams /Structural</a> -->
                                <a href="javascript:void(0)">Beams /Structural</a>
                            </h3>
                            <a class="cmn-btn" href="{{url('media/productspdf/beams-structural.pdf')}}" target="_blank">
                                Download Pdf
                                <i class='bx bx-plus'></i>
                            </a>
                        </div>
                        <div class="offer-shape">
                            <img src="{{asset('assets/img/home-two/offer-shape.png')}}" alt="Offer">
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <div class="offer-item">
                        <div class="offer-top">
                            <img src="{{asset('media/productspdf/image 1.jpeg')}}" alt="Offer">
                            <i class="flaticon-defribillator"></i>
                        </div>
                        <div class="offer-bottom">
                            <h3>
                                <!-- <a href="{{route('productdetails')}}">Pipes & Tubes</a> -->
                                <a href="javascript:void(0)">Pipes & Tubes</a>
                            </h3>
                            <a class="cmn-btn" href="{{url('media/productspdf/pipe-tube.pdf')}}" target="_blank">Download Pdf
                                <i class='bx bx-plus'></i>
                            </a>
                        </div>
                        <div class="offer-shape">
                            <img src="{{asset('assets/img/home-two/offer-shape.png')}}" alt="Offer">
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <div class="offer-item">
                        <div class="offer-top">
                            <img src="{{asset('media/productspdf/image 3.jpeg')}}" alt="Offer">
                            <i class="flaticon-3d-print"></i>
                        </div>
                        <div class="offer-bottom">
                            <h3>
                                <!-- <a href="{{route('productdetails')}}">Sheet & Plate</a> -->
                                <a href="javascript:void(0)">Sheet & Plate</a>
                            </h3>
                            <a class="cmn-btn" href="{{url('media/productspdf/sheet plate.pdf')}}" target="_blank">Download Pdf
                                <i class='bx bx-plus'></i>
                            </a>
                        </div>
                        <div class="offer-shape">
                            <img src="{{asset('assets/img/home-two/offer-shape.png')}}" alt="Offer">
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <div class="offer-item">
                        <div class="offer-top">
                            <img src="{{asset('media/productspdf/image 6.jpeg')}}" alt="Offer">
                            <i class="flaticon-robotic-arm"></i>
                        </div>
                        <div class="offer-bottom">
                            <h3>
                                <!-- <a href="{{route('productdetails')}}">Industrial</a> -->
                                <a href="javascript:void(0)">Industrial</a>
                            </h3>
                            <a class="cmn-btn" href="{{url('media/productspdf/Industrial.pdf')}}" target="_blank">Download Pdf
                                <i class='bx bx-plus'></i>
                            </a>
                        </div>
                        <div class="offer-shape">
                            <img src="{{asset('assets/img/home-two/offer-shape.png')}}" alt="Offer">
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <div class="offer-item">
                        <div class="offer-top">
                            <img src="{{asset('media/productspdf/image 4.jpg')}}" alt="Offer">
                            <i class="flaticon-defribillator"></i>
                        </div>
                        <div class="offer-bottom">
                            <h3>
                                <!-- <a href="{{route('productdetails')}}">Miscellaneous</a> -->
                                <a href="javascript:void(0)">Miscellaneous</a>
                            </h3>
                            <a class="cmn-btn" href="{{url('media/productspdf/misc.pdf')}}" target="_blank">Download Pdf
                                <i class='bx bx-plus'></i>
                            </a>
                        </div>
                        <div class="offer-shape">
                            <img src="{{asset('assets/img/home-two/offer-shape.png')}}" alt="Offer">
                        </div>
                    </div>
                </div>
                
            </div>
            <div class="section-title">
                <a href="{{url('/product')}}">
                     <button type="button" value="Veiw All" class="cmn-btn border-0">View All</button>
                </a>
            </div>
        </div>
    </section>
    <!-- End Project -->


   <!-- Foreign -->
   <!--  <div class="foreign-area" >
        <div class="container-fluid">
            <div class="foreign-slider owl-theme owl-carousel">
                @foreach($expert_team as $slider)
                <?php
                    $url = $slider['url'];
                    $splitUrl = explode('/public/', $url);
                    $urlback = end($splitUrl);
                    $currentUrl = Request::url();
                    $explodeCurrentUrl = explode('/public/', $currentUrl);
                    $firstCurrentUrl = $explodeCurrentUrl[0];
                    $sliderUrl = $firstCurrentUrl.'/public/'.$urlback
                ?>
                <div class="foreign-item">
                    
                    <a href="#">
                        <img src="{{url('media/slider',$slider['image'])}}" alt="Foreign">
                    </a>
                    <div class="foreign-bottom">
                        <h3>
                            <a href="{{$slider['url']}}">{{$slider['title']}}</a>
                        </h3>
                        <span>{!!$slider['description']!!}</span>
                    </div>
                </div>
                @endforeach    
            </div>
        </div>
    </div> -->
    <!-- End Foreign -->
    <!-- Testimonial -->
    <section class="testimonial-area ptb-100" id="testimonials">
            <div class="testimonial-shape">
                <img src="assets/img/home-two/testimonial-bg.png" alt="Testimonial">
            </div>
            <div class="container">
                <div class="section-title">
                    <h2>What Our Clients Say</h2>
                </div>
                <div class="testimonial-slider owl-theme owl-carousel">
                    @foreach($testimonial as $Testimonial)
                    <div class="testimonial-item">
                        <div class="testimonial-top">
                            <i class="flaticon-right-quotation-sign"></i>
                            <p>{{strip_tags($Testimonial->description)}}</p>
                        </div>
                        <ul class="align-items-center">
                            <li>
                                <img src="{{url('media/testimonials',$Testimonial['image'])}}" alt="Testimonial">
                            </li>
                            <li>
                                <h4>{!!$Testimonial->author_name!!}</h4>
                            <!--     <span>Developer</span> -->
                            </li>
                        </ul>
                    </div>
                    @endforeach
                </div>
            </div>
    </section>
    <!-- End Testimonial -->
    <!-- Benefit -->
    <section class="benefit-area pb-100" >
        <div class="container">
            <div class="benefit-content">
                <!-- <div class="section-title">
                    <h2>Get Benefits of Using Latest Artificial Intelligence Technologies.</h2>
                </div> -->
                {!!$exports->content!!}
                <!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.</p> -->
                <!-- <div class="row">
                    <div class="col-sm-6 col-lg-6">
                        <div class="benefit-inner">
                            <i class="flaticon-darts"></i>
                            <h4>Special Packaging for Export</h4>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-6">
                        <div class="benefit-inner">
                            <i class="flaticon-customer"></i>
                            <h4>In house container and Flat Rack loading</h4>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-6">
                        <div class="benefit-inner">
                            <i class="flaticon-security-purposes"></i>
                            <h4>Material Consolidation</h4>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-6">
                        <div class="benefit-inner">
                            <i class="flaticon-artificial-intelligence"></i>
                            <h4>Deliveries to the Port on our own fleet</h4>
                        </div>
                    </div>
                </div> -->
                <a class="cmn-btn" href="{{route('export')}}">
                    Know Details
                    <i class='bx bx-right-arrow-alt'></i>
                </a>
                <div class="benefit-shape">
                    <img src="{{asset('assets/img/home-one/benefit-shape.png')}}" alt="Benefit">
                </div>
            </div>
        </div>
    </section>
    <!-- End Benefit -->

    <!-- Partner -->
    <section class="partner-area " id="contact">
        <div class="container">
            <div class="partner-wrap">
                <div class="partner-shape">
                    <img src="{{asset('assets/img/home-one/partner-shape.png')}}" alt="Partner">
                </div>
                <div class="row align-items-center">
                    <div class="col-lg-6">
                        <div class="partner-content">
                            <div class="section-title">
                                <h2>Looking for a Reliable & Stable partner</h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="partner-btn">
                            <a class="cmn-btn" href="{{ route('contact') }}">
                                Contact Us
                                <i class='bx bx-right-arrow-alt'></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Partner -->

    <!-- Blog -->
    <section class="blog-area pt-30 pb-30" id="blog">
        <div class="container">
            <div class="section-title">
                <span class="sub-title">Blogs</span>
                <h2>Our Latest Blogs</h2>
            </div>
            <div class="row">
                 @foreach($blog as $blog_detail)
                  @if ($loop->first)
                <div class="col-sm-12 col-lg-8">
                    <div class="blog-item">
                        <a href="{{url('/blogdetails',$blog_detail['id'])}}">
                            <img src="{{url('/media/blog',$blog_detail['image'])}}" alt="Blog">
                        </a>
                        <span>{{date('j F Y', strtotime($blog_detail->created_date))}}</span>
                        <div class="blog-inner">
                            <h3>
                                <a href="{{url('/blogdetails',$blog_detail['id'])}}">{{$blog_detail->blog_title}}</a>
                            </h3>
                            <a class="blog-link" href="{{url('/blogdetails',$blog_detail['id'])}}">
                                Read More
                                <i class='bx bx-plus'></i>
                            </a>
                        </div>
                    </div>
                </div>
                @endif
                @endforeach
                <div class="col-sm-12 col-lg-4">  
                <div class="row"> 
                   @foreach($blog as $key => $blog)
                   @if($key > 0)
                        <div class="col-sm-6 col-lg-12">
                            <div class="blog-item">
                                <a href="{{url('/blogdetails',$blog['id'])}}">
                                    <img src="{{url('/media/blog',$blog['image'])}}" alt="Blog">
                                </a>
                                <span>{{date('j F Y', strtotime($blog->created_date))}}</span>
                                <div class="blog-inner">
                                    <h3>
                                        <a href="{{url('/blogdetails',$blog['id'])}}">{{$blog->blog_title}}</a>
                                    </h3>
                                    <a class="blog-link" href="{{url('/blogdetails',$blog['id'])}}">
                                        Read More
                                        <i class='bx bx-plus'></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endif
                    @endforeach
                </div>
                </div>
            </div>
        </div>
          <section class="partner-area pb-100 " id="contact">
        <div class="container">
            <div class="partner-wrap">
                <div class="partner-shape">
                    <img src="{{asset('assets/img/home-one/partner-shape.png')}}" alt="Partner">
                </div>
                  <form  method="post" action="{{ url('subscribe')}}" id="changepassword_form">
                    @csrf
                    @method('POST')
                <div class="row align-items-center">
                    <div class="col-lg-6">
                        <div class="partner-content">
                            <div class="section-title">
                                <h2>Subscription </h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <input type="email" name="email" id="email" class="form-control"  data-error="Please enter your email" placeholder="Email *">
                        <span style="color:red" class="kt-form__help error email"></span>
                        <span style="color:red">{{ $errors->first('email') }}</span>

                    </div>
                    <div class="col-lg-2 partner-wrap">
                        <div class="partner-btn">
                                <button type="submit" name="" value="Subscribe" class="cmn-btn border-0">Subscribe </button>
                                <!-- <i class='bx bx-right-arrow-alt'></i> -->
                        </div>
                    </div>
                    
                </div>
                </form>
            </div>
        </div>
    </section>
    </section>
    <!-- End Blog -->
<!-- End Blog -->
<script src="{{asset('assets/frontend_js/swiper.min.js')}}"></script>
<script src="{{asset('assets/frontend_js/TweenMax.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/frontend_js/custom.js')}}"></script>
<script type="text/javascript" src="{{asset('js/ChangePasswordFormValidation.js')}}"></script>
@endsection

