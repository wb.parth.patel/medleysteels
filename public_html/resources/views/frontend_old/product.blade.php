@extends('layouts_frontend.masters',['title'=>'Everglades'])
@section('content')
    <!-- Page Title -->
    <div class="page-title-area">
        <div class="d-table">
            <div class="d-table-cell">
                <div class="container">
                    <div class="title-item">
                        <!-- <h2>Product</h2> -->
                        <h2>PRODUCTS</h2>
                        <ul>
                            <li>
                                <a href="{{route('index')}}">Home</a>
                            </li>
                            <li>
                                <span>/</span>
                            </li>
                            <li>
                                Product
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Page Title -->

    <!-- Project -->
    <section class="offer-area offer-area-four pt-100 pb-70">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-lg-4">
                    <div class="offer-item">
                        <div class="offer-top">
                            <img src="{{asset('media/productspdf/image 2.jpeg')}}" alt="Offer">
                            <i class="flaticon-3d-print"></i>
                        </div>
                        <div class="offer-bottom">
                            <h3>
                                <!-- <a href="{{route('productdetails')}}">Merchant Bars</a> -->
                                <a href="javascript:void(0)">Merchant Bars</a>
                            </h3>
                            <a class="cmn-btn" href="{{url('media/productspdf/merchant bars.pdf')}}" target="_blank">Download Pdf
                                <i class='bx bx-plus'></i>
                            </a>
                        </div>
                        <div class="offer-shape">
                            <img src="{{asset('assets/img/home-two/offer-shape.png')}}" alt="Offer">
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <div class="offer-item">
                        <div class="offer-top">
                            <img src="{{asset('media/productspdf/image 5.jpeg')}}" alt="Offer">
                            <i class="flaticon-robotic-arm"></i>
                        </div>
                        <div class="offer-bottom">
                            <h3>
                                <!-- <a href="{{route('productdetails')}}">Beams /Structural</a> -->
                                <a href="javascript:void(0)">Beams /Structural</a>
                            </h3>
                            <a class="cmn-btn" href="{{url('media/productspdf/beams-structural.pdf')}}" target="_blank">Download Pdf
                                <i class='bx bx-plus'></i>
                            </a>
                        </div>
                        <div class="offer-shape">
                            <img src="{{asset('assets/img/home-two/offer-shape.png')}}" alt="Offer">
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <div class="offer-item">
                        <div class="offer-top">
                            <img src="{{asset('media/productspdf/image 1.jpeg')}}" alt="Offer">
                            <i class="flaticon-defribillator"></i>
                        </div>
                        <div class="offer-bottom">
                            <h3>
                                <!-- <a href="{{route('productdetails')}}">Pipes & Tubes</a> -->
                                <a href="javascript:void(0)">Pipes & Tubes</a>
                            </h3>
                            <a class="cmn-btn" href="{{url('media/productspdf/pipe-tube.pdf')}}" target="_blank">Download Pdf
                                <i class='bx bx-plus'></i>
                            </a>
                        </div>
                        <div class="offer-shape">
                            <img src="{{asset('assets/img/home-two/offer-shape.png')}}" alt="Offer">
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <div class="offer-item">
                        <div class="offer-top">
                            <img src="{{asset('media/productspdf/image 3.jpeg')}}" alt="Offer">
                            <i class="flaticon-3d-print"></i>
                        </div>
                        <div class="offer-bottom">
                            <h3>
                                <!-- <a href="{{route('productdetails')}}">Sheet & Plate</a> -->
                                <a href="javascript:void(0)">Sheet & Plate</a>
                            </h3>
                            <a class="cmn-btn" href="{{url('media/productspdf/sheet plate.pdf')}}" target="_blank">Download Pdf
                                <i class='bx bx-plus'></i>
                            </a>
                        </div>
                        <div class="offer-shape">
                            <img src="{{asset('assets/img/home-two/offer-shape.png')}}" alt="Offer">
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <div class="offer-item">
                        <div class="offer-top">
                            <img src="{{asset('media/productspdf/image 6.jpeg')}}" alt="Offer">
                            <i class="flaticon-robotic-arm"></i>
                        </div>
                        <div class="offer-bottom">
                            <h3>
                                <!-- <a href="{{route('productdetails')}}">Industrial</a> -->
                                <a href="javascript:void(0)">Industrial</a>
                            </h3>
                            <a class="cmn-btn" href="{{url('media/productspdf/Industrial.pdf')}}" target="_blank">Download Pdf
                                <i class='bx bx-plus'></i>
                            </a>
                        </div>
                        <div class="offer-shape">
                            <img src="{{asset('assets/img/home-two/offer-shape.png')}}" alt="Offer">
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <div class="offer-item">
                        <div class="offer-top">
                            <img src="{{asset('media/productspdf/image 4.jpg')}}" alt="Offer">
                            <i class="flaticon-defribillator"></i>
                        </div>
                        <div class="offer-bottom">
                            <h3>
                                <!-- <a href="{{route('productdetails')}}">Miscellaneous</a> -->
                                <a href="javascript:void(0)">Miscellaneous</a>
                            </h3>
                            <a class="cmn-btn" href="{{url('media/productspdf/misc.pdf')}}" target="_blank">Download Pdf
                                <i class='bx bx-plus'></i>
                            </a>
                        </div>
                        <div class="offer-shape">
                            <img src="{{asset('assets/img/home-two/offer-shape.png')}}" alt="Offer">
                        </div>
                    </div>
                </div>
                <!-- <div class="col-sm-6 col-lg-4">
                    <div class="offer-item">
                        <div class="offer-top">
                            <img src="{{asset('assets/img/home-two/offer1.jpg')}}" alt="Offer">
                            <i class="flaticon-3d-print"></i>
                        </div>
                        <div class="offer-bottom">
                            <h3>
                                <a href="{{route('productdetails')}}">Industrial Technology</a>
                            </h3>
                            
                            <a class="cmn-btn" href="{{url('media/sample.pdf')}}" target="_blank">Download Pdf
                                <i class='bx bx-plus'></i>
                            </a>
                        </div>
                        <div class="offer-shape">
                            <img src="{{asset('assets/img/home-two/offer-shape.png')}}" alt="Offer">
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <div class="offer-item">
                        <div class="offer-top">
                            <img src="{{asset('assets/img/home-two/offer2.jpg')}}" alt="Offer">
                            <i class="flaticon-robotic-arm"></i>
                        </div>
                        <div class="offer-bottom">
                            <h3>
                                <a href="{{route('productdetails')}}">Automotive Industrial</a>
                            </h3>
                            
                            <a class="cmn-btn" href="{{url('media/sample.pdf')}}" target="_blank">Download Pdf
                                <i class='bx bx-plus'></i>
                            </a>
                        </div>
                        <div class="offer-shape">
                            <img src="{{asset('assets/img/home-two/offer-shape.png')}}" alt="Offer">
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <div class="offer-item">
                        <div class="offer-top">
                            <img src="{{asset('assets/img/home-two/offer3.jpg')}}" alt="Offer">
                            <i class="flaticon-defribillator"></i>
                        </div>
                        <div class="offer-bottom">
                            <h3>
                                <a href="{{route('productdetails')}}">Healthcare Technology</a>
                            </h3>
                            
                            <a class="cmn-btn" href="{{url('media/sample.pdf')}}" target="_blank">Download Pdf
                                <i class='bx bx-plus'></i>
                            </a>
                        </div>
                        <div class="offer-shape">
                            <img src="{{asset('assets/img/home-two/offer-shape.png')}}" alt="Offer">
                        </div>
                    </div>
                </div> -->
            </div>
        </div>
    </section>
    <!-- End Project -->

    <!-- Job -->
   <!--  <div class="job-area">
        <div class="job-shape">
            <img src="{{asset('assets/img/home-two/job2.png')}}" alt="Job">
        </div>
        <div class="container-fluid">
            <div class="row m-0">
                <div class="col-lg-6">
                    <div class="job-img">
                        <img src="{{asset('assets/img/home-two/job1.jpg')}}" alt="Job">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="job-content">
                        <div class="section-title">
                            <h2>Enhancing, Not Replacing, Human Jobs</h2>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.</p>
                        <ul>
                            <li>
                                <i class='bx bx-check'></i>
                                Higher Revenue
                            </li>
                            <li>
                                <i class='bx bx-check'></i>
                                Improved Monitoring
                            </li>
                            <li>
                                <i class='bx bx-check'></i>
                                Lower Cost
                            </li>
                            <li>
                                <i class='bx bx-check'></i>
                                Fast Implementation
                            </li>
                            <li>
                                <i class='bx bx-check'></i>
                                Greater Efficiency
                            </li>
                            <li>
                                <i class='bx bx-check'></i>
                                Better Safety
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!-- End Job -->

    <!-- Team -->
    <!-- <section class="team-area team-area-two pt-100 pb-70">
        <div class="container">
            <div class="section-title">
                <span class="sub-title">Team</span>
                <h2>Our Expart Team</h2>
            </div>
            <div class="row">
                <div class="col-sm-6 col-lg-4">
                    <div class="team-item">
                        <img src="assets/img/home-one/team1.jpg" alt="Team">
                        <h3>Jack Farnes</h3>
                        <span>Manager of Company</span>
                        <ul>
                            <li>
                                <a href="{{route('productdetails')}}" target="_blank">
                                    <i class='bx bxl-facebook'></i>
                                </a>
                            </li>
                            <li>
                                <a href="{{route('productdetails')}}" target="_blank">
                                    <i class='bx bxl-twitter'></i>
                                </a>
                            </li>
                            <li>
                                <a href="{{route('productdetails')}}" target="_blank">
                                    <i class='bx bxl-pinterest-alt'></i>
                                </a>
                            </li>
                            <li>
                                <a href="{{route('productdetails')}}" target="_blank">
                                    <i class='bx bxl-linkedin'></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <div class="team-item">
                        <img src="assets/img/home-one/team2.jpg" alt="Team">
                        <h3>Andres Pedlock</h3>
                        <span>CEO, Company</span>
                        <ul>
                            <li>
                                <a href="{{route('productdetails')}}" target="_blank">
                                    <i class='bx bxl-facebook'></i>
                                </a>
                            </li>
                            <li>
                                <a href="{{route('productdetails')}}" target="_blank">
                                    <i class='bx bxl-twitter'></i>
                                </a>
                            </li>
                            <li>
                                <a href="{{route('productdetails')}}" target="_blank">
                                    <i class='bx bxl-pinterest-alt'></i>
                                </a>
                            </li>
                            <li>
                                <a href="{{route('productdetails')}}" target="_blank">
                                    <i class='bx bxl-linkedin'></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6 offset-sm-3 offset-lg-0 col-lg-4">
                    <div class="team-item">
                        <img src="assets/img/home-one/team3.jpg" alt="Team">
                        <h3>Adam Meir</h3>
                        <span>Lead Developer</span>
                        <ul>
                            <li>
                                <a href="{{route('productdetails')}}" target="_blank">
                                    <i class='bx bxl-facebook'></i>
                                </a>
                            </li>
                            <li>
                                <a href="{{route('productdetails')}}" target="_blank">
                                    <i class='bx bxl-twitter'></i>
                                </a>
                            </li>
                            <li>
                                <a href="{{route('productdetails')}}" target="_blank">
                                    <i class='bx bxl-pinterest-alt'></i>
                                </a>
                            </li>
                            <li>
                                <a href="{{route('productdetails')}}" target="_blank">
                                    <i class='bx bxl-linkedin'></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section> -->
    <!-- End Team --> 

@endsection