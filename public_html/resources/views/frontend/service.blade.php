@extends('layouts_frontend_new.masters',['title'=>'services'])
@section('content')
  <!-- SERVICES-->
      <div class="glax_tm_section">
         <div class="glax_tm_main_title_holder">
            <div class="container">
               <div class="title_holder">
                  <h3>Services</h3>
               </div>
               <div class="builify_tm_breadcrumbs">
                  <ul>
                     <li><a href="{{url('index')}}">Glax Home</a></li>
                     <li class="shape"><span></span></li>
                     <li><span>Services</span></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <div class="glax_tm_section">
         <div class="container">
            <div class="glax_tm_services_wrap">
               <div class="glax_tm_twicebox_wrap">
                  <div class="leftbox">
                     <div class="glax_tm_service_title_holder">
                        <h3>What we offer</h3>
                        <p>Bring to the table win-win survival strategies to ensure proactive domination. At the end of the day, going forward, a new normal that has evolved from generation X is on the runway heading towards.</p>
                     </div>
                     <div class="glax_tm_service_list_wrap">
                        <ul>
                           <li>
                              @foreach($services as $service)
                              <div class="inner">
                                 <div class="list_image_wrap">
                                    <div class="image" data-img-url="{{$service->service_image}}"></div>
                                    <a href="#"></a>
                                 </div>
                                 <div class="main_infos">
                                    <div class="title_holder">
                                       <h3><a href="#">{!! $service->service_name !!}</a></h3>

                                       <p>{!! $service->service_desctiprion !!}</p>
                                    </div>
                                 </div>
                                 
                                 <div class="glax_tm_shape_read_more_wrap">
                                    <div class="read_more_in">
                                       <a href="{{url( 'servicedetail', $service->id) }}" >Read More</a>
                                    </div>
                                 </div>
                              </div>
                              @endforeach
                           </li>
                           <!-- <li>
                              <div class="inner">
                                 <div class="list_image_wrap">
                                    <div class="image" data-img-url="{{asset('assets/img/services/12.jpg')}}"></div>
                                    <a href="#"></a>
                                 </div>
                                 <div class="main_infos">
                                    <div class="title_holder">
                                       <h3><a href="#">Medical Devices</a></h3>
                                       <p>Get your medical device tested and into the hands of your customers faster than ever before. Time to market starts with partnership, and for</p>
                                    </div>
                                 </div>
                                 <div class="glax_tm_shape_read_more_wrap">
                                    <div class="read_more_in">
                                       <a href="#">Read More</a>
                                    </div>
                                 </div>
                              </div>
                           </li>
                           <li>
                              <div class="inner">
                                 <div class="list_image_wrap">
                                    <div class="image" data-img-url="{{asset('assets/img/services/13.jpg')}}"></div>
                                    <a href="#"></a>
                                 </div>
                                 <div class="main_infos">
                                    <div class="title_holder">
                                       <h3><a href="#">Housewares &amp; Home Decor</a></h3>
                                       <p>From candles to cutlery and from picture frames to pet toys, Glax helps to establish the compliance of your household products to quality and</p>
                                    </div>
                                 </div>
                                 <div class="glax_tm_shape_read_more_wrap">
                                    <div class="read_more_in">
                                       <a href="#">Read More</a>
                                    </div>
                                 </div>
                              </div>
                           </li>
                           <li>
                              <div class="inner">
                                 <div class="list_image_wrap">
                                    <div class="image" data-img-url="{{asset('assets/img/services/14.jpg')}}"></div>
                                    <a href="#"></a>
                                 </div>
                                 <div class="main_infos"> 
                                    <div class="title_holder">
                                       <h3><a href="#">The Shale Oil and Gas Revolution</a></h3>
                                       <p>The Shale Oil and Gas Revolution has changed the energy world. Glax has kept pace by providing crucial and timely exploration, production, quality, quantity</p>
                                    </div>
                                 </div>
                                 <div class="glax_tm_shape_read_more_wrap">
                                    <div class="read_more_in">
                                       <a href="#">Read More</a>
                                    </div>
                                 </div>
                              </div>
                           </li>
                           <li>
                              <div class="inner">
                                 <div class="list_image_wrap">
                                    <div class="image" data-img-url="{{asset('assets/img/services/15.jpg')}}"></div>
                                    <a href="#"></a>
                                 </div>
                                 <div class="main_infos">
                                    <div class="title_holder">
                                       <h3><a href="#">Textiles &amp; Apparel</a></h3>
                                       <p>Glax’s tailored solutions enable retailers, brands and manufacturers of textile products, apparel and home textiles to ensure the safety, quality and performance of their</p>
                                    </div>
                                 </div>
                                 <div class="glax_tm_shape_read_more_wrap">
                                    <div class="read_more_in">
                                       <a href="#">Read More</a>
                                    </div>
                                 </div>
                              </div>
                           </li>
                           <li>
                              <div class="inner">
                                 <div class="list_image_wrap">
                                    <div class="image" data-img-url="{{asset('assets/img/services/16.jpg')}}"></div>
                                    <a href="#"></a>
                                 </div>
                                 <div class="main_infos">
                                    <div class="title_holder">
                                       <h3><a href="#">Construction &amp; Engineering</a></h3>
                                       <p>Ensure the strength, integrity and conformity of your construction and engineering processes and products by using Glax’s specialised services. The construction and engineering industries</p>
                                    </div>
                                 </div>
                                 <div class="glax_tm_shape_read_more_wrap">
                                    <div class="read_more_in">
                                       <a href="#">Read More</a>
                                    </div>
                                 </div>
                              </div>
                           </li> -->
                        </ul>
                     </div>
                  </div>
                  <div class="rightbox">
                     <div class="glax_tm_categories">
                        <div class="title_holder_wrap">
                           <h3>Full list of services</h3>
                        </div>
                        <div class="list_wrap">
                           <ul> 
                              @foreach($services as $service)
                             <li><a href="#">{!! $service->service_name !!}</a></li>
                             @endforeach 
                             <!-- <li><a href="#">Medical Devices</a></li>
                              <li><a href="#">Housewares &amp; Home Decor</a></li>
                              <li><a href="#">The Shale Oil and Gas Revolution</a></li>
                              <li><a href="#">Textiles &amp; Apparel</a></li>
                              <li><a href="#">Construction &amp; Engineering</a></li>
                              <li><a href="#">Basic &amp; Industrial Chemicals</a></li> -->
                           </ul>
                        </div>
                     </div>
                     <div class="glax_tm_request_estimate_wrap">
                        <div class="image_wrap">
                           <img src="{{asset('assets/img/services/300x460.jpg')}}" alt="" />
                           <div class="image"></div>
                        </div>
                        <div class="definition">
                           <div class="text">
                              <p>Lets get started! Contact us for a free quote on your next home improvement project.</p>
                           </div>
                           <div class="button">
                              <a href="#">Request an estimate</a>
                           </div>
                           <div class="first_shape">
                              <span class="first"></span>
                              <span class="second"></span>
                              <span class="third"></span>
                           </div>
                           <div class="second_shape">
                              <span class="first"></span>
                              <span class="second"></span>
                              <span class="third"></span>
                           </div>
                        </div>
                     </div>
                     <div class="glax_tm_brochures_wrap">
                        <div class="title_holder">
                           <span>Company Presentation</span>
                        </div>
                        <ul>
                           <li>
                              <div class="inner">
                                 <div class="icon">
                                    <img class="svg" src="{{asset('assets/img/svg/file-pdf.svg')}}" alt="" />
                                 </div>
                                 <span class="text">Download .PDF</span>
                                 <span class="arrow"></span>
                              </div>
                           </li>
                           <li>
                              <div class="inner">
                                 <div class="icon">
                                    <img class="svg" src="{{asset('assets/img/svg/file-zip.svg')}}" alt="" />
                                 </div>
                                 <span class="text">Download .ZIP</span>
                                 <span class="arrow"></span>
                              </div>
                           </li>
                           <li>
                              <div class="inner">
                                 <div class="icon">
                                    <img class="svg" src="{{asset('assets/img/svg/file-doc.svg')}}" alt="" />
                                 </div>
                                 <span class="text">Download .DOC</span>
                                 <span class="arrow"></span>
                              </div>
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="glax_tm_section">
                  <div class="glax_tm_pagination">
                     <ul>
                        <li><span>Prev</span></li>
                        <li class="active"><a href="#">Next</a></li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- /SERVICES -->
      @endsection