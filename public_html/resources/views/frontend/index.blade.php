@extends('layouts_frontend_new.masters',['title'=>'Medley Steel'])
@section('content')
<!-- WRAPPER ALL -->

<!-- HERO HEADER -->
<div class="glax_tm_hero_header_wrap">
   <div class="slider_total_wrap">
      <div class="swiper-container">
         <div class="swiper-wrapper">
            <div class="image_wrap swiper-slide">
               <div class="bg_img" data-img-url="{{asset('assets/frontend_new/img/slider/1.jpg')}}"></div>
               <div class="swiper_content">
                  <div class="texts_wrap">
                     <h3>We are more than industrial company</h3>
                     <p>The foundations and aspirations of our business remain true to those established by our visionary founders, and their innovation and energy continue to be our inspiration. Our passion and entrepreneurial culture will ensure that we deliver for our customers in safety, quality and assurance – today and in the future.</p>
                  </div>
                  <div class="switches">
                     <div class="prev_next">
                        <div class="tm_next_button"></div>
                        <div class="tm_prev_button"></div>
                     </div>
                     <div class="swiper-pagination my_swiper_pagination"></div>
                  </div>
               </div>
            </div>
            <div class="image_wrap swiper-slide">
               <div class="bg_img" data-img-url="{{asset('assets/frontend_new/img/slider/4.jpg')}}"></div>
               <div class="swiper_content">
                  <div class="texts_wrap">
                     <h3>We are more than industrial company</h3>
                     <p>The foundations and aspirations of our business remain true to those established by our visionary founders, and their innovation and energy continue to be our inspiration. Our passion and entrepreneurial culture will ensure that we deliver for our customers in safety, quality and assurance – today and in the future.</p>
                  </div>
                  <div class="switches">
                     <div class="prev_next">
                        <div class="tm_next_button"></div>
                        <div class="tm_prev_button"></div>
                     </div>
                     <div class="swiper-pagination my_swiper_pagination"></div>
                  </div>
               </div>
            </div>
            <div class="image_wrap swiper-slide">
               <div class="bg_img" data-img-url="{{asset('assets/frontend_new/img/slider/5.jpg')}}"></div>
               <div class="swiper_content">
                  <div class="texts_wrap">
                     <h3>We are more than industrial company</h3>
                     <p>The foundations and aspirations of our business remain true to those established by our visionary founders, and their innovation and energy continue to be our inspiration. Our passion and entrepreneurial culture will ensure that we deliver for our customers in safety, quality and assurance – today and in the future.</p>
                  </div>
                  <div class="switches">
                     <div class="prev_next">
                        <div class="tm_next_button"></div>
                        <div class="tm_prev_button"></div>
                     </div>
                     <div class="swiper-pagination my_swiper_pagination"></div>
                  </div>
               </div>
            </div>
            <div class="image_wrap swiper-slide">
               <div class="bg_img" data-img-url="{{asset('assets/frontend_new/img/slider/7.jpg')}}"></div>
               <div class="swiper_content">
                  <div class="texts_wrap">
                     <h3>We are more than industrial company</h3>
                     <p>The foundations and aspirations of our business remain true to those established by our visionary founders, and their innovation and energy continue to be our inspiration. Our passion and entrepreneurial culture will ensure that we deliver for our customers in safety, quality and assurance – today and in the future.</p>
                  </div>
                  <div class="switches">
                     <div class="prev_next">
                        <div class="tm_next_button"></div>
                        <div class="tm_prev_button"></div>
                     </div>
                     <div class="swiper-pagination my_swiper_pagination"></div>
                  </div>
               </div>
            </div>
            <div class="image_wrap swiper-slide">
               <div class="bg_img" data-img-url="https://medleysteel.xceltec.in/public/assets/frontend_new/img/slider/8.jpg"></div>
               <div class="swiper_content">
                  <div class="texts_wrap">
                     <h3>We are more than industrial company</h3>
                     <p>The foundations and aspirations of our business remain true to those established by our visionary founders, and their innovation and energy continue to be our inspiration. Our passion and entrepreneurial culture will ensure that we deliver for our customers in safety, quality and assurance – today and in the future.</p>
                  </div>
                  <div class="switches">
                     <div class="prev_next">
                        <div class="tm_next_button"></div>
                        <div class="tm_prev_button"></div>
                     </div>
                     <div class="swiper-pagination my_swiper_pagination"></div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="glax_tm_swiper_content">
      <div class="container swiper">
         <div class="swiper_content">
            <div class="texts_wrap">
               <!-- <h3>We are more than industrial company</h3>
               <p>The foundations and aspirations of our business remain true to those established by our visionary founders, and their innovation and energy continue to be our inspiration. Our passion and entrepreneurial culture will ensure that we deliver for our customers in safety, quality and assurance – today and in the future.</p> -->
               <h3>{{ $weAreService->title }}</h3>
               <p>{!! $weAreService->content !!}</p>
            </div>
            <div class="switches">
               <div class="prev_next">
                  <div class="tm_next_button"></div>
                  <div class="tm_prev_button"></div>
               </div>
               <div class="swiper-pagination my_swiper_pagination"></div>
            </div>
         </div>
      </div>
   </div>
   <div class="swiper_overlay"></div>
</div>
<!-- /HERO HEADER -->

<!-- HOME INTRODUCE -->
<div class="glax_tm_section introduce">
   <div class="container">
      <div class="qqq">
         <div class="glax_tm_introduce_wrap">
            <div class="inner_wrap">
               <div class="main_info_wrap">
                  <span class="top_title">Let me introduce</span>
                  <!-- <h3 class="title">Our Company</h3>
                  <p class="text">For over 47 years, the Glax family has been building relationships and projects that last. We build safe environments and eco-friendly solutions in the communities in which we work. Most importantly, we build strong relationships that allow us to build anything, anywhere. No matter the job, we go beyond building.</p> -->
                  <h3 class="title">{{$ourCompany->title}}</h3>
                  <p class="text">{!! $ourCompany->content !!}</p>
               </div>
               <div class="experience_box">
                  <div class="top">
                     <p>World's Leading Industry Corporation</p>
                  </div>
                  <div class="bottom">
                     <div class="number">
                        <span>25</span>
                     </div>
                     <div class="definition">
                        <p>Years of experience</p>
                     </div>
                  </div>
               </div>
            </div>
            <div class="play_video">
               <a class="popup-youtube" href="https://www.youtube.com/watch?v=se4yc09w7Ic"></a>
            </div>
         </div>
         <div class="shape_top">
            <span class="first"></span>
            <span class="second"></span>
         </div>
         <div class="shape_bottom">
            <span class="first"></span>
            <span class="second"></span>
         </div>
      </div>
   </div>
</div>
<!-- /HOME INTRODUCE -->

<!-- HOME SERVICE -->
<div class="glax_tm_section">
   <div class="container">
      <div class="glax_tm_home_service_list">
         @foreach($services as $service)
         <ul class="glax_tm_miniboxes">
            <li>
               <div class="inner_list glax_tm_minibox">
                  <div class="icon_wrap">
                     <!-- <img class="svg" src="{{asset('assets/frontend_new/img/svg/service-flasks.svg')}}" alt="" /> -->
                     <!-- <p>{{$service->service_icon}}</p> -->
                  </div>
                  <div class="title_holder">
                     <!-- <h3>Basic &amp; Industrial Chemicals</h3> -->
                     <h3>{{$service->service_name}}</h3>
                  </div>
                  <div class="description">
                     <!-- <p>During this phase, we will work to provide a detailed analysis of the project and we will establish project expectations along...</p> -->
                     <p>{!! $service->service_desctiprion !!}</p>
                  </div>
                  <div class="glax_tm_button_more_wrap">
                     <a href="#">
                        More Details
                        <span class="arrow_wrap">
                           <span class="first"><img class="svg" src="img/svg/arrow-right.svg" alt="" /></span>
                           <span class="second"><img class="svg" src="img/svg/arrow-right.svg" alt="" /></span>
                        </span>
                     </a>
                  </div>
                  <a class="service_link" href="#"></a>
               </div>
            </li>
         </ul>
         @endforeach
      </div>
   </div>
</div>

<!-- /HOME SERVICE -->

<!-- WHY CHOOSE US-->
<div class="glax_tm_section">
   <div class="glax_tm_rating_wrap">
      <div class="container">
         <div class="inner">
            <div class="leftbox">
               <div class="title">
                  <h3><!-- World's Leading Building Corporation -->
                     {{ $takeAdvantage->title }}
                  </h3>
               </div>
               <div class="text">
                  <p><!-- To further develop our corporate strengths we have established a corporate mandate to maintain strong core values that truly reflect the companys philosophy. -->
                     {!! $takeAdvantage->content !!}
  
                  </p>
               </div>
              <!--  <div class="glax_tm_project_video">
                <span>
                  <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="443.307px" height="443.306px" viewBox="0 0 443.307 443.306" style="enable-background:new 0 0 443.307 443.306;" xml:space="preserve" class="svg replaced-svg">
                     <g>
                        <path d="M415.934,212.799L36.788,2.097C32.411-0.377,28.65-0.661,25.51,1.242c-3.14,1.902-4.708,5.328-4.708,10.276V431.78   c0,4.952,1.569,8.381,4.708,10.284c3.14,1.902,6.901,1.622,11.278-0.855l379.146-210.703c4.381-2.478,6.571-5.434,6.571-8.856   C422.505,218.224,420.314,215.274,415.934,212.799z"></path></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg>
                     </span>
                     <a class="project_time" href="#">View Company Promo Video</a>
                     <a class="project_video_button popup-youtube" href="https://www.youtube.com/watch?v=se4yc09w7Ic"></a>
                  </div> -->
               </div>
               <div class="ratingbox">
                  <div class="rating_wrap">
                     <div class="inner_wrap">
                        <div class="star">
                           <img src="img/rating/rate.png" alt="" />
                        </div>
                        <div class="number">
                           <span>9.7</span>
                        </div>
                        <div class="title">
                           <p>Customer Rating</p>
                        </div>
                     </div>
                  </div>
                  <div class="rating_text">
                     <div class="inner">
                        <span>Full reviews at Trustpilot</span>
                     </div>
                  </div>
               </div>
               <div class="rightbox">
                  <div class="bg_image"></div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- /WHY CHOOSE US-->

   <!-- PRINCIPLES WRAP-->
   <div class="glax_tm_section">
      <div class="glax_tm_principles_wrapper_all">
         <div class="container">
            <div class="glax_tm_twice_box_wrap">
               <div class="inner_box">
                  <div class="leftbox">
                     <div class="title_holder">
                        <h3>Our Guiding Principles</h3>
                     </div>
                     <div class="description">
                        <p>For over 35 years, the Glax family has been building relationships and projects that last. As a diversified construction management, design-build, and general contracting firm, Glax is recognized as one of Upstate New York's largest construction companies.</p>
                        <p>Serving an impressive list of long-term clients, we are an organization of seasoned professionals with a tremendous breadth of construction experience and expertise across multiple industries.</p>
                     </div>
                  </div>
                  <div class="rightbox">
                     <div class="glax_tm_principles_wrap">
                        <div class="list_wrap">
                           <ul class="masonry">
                              <li class="item">
                                 <div class="inner">
                                    <span class="leftshape"></span>
                                    <span class="topshape"></span>
                                    <div class="in">
                                       <div class="title">
                                          <h3>Humility</h3>
                                       </div>
                                       <div class="definition">
                                          <p>Be humble in all dealings with our partners, clients and team members. True wisdom and understanding belong to the humble.</p>
                                       </div>
                                       <div class="number">
                                          <span>01</span>
                                       </div>
                                    </div>
                                 </div>
                              </li>
                              <li class="item">
                                 <div class="inner">
                                    <span class="leftshape"></span>
                                    <span class="topshape"></span>
                                    <div class="in">
                                       <div class="title">
                                          <h3>Honesty</h3>
                                       </div>
                                       <div class="definition">
                                          <p>Be sure of our facts and be honest and straightforward in all of our dealings with each other and good our clients.</p>
                                       </div>
                                       <div class="number">
                                          <span>02</span>
                                       </div>
                                    </div>
                                 </div>
                              </li>
                              <li class="item">
                                 <div class="inner">
                                    <span class="leftshape"></span>
                                    <span class="topshape"></span>
                                    <div class="in">
                                       <div class="title">
                                          <h3>Integrity</h3>
                                       </div>
                                       <div class="definition">
                                          <p>Over the years, we have gained a reputation for integrity and trust from our customers who continue to use our services.</p>
                                       </div>
                                       <div class="number">
                                          <span>03</span>
                                       </div>
                                    </div>
                                 </div>
                              </li>
                              <li class="item">
                                 <div class="inner">
                                    <span class="leftshape"></span>
                                    <span class="topshape"></span>
                                    <div class="in">
                                       <div class="title">
                                          <h3>Quality Work</h3>
                                       </div>
                                       <div class="definition">
                                          <p>We ensure that all projects are done with professionalism using quality materials while offering clients the support and accessibility.</p>
                                       </div>
                                       <div class="number">
                                          <span>04</span>
                                       </div>
                                    </div>
                                 </div>
                              </li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- /PRINCIPLES WRAP-->

   <!-- RESPOSIBILITY-->
   <div class="glax_tm_section">
      <div class="glax_tm_main_responsibility_wrap">
         <div class="glax_tm_universal_parallax_wrap">
            <div class="main_bg">
               <div class="overlay_image responsibility jarallax" data-speed="0"></div>
               <div class="overlay_color responsibility"></div>
            </div>
            <div class="main_content responsibility">
               <div class="container">
                  <div class="content_inner_wrap">
                     <div class="glax_tm_experience_box">
                        <div class="top">
                           <p>World's Leading Industry Corporation</p>
                        </div>
                        <div class="bottom">
                           <div class="number">
                              <span>25</span>
                           </div>
                           <div class="definition">
                              <p>Years of experience</p>
                           </div>
                        </div>
                     </div>
                     <div class="experience_list">
                        <ul>
                           <li><span>Unrivalled workmanship</span></li>
                           <li><span>Professional and Qualified</span></li>
                           <li><span>Competitive prices</span></li>
                           <li><span>Performance Measures</span></li>
                           <li><span>Environmental Sensitivity</span></li>
                           <li><span>Core Placement</span></li>
                           <li><span>Communication skills</span></li>
                           <li><span>Responsive and Respectful</span></li>
                           <li><span>Personalised solutions</span></li>
                           <li><span>Functional Objectives</span></li>
                           <li><span>Integrated Design</span></li>
                           <li><span>Urban Context</span></li>
                           <li><span>Critical thinking</span></li>
                           <li><span>Problem solving</span></li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- /RESPOSIBILITY-->

   <!-- HOME PROJECT-->
   <div class="glax_tm_section">
      <div class="glax_tm_home_project_wrapper_all">
         <div class="container">
            <div class="glax_tm_twice_box_wrap fn_w_sminiboxes">
               <div class="inner_box">
                  <div class="leftbox project fn_w_sminibox">
                     <div class="constructify_fn_sticky_section">
                        <div class="title_holder">
                           <h3>Our Latest Projects</h3>
                        </div>
                        <div class="description">
                           <p>For over 35 years, the Glax family has been building relationships and projects that last. As a diversified construction management, design-build, and general contracting firm, Glax is recognized as one of Upstate New York's largest construction companies.</p>
                        </div>
                        <div class="glax_tm_button_more_wrap">
                           <a href="#">
                              View All Projects
                              <span class="arrow_wrap">
                                 <span class="first"><img class="svg" src="{{asset('assets/frontend_new/img/svg/arrow-right.svg')}}" alt="" /></span>
                                 <span class="second"><img class="svg" src="{{asset('assets/frontend_new/img/svg/arrow-right.svg')}}" alt="" /></span>
                              </span>
                           </a>
                        </div>
                     </div>
                  </div>
                  <div class="rightbox fn_w_sminibox">
                     <div class="constructify_fn_sticky_section">
                        <ul>
                           <li>
                              <div class="inner">
                                 <div class="image_wrap">
                                    <img src="{{asset('assets/frontend_new/img/portfolio/750x500.jpg')}}" alt="" />
                                    <div class="image" data-img-url="{{asset('assets/frontend_new/img/portfolio/1.jpg')}}" style="background-image: url('assets/frontend_new/img/portfolio/1.jpg')"></div>
                                    <div class="overlay_color"></div>
                                    <span class="plus"></span>
                                    <div class="title_holder">
                                       <h3>Matao Gas and Oil Organization</h3>
                                       <div class="glax_tm_view_more_wrap">
                                          <a href="#">
                                             <span class="text">View More</span>
                                             <span class="arrow"><img class="svg" src="{{asset('assets/frontend_new/img/svg/arrow-right.svg')}}" alt="" /></span>
                                          </a>
                                       </div>
                                    </div>
                                    <a class="link" href="#"></a>
                                 </div>
                              </div>
                           </li>
                           <li>
                              <div class="inner">
                                 <div class="image_wrap">
                                    <img src="{{asset('assets/frontend_new/img/portfolio/750x500.jpg')}}" alt="" />
                                    <div class="image" data-img-url="{{asset('assets/frontend_new/img/portfolio/2.jpg')}}" style="background-image: url('assets/frontend_new/img/portfolio/2.jpg')"></div>
                                    <div class="overlay_color"></div>
                                    <span class="plus"></span>
                                    <div class="title_holder">
                                       <h3>Odeon Industry Machinery</h3>
                                       <div class="glax_tm_view_more_wrap">
                                          <a href="#">
                                             <span class="text">View More</span>
                                             <span class="arrow"><img class="svg" src="{{asset('assets/frontend_new/img/svg/arrow-right.svg')}}" alt="" /></span>
                                          </a>
                                       </div>
                                    </div>
                                    <a class="link" href="#"></a>
                                 </div>
                              </div>
                           </li>
                           <li>
                              <div class="inner">
                                 <div class="image_wrap">
                                    <img src="{{asset('assets/frontend_new/img/portfolio/750x500.jpg')}}" alt="" />
                                    <div class="image" data-img-url="{{asset('assets/frontend_new/img/portfolio/3.jpg')}}" style="background-image: url('assets/frontend_new/img/portfolio/3.jpg')"></div>
                                    <div class="overlay_color"></div>
                                    <span class="plus"></span>
                                    <div class="title_holder">
                                       <h3>Chaban Car Industry</h3>
                                       <div class="glax_tm_view_more_wrap">
                                          <a href="#">
                                             <span class="text">View More</span>
                                             <span class="arrow"><img class="svg" src="{{asset('assets/frontend_new/img/svg/arrow-right.svg')}}" alt="" /></span>
                                          </a>
                                       </div>
                                    </div>
                                    <a class="link" href="#"></a>
                                 </div>
                              </div>
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- /HOME PROJECT --> 

     <div class="glax_tm_section">
        
          <div class="title_holder text-center mb-30">
                                    <h3>What Our Clients Say</h3>
                                 </div>

                   <section class="regular slider testimonial-slider"> 


                  <div class="testi-slide">
                     <img src="https://i.pravatar.cc/150?img=22" class="avtar-img">
                     <p>Glax is committed to the development of a culture based on Excellence Plus performance. The Group sets stretching targets in all its operations globally and by applying the four elements of Excellence Plus performance Client Focus, Innovation, Responsible Behaviour, </p>
                     <h5 class="name">Test Company</h5>

                  </div> 
                    <div class="testi-slide">
                     <img src="https://i.pravatar.cc/150?img=22" class="avtar-img">
                     <p>Glax is committed to the development of a culture based on Excellence Plus performance. The Group sets stretching targets in all its operations globally and by applying the four elements of Excellence Plus performance Client Focus, Innovation, Responsible Behaviour, </p>
                     <h5 class="name">Test Company</h5>

                  </div>
                    <div class="testi-slide">
                     <img src="https://i.pravatar.cc/150?img=22" class="avtar-img">
                     <p>Glax is committed to the development of a culture based on Excellence Plus performance. The Group sets stretching targets in all its operations globally and by applying the four elements of Excellence Plus performance Client Focus, Innovation, Responsible Behaviour, </p>
                     <h5 class="name">Test Company</h5>

                  </div>
          </section>
     </div>

   <!-- HOME BLOG -->
   <div class="glax_tm_section">
      <div class="glax_tm_home_blog_wrap">
         <div class="container">
            <div class="inner_wrap">
               <div class="blog_title_holder">
                  <h3>Thoughts &amp; Experiments</h3>
               </div>
               <div class="blog_list">
                  <ul>
                     <li>
                        <div class="inner">
                           <div class="image_holder">
                              <img src="{{asset('assets/frontend_new/img/blog/370x250.jpg')}}" alt="" />
                              <div class="main_image" data-img-url="{{asset('assets/frontend_new/img/blog/1.jpg')}}" style="background-image: url('assets/frontend_new/img/blog/1.jpg')"></div>
                              <div class="overlay"></div>
                              <div class="date_wrap">
                                 <h3><span>08</span></h3>
                                 <h5>Aug</h5>
                                 <h5>2018</h5>
                              </div>
                              <a class="full_link" href="#"></a>
                           </div>
                           <div class="descriptions_wrap">
                              <p class="category">
                                 <span class="author">By <a href="#">Marketify</a></span>
                                 <span class="city">In <a href="#">Australia</a></span>
                              </p>
                           </div>
                           <div class="title_holder">
                              <h3><a href="#">Laing O’Rourke: Moves, projects and bids</a></h3>
                           </div>
                           <div class="glax_tm_button_more_wrap">
                              <a href="#">
                                 Read More
                                 <span class="arrow_wrap">
                                    <span class="first"><img class="svg" src="{{asset('assets/frontend_new/img/svg/arrow-right.svg')}}" alt="" /></span>
                                    <span class="second"><img class="svg" src="{{asset('assets/frontend_new/img/svg/arrow-right.svg')}}" alt="" /></span>
                                 </span>
                              </a>
                           </div>
                        </div>
                     </li>
                     <li>
                        <div class="inner">
                           <div class="image_holder">
                              <img src="{{asset('assets/frontend_new/img/blog/370x250.jpg')}}" alt="" />
                              <div class="main_image" data-img-url="{{asset('assets/frontend_new/img/blog/2.jpg')}}" style="background-image: url('assets/frontend_new/img/blog/2.jpg')"></div>
                              <div class="overlay"></div>
                              <div class="date_wrap">
                                 <h3><span>07</span></h3>
                                 <h5>Aug</h5>
                                 <h5>2018</h5>
                              </div>
                              <a class="full_link" href="#"></a>
                           </div>
                           <div class="descriptions_wrap">
                              <p class="category">
                                 <span class="author">By <a href="#">Marketify</a></span>
                                 <span class="city">In <a href="#">Discussion</a></span>
                              </p>
                           </div>
                           <div class="title_holder">
                              <h3><a href="#">How to turn Victorian gasholders apartments</a></h3>
                           </div>
                           <div class="glax_tm_button_more_wrap">
                              <a href="#">
                                 Read More
                                 <span class="arrow_wrap">
                                    <span class="first"><img class="svg" src="{{asset('assets/frontend_new/img/svg/arrow-right.svg')}}" alt="" /></span>
                                    <span class="second"><img class="svg" src="{{asset('assets/frontend_new/img/svg/arrow-right.svg')}}" alt="" /></span>
                                 </span>
                              </a>
                           </div>
                        </div>
                     </li>
                     <li>
                        <div class="inner">
                           <div class="image_holder">
                              <img src="{{asset('assets/frontend_new/img/blog/370x250.jpg')}}" alt="" />
                              <div class="main_image" data-img-url="{{asset('assets/frontend_new/img/blog/3.jpg')}}" style="background-image: url('assets/frontend_new/img/blog/3.jpg')"></div>
                              <div class="overlay"></div>
                              <div class="date_wrap">
                                 <h3><span>06</span></h3>
                                 <h5>Aug</h5>
                                 <h5>2018</h5>
                              </div>
                              <a class="full_link" href="#"></a>
                           </div>
                           <div class="descriptions_wrap">
                              <p class="category">
                                 <span class="author">By <a href="#">Marketify</a></span>
                                 <span class="city">In <a href="#">Video</a></span>
                              </p>
                           </div>
                           <div class="title_holder">
                              <h3><a href="#">CITB appoints Peter Lauener as new chairman</a></h3>
                           </div>
                           <div class="glax_tm_button_more_wrap">
                              <a href="#">
                                 Read More
                                 <span class="arrow_wrap">
                                    <span class="first"><img class="svg" src="{{asset('assets/frontend_new/img/svg/arrow-right.svg')}}" alt="" /></span>
                                    <span class="second"><img class="svg" src="{{asset('assets/frontend_new/img/svg/arrow-right.svg')}}" alt="" /></span>
                                 </span>
                              </a>
                           </div>
                        </div>
                     </li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- /HOME BLOG -->

   @endsection
