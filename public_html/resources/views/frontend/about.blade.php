@extends('layouts_frontend_new.masters',['title'=>'contact'])
@section('content')
<!-- WRAPPER ALL -->
<!-- <style type="text/css">
   .glax_tm_header_wrap[data-position="absolute"] {
   position: relative !important;
} 
.glax_tm_header_wrap[data-style="transparent"] .header_inner_wrap .menu_wrap > ul > li > a {
   color: #000;
} 

.glax_tm_header_wrap[data-style="transparent"] .header_inner_wrap .menu_wrap > ul > li > a::before {
   border-top-color: #000;
} 
.glax_tm_header_wrap[data-style="transparent"] .purchase_button a {
   color: #000000;
} 
.glax_tm_header_wrap[data-style="transparent"] .purchase_button a:hover {
   border-color: #000000;
}
</style> -->
<!-- ABOUT -->
      <div class="glax_tm_section">
         <div class="glax_tm_main_title_holder">
            <div class="container">
               <div class="title_holder">
                  <h3>About Us</h3>
               </div>
               <div class="builify_tm_breadcrumbs">
                  <ul>
                     <li><a href="#">Medley Steel</a></li>
                     <li class="shape"><span></span></li>
                     <li><span>About Us</span></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <div class="glax_tm_section">
         <div class="glax_tm_about_wrap">
            <div class="container">
               <div class="subtitle">
                  <p>Glax is a privately owned, internationally focussed engineering enterprise with world-class capabilities spanning the entire client value chain. We operate an integrated business model comprising the full range of engineering, construction and asset management services delivering single-source solutions for some of the world's most prestigious public and private organisations.</p>
               </div>
               <div class="about_service_list">
                  <h3>Performance Excellence</h3>
                  <p>Glax is committed to the development of a culture based on Excellence Plus performance. The Group sets stretching targets in all its operations globally and by applying the four elements of Excellence Plus performance  Client Focus, Innovation, Responsible Behaviour, People Development it will achieve its vision to be the company of first choice for all stakeholders, able to challenge and change the poor practices synonymous with the construction industry, and compete alongside world-leading businesses.</p>
               </div>
            </div>
            <div class="glax_tm_about_counter_wrap">
               <div class="container">
                  <div class="inner_counter">
                     <div class="leftbox">
                        <div class="glax_tm_counter_wrap" data-col="4" data-delay="300">
                           <ul class="glax_tm_counter_list">
                              <li>
                                 <div class="inner">
                                    <h3><span><span class="glax_tm_counter" data-from="0" data-to="3572" data-speed="3000">0</span></span></h3>
                                    <span>Projects Completed</span>
                                 </div>
                              </li>
                              <li>
                                 <div class="inner">
                                    <h3><span><span class="glax_tm_counter" data-from="0" data-to="300" data-speed="3000">0</span>K</span></h3>
                                    <span>Company Clients</span>
                                 </div>
                              </li>
                              <li>
                                 <div class="inner">
                                    <h3><span><span class="glax_tm_counter" data-from="0" data-to="2348" data-speed="3000">0</span></span></h3>
                                    <span>Professional Workers</span>
                                 </div>
                              </li>
                              <li>
                                 <div class="inner">
                                    <h3><span><span class="glax_tm_counter" data-from="0" data-to="120" data-speed="3000">0</span>+</span></h3>
                                    <span>Company Awards</span>
                                 </div>
                              </li>
                           </ul>
                        </div>
                     </div>
                     <div class="rightbox">
                        <div class="inner_rightbox">
                           <span class="top_title">You have a reason</span>
                           <h3 class="title">Just Choose Us!</h3>
                           <p class="text">We aim to eliminate the task of dividing your project between different architecture and construction company. We are a company that offers design and build services for you from initial sketches to the final construction.</p>
                           <div class="counter_image_wrap gallery_zoom">
                              <ul>
                                 <li>
                                    <div class="inner">
                                       <a class="zoom" href="img/about/11.jpg"><div class="image"></div></a>
                                       <div class="overlay">
                                          <div class="in">
                                             <span></span>
                                          </div>
                                       </div>
                                       <a class="about_zoom zoom" href="img/about/11.jpg"></a>
                                    </div>
                                 </li>
                                 <li>
                                    <div class="inner">
                                       <a class="zoom" href="img/about/12.jpg"><div class="image"></div></a>
                                       <div class="overlay">
                                          <div class="in">
                                             <span></span>
                                          </div>
                                       </div>
                                       <a class="about_zoom zoom" href="img/about/12.jpg"></a>
                                    </div>
                                 </li>
                                 <li>
                                    <div class="inner">
                                       <a class="zoom" href="img/about/13.jpg"><div class="image"></div></a>
                                       <div class="overlay">
                                          <div class="in">
                                             <span></span>
                                          </div>
                                       </div>
                                       <a class="about_zoom zoom" href="img/about/13.jpg"></a>
                                    </div>
                                 </li>
                              </ul>
                           </div>
                        </div>
                        <div class="counter_png"></div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="container">
               <div class="about_service_list client">
                  <h3>Client Focus</h3>
                  <p>Our approach is led by an unwavering focus on fully understanding the needs of clients and delivering on our promises, regardless of the scale or complexity of the challenge in hand. Working collaboratively with our clients, we develop bespoke teams and solutions from concept through every stage of the project to completion, ensuring the best expertise and resources are deployed to achieve the required time, cost, quality, safety and sustainability outcomes.</p>
               </div>
            </div>
            <div class="container">
               <div class="about_service_list">
                  <h3>Responsible Behaviour</h3>
                  <p>We take our duty to act responsibly at all times very seriously through proactively managing the impacts of our activities on the environment, our people and the communities in which we operate. We work according to the guiding principles of our founding shareholders and by complying with the high standards set out in our Global Code of Conduct. In particular, we place the safety of those involved in or affected by our operations as our core organisational value, aiming to eliminate all accidents on our projects by 2020. We also take a leading role in driving change across our industry by seeking ways to reduce the effect of our operations on the environment through modernisation of traditional construction methods and practices.</p>
               </div>
            </div>
            <div class="glax_tm_section">
               <div class="container">
                  <div class="glax_tm_about_team_wrap">
                     <ul>
                        <li>
                           <div class="inner">
                              <div class="image_wrap">
                                 <img src="img/about/250x190.jpg" alt="" />
                                 <div class="image"></div>
                              </div>
                              <div class="main_definitions">
                                 <h3>Alan Michaelis</h3>
                                 <span>Chief Executive</span>
                              </div>
                           </div>
                        </li>
                        <li>
                           <div class="inner">
                              <div class="image_wrap">
                                 <img src="img/about/250x190.jpg" alt="" />
                                 <div class="image"></div>
                              </div>
                              <div class="main_definitions">
                                 <h3>James Hind</h3>
                                 <span>Finance Director</span>
                              </div>
                           </div>
                        </li>
                        <li>
                           <div class="inner">
                              <div class="image_wrap">
                                 <img src="img/about/250x190.jpg" alt="" />
                                 <div class="image"></div>
                              </div>
                              <div class="main_definitions">
                                 <h3>Venu Raju</h3>
                                 <span>Engineering Director</span>
                              </div>
                           </div>
                        </li>
                        <li>
                           <div class="inner">
                              <div class="image_wrap">
                                 <img src="img/about/250x190.jpg" alt="" />
                                 <div class="image"></div>
                              </div>
                              <div class="main_definitions">
                                 <h3>Paul Withers</h3>
                                 <span>Senior Independent Director</span>
                              </div>
                           </div>
                        </li>
                        <li>
                           <div class="inner">
                              <div class="image_wrap">
                                 <img src="img/about/250x190.jpg" alt="" />
                                 <div class="image"></div>
                              </div>
                              <div class="main_definitions">
                                 <h3>Chris Girling</h3>
                                 <span>Independent Director</span>
                              </div>
                           </div>
                        </li>
                        <li>
                           <div class="inner">
                              <div class="image_wrap">
                                 <img src="img/about/250x190.jpg" alt="" />
                                 <div class="image"></div>
                              </div>
                              <div class="main_definitions">
                                 <h3>Mark Hill</h3>
                                 <span>Non-executive Chairman</span>
                              </div>
                           </div>
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- /ABOUT -->

@endsection
