@extends('layouts_frontend_new.masters',['title'=>'founder'])
@section('content')
<!-- ABOUT -->
		<div class="glax_tm_section">
			<div class="glax_tm_main_title_holder">
				<div class="container">
					<div class="title_holder">
						<h3>Founder</h3>
					</div>
					<div class="builify_tm_breadcrumbs">
						<ul>
							<li><a href="{{url('index')}}">Glax Home</a></li>
							<li class="shape"><span></span></li>
							<li><span>Founder</span></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="glax_tm_section">
			<div class="glax_tm_about_wrap">
				<div class="container"> 

               <div class="founder-section">
                  <div  class="founder-left">
                        <div class="about_service_list mb-0">
                  <h3><!-- Performance Excellence -->
                  	{!! $ourfounder->title !!}
                  </h3>
                  <p><!-- Glax is committed to the development of a culture based on Excellence Plus performance. The Group sets stretching targets in all its operations globally and by applying the four elements of Excellence Plus performance  Client Focus, Innovation, Responsible Behaviour, People Development it will achieve its vision to be the company of first choice for all stakeholders, able to challenge and change the poor practices synonymous with the construction industry, and compete alongside world-leading businesses. -->
                  		{!! $ourfounder->content !!}
                  
                  </p> 
                    
               </div>
                  </div>
               <!--    <div  class="founder-right"> 
                      <div class="founder-shape"></div>
                     <img src="https://medleysteel.xceltec.in/public/assets/frontend_new/img/founder.jpg" class="img-fluid"> 

               
                  </div> -->
               </div>
				
				
				</div>
			
			</div>
		</div>
		<!-- /ABOUT -->

@endsection
