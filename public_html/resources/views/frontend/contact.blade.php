@extends('layouts_frontend_new.masters',['title'=>'contact'])
@section('content')
<!-- WRAPPER ALL -->
<!-- <style type="text/css">
   .glax_tm_header_wrap[data-position="absolute"] {
   position: relative !important;
} 
.glax_tm_header_wrap[data-style="transparent"] .header_inner_wrap .menu_wrap > ul > li > a {
   color: #000;
} 

.glax_tm_header_wrap[data-style="transparent"] .header_inner_wrap .menu_wrap > ul > li > a::before {
   border-top-color: #000;
} 
.glax_tm_header_wrap[data-style="transparent"] .purchase_button a {
   color: #000000;
} 
.glax_tm_header_wrap[data-style="transparent"] .purchase_button a:hover {
   border-color: #000000;
}
</style> -->
<!-- PROJECTS -->
<div class="glax_tm_section"> 
   <div class="glax_tm_main_title_holder">
      <div class="container">
         <div class="title_holder">
            <h3>Contact</h3>
         </div>
         <div class="builify_tm_breadcrumbs">
            <ul>
               <li><a href="{{url('index')}}">Medley Steel Home</a></li>
               <li class="shape"><span></span></li>
               <li><span>Contact</span></li>
            </ul>
         </div>
      </div>
   </div>
</div>
<div class="glax_tm_section">
   <div class="container">
      <div class="glax_tm_main_contact_wrap">
         <div class="office_list">
            <ul>
               <li>
                  <div class="inner">
                     <div class="image_wrap">
                        <img src="{{asset('assets/frontend_new/img/contact/370x220.jpg')}}" alt="" />
                        <div class="image"></div>
                     </div>
                     <div class="definitions_wrap">
                        <div class="office">
                           <h3>Washington Office</h3>
                           <div class="icon">
                              <img class="svg" src="{{asset('assets/frontend_new/img/svg/location.svg')}}" alt="" />
                           </div>
                        </div>
                        <div class="short_info_wrap">
                           <div class="row">
                              <p>100-120 Ft.Drive NE, Washington, DC 20011</p>
                           </div>
                           <div class="row">
                              <label>Phone:</label>
                              <span>+1 202-415-7234</span>
                           </div>
                           <div class="row">
                              <label>Email:</label>
                              <span><a href="#">w.constructify@gmail.com</a></span>
                           </div>
                        </div>
                     </div>
                  </div>
               </li>
               <li>
                  <div class="inner">
                     <div class="image_wrap">
                        <img src="{{asset('assets/frontend_new/img/contact/370x220.jpg')}}" alt="" />
                        <div class="image"></div>
                     </div>
                     <div class="definitions_wrap">
                        <div class="office">
                           <h3>New York Office</h3>
                           <div class="icon">
                              <img class="svg" src="{{asset('assets/frontend_new/img/svg/location.svg')}}" alt="" />
                           </div>
                        </div>
                        <div class="short_info_wrap">
                           <div class="row">
                              <p>110-115 Ft.Consort NE, New-York, DC 20031</p>
                           </div>
                           <div class="row">
                              <label>Phone:</label>
                              <span>+1 272-436-4524</span>
                           </div>
                           <div class="row">
                              <label>Email:</label>
                              <span><a href="#">n.constructify@gmail.com</a></span>
                           </div>
                        </div>
                     </div>
                  </div>
               </li>
               <li>
                  <div class="inner">
                     <div class="image_wrap">
                        <img src="{{asset('assets/frontend_new/img/contact/370x220.jpg')}}" alt="" />
                        <div class="image"></div>
                     </div>
                     <div class="definitions_wrap">
                        <div class="office">
                           <h3>Boston Office</h3>
                           <div class="icon">
                              <img class="svg" src="{{asset('assets/frontend_new/img/svg/location.svg')}}" alt="" />
                           </div>
                        </div>
                        <div class="short_info_wrap">
                           <div class="row">
                              <p>100-120 Ft.Albemarle NE, Boston, DC 20017</p>
                           </div>
                           <div class="row">
                              <label>Phone:</label>
                              <span>+1 252-925-7564</span>
                           </div>
                           <div class="row">
                              <label>Email:</label>
                              <span><a href="#">b.constructify@gmail.com</a></span>
                           </div>
                        </div>
                     </div>
                  </div>
               </li>
            </ul>
         </div>
         <div class="contact_text">
            <p>Glax is a privately owned, internationally focussed engineering enterprise with world-class capabilities spanning the entire client value chain. We operate an integrated business model comprising the full range of engineering, construction and asset management services delivering single-source solutions for some of the world's most prestigious public and private organisations.</p>
         </div>
         <div class="glax_tm_contact_wrap">
            <div class="get_in_touch">
               <h3>Get in Touch With Us</h3>
            </div>
            <div class="inner_wrap">
               <form  action="{{ url('contact/store') }}" method="POST" class="contactForm" id="contactForm">
                    @csrf
                    @method('POST')
              
                  <div class="returnmessage" data-success="Your message has been received, We will contact you soon."></div>
                          
                  <div class="empty_notice"><span>Please Fill Required Fields</span></div>
                  <div class="row">
                     <label>Full Name<span></span></label>
                     <input class="name" type="name" name="name" id="name" type="text" />
                     <span class="kt-form__help error name">

                  </div>
                  <div class="row">
                     <label>Your E-mail<span></span></label>
                     <input class="email_id" type="email_id" name="email_id" id="email_id" type="text" />
                     <span class="kt-form__help error email_id">
                  </div>
                  <div class="row">
                     <label>Your Subject<span></span></label>
                     <input class="queries" type="queries" name="queries" id="queries" type="text" />
                     <span class="kt-form__help error queries">
                  
                  </div>
                  <div class="row">
                     <label>Contect Number<span></span></label>
                     <input class="contact_number" type="contact_number" name="contact_number" id="contact_number" type="numeric" />
                        <span class="kt-form__help error contact_number">
                  

                  </div>
                  <div class="row">
                     <label>Your Message<span></span></label>
                     <textarea class="message" type="message" name="message" id="message"></textarea>
                           <span class="kt-form__help error message">
                  
                  </div>
                  <div class="row">
                   <button type="submit" name="submit" class="button btn btn-primary">Send message</button>
                 </div>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- /PROJECTS -->
@endsection
<script type="text/javascript" src="{{asset('js/contact-form-script.js')}}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

