@extends('layouts_frontend_new.masters',['title'=>'career'])
@section('content')

<!-- WRAPPER ALL -->
<div class="glax_tm_section">
   <div class="glax_tm_main_title_holder">
      <div class="container">
         <div class="title_holder">
            <h3>Career</h3>
         </div>
         <div class="builify_tm_breadcrumbs">
            <ul>
               <li><a href="{{url('index')}}">Glax Home</a></li>
               <li class="shape"><span></span></li>
               <li><span>Career</span></li>
            </ul>
         </div>
      </div>
   </div>
</div>
<div class="glax_tm_section">
   <div class="glax_tm_about_wrap">
      <div class="container">
         <div class="subtitle">
            <p> {!! $careerdata->content !!}</p>
         </div>
      </div>
   </div>
</div>
@endsection