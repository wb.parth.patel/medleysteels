   @extends('layouts_frontend_new.masters',['title'=>'product'])
@section('content')
  
      <!-- PROJECTS -->
         <div class="glax_tm_section">
            <div class="glax_tm_main_title_holder">
               <div class="container">
                  <div class="title_holder">
                     <h3>Projects</h3>
                  </div>
                  <div class="builify_tm_breadcrumbs">
                     <ul>
                        <li><a href="index.html">Glax Home</a></li>
                        <li class="shape"><span></span></li>
                        <li><span>Projects</span></li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
         <div class="glax_tm_section">
            <div class="container">
               <div class="glax_tm_projects_wrap">
                  <div class="filter_wrap">
                     <div class="all_projects">
                        <span><a href="#">All Projects</a></span>
                     </div>
                     <div class="filter_list">
                        <ul class="glax_tm_portfolio_filter">
                           <li><a href="#" class="current" data-filter="*">All</a></li>
                           <li><a href="#" data-filter=".architecture">Architecture</a></li>
                           <li><a href="#" data-filter=".interior">Interior</a></li>
                           <li><a href="#" data-filter=".renovation">Renovation</a></li>
                        </ul>
                     </div>
                  </div>
                  <ul class="glax_tm_portfolio_list gallery_zoom">
                     <li class="architecture">
                        <div class="inner">
                           <div class="image_wrap">
                              <img src="{{asset('assets/img/portfolio/750x500.jpg')}}" alt="" />
                              <div class="image" data-img-url="{{asset('assets/img/portfolio/1.jpg')}}"></div>
                              <div class="overlay_color"></div>
                              <span class="plus"></span>
                              <div class="title_holder">
                                 <h3>Matao Gas and Oil Organization</h3>
                                 <div class="glax_tm_view_more_wrap">
                                    <a href="#">
                                       <span class="text">View More</span>
                                       <span class="arrow"><img class="svg" src="img/svg/arrow-right.svg" alt="" /></span>
                                    </a>
                                 </div>
                              </div>
                              <a class="link" href="#"></a>
                           </div>
                        </div>
                     </li>
                     <li class="renovation">
                        <div class="inner">
                           <div class="image_wrap">
                              <img src="{{asset('assets/img/portfolio/750x500.jpg')}}" alt="" />
                              <div class="image" data-img-url="{{asset('assets/img/portfolio/2.jpg')}}"></div>
                              <div class="overlay_color"></div>
                              <span class="plus"></span>
                              <div class="title_holder">
                                 <h3>Odeon Industrial Machinery</h3>
                                 <div class="glax_tm_view_more_wrap">
                                    <a href="#">
                                       <span class="text">View More</span>
                                       <span class="arrow"><img class="svg" src="img/svg/arrow-right.svg" alt="" /></span>
                                    </a>
                                 </div>
                              </div>
                              <a class="link" href="#"></a>
                           </div>
                        </div>
                     </li>
                     <li class="interior">
                        <div class="inner">
                           <div class="image_wrap">
                              <img src="{{asset('assets/img/portfolio/750x500.jpg')}}" alt="" />
                              <div class="image" data-img-url="{{asset('assets/img/portfolio/3.jpg')}}"></div>
                              <div class="overlay_color"></div>
                              <span class="plus"></span>
                              <div class="title_holder">
                                 <h3>Chaban Car Industry</h3>
                                 <div class="glax_tm_view_more_wrap">
                                    <a href="#">
                                       <span class="text">View More</span>
                                       <span class="arrow"><img class="svg" src="img/svg/arrow-right.svg" alt="" /></span>
                                    </a>
                                 </div>
                              </div>
                              <a class="link" href="#"></a>
                           </div>
                        </div>
                     </li>
                     <li class="architecture">
                        <div class="inner">
                           <div class="image_wrap">
                              <img src="{{asset('assets/img/portfolio/750x500.jpg')}}" alt="" />
                              <div class="image" data-img-url="{{asset('assets/img/portfolio/23.jpg')}}"></div>
                              <div class="overlay_color"></div>
                              <span class="plus"></span>
                              <div class="title_holder">
                                 <h3>Arturo-Merino-Benitez</h3>
                                 <div class="glax_tm_view_more_wrap">
                                    <a href="#">
                                       <span class="text">View More</span>
                                       <span class="arrow"><img class="svg" src="img/svg/arrow-right.svg" alt="" /></span>
                                    </a>
                                 </div>
                              </div>
                              <a class="link" href="#"></a>
                           </div>
                        </div>
                     </li>
                     <li class="renovation">
                        <div class="inner">
                           <div class="image_wrap">
                              <img src="{{asset('assets/img/portfolio/750x500.jpg')}}" alt="" />
                              <div class="image" data-img-url="{{asset('assets/img/portfolio/24.jpg')}}"></div>
                              <div class="overlay_color"></div>
                              <span class="plus"></span>
                              <div class="title_holder">
                                 <h3>Femern Tunnel,Germany-Denmark</h3>
                                 <div class="glax_tm_view_more_wrap">
                                    <a href="#">
                                       <span class="text">View More</span>
                                       <span class="arrow"><img class="svg" src="img/svg/arrow-right.svg" alt="" /></span>
                                    </a>
                                 </div>
                              </div>
                              <a class="link" href="#"></a>
                           </div>
                        </div>
                     </li>
                     <li class="interior">
                        <div class="inner">
                           <div class="image_wrap">
                              <img src="{{asset('assets/img/portfolio/750x500.jpg')}}" alt="" />
                              <div class="image" data-img-url="{{asset('assets/img/portfolio/22.jpg')}}"></div>
                              <div class="overlay_color"></div>
                              <span class="plus"></span>
                              <div class="title_holder">
                                 <h3>Melia Hotel La Defense</h3>
                                 <div class="glax_tm_view_more_wrap">
                                    <a href="#">
                                       <span class="text">View More</span>
                                       <span class="arrow"><img class="svg" src="img/svg/arrow-right.svg" alt="" /></span>
                                    </a>
                                 </div>
                              </div>
                              <a class="link" href="#"></a>
                           </div>
                        </div>
                     </li>
                  </ul>
               </div>
            </div>
         </div>
         <div class="glax_tm_section">
            <div class="container">
               <div class="glax_tm_pagination">
                  <ul>
                     <li><span>Prev</span></li>
                     <li class="active"><a href="#">Next</a></li>
                  </ul>
               </div>
            </div>
         </div>
         <!-- /PROJECTS -->
         
         @endsection