@extends('layouts_frontend_new.masters',['title'=>'testimonial'])
@section('content')
<!-- ABOUT -->
		<div class="glax_tm_section">
			<div class="glax_tm_main_title_holder">
				<div class="container">
					<div class="title_holder">
						<h3>Testimonial</h3>
					</div>
					<div class="builify_tm_breadcrumbs">
						<ul>
							<li><a href="{{url('index')}}">Glax Home</a></li>
							<li class="shape"><span></span></li>
							<li><span>Testimonial</span></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		
		     <div class="glax_tm_section">
        
          <div class="title_holder text-center mb-30">
                                    <h3>What Our Clients Say</h3>
                                 </div>
                   <section class="regular slider testimonial-slider"> 
                       @foreach($Testimonial as $Testimonial)
                       
                    <div class="testi-slide">
                     <img src="{{$Testimonial->image}}" class="avtar-img">
                        <h5 class="name">{{ $Testimonial->author_name }}</h5>
                        <p>{{ $Testimonial->title }} </p>
                         <p> {!! $Testimonial->description !!} </p>

                    </div> 
                    @endforeach
                  <!--   <div class="testi-slide">
                     <img src="https://i.pravatar.cc/150?img=22" class="avtar-img">
                     <p>Glax is committed to the development of a culture based on Excellence Plus performance. The Group sets stretching targets in all its operations globally and by applying the four elements of Excellence Plus performance Client Focus, Innovation, Responsible Behaviour, </p>
                     <h5 class="name">Test Company</h5>
 
                  </div>
                    <div class="testi-slide">
                     <img src="https://i.pravatar.cc/150?img=22" class="avtar-img">
                     <p>Glax is committed to the development of a culture based on Excellence Plus performance. The Group sets stretching targets in all its operations globally and by applying the four elements of Excellence Plus performance Client Focus, Innovation, Responsible Behaviour, </p>
                     <h5 class="name">Test Company</h5>


                  </div> -->
          </section>
     </div>

   <!-- HOME BLOG -->
   <div class="glax_tm_section">
      <div class="glax_tm_home_blog_wrap">
         <div class="container">
            <div class="inner_wrap">
               <div class="blog_title_holder">
                  <h3>Thoughts &amp; Experiments</h3>
               </div>
               <div class="blog_list">
                  <ul>
                     <li>
                        <div class="inner">
                           <div class="image_holder">
                              <img src="{{asset('assets/frontend_new/img/blog/370x250.jpg')}}" alt="" />
                              <div class="main_image" data-img-url="{{asset('assets/frontend_new/img/blog/1.jpg')}}" style="background-image: url('assets/frontend_new/img/blog/1.jpg')"></div>
                              <div class="overlay"></div>
                              <div class="date_wrap">
                                 <h3><span>08</span></h3>
                                 <h5>Aug</h5>
                                 <h5>2018</h5>
                              </div>
                              <a class="full_link" href="#"></a>
                           </div>
                           <div class="descriptions_wrap">
                              <p class="category">
                                 <span class="author">By <a href="#">Marketify</a></span>
                                 <span class="city">In <a href="#">Australia</a></span>
                              </p>
                           </div>
                           <div class="title_holder">
                              <h3><a href="#">Laing O’Rourke: Moves, projects and bids</a></h3>
                           </div>
                           <div class="glax_tm_button_more_wrap">
                              <a href="#">
                                 Read More
                                 <span class="arrow_wrap">
                                    <span class="first"><img class="svg" src="{{asset('assets/frontend_new/img/svg/arrow-right.svg')}}" alt="" /></span>
                                    <span class="second"><img class="svg" src="{{asset('assets/frontend_new/img/svg/arrow-right.svg')}}" alt="" /></span>
                                 </span>
                              </a>
                           </div>
                        </div>
                     </li>
                     <li>
                        <div class="inner">
                           <div class="image_holder">
                              <img src="{{asset('assets/frontend_new/img/blog/370x250.jpg')}}" alt="" />
                              <div class="main_image" data-img-url="{{asset('assets/frontend_new/img/blog/2.jpg')}}" style="background-image: url('assets/frontend_new/img/blog/2.jpg')"></div>
                              <div class="overlay"></div>
                              <div class="date_wrap">
                                 <h3><span>07</span></h3>
                                 <h5>Aug</h5>
                                 <h5>2018</h5>
                              </div>
                              <a class="full_link" href="#"></a>
                           </div>
                           <div class="descriptions_wrap">
                              <p class="category">
                                 <span class="author">By <a href="#">Marketify</a></span>
                                 <span class="city">In <a href="#">Discussion</a></span>
                              </p>
                           </div>
                           <div class="title_holder">
                              <h3><a href="#">How to turn Victorian gasholders apartments</a></h3>
                           </div>
                           <div class="glax_tm_button_more_wrap">
                              <a href="#">
                                 Read More
                                 <span class="arrow_wrap">
                                    <span class="first"><img class="svg" src="{{asset('assets/frontend_new/img/svg/arrow-right.svg')}}" alt="" /></span>
                                    <span class="second"><img class="svg" src="{{asset('assets/frontend_new/img/svg/arrow-right.svg')}}" alt="" /></span>
                                 </span>
                              </a>
                           </div>
                        </div>
                     </li>
                     <li>
                        <div class="inner">
                           <div class="image_holder">
                              <img src="{{asset('assets/frontend_new/img/blog/370x250.jpg')}}" alt="" />
                              <div class="main_image" data-img-url="{{asset('assets/frontend_new/img/blog/3.jpg')}}" style="background-image: url('assets/frontend_new/img/blog/3.jpg')"></div>
                              <div class="overlay"></div>
                              <div class="date_wrap">
                                 <h3><span>06</span></h3>
                                 <h5>Aug</h5>
                                 <h5>2018</h5>
                              </div>
                              <a class="full_link" href="#"></a>
                           </div>
                           <div class="descriptions_wrap">
                              <p class="category">
                                 <span class="author">By <a href="#">Marketify</a></span>
                                 <span class="city">In <a href="#">Video</a></span>
                              </p>
                           </div>
                           <div class="title_holder">
                              <h3><a href="#">CITB appoints Peter Lauener as new chairman</a></h3>
                           </div> 
                           <div class="glax_tm_button_more_wrap">
                              <a href="#">
                                 Read More
                                 <span class="arrow_wrap">
                                    <span class="first"><img class="svg" src="{{asset('assets/frontend_new/img/svg/arrow-right.svg')}}" alt="" /></span>
                                    <span class="second"><img class="svg" src="{{asset('assets/frontend_new/img/svg/arrow-right.svg')}}" alt="" /></span>
                                 </span>
                              </a>
                           </div>
                        </div>
                     </li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
   </div>
		<!-- /ABOUT -->

@endsection
