 @extends('layouts_frontend_new.masters',['title'=>'blog'])
@section('content')

<!-- SERVICE SINGLE -->
      <div class="glax_tm_section">
         <div class="glax_tm_main_title_holder">
            <div class="container">
               <div class="title_holder">
                  <h3>News &amp; Articles</h3>
               </div>
               <div class="builify_tm_breadcrumbs">
                  <ul>
                     <li><a href="index.html">Glax Home</a></li>
                     <li class="shape"><span></span></li>
                     <li><span>News &amp; Articles</span></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <div class="glax_tm_section">
         <div class="container">
            <div class="glax_tm_blog_wrap">
               <div class="glax_tm_twicebox_wrap">
                  <div class="leftbox">
                     <div class="blog_list_wrap">
                        <ul>
                           <li>
                              <div class="inner">
                                 <div class="date_wrap">
                                    <h3><span>01</span></h3>
                                    <h5>Aug</h5>
                                    <h5>2018</h5>
                                 </div>
                                 <div class="image">
                                    <a href="#"><img src="{{asset('assets/img/blog/1.jpg')}}" alt="" /></a>
                                 </div>
                                 <div class="definitions_wrap">
                                    <div class="title_holder">
                                       <h3><a href="#">Laing O'Rourke: Moves, projects and bids</a></h3>
                                    </div>
                                    <div class="info_wrap">
                                       <div class="short_info">
                                          <span class="date">Posted on March 07, 2018</span>
                                          <span class="by">By Marketify</span>
                                          <span class="category">in <a href="#">Upgrade</a></span>
                                       </div>
                                    </div>
                                    <div class="text">
                                       <p>Seymour Whyte employs 475 people and generated revenue of A$433 million in the fiscal year ended on 30 June 2017. Founded in 1987, Seymour Whyte is a well-known Australian.</p>
                                    </div>
                                    <div class="continue">
                                       <a href="#">Read More</a>
                                    </div>
                                 </div>
                              </div>
                           </li>
                           <li>
                              <div class="inner">
                                 <div class="date_wrap">
                                    <h3><span>02</span></h3>
                                    <h5>Aug</h5>
                                    <h5>2018</h5>
                                 </div>
                                 <div class="image">
                                    <a href="#"><img src="{{asset('assets/img/blog/2.jpg')}}" alt="" /></a>
                                 </div>
                                 <div class="definitions_wrap">
                                    <div class="title_holder">
                                       <h3><a href="#">How to turn Victorian gasholders apartments</a></h3>
                                    </div>
                                    <div class="info_wrap">
                                       <div class="short_info">
                                          <span class="date">Posted on March 07, 2018</span>
                                          <span class="by">By Marketify</span>
                                          <span class="category">in <a href="#">Upgrade</a></span>
                                       </div>
                                    </div>
                                    <div class="text">
                                       <p>Seymour Whyte employs 475 people and generated revenue of A$433 million in the fiscal year ended on 30 June 2017. Founded in 1987, Seymour Whyte is a well-known Australian.</p>
                                    </div>
                                    <div class="continue">
                                       <a href="#">Read More</a>
                                    </div>
                                 </div>
                              </div>
                           </li>
                           <li>
                              <div class="inner">
                                 <div class="date_wrap">
                                    <h3><span>03</span></h3>
                                    <h5>Aug</h5>
                                    <h5>2018</h5>
                                 </div>
                                 <div class="image">
                                    <a href="#"><img src="{{asset('assets/img/blog/3.jpg')}}" alt="" /></a>
                                 </div>
                                 <div class="definitions_wrap">
                                    <div class="title_holder">
                                       <h3><a href="#">CITB appoints Peter Lauener as new chairman</a></h3>
                                    </div>
                                    <div class="info_wrap">
                                       <div class="short_info">
                                          <span class="date">Posted on March 07, 2018</span>
                                          <span class="by">By Marketify</span>
                                          <span class="category">in <a href="#">Upgrade</a></span>
                                       </div>
                                    </div>
                                    <div class="text">
                                       <p>Seymour Whyte employs 475 people and generated revenue of A$433 million in the fiscal year ended on 30 June 2017. Founded in 1987, Seymour Whyte is a well-known Australian.</p>
                                    </div>
                                    <div class="continue">
                                       <a href="#">Read More</a>
                                    </div>
                                 </div>
                              </div>
                           </li>
                           <li>
                              <div class="inner">
                                 <div class="date_wrap">
                                    <h3><span>04</span></h3>
                                    <h5>Aug</h5>
                                    <h5>2018</h5>
                                 </div>
                                 <div class="image">
                                    <a href="#"><img src="{{asset('assets/img/blog/7.jpg')}}" alt="" /></a>
                                 </div>
                                 <div class="definitions_wrap">
                                    <div class="title_holder">
                                       <h3><a href="#">New Tower in the Centre of Warsaw</a></h3>
                                    </div>
                                    <div class="info_wrap">
                                       <div class="short_info">
                                          <span class="date">Posted on March 07, 2018</span>
                                          <span class="by">By Marketify</span>
                                          <span class="category">in <a href="#">Upgrade</a></span>
                                       </div>
                                    </div>
                                    <div class="text">
                                       <p>Seymour Whyte employs 475 people and generated revenue of A$433 million in the fiscal year ended on 30 June 2017. Founded in 1987, Seymour Whyte is a well-known Australian.</p>
                                    </div>
                                    <div class="continue">
                                       <a href="#">Read More</a>
                                    </div>
                                 </div>
                              </div>
                           </li>
                           <li>
                              <div class="inner">
                                 <div class="date_wrap">
                                    <h3><span>05</span></h3>
                                    <h5>Aug</h5>
                                    <h5>2018</h5>
                                 </div>
                                 <div class="image">
                                    <a href="#"><img src="{{asset('assets/img/blog/8.jpg')}}" alt="" /></a>
                                 </div>
                                 <div class="definitions_wrap">
                                    <div class="title_holder">
                                       <h3><a href="#">New Motorway Connection</a></h3>
                                    </div>
                                    <div class="info_wrap">
                                       <div class="short_info">
                                          <span class="date">Posted on March 07, 2018</span>
                                          <span class="by">By Marketify</span>
                                          <span class="category">in <a href="#">Upgrade</a></span>
                                       </div>
                                    </div>
                                    <div class="text">
                                       <p>Seymour Whyte employs 475 people and generated revenue of A$433 million in the fiscal year ended on 30 June 2017. Founded in 1987, Seymour Whyte is a well-known Australian.</p>
                                    </div>
                                    <div class="continue">
                                       <a href="#">Read More</a>
                                    </div>
                                 </div>
                              </div>
                           </li>
                           <li>
                              <div class="inner">
                                 <div class="date_wrap">
                                    <h3><span>06</span></h3>
                                    <h5>Aug</h5>
                                    <h5>2018</h5>
                                 </div>
                                 <div class="image">
                                    <a href="#"><img src="{{asset('assets/img/blog/9.jpg')}}" alt="" /></a>
                                 </div>
                                 <div class="definitions_wrap">
                                    <div class="title_holder">
                                       <h3><a href="#">Metro Line in Copenhagen</a></h3>
                                    </div>
                                    <div class="info_wrap">
                                       <div class="short_info">
                                          <span class="date">Posted on March 07, 2018</span>
                                          <span class="by">By Marketify</span>
                                          <span class="category">in <a href="#">Upgrade</a></span>
                                       </div>
                                    </div>
                                    <div class="text">
                                       <p>Seymour Whyte employs 475 people and generated revenue of A$433 million in the fiscal year ended on 30 June 2017. Founded in 1987, Seymour Whyte is a well-known Australian.</p>
                                    </div>
                                    <div class="continue">
                                       <a href="#">Read More</a>
                                    </div>
                                 </div>
                              </div>
                           </li>
                        </ul>
                     </div>
                  </div>
                  <div class="rightbox">
                     <div class="glax_tm_request_estimate_wrap">
                        <div class="image_wrap">
                           <img src="{{asset('assets/img/services/300x460.jpg')}}" alt="" />
                           <div class="image"></div>
                        </div>
                        <div class="definition">
                           <div class="text">
                              <p>Lets get started! Contact us for a free quote on your next home improvement project.</p>
                           </div>
                           <div class="button">
                              <a href="#">Request an estimate</a>
                           </div>
                           <div class="first_shape">
                              <span class="first"></span>
                              <span class="second"></span>
                              <span class="third"></span>
                           </div>
                           <div class="second_shape">
                              <span class="first"></span>
                              <span class="second"></span>
                              <span class="third"></span>
                           </div>
                        </div>
                     </div>
                     <div class="glax_tm_brochures_wrap">
                        <div class="title_holder">
                           <span>Company Presentation</span>
                        </div>
                        <ul>
                           <li>
                              <div class="inner">
                                 <div class="icon">
                                    <img class="svg" src="{{asset('assets/img/svg/file-pdf.svg')}}" alt="" />
                                 </div>
                                 <span class="text">Download .PDF</span>
                                 <span class="arrow"></span>
                              </div>
                           </li>
                           <li>
                              <div class="inner">
                                 <div class="icon">
                                    <img class="svg" src="{{asset('assets/img/svg/file-zip.svg')}}" alt="" />
                                 </div>
                                 <span class="text">Download .ZIP</span>
                                 <span class="arrow"></span>
                              </div>
                           </li>
                           <li>
                              <div class="inner">
                                 <div class="icon">
                                    <img class="svg" src="{{asset('assets/img/svg/file-doc.svg')}}" alt="" />
                                 </div>
                                 <span class="text">Download .DOC</span>
                                 <span class="arrow"></span>
                              </div>
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- /SERVICE SINGLE -->
      
   @endsection
      
     