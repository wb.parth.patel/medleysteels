@extends('layouts_frontend_new.masters',['title'=>'servicedetail'])
@section('content')
 
   <!-- SERVICE SINGLE -->
      <div class="glax_tm_section">
         <div class="glax_tm_main_title_holder">
            <div class="container">
               <div class="title_holder">
                  <h3>{{ $serviceDetails->service_name }}</h3>
               </div>
               <div class="builify_tm_breadcrumbs">
                  <ul>
                     <li><a href="{{url('index')}}">Glax Home</a></li>
                     <li class="shape"><span></span></li>
                     <li><a href="{{url('service')}}">Service Posts</a></li>
                     <li class="shape"><span></span></li>
                     <li><span>{{ $serviceDetails->service_name }}</span></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <div class="glax_tm_section">
         <div class="container">
            <div class="glax_tm_service_single_wrap">
               <div class="glax_tm_twicebox_wrap">
                  <div class="leftbox">
                     <div class="main_image_wrap">
                        <div class="image_wrap">
                           <img src="img/slider/5.jpg" alt="" />
                        </div>
                        <div class="image_definition">
                           <p>{!! $serviceDetails->service_desctiprion !!}</p>
                           
                           <p>{{ $serviceDetails->service_detail }}</p>
                           
                        </div>
                     </div>
                   <!--   <div class="service_features">
                        <h3>Service Features</h3>
                        <ul>
                           <li>
                              <div class="inner">
                                 <div class="definition">
                                    <p>We Have ISO Certificate</p>
                                 </div>
                                 <div class="icon">
                                    <img class="svg" src="{{asset('assets/frontend_new/img/svg/check.svg')}}" alt="" />
                                 </div>
                              </div>
                           </li>
                           <li>
                              <div class="inner">
                                 <div class="definition">
                                    <p>We Provide High Services</p>
                                 </div>
                                 <div class="icon">
                                    <img class="svg" src="{{asset('assets/frontend_new/img/svg/check.svg')}}" alt="" />
                                 </div>
                              </div>
                           </li>
                           <li>
                              <div class="inner">
                                 <div class="definition">
                                    <p>Most Expirienced Company</p>
                                 </div>
                                 <div class="icon">
                                    <img class="svg" src="{{asset('assets/frontend_new/img/svg/check.svg')}}" alt="" />
                                 </div>
                              </div>
                           </li>
                           <li>
                              <div class="inner">
                                 <div class="definition">
                                    <p>Responsive and Respectful</p>
                                 </div>
                                 <div class="icon">
                                    <img class="svg" src="{{asset('assets/frontend_new/img/svg/check.svg')}}" alt="" />
                                 </div>
                              </div>
                           </li>
                           <li>
                              <div class="inner">
                                 <div class="definition">
                                    <p>Environmental Sensitivity</p>
                                 </div>
                                 <div class="icon">
                                    <img class="svg" src="{{asset('assets/frontend_new/img/svg/check.svg')}}" alt="" />
                                 </div>
                              </div>
                           </li>
                        </ul>
                     </div>
                     <div class="accordion_wrap_all">
                        <div class="title">
                           <h3>How We Do It</h3>
                        </div>
                        <div class="accordion_wrap">
                           <div class="glax_tm_accordion">
                              <div class="accordion_in">
                                 <div class="acc_head">We are guided by the same basic precepts<div class="wow_shape"><span class="wow_effect"></span></div></div>
                                 <div class="acc_content">
                                    Provide quality workmanship and exemplary client service. Employ people of the highest integrity and skill. Provide a safe work environment for our employees and subcontractors.
                                 </div>
                              </div>
                              <div class="accordion_in">
                                 <div class="acc_head">Glax's work ethic runs deep<div class="wow_shape"><span class="wow_effect"></span></div></div>
                                 <div class="acc_content">
                                    From the beginning, honesty and clear vision along with hard work and imagination have been integral parts of our steady and diversified growth.
                                 </div>
                              </div>
                              <div class="accordion_in">
                                 <div class="acc_head">We have created a culture<div class="wow_shape"><span class="wow_effect"></span></div></div>
                                 <div class="acc_content">
                                    We have created a culture that promotes Trust and we are extremely proud of that. This culture has produced a team that likes what they are doing and that is why we do what we do so well.
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="glax_tm_short_contact">
                        <div class="text">
                           <h3>Glax</h3>
                           <p>We build your dream house. Contact us for detailed information.</p>
                        </div>
                        <div class="glax_tm_button_wrap">
                           <a href="#">Our Resposibility</a>
                        </div>
                     </div>
                     <div class="glax_tm_line"></div>
                     <div class="glax_tm_other_services_wrap">
                        <h3>Other Services</h3>
                           <div class="services_list_wrap">
                           <ul>
                              <li>
                                 <div class="inner">
                                    <div class="title_holder">
                                       <h3><a href="#">New Construction</a></h3>
                                    </div>
                                    <div class="description">
                                       <p>It's the details that count. Because when they are given a backseat, they inevitably move up front and can overtake the</p>
                                    </div>
                                    <div class="read_more_wrap">
                                       <div class="read_more_in">
                                          <a href="#">Read More</a>
                                       </div>
                                    </div>
                                 </div>
                              </li>
                              <li>
                                 <div class="inner">
                                    <div class="title_holder">
                                       <h3><a href="#">Adaptive Reuse</a></h3>
                                    </div>
                                    <div class="description">
                                       <p>Achieving this successfully calls for a vision of what can be and a distinct ability to identify opportunities for salvaging elements</p>
                                    </div>
                                    <div class="read_more_wrap">
                                       <div class="read_more_in">
                                          <a href="#">Read More</a>
                                       </div>
                                    </div>
                                 </div>
                              </li>
                           </ul>
                        </div>
                     </div -->
                  </div>
                <!--   <div class="rightbox">
                     <div class="glax_tm_categories">
                        <div class="title_holder_wrap">
                           <h3>Full list of services</h3>
                        </div>
                        <div class="list_wrap">
                           <ul>
                              <li><a href="#">Energy &amp; Commodities</a></li>
                              <li><a href="#">Medical Devices</a></li>
                              <li><a href="#">Housewares &amp; Home Decor</a></li>
                              <li><a href="#">The Shale Oil and Gas Revolution</a></li>
                              <li><a href="#">Textiles &amp; Apparel</a></li>
                              <li><a href="#">Construction &amp; Engineering</a></li>
                              <li><a href="#">Basic &amp; Industrial Chemicals</a></li>
                           </ul>
                        </div>
                     </div>
                     <div class="glax_tm_request_estimate_wrap">
                        <div class="image_wrap">
                           <img src="img/services/300x460.jpg" alt="" />
                           <div class="image"></div>
                        </div>
                        <div class="definition">
                           <div class="text">
                              <p>Lets get started! Contact us for a free quote on your next home improvement project.</p>
                           </div>
                           <div class="button">
                              <a href="#">Request an estimate</a>
                           </div>
                           <div class="first_shape">
                              <span class="first"></span>
                              <span class="second"></span>
                              <span class="third"></span>
                           </div>
                           <div class="second_shape">
                              <span class="first"></span>
                              <span class="second"></span>
                              <span class="third"></span>
                           </div>
                        </div>
                     </div>
                     <div class="glax_tm_brochures_wrap">
                        <div class="title_holder">
                           <span>Company Presentation</span>
                        </div>
                        <ul>
                           <li>
                              <div class="inner">
                                 <div class="icon">
                                    <img class="svg" src="img/svg/file-pdf.svg" alt="" />
                                 </div>
                                 <span class="text">Download .PDF</span>
                                 <span class="arrow"></span>
                              </div>
                           </li>
                           <li>
                              <div class="inner">
                                 <div class="icon">
                                    <img class="svg" src="img/svg/file-zip.svg" alt="" />
                                 </div>
                                 <span class="text">Download .ZIP</span>
                                 <span class="arrow"></span>
                              </div>
                           </li>
                           <li>
                              <div class="inner">
                                 <div class="icon">
                                    <img class="svg" src="img/svg/file-doc.svg" alt="" />
                                 </div>
                                 <span class="text">Download .DOC</span>
                                 <span class="arrow"></span>
                              </div>
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>-->
            </div>
         </div>
      </div>
     <!-- /SERVICE SINGLE -->
      @endsection