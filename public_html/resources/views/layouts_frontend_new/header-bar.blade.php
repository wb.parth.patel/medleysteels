  <!-- LANG BOX -->
  <div class="lang_box">
            <!-- <ul>
               <li><span>Eng</span></li>
               <li><a href="#">Spa</a></li>
               <li><a href="#">Rus</a></li>
            </ul> -->
         </div>
         <!-- /LANG BOX -->
         <!-- BORDERS -->
         <div class="glax_tm_border_wrap">
            <div class="border top"></div>
            <div class="border left"></div>
            <div class="border right"></div>
         </div>
         <!-- /BORDERS -->
         <!-- HOLDER -->
         <div class="glax_tm_holder_wrap">
            <div class="holder left"></div>
            <div class="holder right"></div>
         </div>
         <!-- /HOLDER -->
         <!-- TOP BAR -->
         <div class="glax_tm_topbar_wrap">
            <div class="container"> 
               <div class="inner_wrap">
                  <div class="left_part_wrap">
                     <div class="share_wrap">
                        <ul>
                           <li><a href="#"><i class="xcon-facebook"></i></a></li>
                           <li><a href="#"><i class="xcon-twitter"></i></a></li>
                           <li><a href="#"><i class="xcon-instagram"></i></a></li>
                           <li><a href="#"><i class="xcon-pinterest"></i></a></li>
                           <li><a href="#"><i class="xcon-behance"></i></a></li>
                        </ul>
                     </div>
                     <div class="language">
                        <!-- <a class="selected" href="#">Eng</a> -->
                     </div>
                  </div>
                  <div class="right_part_wrap">
                     <ul>
                        <li data-style="home">
                           <a href="#">
                              <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="460.298px" height="460.297px" viewBox="0 0 460.298 460.297" style="enable-background:new 0 0 460.298 460.297;" xml:space="preserve" class="svg replaced-svg">
                                 <g>
                                    <g>
                                       <path d="M230.149,120.939L65.986,256.274c0,0.191-0.048,0.472-0.144,0.855c-0.094,0.38-0.144,0.656-0.144,0.852v137.041    c0,4.948,1.809,9.236,5.426,12.847c3.616,3.613,7.898,5.431,12.847,5.431h109.63V303.664h73.097v109.64h109.629    c4.948,0,9.236-1.814,12.847-5.435c3.617-3.607,5.432-7.898,5.432-12.847V257.981c0-0.76-0.104-1.334-0.288-1.707L230.149,120.939    z"></path>
                                       <path d="M457.122,225.438L394.6,173.476V56.989c0-2.663-0.856-4.853-2.574-6.567c-1.704-1.712-3.894-2.568-6.563-2.568h-54.816    c-2.666,0-4.855,0.856-6.57,2.568c-1.711,1.714-2.566,3.905-2.566,6.567v55.673l-69.662-58.245    c-6.084-4.949-13.318-7.423-21.694-7.423c-8.375,0-15.608,2.474-21.698,7.423L3.172,225.438c-1.903,1.52-2.946,3.566-3.14,6.136    c-0.193,2.568,0.472,4.811,1.997,6.713l17.701,21.128c1.525,1.712,3.521,2.759,5.996,3.142c2.285,0.192,4.57-0.476,6.855-1.998    L230.149,95.817l197.57,164.741c1.526,1.328,3.521,1.991,5.996,1.991h0.858c2.471-0.376,4.463-1.43,5.996-3.138l17.703-21.125    c1.522-1.906,2.189-4.145,1.991-6.716C460.068,229.007,459.021,226.961,457.122,225.438z"></path>
                                    </g>
                                 </g>
                                 <g></g>
                                 <g></g>
                                 <g></g>
                                 <g></g>
                                 <g></g>
                                 <g></g>
                                 <g></g>
                                 <g></g>
                                 <g></g>
                                 <g></g>
                                 <g></g>
                                 <g></g>
                                 <g></g>
                                 <g></g>
                                 <g></g>
                              </svg>
                           </a>
                        </li>
                        <li data-style="message">
                           <a href="#">
                              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 550.795 550.795" style="enable-background:new 0 0 550.795 550.795;" xml:space="preserve" class="svg replaced-svg">
                                 <path d="M501.613,491.782c12.381,0,23.109-4.088,32.229-12.16L377.793,323.567c-3.744,2.681-7.373,5.288-10.801,7.767    c-11.678,8.604-21.156,15.318-28.434,20.129c-7.277,4.822-16.959,9.737-29.045,14.755c-12.094,5.024-23.361,7.528-33.813,7.528    h-0.306h-0.306c-10.453,0-21.72-2.503-33.813-7.528c-12.093-5.018-21.775-9.933-29.045-14.755    c-7.277-4.811-16.75-11.524-28.434-20.129c-3.256-2.387-6.867-5.006-10.771-7.809L16.946,479.622    c9.119,8.072,19.854,12.16,32.234,12.16H501.613z"></path>
                                 <path d="M31.047,225.299C19.37,217.514,9.015,208.598,0,198.555V435.98l137.541-137.541    C110.025,279.229,74.572,254.877,31.047,225.299z"></path>
                                 <path d="M520.059,225.299c-41.865,28.336-77.447,52.73-106.75,73.195l137.486,137.492V198.555    C541.98,208.396,531.736,217.306,520.059,225.299z"></path>
                                 <path d="M501.613,59.013H49.181c-15.784,0-27.919,5.33-36.42,15.979C4.253,85.646,0.006,98.97,0.006,114.949    c0,12.907,5.636,26.892,16.903,41.959c11.267,15.061,23.256,26.891,35.961,35.496c6.965,4.921,27.969,19.523,63.012,43.801    c18.917,13.109,35.368,24.535,49.505,34.395c12.05,8.396,22.442,15.667,31.022,21.701c0.985,0.691,2.534,1.799,4.59,3.269    c2.215,1.591,5.018,3.61,8.476,6.107c6.659,4.816,12.191,8.709,16.597,11.683c4.4,2.975,9.731,6.298,15.985,9.988    c6.249,3.685,12.143,6.456,17.675,8.299c5.533,1.842,10.655,2.766,15.367,2.766h0.306h0.306c4.711,0,9.834-0.924,15.368-2.766    c5.531-1.843,11.42-4.608,17.674-8.299c6.248-3.69,11.572-7.02,15.986-9.988c4.406-2.974,9.938-6.866,16.598-11.683    c3.451-2.497,6.254-4.517,8.469-6.102c2.057-1.476,3.605-2.577,4.596-3.274c6.684-4.651,17.1-11.892,31.104-21.616    c25.482-17.705,63.01-43.764,112.742-78.281c14.957-10.447,27.453-23.054,37.496-37.803c10.025-14.749,15.051-30.22,15.051-46.408    c0-13.525-4.873-25.098-14.598-34.737C526.461,63.829,514.932,59.013,501.613,59.013z"></path>
                              </svg>
                           </a>
                        </li>
                        <li data-style="phone">
                           <a href="#">
                              <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="34.546px" height="34.546px" viewBox="0 0 34.546 34.546" style="enable-background:new 0 0 34.546 34.546;" xml:space="preserve" class="svg replaced-svg">
                                 <g>
                                    <path d="M29.33,19.339c-2.326-0.545-5.3-1.969-6.377-3.737c-0.603-0.984-0.666-2.094-0.179-3.042   c0.027-0.051,0.074-0.128,0.109-0.188c-0.028-0.209,0.021-0.486,0.172-0.847c0.011-0.024,0.012-0.027,0.02-0.044H11.122   c0.006,0.014,0.006,0.014,0.014,0.032c0,0,0.001,0.001,0.001,0.002c0.268,0.418,0.502,0.792,0.627,1.033   c0.495,0.961,0.431,2.07-0.17,3.055c-1.08,1.77-4.053,3.193-6.381,3.738L4.57,30.124c-0.07,1.183,0.705,2.149,1.723,2.149h21.958   c1.018,0,1.792-0.967,1.721-2.149L29.33,19.339z M13.683,26.765h-2.062c-0.38,0-0.688-0.309-0.688-0.688s0.308-0.688,0.688-0.688   h2.062c0.379,0,0.688,0.309,0.688,0.688S14.062,26.765,13.683,26.765z M13.683,24.143h-2.062c-0.38,0-0.688-0.308-0.688-0.687   c0-0.382,0.308-0.688,0.688-0.688h2.062c0.379,0,0.688,0.306,0.688,0.688C14.371,23.835,14.062,24.143,13.683,24.143z    M13.683,21.521h-2.062c-0.38,0-0.688-0.308-0.688-0.688c0-0.379,0.308-0.688,0.688-0.688h2.062c0.379,0,0.688,0.309,0.688,0.688   S14.062,21.521,13.683,21.521z M18.304,26.765h-2.061c-0.38,0-0.688-0.309-0.688-0.688s0.309-0.688,0.688-0.688h2.062   c0.379,0,0.688,0.309,0.688,0.688C18.991,26.456,18.683,26.765,18.304,26.765z M18.304,24.143h-2.061   c-0.38,0-0.688-0.308-0.688-0.687c0-0.382,0.309-0.688,0.688-0.688h2.062c0.379,0,0.688,0.306,0.688,0.688   C18.991,23.835,18.683,24.143,18.304,24.143z M18.304,21.521h-2.061c-0.38,0-0.688-0.308-0.688-0.688   c0-0.379,0.309-0.688,0.688-0.688h2.062c0.379,0,0.688,0.309,0.688,0.688C18.991,21.212,18.683,21.521,18.304,21.521z    M22.924,26.765h-2.062c-0.379,0-0.687-0.309-0.687-0.688s0.308-0.688,0.687-0.688h2.062c0.381,0,0.688,0.309,0.688,0.688   C23.612,26.456,23.306,26.765,22.924,26.765z M22.924,24.143h-2.062c-0.379,0-0.687-0.308-0.687-0.687   c0-0.382,0.308-0.688,0.687-0.688h2.062c0.381,0,0.688,0.306,0.688,0.688C23.612,23.835,23.306,24.143,22.924,24.143z    M22.924,21.521h-2.062c-0.379,0-0.687-0.308-0.687-0.688c0-0.379,0.308-0.688,0.687-0.688h2.062c0.381,0,0.688,0.309,0.688,0.688   C23.612,21.212,23.306,21.521,22.924,21.521z M34.372,13.114c-0.043,0.216-0.045,0.441-0.13,0.646   c-0.497,1.202-1.429,2.197-2.115,3.305c-0.885,1.414-8.406-1.634-7.437-3.521c0.365-0.698,1.789-2.626,1.896-3.401   c0.098-0.692-0.818-1.233-1.664-1.302c-2.232-0.181-5.083-0.017-7.452-0.002v0.002c-0.063,0-0.133-0.001-0.198-0.001   c-0.064,0-0.134,0.001-0.197,0.001V8.839c-2.369-0.015-5.22-0.178-7.452,0.002c-0.846,0.069-1.762,0.61-1.665,1.302   c0.108,0.775,1.531,2.703,1.896,3.401c0.971,1.887-6.55,4.935-7.435,3.521c-0.688-1.108-1.618-2.103-2.116-3.305   c-0.084-0.205-0.086-0.43-0.129-0.646c-1.104-4.93,3.148-9.96,9.551-10.476c2.445-0.198,4.896-0.31,7.35-0.354V2.272   c0.066,0.001,0.131,0.005,0.197,0.006c0.065-0.001,0.131-0.005,0.198-0.006v0.012c2.452,0.044,4.903,0.156,7.35,0.354   C31.222,3.153,35.474,8.185,34.372,13.114z"></path>
                                 </g>
                                 <g></g>
                                 <g></g>
                                 <g></g>
                                 <g></g>
                                 <g></g>
                                 <g></g>
                                 <g></g>
                                 <g></g>
                                 <g></g>
                                 <g></g>
                                 <g></g>
                                 <g></g>
                                 <g></g>
                                 <g></g>
                                 <g></g>
                              </svg>
                           </a>
                        </li>
                        <li data-style="clock">
                           <a href="#">
                              <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 299.995 299.995" style="enable-background:new 0 0 299.995 299.995;" xml:space="preserve" class="svg replaced-svg">
                                 <g>
                                    <g>
                                       <path d="M149.995,0C67.156,0,0,67.158,0,149.995s67.156,150,149.995,150s150-67.163,150-150S232.834,0,149.995,0z     M214.842,178.524H151.25c-0.215,0-0.415-0.052-0.628-0.06c-0.213,0.01-0.412,0.06-0.628,0.06    c-5.729,0-10.374-4.645-10.374-10.374V62.249c0-5.729,4.645-10.374,10.374-10.374s10.374,4.645,10.374,10.374v95.527h54.47    c5.729,0,10.374,4.645,10.374,10.374C225.212,173.879,220.571,178.524,214.842,178.524z"></path>
                                    </g>
                                 </g>
                                 <g></g>
                                 <g></g>
                                 <g></g>
                                 <g></g>
                                 <g></g>
                                 <g></g>
                                 <g></g>
                                 <g></g>
                                 <g></g>
                                 <g></g>
                                 <g></g>
                                 <g></g>
                                 <g></g>
                                 <g></g>
                                 <g></g>
                              </svg>
                           </a>
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
         <!-- /TOP BAR -->
         <div class="wrapper_all_inner_wrap">
            <!-- HEADER -->
            <div class="glax_tm_header_wrap" data-style="transparent" data-position="absolute">
               <div class="container">
                  <div class="header_inner_wrap">
                     <div class="menu_wrap">
                        <ul>
                           <li><a href="{{url('index')}}">Home</a></li>
                           <li class="shape">
                              <a href="#">Projects</a>
                              <div class="submenu_wrap">
                                 <ul>
                                    <li><a href="#">Project</a></li>
                                    <li><a href="#">Project Single</a></li>
                                 </ul>
                              </div>
                           </li>
                           <li class="shape">
                              <a href="#">Services</a>
                              <div class="submenu_wrap">
                                 <ul>
                                    <li><a href="{{url('service')}}">Service</a></li>
                                    {{-- <li><a href="#">Service Single</a></li>
                                  --}}</ul>
                              </div>
                           </li>
                           <li class="shape">
                              <a href="#">News</a>
                              <div class="submenu_wrap">
                                 <ul>
                                    <li><a href="#">News</a></li>
                                    <li><a href="#">News Single</a></li>
                                 </ul>
                              </div>
                           </li>
                           <li><a href="{{url('about-us')}}">About</a></li>
                           <li><a href="{{url('contact')}}">Contact</a></li>
                        </ul>
                     </div>
                     <div class="purchase_button">
                        <a href="#">Purchase now</a>
                     </div>
                     <div class="logo_wrap">
                        <img src="{{asset('assets/frontend_new/img/desktop-logo.png')}}" alt="" />
                        <span class="left"></span>
                        <span class="right"></span>
                        <span class="extra_first"></span>
                        <span class="extra_second"></span>
                        <a class="full_link" href="#"></a>
                     </div>
                  </div>
               </div>
            </div>
            <!-- /HEADER -->
            <!-- MOBILE BAR -->
            <div class="glax_tm_mobile_bar_wrap">
               <div class="mobile_topbar_wrap">
                  <div class="container">
                     <div class="inner_wrap">
                        <div class="short_info_wrap">
                           <ul>
                              <li data-type="home">  <a href="{{url('index')}}"><img class="svg" src="{{asset('assets/frontend_new/img/svg/home.svg')}}" alt="" /></a></li>
                              <li data-type="message"><a href="mailto:shikhar.chokshi@xceltec.in"><img class="svg" src="{{asset('assets/frontend_new/img/svg/message2.svg')}}" alt="" /></a></li>
                              <li data-type="phone"><a href="tel:18009876543"><img class="svg" src="{{asset('assets/frontend_new/img/svg/old-phone.svg')}}" alt="" /></a></li>
                              <li data-type="clock"><a href="#"><img class="svg" src="{{asset('assets/frontend_new/img/svg/clock.svg')}}" alt="" /></a></li>
                           </ul>
                        </div>
                        <div class="mobile_socials_wrap">
                           <ul>
                              <li><a href="#"><i class="xcon-facebook"></i></a></li>
                              <li><a href="#"><i class="xcon-twitter"></i></a></li>
                              <li><a href="#"><i class="xcon-instagram"></i></a></li>
                              <li><a href="#"><i class="xcon-pinterest"></i></a></li>
                              <li><a href="#"><i class="xcon-behance"></i></a></li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="mobile_header_wrap">
                  <div class="container">
                     <div class="inner_wrap">
                        <div class="logo_wrap">
                           <a href="#"><img src="img/mobile-logo.png" alt="" /></a>
                        </div>
                        <div class="trigger_wrap">
                           <div class="hamburger hamburger--collapse-r">
                              <div class="hamburger-box">
                                 <div class="hamburger-inner"></div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- MENU LIST -->
               <div class="menu_list_wrap">
                  <ul class="nav">
                     <li><a href="#">Homepage</a></li>
                     <li>
                        <a href="#">Projects</a>
                        <ul class="sub_menu">
                           <li><a href="#">Project</a></li>
                           <li><a href="#">Project Single</a></li>
                        </ul>
                     </li>
                     <li>
                        <a href="#">Our Services</a>
                        <ul class="sub_menu">
                           <li><a href="#">Service</a></li>
                           <li><a href="#">Service Single</a></li>
                        </ul>
                     </li>
                     <li><a href="#">Blog</a></li>
                     <li><a href="#">About Us</a></li>
                     <li><a href="#">Contact</a></li>
                  </ul>
               </div>
               <!-- /MENU LIST -->
               <!-- DROPDOWN -->
               <div class="glax_tm_dropdown_wrap">
                  <div class="container">
                     <div class="drop_list home">
                        <div class="adress_wrap">
                           <div class="office_image">
                            <img src="{{asset('assets/frontend_new/img/contact/1.jpg')}}" alt="" />
                        </div>
                        <div class="definitions_wrap">
                            <h3>Head Office in New-York</h3>
                            <p>775 New York Ave, Brooklyn, NY 11203</p>
                            <p>Phone: +1 202-415-7234</p>
                            <p><span>Email:</span><a href="#">w.constructify@gmail.com</a></p>
                        </div>
                        </div>
                     </div>
                     <div class="drop_list message">
                        <div class="short_contact">
                           <h3 class="title">Request a Quote</h3>
                           <p class="subtitle">Looking for a quality and affordable builder for your next project?</p>
                           <div class="inputs_wrap">
                              <form action="#" method="post">
                                 <div class="input_list_wrap">
                                    <ul>
                                       <li>
                                          <input type="text" placeholder="Your Name" />
                                       </li>
                                       <li>
                                          <input type="text" placeholder="E-mail Address" />
                                       </li>
                                       <li>
                                          <input type="text" placeholder="Main Subject" />
                                       </li>
                                    </ul>
                                 </div>
                                 <textarea placeholder="Message"></textarea>
                               <button type="submit" name="submit" id="submit"class="button btn btn-primary">Send</button>
                       
                              </form>
                           </div>
                        </div>
                     </div>
                     <div class="drop_list phone">
                        <div class="call_wrap">
                           <div class="image">
                            <img src="{{asset('assets/frontend_new/img/estimate/call.png')}}" alt="" />
                        </div>
                           <h3>Toll Free</h3>
                           <p><a href="tel:18009876543">1-800-987-6543</a></p>

                        </div>
                     </div>
                     <div class="drop_list clock">
                        <div class="working_hours_wrap_short">
                           <h3>Working Hours</h3>
                           <p class="subtitle">We are happy to meet you during our working hours. Please make an appointment.</p>
                           <div class="hour_list">
                              <ul>
                                 <li>
                                    <span class="left">Monday-Friday:</span>
                                    <span class="right">9am to 5pm</span>
                                 </li>
                                 <li>
                                    <span class="left">Saturday:</span>
                                    <span class="right">10am to 3pm</span>
                                 </li>
                                 <li>
                                    <span class="left">Sunday:</span>
                                    <span class="right">Closed</span>
                                 </li>
                              </ul> 

                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- /DROPDOWN -->
            </div>
            <!-- /MOBILE BAR -->
            <!-- SIDEBAR WIDGET -->
            <div class="glax_tm_widget_wrap">
               <div class="widget_inner_wrap">
                  <div class="widget_icons_wrap">
                     <ul>
                        <li class="home" data-style="home">
                           <a href="{{url('index')}}"><img class="svg" src="{{asset('assets/frontend_new/img/svg/home.svg')}}" alt="" /></a>
                        </li>
                        <li class="message" data-style="message">
                           <a href="mailto:shikhar.chokshi@xceltec.in"><img class="svg" src="{{asset('assets/frontend_new/img/svg/message2.svg')}}" alt="" /></a>
                        </li>
                        <li class="phone" data-style="phone">
                           <a href="tel:18009876543"><img class="svg" src="{{asset('assets/frontend_new/img/svg/old_phone.svg')}}" alt="" /></a>
                        </li>
                        <li class="clock" data-style="clock">
                           <a href="#"><img class="svg" src="{{asset('assets/frontend_new/img/svg/clock.svg')}}" alt="" /></a>
                        </li>
                     </ul>
                  </div>
                  <!-- WIDGET DROPDOWN -->
                  <div class="widget_dropdown_wrap">
                     <div class="drop_list home">
                        <div class="adress_wrap">
                           <div class="office_image">
                          <img src="{{asset('assets/frontend_new/img/contact/1.jpg')}}" alt="" />
                          </div>
                           <div class="definitions_wrap">
                              <h3>Head Office in New-York</h3>
                              <p>775 New York Ave, Brooklyn, NY 11203</p>
                              <p>Phone: +1 202-415-7234</p>
                              <p><span>Email:</span><a href="#">w.constructify@gmail.com</a></p>
                           </div>
                        </div>
                     </div>
                     <div class="drop_list message">
                        <div class="short_contact">
                           <h3 class="title">Request a Quote</h3>
                           <p class="subtitle">Looking for a quality and affordable builder for your next project?</p>
                           <div class="inputs_wrap">
                              <form action="#" method="post">
                                 <div class="input_list_wrap">
                                     <ul>
                                        <li>
                                            <input type="text" placeholder="Your Name" required />

                                        </li>
                                        <li>
                                            <input type="text" placeholder="E-mail Address" required />
                                        </li>
                                        <li>
                                            <input type="text" placeholder="Main Subject" required />
                                        </li>
                                    <textarea placeholder="Message"></textarea>
                                   <button type="submit" name="submit" id="submit" class="button">Submit</button>
                                    </ul>
                              </div>
                           </form>
                        </div>
                     </div>
                  </div>
                  <div class="drop_list phone">
                     <div class="call_wrap">
                        <div class="image">
                          <img src="{{asset('assets/frontend_new/img/estimate/call.png')}}" alt="" />
                        </div>
                        <h3>Toll Free</h3>
                        <p><a href="tel:18009876543">1-800-987-6543</a></p>

                     </div>
                  </div>
                  <div class="drop_list clock">
                     <div class="working_hours_wrap_short">
                        <h3>Working Hours</h3>
                        <p class="subtitle">We are happy to meet you during our working hours. Please make an appointment.</p>
                        <div class="hour_list">
                           <ul>
                              <li>
                                 <span class="left">Monday-Friday:</span>
                                 <span class="right">9am to 5pm</span>
                              </li>
                              <li>
                                 <span class="left">Saturday:</span>
                                 <span class="right">10am to 3pm</span>
                              </li>
                              <li>
                                 <span class="left">Sunday:</span>
                                 <span class="right">Closed</span>
                              </li>
                           </ul> 
                        </div>
                     </div>
                  </div>
               </div>
               <!-- /WIDGET DROPDOWN -->
            </div>
         </div>
         <div class="glax_tm_widget_window_overlay"></div>
            <!-- /SIDEBAR WIDGET -->