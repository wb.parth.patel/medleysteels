 <!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en"> <!--<![endif]-->
<!-- HEAD -->
@include('layouts_frontend_new.header',['title'=>$title??'']) 
<!-- HEAD -->
<body>
    <!-- WRAPPER ALL -->
    <div class="glax_tm_wrapper_all">

    	@if(str_contains(Request::fullUrl(), 'index'))
    	@include('layouts_frontend_new.header-bar') 
    	@else
    	@include('layouts_frontend_new.header-bar-otherpage')
    	@endif

        @include('layouts_frontend_new.content') 

        @include('layouts_frontend_new.footer')
</body>
</html>
