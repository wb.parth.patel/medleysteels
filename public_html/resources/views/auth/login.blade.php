@extends('layouts_backend.login.master')
@section('content')  
<style type="text/css">
  /*  .login-logo{
        margin: 0 auto;
        text-align: center;
        height: 150px;
    }
    */
    .was-validated .form-control:invalid, .form-control.is-invalid {
        background-image: none;
    }
</style>
<!--begin::Aside-->
<div class="kt-grid__item kt-grid__item--order-tablet-and-mobile-2 kt-grid kt-grid--hor kt-login__aside" style="background-image: url({{ asset('assets/media/bg/bg-2.jpg') }} )">
    <div class="kt-grid__item"></div>
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver">
        <div class="kt-grid__item kt-grid__item--middle ml-auto mr-auto">
            <a href="{{ route('login') }}" class="kt-login__logo">
                <!-- <img class="login-logo mb-3" src="https://dev.webmaffia.com/fabgetaways/public/vendor/tcg/voyager/assets/images/logo-icon-light.png"> -->
                <!-- <img  class="login-logo mb-3" src="{{ asset('assets/img/logo.jpg') }}"> -->
                 <!-- <img class="login-logo mb-3" src="{{asset('assets/img/new-logo.png')}}" alt="Logo"> -->
            </a>
            <h3 class="kt-login__title mt-5">Welcome to Medley Steel!</h3>
        </div>
    </div>
    <div class="kt-grid__item">
        <div class="kt-login__info">
            <div class="kt-login__copyright">
                &copy <?php echo date("Y");?> Medley Steel
            </div>
        </div>
    </div>
</div>
<!--begin::Aside-->
<!--begin::Content-->
<div class="kt-grid__item kt-grid__item--fluid  kt-grid__item--order-tablet-and-mobile-1  kt-login__wrapper">
    <!--begin::Head-->
    <!--end::Head-->

    <!--begin::Body-->
    <div class="kt-login__body">
        <!--begin::Signin-->
        <div class="kt-login__form">
            <div class="kt-login__title">
                <h3>{{ __('Login') }}</h3>
            </div>
            <!--begin::Form-->
            <form class="kt-form" id="kt_login_form" method="POST" action="{{ route('login') }}">
                @csrf
                <div class="form-group mb-3">
                    <input id="email" type="email" class="form-control kt-input{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" placeholder="Enter email address" value="{{ old('email') }}"  autocomplete="email" autofocus maxlength="35">
                    @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        {{ $errors->first('email') }}
                    </span>
                    @endif
                </div>
                <div class="form-group m-form__group login-passowrd">
                    <!-- <input id="password" type="password" class="form-control kt-input kt-login__form-input--last{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Enter password" name="password" required autocomplete="current-password" data-toggle="password" maxlength="20"> -->
                    <input id="password" type="password" name='password' class="form-control m-input m-login__form-input--last{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password"  placeholder="Enter password">
                    <span toggle="#password" class="show-password fa fa-fw fa-eye field-icon toggle-password"></span>
                    @if ($errors->has('password'))
                    <span class="invalid-feedback error" id="error_msg_css" role="alert">
                        {{ $errors->first('password') }}
                    </span>
                    @endif 
                </div>
                <!--begin::Action-->
                <div class="kt-login__actions">
                    <button  class="btn btn-primary btn-elevate kt-login__btn-primary">  {{ __('Login') }}</button>
                    @if (Route::has('password.request'))
                    <a class="btn btn-primary btn-elevate kt-login__btn-primary" href="{{ route('password.request') }}" style="line-height: 33px;">
                        {{ __('Forgot Your Password?') }}
                    </a>
                    @endif
                </div>
                <!--end::Action-->
            </form>
            <!--end::Form-->
        </div>
        <!--end::Signin-->
    </div>
    <!--end::Body-->
</div>
<script src="https://code.jquery.com/jquery-3.4.1.slim.js" integrity="sha256-BTlTdQO9/fascB1drekrDVkaKd9PkwBymMlHOiG+qLI=" crossorigin="anonymous"></script>
<script type="text/javascript">
    // $("#password").password('toggle');
    $(".toggle-password").click(function() {
        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });
</script>
<!--end::Content-->
@endsection
