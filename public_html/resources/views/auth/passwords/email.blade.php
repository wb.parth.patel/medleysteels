@extends('layouts_backend.login.master')
@section('content')  
<style type="text/css">
    .login-logo{
        margin: 0 auto;
        text-align: center;
        height: 150px;
    }
    .was-validated .form-control:invalid, .form-control.is-invalid {
        background-image: none;
    }
</style>
<!--begin::Aside-->
        <div class="kt-grid__item kt-grid__item--order-tablet-and-mobile-2 kt-grid kt-grid--hor kt-login__aside" style="background-image: url({{ asset('assets/media/bg/bg-2.jpg') }} )">
            <div class="kt-grid__item">
               
            </div>
            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver">
                <div class="kt-grid__item kt-grid__item--middle ml-auto mr-auto"> 
                     <a href="#" class="kt-login__logo">
                      <!-- <img  class="login-logo mb-3" src="{{ asset('assets/img/logo.jpg') }}"> -->
                     <!-- <img class="login-logo mb-3" src="{{asset('assets/img/new-logo.png')}}" alt="Logo"> -->
                    
                </a>
                    <h3 class="kt-login__title mt-5">Welcome to Medley Steel</h3>
                </div>
            </div>
            <div class="kt-grid__item">
                <div class="kt-login__info">
                    <div class="kt-login__copyright">
                        &copy <?php echo date("Y");?> Medley Steel
                    </div>
                </div>
            </div>
        </div>
        <!--begin::Aside-->
<!--begin::Content-->
        <div class="kt-grid__item kt-grid__item--fluid  kt-grid__item--order-tablet-and-mobile-1  kt-login__wrapper">
            <!--begin::Head-->
            <!--end::Head-->

            <!--begin::Body-->
            <div class="kt-login__body">

                <!--begin::Signin-->
                <div class="kt-login__form">
                    <div class="kt-login__title">
                        <h3>{{ __('Forgot Password') }}</h3>
                    </div>

                   
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <!--begin::Form-->
                     <form class="kt-form"  method="POST" action="{{ route('password.email') }}">
                        @csrf
                         <div class="form-group mb-3">
                           

                          
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" autocomplete="email" autofocus placeholder="Email" maxlength="50">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                          
                        </div>
                       <!--  <div class="form-group">
                             <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" placeholder="Enter password" name="password" required autocomplete="current-password" data-toggle="password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div> -->
                        <!--begin::Action-->
                        <div class="kt-login__actions">
                          <!--   @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif -->  


                                 <!-- <a href="http://everglades.xceltec.in/public/login"> -->
                                 <a href="{{url('login')}}">

                            <span  class="btn btn-primary btn-elevate kt-login__btn-primary" style="line-height: 33px;">  {{ __('Back to Login') }}</span>
                                  </a>
                                  <button  class="btn btn-primary btn-elevate kt-login__btn-primary" type="submit">Send Password Reset Link</button>
                        </div>
                        <!--end::Action-->
                    </form>

                                 
                    <!--end::Form-->

                  
                   
                </div>
                <!--end::Signin-->
            </div>
            <!--end::Body-->
        </div>
    <script type="text/javascript">

    $("#password").password('toggle');

    </script>
        <!--end::Content-->
@endsection
