@extends('layouts_backend.login.master')
@section('content')  

<!--begin::Aside-->
        <div class="kt-grid__item kt-grid__item--order-tablet-and-mobile-2 kt-grid kt-grid--hor kt-login__aside"style="background-image: url({{ asset('assets/media/bg/bg-2.jpg') }} )">
            <div class="kt-grid__item">
               
            </div>
            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver">
                <div class="kt-grid__item kt-grid__item--middle ml-auto mr-auto"> 
                     <a href="#" class="kt-login__logo">
                      <!-- <img  class="login-logo mb-3" src="{{ asset('assets/img/logo.jpg') }}"> -->
                       <img class="login-logo mb-3" src="{{asset('assets/img/new-logo.png')}}" alt="Logo">
                </a>
                    <h3 class="kt-login__title mt-5">Welcome to Medley Steel</h3>
                </div>
            </div>
            <div class="kt-grid__item">
                <div class="kt-login__info">
                    <div class="kt-login__copyright">
                        &copy <?php echo date("Y");?> Medley Steel
                    </div>
                </div>
            </div>
        </div>
        <!--begin::Aside-->
<!--begin::Content-->
        <div class="kt-grid__item kt-grid__item--fluid  kt-grid__item--order-tablet-and-mobile-1  kt-login__wrapper">
            <!--begin::Head-->
            <!--end::Head-->

            <!--begin::Body-->
            <div class="kt-login__body">

                <!--begin::Signin-->
                <div class="kt-login__form">
                    <div class="kt-login__title">
                        <h3>{{ __('Reset Password') }}</h3>
                    </div>

                    <!--begin::Form-->
                    <form class="kt-form" method="POST" action="{{ route('password.update') }}">
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">

                      <div class="form-group mb-3">
                           <!--  <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label> -->

                           
                                <input id="email" type="email" class="form-control  kt-input @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" readonly="" required autocomplete="email" autofocus maxlength="45" placeholder="Enter email address">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                          
                        </div>

                         <div class="form-group mb-3">
                           

                           
                                <input id="password" type="password" class="form-control  kt-input @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" maxlength="20" placeholder="Enter Password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                          
                        </div>

                        <!--begin::Action-->
                        <div class="form-group mb-3">
                           

                           
                                <input id="password-confirm" type="password" class="form-control  kt-input" name="password_confirmation" required autocomplete="new-password" maxlength="20" placeholder="Enter Confirm Passowrd">
                      
                        </div>

                        <div class="form-group  mb-3">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Reset Password') }}
                                </button>
                            </div>
                        </div>
                        <!--end::Action-->
                    </form>
                    <!--end::Form-->

            
                   
                </div>
                <!--end::Signin-->
            </div>
            <!--end::Body-->
        </div>
    <script type="text/javascript">

    $("#password").password('toggle');

    </script>
        <!--end::Content-->
@endsection
