<!DOCTYPE html>
<html lang="zxx">
    <!-- begin::Head -->
    @include('layouts_frontend.header',['title'=>$title??''])
    <!-- end::Head -->
    <!-- begin::Body -->
    <body>
       <div class="loader">
                <div class="d-table">
                    <div class="d-table-cell">
                        <div class="spinner"></div>
                    </div>
                </div>
        </div>

        <div class="header-top-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8">
                        <div class="header-top-item">
                            <div class="header-left">
                                 <ul>
                                    <li>
                                        <i class="flaticon-factory"></i>
                                        <a href="javascript:void(0)">
                                      
                                        {{ App\Http\Controllers\Controller::getSiteconfigValueByKey('HEADER_INFORMATION')['value']}}
                                            
                                        </a>
                                    </li>
                                    <li>
                                        <i class="flaticon-mail"></i>
                                        <a href="mailto:{{App\Http\Controllers\Controller::getSiteconfigValueByKey('Email')['value']}}">
                                            {{App\Http\Controllers\Controller::getSiteconfigValueByKey('Email')['value']}}
                                        </a>
                                    </li>
                                    <li>
                                        <i class="flaticon-phone"></i>
                                        <a href="tel:{{App\Http\Controllers\Controller::getSiteconfigValueByKey('CONTACT_NUMBER')['value']}}">
                                            {{App\Http\Controllers\Controller::getSiteconfigValueByKey('CONTACT_NUMBER')['value']}}
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="header-top-item">
                            <div class="header-right">
                                <ul>
                                    <li>
                                        <a href="https://www.facebook.com/EvergladesSteel" target="_blank">
                                            <i class='bx bxl-facebook'></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://twitter.com/" target="_blank">
                                            <i class='bx bxl-twitter'></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://www.instagram.com/" target="_blank">
                                            <i class='bx bxl-instagram'></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://in.linkedin.com/" target="_blank">
                                            <i class='bx bxl-linkedin'></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a target="blank" href="https://api.whatsapp.com/send?phone=17864124375">
                                            <i class='bx bxl-whatsapp'></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @include('layouts_frontend.header-bar') 

                       
        @include('layouts_frontend.content')  
                       
            
        @include('layouts_frontend.footer')
        <!-- Copyright -->
            <div class="copyright-area">
                <div class="container">
                    <div class="copyright-item">
                        <p>{{App\Http\Controllers\Controller::getSiteconfigValueByKey('COPYRIGHT')['value']}}
                            <!-- <a href="https://envytheme.com/">EnvyTheme</a> -->
                            <!-- <a href="#">EnvyTheme</a> -->
                        </p>
                    </div>
                </div>
            </div>
            <!-- End Copyright -->
    		

        <!-- Essential JS -->
        <script src="{{asset('assets/js/jquery-3.5.1.min.js')}}"></script>
        <script src="{{asset('assets/js/popper.min.js')}}"></script>
        <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
        <!-- Form Validator JS -->
        <script src="{{asset('assets/js/form-validator.min.js')}}"></script>
        <!-- Contact JS -->
        <script src="{{asset('assets/js/contact-form-script.js')}}"></script>
        <!-- Meanmenu JS -->
        <script src="{{asset('assets/js/jquery.meanmenu.js')}}"></script>
        <!-- Owl Carousel -->
        <script src="{{asset('assets/js/owl.carousel.min.js')}}"></script>
        <!-- Appear JS --> 
        <script src="{{asset('assets/js/jquery.appear.js')}}"></script>
        <!-- Odometer JS --> 
        <script src="{{asset('assets/js/odometer.min.js')}}"></script>
        <!-- Magnific Popup JS -->
        <script src="{{asset('assets/js/jquery.magnific-popup.min.js')}}"></script>
        <!-- Custom JS -->
        <script src="{{asset('assets/js/custom.js')}}"></script>
        <!-- end::Body -->
    </body>
    @stack('scripts')
    
</html>
