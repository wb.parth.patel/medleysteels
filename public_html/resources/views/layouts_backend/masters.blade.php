<!DOCTYPE html>
<html lang="en">
<!-- begin::Head -->
@include('layouts_backend.header',['title'=>$title??''])
<!-- end::Head -->
<!-- begin::Body -->
<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />
 <body  class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed"  >
   <div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed " >
         <div class="kt-header-mobile__logo">
            <a href="/metronic/preview/demo1/index.html">
            <img alt="Logo" src="https://keenthemes.com/metronic/themes/metronic/theme/default/demo1/dist/assets/media/logos/logo-light.png"/>
            <!-- <img alt="Logo" src="https://keenthemes.com/metronic/themes/metronic/theme/default/demo1/dist/assets/media/logos/logo-light.png"/> -->
            </a>
         </div>
         <div class="kt-header-mobile__toolbar">
            <button class="kt-header-mobile__toggler kt-header-mobile__toggler--left" id="kt_aside_mobile_toggler"><span></span></button>
            <button class="kt-header-mobile__toggler" id="kt_header_mobile_toggler"><span></span></button>
            <button class="kt-header-mobile__topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
         </div>
      </div>
	<!-- begin:: Page -->
<div class="kt-grid kt-grid--hor kt-grid--root">
         <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
               @include('layouts_backend.sidebar_new') 
               <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">
                     @include('layouts_backend.header-bar')  
                   
                       @include('layouts_backend.contenct')  
                   
                         @include('layouts_backend.footer') 

               </div>
        </div>
      
    </div>
    <a href="#top" id="bottom" class="scrolltop">
        <div id="m_scroll_top" class="m-scroll-top">
            <i class="la la-arrow-up"></i>
        </div>
    </a>
		 @stack('scripts') 

</body>


<!-- end::Body -->
</html>
