<!DOCTYPE html>
<html lang="en" >
@include('layouts_backend.login.header')
   
       <body  class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">

   <!-- Begin Page -->
  <div class="kt-grid kt-grid--ver kt-grid--root">
		<div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v1" id="kt_login">
	<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--desktop kt-grid--ver-desktop kt-grid--hor-tablet-and-mobile">
		
	@yield('content')	
		
	</div>
</div>
	</div>
<!-- end:: Page -->



        <!-- end::Global Config -->

    	<!--begin::Global Theme Bundle(used by all pages) -->
    	    	   <script src="{{ asset('assets/plugins/global/plugins.bundle.js') }}" type="text/javascript"></script>
		    	   <script src="{{ asset('assets/js/scripts.bundle.js') }}" type="text/javascript"></script>
				<!--end::Global Theme Bundle -->
					<script src="{{ asset('assets/js/pages/custom/login/login-1.js') }}" type="text/javascript"></script>
                        <!--end::Page Scripts -->
            </body>
    <!-- end::Body -->
</html>
