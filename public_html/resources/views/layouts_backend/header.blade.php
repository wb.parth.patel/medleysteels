<head>
		<base href="">
		<meta charset="utf-8" />
		<title>Medley Steel | {{$title}}</title>
		<meta name="description" content="Updates and statistics">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Expires" content="0">

		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
	<script>
		WebFont.load({
			google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
			active: function() {
				sessionStorage.fonts = true;
			}
		});
	</script>
	
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700"> 

		<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/custom/datatables/datatables.bundle.css')}}">
		<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/general/summernote/dist/summernote.css')}}">

		 <link rel="stylesheet" type="text/css" href="{{asset('assets/css/style.bundle.css')}}">

         <link rel="stylesheet" type="text/css" href="{{asset('assets/css/skins/header/base/light.css')}}">
         <link rel="stylesheet" type="text/css" href="{{asset('assets/css/skins/header/menu/light.css')}}">
         <link rel="stylesheet" type="text/css" href="{{asset('assets/css/skins/brand/dark.css')}}">
         <link rel="stylesheet" type="text/css" href="{{asset('assets/css/skins/aside/dark.css')}}">
         <link href="{{asset('assets/plugins/general/plugins/flaticon/flaticon.css')}}" rel="stylesheet" type="text/css" />
         <link href="{{asset('assets/plugins/general/plugins/fontawesome5/css/all.min.css')}}" rel="stylesheet" type="text/css" />

		<link href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />  

		<link href="{{ asset('assets/css/custom.css') }}" rel="stylesheet">
		<link href="{{ asset('assets/css/plugins.bundle.css') }}" rel="stylesheet">
		<link href="{{ asset('assets/css/custom.css') }}" rel="stylesheet" type="text/css" /> 
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

		<!--end::Layout Skins -->
		<!-- <link rel="shortcut icon" href="{{asset('assets/img/favicon.png')}}" /> -->
		<!-- <link rel="icon" type="image/png" href="{{asset('assets/img/favicon_2VQ_icon.ico')}}"> -->
	</head>