@extends('layouts_backend.masters',['title'=>'Profile Management'])
@section('content')

<style type="text/css">
	.file-drop-zone-title{
		display: none;
	}
	.fileinput-remove{
		display: none;
	}
	.file-footer-buttons{
		display: none;
	}
	.was-validated .form-control:invalid, .form-control.is-invalid {
		background-image: none;
	}
</style>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" media="all" rel="stylesheet" type="text/css"/>

<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	@if(session()->get('success'))
	<div class="col-5 pull-right">
		<div class="alert alert-success alert-dismissible fade show elementToFadeInAndOut" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			</button>
			{{ session()->get('success') }}
		</div>
	</div>
	@elseif(session()->get('error'))
	<div class="col-5 pull-right">
		<div class="alert alert-danger alert-dismissible fade show elementToFadeInAndOut" role="alert">

			{{ session()->get('error') }}
		</div>
	</div>
	@endif
	<div class="kt-container  kt-container--fluid ">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">Profile Management</h3>
			<span class="kt-subheader__separator kt-hidden"></span>
			<div class="kt-subheader__breadcrumbs">
				<a href="{{ route('dashboard') }}" class="kt-subheader__breadcrumbs-home"><i class="fa fa-home"></i></a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a class="kt-subheader__breadcrumbs-link">Edit Profile</a>
				<!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
			</div>
		</div>
	</div>
</div>

<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
	<div class="row">
		<div class="col-lg-12">
			<!--begin::Portlet-->
			<div class="kt-portlet">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							Edit Profile
						</h3>
					</div>
					<div class="kt-portlet__head-toolbar">
						<div class="kt-portlet__head-actions">
							<a href="{{ route('dashboard') }}" class="btn btn-brand btn-elevate btn-icon-sm btn-bold">
								Back
							</a>
						</div>
					</div>
				</div>
				<!--begin::Form-->
				<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" method="post"
				action="" name="profile_form" id="profile_form" enctype="multipart/form-data">
				{{--  @csrf
				@method('PATCH')  --}}
				<div class="kt-portlet__body">
					<div class="form-group row">
						<div class="col-lg-6">
							<label>Name:</label>
							<input type="text" class="form-control txtOnly" name="name" placeholder="Enter name" maxlength="25" value="">
							<span class="kt-form__help error name"></span>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-6">
							<label>Email:</label>
							<input type="text" class="form-control" name="email" placeholder="Enter email" maxlength="25" value="" readonly>
							<span class="kt-form__help error email"></span>
						</div>
						<div class="col-lg-6">
							<label class="">Contact Number:</label>
							<input type="text" class="form-control" name="contact_number" placeholder="Enter contact number" value="" >
							<span class="kt-form__help error contact_number"></span>

						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-6">
							<label>Profile Image: </label>
							{{--  <input type="file" class="file" name="image" id="file-1" class="file" data-min-file-count="1" value="{{url('/media/userimage',$user['image'])}}">  --}}
                            <input type="file" class="file" name="image" id="file-1" class="file" data-min-file-count="1" value="">
                            <span class="kt-form__help error image"></span>
						</div>
					</div>
					<!-- <div class="form-group row">
						<div class="col-lg-6">
							<label>Role:</label>
							<input type="text" class="form-control" name="current_password" placeholder="Enter current password" id="current-password" max='25'>
						</div>
						<div class="col-lg-6">
							<label class="">Date of Birth:</label>
							<input type="text" class="form-control" name="dob" placeholder="Enter date of birth">

						</div>
					</div> -->
				</div>
				<div class="kt-portlet__foot">
					<div class="kt-form__actions">
						<div class="row">
							<div class="col-lg-6">
								<button type="submit" name="city_submit" id="city_submit"  class="btn btn-primary">Save</button>
								<!-- <button type="reset" class="btn btn-secondary">Cancel</button> -->
								{{--  <a href="{{url('dashboard')}}">  --}}
                                    <a href="">
									<button type="button" class="btn btn-secondary">Cancel</button>
								</a>
							</div>
						</div>
					</div>
				</div>
			</form>
			<!--end::Form-->
		</div>
	</div>
</div>
</div>
@endsection
@push('scripts')
<script type="text/javascript" src="{{asset('js/MyProfileFormValidation.js')}}"></script>
<!-- <script src="https://code.jquery.com/jquery-latest.min.js"></script> -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/js/fileinput.js" type="text/javascript"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/themes/fa/theme.js" type="text/javascript"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" type="text/javascript"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" type="text/javascript"></script>

<script type="text/javascript">
	$("#file-1").fileinput({
		theme: 'fa',
		showUpload:false,
		showRemove:false,
		uploadUrl: "/image-view",
		uploadExtraData: function() {
			return {
				_token: $("input[name='_token']").val(),
			};
		},
		remove :false,
		allowedFileExtensions: ['jpg','jpeg', 'png', 'gif'],
        //initialPreview:"{{url('media/userimage',$user->image)}}",
        initialPreview:"{{url('media/userimage')}}",
		initialPreviewAsData:true,
		overwriteInitial: true,
		maxFileSize:2000,
		maxFilesNum: 1,
	});

	$( document ).ready(function() {
		$('.txtOnly').bind('keyup blur',function(){
			var node = $(this);
			node.val(node.val().replace(/[^A-Za-z_\s]/,'') ); }
			);
	});
</script>
@endpush

