@extends('layouts_backend.masters',['title'=>'User Group'])
@section('content')
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
       <div class="kt-subheader__main">
             <h3 class="kt-subheader__title">User Group</h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="{{ route('dashboard') }}" class="kt-subheader__breadcrumbs-home"><i class="fa fa-home"></i></a>
                                            <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="{{ route('user-group.index') }}" class="kt-subheader__breadcrumbs-link">User Group</a>
                         <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a class="kt-subheader__breadcrumbs-link">Add</a>
                                        <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
                </div>
      </div>
    </div>
</div>
<!-- end:: Subheader -->

<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
		<div class="row">
	<div class="col-lg-12">
		<!--begin::Portlet-->
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						Add User Group
					</h3>
				</div>
			</div>
			<!--begin::Form-->
			<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" method="post" 
				action="{{ url('user-group/store')}}" name="content_form" id="content_form">
				@csrf
				@method('POST')
				<div class="kt-portlet__body">
					
					<div class="form-group row">
						<div class="col-lg-12">
							<label class="">Group Name:</label>
							<!-- <input type="text" class="form-control" name="group_name" placeholder="Enter Group Name" maxlength="20"> -->
							<select class="form-control" name="group_id">
								<option value="">Select Group</option>
								@foreach($group as $val)
								<option value="{{$val['id']}}">{{$val['group_name']}}</option>
								@endforeach
							</select>
							<span class="kt-form__help error group_id"></span>
						</div>	
					</div>
					
					<div class="form-group row">
						<div class="col-lg-12">
							<label class="">User:</label>
							<select class="form-control select2" id="kt_select2_1" name="user_id[]" multiple="">
								<option value="">Select User</option>
								@foreach($users as $val)
								<option value="{{$val['id']}}">{{$val['name']}}</option>
								@endforeach
							</select>
							<span class="kt-form__help error user_id"></span>
						</div>	
					</div>
				
	            </div>
	            <div class="kt-portlet__foot">
					<div class="kt-form__actions">
						<div class="row">
							<div class="col-lg-6">
								<button type="submit" name="city_submit" id="city_submit"  class="btn btn-primary">Save</button>
								<!-- <button type="reset" class="btn btn-secondary">Cancel</button> -->
								<a href="{{url('user-group/index')}}">
                                    <button type="button" class="btn btn-secondary">Cancel</button>
                                </a>
							</div>
						</div>
					</div>
				</div>
			</form>
			<!--end::Form-->
		</div>
	</div>
</div>
</div>
@endsection
@push('scripts')
<script type="text/javascript" src="{{asset('js/ContentFormValidation.js')}}"></script>

<script src="{{asset('plugins/general/summernote/dist/summernote.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/js/pages/crud/forms/widgets/summernote.js')}}" type="text/javascript"></script>

<script src="{{asset('assets/js/pages/crud/forms/widgets/select2.js')}}" type="text/javascript"></script>
<script src="{{asset('plugins/general/select2/dist/js/select2.full.js')}}" type="text/javascript"></script>
@endpush