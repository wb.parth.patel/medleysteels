@extends('layouts_backend.masters',['title'=>'Dashboard Management'])
@section('content')
<div class="kt-subheader">
@if(session()->get('success'))
    <div class="col-5 pull-right">
    <div class="alert alert-success alert-dismissible fade show elementToFadeInAndOut" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        </button>
        {{ session()->get('success') }}
    </div>
    </div>
    @elseif(session()->get('error'))
    <div class="col-5 pull-right">
    <div class="alert alert-danger alert-dismissible fade show elementToFadeInAndOut" role="alert">

        {{ session()->get('error') }}
    </div>
    </div>
    @endif
</div>
<div class="kt-subheader  kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">

            <h3 class="kt-subheader__title">Dashboard</h3>
        </div>
        <div class="kt-subheader__toolbar">

            <select class="custom-select form-control" id='dd_dashboard_filter'>
                <option value="{{ROUTE('dashboard.all')}}">All</option>
                <option value="{{ROUTE('dashboard.daily')}}"> Daily</option>
                <option value="{{ROUTE('dashboard.weekly')}}">Weekly</option>
                <option value="{{ROUTE('dashboard.monthly')}}">Monthly</option>
                <option value="{{ROUTE('dashboard.yearly')}}">Yearly</option>
            </select>
        </div>

    </div>
</div>
<!-- END: Subheader -->



<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <!--begin:: Widgets/Stats-->
    <div class="kt-portlet">
    <div class="kt-portlet__body  kt-portlet__body--fit">
        <div class="row row-no-padding row-col-separator-lg">

            <div class="col-md-12 col-lg-6 col-xl-3">
                <!--begin::Total Profit-->
                <div class="kt-widget24">
                    <div class="kt-widget24__details">
                        <div class="kt-widget24__info">
                            <h4 class="kt-widget24__title">
                                Total Users
                            </h4>
                            <span class="kt-widget24__desc">
                                <a href="">All Users</a>

                            </span>
                        </div>

                        <span class="kt-widget24__stats kt-font-brand"  id="lbl_user">

                        </span>
                    </div>

                    <div class="progress progress--sm">
                        <div class="progress-bar kt-bg-brand" role="progressbar" style="width: 78%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>
                <!--end::Total Profit-->
            </div>

            <div class="col-md-12 col-lg-6 col-xl-3">
                <!-- begin::New Feedbacks -->
                <div class="kt-widget24">
                    <div class="kt-widget24__details">
                        <div class="kt-widget24__info">
                            <h4 class="kt-widget24__title">
                                Total Blogs
                            </h4>
                            <span class="kt-widget24__desc">
                                All Blogs
                            </span>
                        </div>

                        <span class="kt-widget24__stats kt-font-warning" id="lbl_blog">

                        </span>
                    </div>

                    <div class="progress progress--sm">
                        <div class="progress-bar kt-bg-warning" role="progressbar" style="width: 84%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>
                <!--end::New Feedbacks-->
            </div>

             <!-- <div class="col-md-12 col-lg-6 col-xl-3"> -->
                <!-- begin::New Orders -->
              <!--   <div class="kt-widget24">
                    <div class="kt-widget24__details">
                        <div class="kt-widget24__info">
                            <h4 class="kt-widget24__title">
                                Total Orders
                            </h4>
                            <span class="kt-widget24__desc">
                                All Orders
                            </span>
                        </div>

                        <span class="kt-widget24__stats kt-font-danger" id="lbl_orders">

                        </span>
                    </div>

                    <div class="progress progress--sm">
                        <div class="progress-bar kt-bg-danger" role="progressbar" style="width: 69%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div> -->
                <!-- end::New Orders -->
            <!-- </div>  -->


            <!-- <div class="col-md-12 col-lg-6 col-xl-3"> -->
                <!--begin::New Feedbacks-->
                <!-- <div class="kt-widget24">
                    <div class="kt-widget24__details">
                        <div class="kt-widget24__info">
                            <h4 class="kt-widget24__title">
                                Total Revenue
                            </h4>
                            <span class="kt-widget24__desc">
                                All Revenue
                            </span>
                        </div>

                        <span class="kt-widget24__stats kt-font-warning" id="lbl_revenue">

                        </span>
                    </div>

                    <div class="progress progress--sm">
                        <div class="progress-bar kt-bg-warning" role="progressbar" style="width: 84%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div> -->
                <!--end::New Feedbacks-->
            <!-- </div> -->

        </div>
    </div>
</div>


    <div class="kt-portlet"></div>
    <!--End::Section-->
</div>
@endsection
@push('scripts')
    <script type="text/javascript" src="{{asset('js/dashboard.js')}}"></script>
    <input type="hidden" name="txt_url" value="{{ROUTE('dashboard.all')}}">
@endpush
