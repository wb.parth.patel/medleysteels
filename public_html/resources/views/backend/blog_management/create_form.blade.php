@extends('layouts_backend.masters',['title'=>'Blog Management'])
@section('content')

<style type="text/css">
  .file-drop-zone-title{
    display: none;
  }
  .fileinput-remove{
    display: none;
  }
  .file-footer-buttons{
    display: none;
  }
</style>

<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" media="all" rel="stylesheet" type="text/css"/>

<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
       <div class="kt-subheader__main">
             <h3 class="kt-subheader__title">Blog Management</h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="{{ route('dashboard') }}" class="kt-subheader__breadcrumbs-home"><i class="fa fa-home"></i></a>
                                            <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="{{ route('blog-management.index') }}" class="kt-subheader__breadcrumbs-link">Blog Management</a>
                         <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a class="kt-subheader__breadcrumbs-link">Add</a>
                                        <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
                </div>
      </div>
    </div>
</div>
<!-- end:: Subheader -->

<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
		<div class="row">
	<div class="col-lg-12">
		<!--begin::Portlet-->
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						Add Blog
					</h3>
				</div>
			</div>
			<!--begin::Form-->
			<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" method="post"
				action="" name="city_form" id="city_form" enctype="multipart/form-data">
				{{--  @csrf
				@method('POST')  --}}
				<div class="kt-portlet__body">
					<div class="form-group row">
						<div class="col-lg-12">
							<label class="">Blog Category:</label>
							<select class="form-control kt-input selectpicker" data-live-search="true" name="category_id" >
								<option value="">Select Category</option>
								{{--  @foreach($blog_category as $val)  --}}
								<option value=""></option>
								{{--  @endforeach  --}}
							</select>
							<span class="kt-form__help error category_id"></span>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-12">
							<label>Blog Title:</label>
							<input type="text" class="form-control" name="blog_title" maxlength="65" placeholder="Enter title name">
							<span class="kt-form__help error blog_title"></span>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-12">
							<label class="">Enter Blog Description: </label>
							<textarea class="summernote" name="blog_description"></textarea>
							<span class="kt-form__help error blog_description"></span>
						</div>

					</div>
					<div class="form-group row">

						<div class="col-lg-6">
							<label class="">Blog Image: </label>
							<input type="file" name="image" id="file-1" class="file" data-overwrite-initial="false" data-min-file-count="1">
							<span class="kt-form__help error user_image"></span>
						</div>
					</div>

					</div>

	            <div class="kt-portlet__foot">
					<div class="kt-form__actions">
						<div class="row">
							<div class="col-lg-6">
								<button type="submit" name="city_submit" id="city_submit"  class="btn btn-primary">Save</button>
								<!-- <button type="reset" class="btn btn-secondary">Cancel</button> -->
								<a href="">
                                    <button type="button" class="btn btn-secondary">Cancel</button>
                                </a>
							</div>
						</div>
					</div>
				</div>
			</form>
			<!--end::Form-->
		</div>
	</div>
</div>
</div>
@endsection
@push('scripts')
<script type="text/javascript" src="{{asset('js/BlogManagementFormValidation.js')}}"></script>

<script src="{{asset('plugins/general/summernote/dist/summernote.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/js/pages/crud/forms/widgets/summernote.js')}}" type="text/javascript"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/js/fileinput.js" type="text/javascript"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/themes/fa/theme.js" type="text/javascript"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" type="text/javascript"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" type="text/javascript"></script>

<script type="text/javascript" src="https://cdn.datatables.net/plug-ins/1.10.9/api/fnFilterClear.js"></script>

<script type="text/javascript">
        $("#file-1").fileinput({
            theme: 'fa',
			showUpload:false,
			showRemove:false,
            uploadUrl: "/image-view",
            uploadExtraData: function() {
                return {
                    _token: $("input[name='_token']").val(),
                };
            },
            remove :false,
            allowedFileExtensions: ['jpg','jpeg', 'png', 'gif'],
            initialPreviewAsData:true,
            overwriteInitial: true,
            maxFileSize:2000,
            maxFilesNum: 1,
        });
    </script>
@endpush
