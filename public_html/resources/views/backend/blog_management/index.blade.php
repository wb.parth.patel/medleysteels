@extends('layouts_backend.masters',['title'=>'Blog Management'])
@section('content')
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
   <div class="kt-container  kt-container--fluid ">
      <div class="kt-subheader__main">
         <ul class="nav nav-light-success nav-bold nav-pills mb-0">
            <li class="nav-item">
               <a class="nav-link active border px-5" data-toggle="tab" href="#kt_tab_pane_4_1">
               <span class="nav-icon"><i class="fa fa-home"></i></span>
               <span class="nav-text">Blog Management</span>
               </a>
            </li>
            <li class="nav-item">
               <a class="nav-link border px-5" data-toggle="tab" href="#kt_tab_pane_4_2">
               <span class="nav-icon"><i class="fa fa-hotel"></i></span>
               <span class="nav-text">Blog Category Management</span>
               </a>
            </li>
         </ul>
      </div>
      <div class="kt-subheader__main">
         <h3 class="kt-subheader__title">Blog Management</h3>
         <span class="kt-subheader__separator kt-hidden"></span>
         <div class="kt-subheader__breadcrumbs">
            <a href="{{ route('dashboard') }}" class="kt-subheader__breadcrumbs-home"><i class="fa fa-home"></i></a>
            <span class="kt-subheader__breadcrumbs-separator"></span>
            <a class="kt-subheader__breadcrumbs-link">Blog Management</a>
            <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
         </div>
      </div>
      <div class="site-alert">
         @if(session()->get('success'))
         <div class="col-5 pull-right">
            <div class="alert alert-success alert-dismissible fade show elementToFadeInAndOut" role="alert">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               </button>
               {{ session()->get('success') }}
            </div>
         </div>
         @elseif(session()->get('error'))
         <div class="col-5 pull-right">
            <div class="alert alert-danger alert-dismissible fade show elementToFadeInAndOut" role="alert">
               {{ session()->get('error') }}
            </div>
         </div>
         @endif
      </div>
   </div>
</div>
<!-- end:: Subheader -->
<!-- begin :: content -->

<div class="tab-content">
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid tab-pane fade show active" id="kt_tab_pane_4_1" role="tabpanel" aria-labelledby="kt_tab_pane_4_1">
   <div class="kt-portlet kt-portlet--mobile">
      <div class="kt-portlet__head kt-portlet__head--lg">
         <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
               <div class="kt-portlet__head-actions">
                  &nbsp;
                  <a href="{{ route('blog-management.create') }}" class="btn btn-brand btn-elevate btn-icon-sm">
                  <i class="fa fa-plus"></i>
                  Add New Blog
                  </a>
               </div>
               <div class="kt-portlet__head-actions">
                  &nbsp;
                  <a href="{{ route('blog-management.index') }}" class="btn btn-brand btn-elevate btn-icon-sm">
                  Reset
                  </a>
               </div>
            </div>
         </div>
      </div>
      <div class="kt-portlet__body">
         <!--begin: Datatable -->
         <!-- <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1"></table> -->
         <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_2">
            <thead>
               <tr>
                  <th>#</th>
                  <th>Blog</th>
                  <th>Description</th>
                  <th>Status</th>
                  <th>Actions</th>
               </tr>
            </thead>
         </table>
      </div>
      <!--end: Datatable -->
   </div>
</div>
<!-- Second  Tab-->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid tab-pane fade" id="kt_tab_pane_4_2" role="tabpanel" aria-labelledby="kt_tab_pane_4_2">
   <div class="kt-portlet kt-portlet--mobile">
      <div class="kt-portlet__head kt-portlet__head--lg">
         <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
               <div class="kt-portlet__head-actions">
                  &nbsp;
                  <a href="{{ route('blog-category-master.create') }}" class="btn btn-brand btn-elevate btn-icon-sm">
                     <i class="fa fa-plus"></i>
                     Add New Blog Category
                     </a>
               </div>
               <div class="kt-portlet__head-actions">
                  &nbsp;
                  <a href="{{ route('blog-category-master.index') }}" class="btn btn-brand btn-elevate btn-icon-sm">
                  Reset
                  </a>
               </div>
            </div>
         </div>
      </div>
      <div class="kt-portlet__body">
         <!--begin: Datatable -->
         <!-- <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1"></table> -->
         <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_2_comment">
            <thead>
               <tr>
                  <th>ID</th>
                  <th>Category Name</th>
                  <th>Status</th>
                  <th>Actions</th>
               </tr>
            </thead>
         </table>
      </div>
      <!--end: Datatable -->
   </div>
</div> </div>
<!-- end:content -->
@stop
@push('scripts')
<script type="text/javascript" src="{{asset('js/SiteConfigurationFormValidation.js')}}"></script>
<script type="text/javascript" src="https://cdn.datatables.net/plug-ins/1.10.9/api/fnFilterClear.js"></script>
<script>
 var editor;
// Enable RowReorder by defaul
   $(function() {
      /* var siteconfig_table = $('#m_table_2').DataTable({
        lengthMenu: [ [5, 10, 25, 50, -1], [5, 10, 25, 50, 'All'] ],
        //lengthMenu:{{env('PAGE_LIST')}},
           processing: true,
           serverSide: true,
           order: [ [0, 'desc'] ],
           ajax: '{!! url('blog-management/data') !!}',
           columns: [
               { data: 'id', name: 'id' },
               { data: 'blog_title', name: 'blog_title' , "class" : "class001",},
               { data: 'blog_description', name: 'blog_description' , "class" : "class001",},
               { data: 'status', name: 'status'},
               { data: 'action', name: 'action', orderable: false, searchable: false}
           ]
       });
   });/*

   //  var html = $(".odd").html();
   // html = html.substring(0, 4) + "<br>" + html.substring(4);
   // $("#wine-name").html(html);
</script>
<script>
   $(function() {
       var siteconfig_table = $('#m_table_2_comment').DataTable({
        lengthMenu: [ [5, 10, 25, 50, -1], [5, 10, 25, 50, 'All'] ],
        //lengthMenu:{{env('PAGE_LIST')}},
           processing: true,
           serverSide: true,
           order: [ [0, 'desc'] ],
           ajax: '{!! url('blog-category-master/data') !!}',
           columns: [
               { data: 'id', name: 'id' },
               { data: 'category_name', name: 'category_name' , "class" : "class001",},
               { data: 'status', name: 'status'},
               { data: 'action', name: 'action', orderable: false, searchable: false}
           ]
       });
       /*$('<label style="margin-left: 10px;">Filter by ' +
           '<select class="" id="siteconfig_status">'+
               '<option value="">Select status</option>'+
               '<option value="Y">Active</option>'+
               '<option value="N">Inactive</option>'+
           '</select>' +
           '</label>').appendTo("#m_table_2_wrapper #m_table_2_length");

    $('#siteconfig_status').on('change', function(){
      var filter_value = $(this).val();
        var new_url = "{!! url('site-configuration/data')!!}"+'/'+filter_value;
        siteconfig_table.ajax.url(new_url).load();
    });*/

       // $('#reset').on('click',function(e){
       //     e.preventDefault();
       //     $('#siteconfig_status').val('');
       //     $('#siteconfig_status').trigger('change');
       //     $('#m_table_2').DataTable().search('').draw();
        // });

   });

   //  var html = $(".odd").html();
   // html = html.substring(0, 4) + "<br>" + html.substring(4);
   // $("#wine-name").html(html);
</script>
@endpush

