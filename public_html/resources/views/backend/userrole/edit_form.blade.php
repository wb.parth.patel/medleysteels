@extends('layouts_backend.masters',['title'=>'Userrole Management'])
@section('content')
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
             <h3 class="kt-subheader__title">User Role Management</h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="{{ route('dashboard') }}" class="kt-subheader__breadcrumbs-home"><i class="fa fa-home"></i></a>
                                            <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="{{ route('userrole.index') }}" class="kt-subheader__breadcrumbs-link">User Role Management</a>
                        <a href="" class="kt-subheader__breadcrumbs-link">-</a>
                        <a class="kt-subheader__breadcrumbs-link">Edit</a>
                                        <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
                </div>
      </div>
    </div>
</div>
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

		<!--begin::Portlet-->
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						Edit User role
					</h3>
				</div>
				<div class="kt-portlet__head-toolbar">
          		<div class="kt-portlet__head-actions">
                    <a href="" class="btn btn-brand btn-elevate btn-icon-sm btn-bold">
                        {{--  <a href="{{ url('userrole/index') }}" class="btn btn-brand btn-elevate btn-icon-sm btn-bold">  --}}
              		Back
            		</a>
          		</div>
        		</div>
			</div>
			<!-- <div class="back-btn">
                <a href=""><button type="button" class="btn btn-primary">Back</button></a>
                {{--  <a href="{{url('userrole/index')}}"><button type="button" class="btn btn-primary">Back</button></a>  --}}
			</div> -->

            <!--begin::Form-->
            <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" method="post"
				action="" name="userrole_form" id="userrole_form">
			{{--  <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" method="post"
				action="{{ url('userrole/update',$role_value->id)}}" name="userrole_form" id="userrole_form">  --}}
			{{--  @csrf
			@method('PATCH')  --}}
			<div class="kt-portlet__body">
					<div class="form-group row">
						<div class="col-lg-6">
							<label>Role: </label>
							<input type="text" class="form-control" name="name" maxlength="25" placeholder="Enter user role name"  value="{{$role_value->name}}">
							<span class="kt-form__help error name"></span>
						</div>
						<div class="col-lg-6">
							<label for="">Module and Actions:</label>
							<div class="kt-checkbox-inline">
								{{--  @foreach($module_array as $key=>$val)  --}}
								<div class="module_action_checkbox">
								<div class="module_parent">
								<label class="kt-checkbox kt-checkbox--solid kt-checkbox--state-brand bold">
                                     <input type="checkbox" name='module[]' class='module_class' data-check='false' data-class="action_click_" id="module_click" value="">
                                     {{--  <input type="checkbox" name='module[]' class='module_class' data-check='false' data-class="action_click_{{$key}}" id="module_click" value="{{$val['module_id']}}">{{$val['menu_name']}}  --}}
									<span></span>
								</label>
								</div>
									@foreach($val['action'] as $k=>$value)
									<div class="action_child">
									 <label class="kt-checkbox kt-checkbox--solid kt-checkbox--state-success">
                                         <input type="checkbox" name="action[]" class="action_click_ action_" data-class="action_click_" value="" >
                                         {{--  <input type="checkbox" name="action[]" class="action_click_{{$key}} action_" data-class="action_click_{{$key}}" value="{{$val['module_id'].'_'.$value['action_id']}}" {{$value['status']}} >{{$value['action_name']}}  --}}
									 	<span></span>
									 </label>
									 </div>
									{{--  @endforeach  --}}
								</div>
									<br>
								{{--  @endforeach  --}}
								<span class="kt-form__help error module"></span>
							</div>
						</div>
					</div>
	        </div>
	            <div class="kt-portlet__foot">
					<div class="kt-form__actions">
						<div class="row">
							<div class="col-lg-6">
								<button type="submit" name="city_submit" id="city_submit"  class="btn btn-primary">Save</button>
								<!-- <button type="reset" class="btn btn-secondary">Cancel</button> -->
								<a href="">
                                    <button type="button" class="btn btn-secondary">Cancel</button>
                                </a>
							</div>
						</div>
					</div>
				</div>
			</form>
			<!--end::Forkt-->
		</div>
	</div>
@endsection
@push('scripts')
<script type="text/javascript" src="{{asset('js/UserroleFormValidation.js')}}"></script>
@endpush
