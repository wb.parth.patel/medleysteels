@extends('layouts_backend.masters',['title'=>'Country Management'])
@section('content')

<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" media="all" rel="stylesheet" type="text/css"/>

<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
       <div class="kt-subheader__main">
             <h3 class="kt-subheader__title">Country Management</h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="{{ route('dashboard') }}" class="kt-subheader__breadcrumbs-home"><i class="fa fa-home"></i></a>
                                            <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="{{ route('country.index') }}" class="kt-subheader__breadcrumbs-link">Country Management</a>
                        <a href="" class="kt-subheader__breadcrumbs-link">-</a>
                        <a class="kt-subheader__breadcrumbs-link">Edit</a>
                                        <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
                </div>
      </div>
    </div>
</div>
<!-- end:: Subheader -->

<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
	<div class="row">
		<div class="col-lg-12">
		<!--begin::Portlet-->
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						Edit Country
					</h3>
				</div>
				<div class="kt-portlet__head-toolbar">
          		<div class="kt-portlet__head-actions">
            		<a href="{{ url('country/index') }}" class="btn btn-brand btn-elevate btn-icon-sm btn-bold">
              		Back
            		</a>
          		</div>
        		</div>
			</div>
			<!--begin::Form-->
			<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" method="post" 
				action="{{ route('country.update',$category_data->id) }}" name="country_form" id="country_form" enctype="multipart/form-data">
			@csrf
			@method('PATCH')
			<div class="kt-portlet__body">
					<div class="form-group row">
						<div class="col-lg-6">
							<label>Country Name:</label>

							<input type="text" class="form-control" name="name" maxlength="65" placeholder="Enter country name" value="{{$category_data['name']}}">
							<span class="kt-form__help error name"></span>
						</div>
						</div>
						
					</div>
	        
	            <div class="kt-portlet__foot">
					<div class="kt-form__actions">
						<div class="row">
							<div class="col-lg-6">
								<button type="submit" name="city_submit" id="city_submit"  class="btn btn-primary">Save</button>
								<!-- <button type="reset" class="btn btn-secondary">Cancel</button> -->
								<a href="{{ route('country.index') }}">
                                    <button type="button" class="btn btn-secondary">Cancel</button>
                                </a>
							</div>
						</div>
					</div>
				</div>
			</form>
			<!--end::Form-->
		</div>
	</div>
</div>
</div>

@endsection
@push('scripts')
<script type="text/javascript" src="{{asset('js/CountryFormValidation.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/js/fileinput.js" type="text/javascript"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/themes/fa/theme.js" type="text/javascript"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" type="text/javascript"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" type="text/javascript"></script>

<script type="text/javascript" src="https://cdn.datatables.net/plug-ins/1.10.9/api/fnFilterClear.js"></script>
@endpush