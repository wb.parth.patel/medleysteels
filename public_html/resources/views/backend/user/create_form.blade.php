@extends('layouts_backend.masters',['title'=>'User Management'])
@section('content')

<style type="text/css">
	.file-drop-zone-title{
		display: none;
	}
	.fileinput-remove{
		display: none;
	}
	.file-footer-buttons{
		display: none;
	}
</style>


<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" media="all" rel="stylesheet" type="text/css"/>


<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
             <h3 class="kt-subheader__title">User Management</h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="{{ route('dashboard') }}" class="kt-subheader__breadcrumbs-home"><i class="fa fa-home"></i></a>
                                            <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="{{ route('user.index') }}" class="kt-subheader__breadcrumbs-link">User Management</a>
                        <a href="" class="kt-subheader__breadcrumbs-link">-</a>
                        <a class="kt-subheader__breadcrumbs-link">Add</a>
                                        <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
                </div>
      </div>
    </div>
</div>
<!-- end:: Subheader -->

<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
		<div class="row">
	<div class="col-lg-12">
		<!--begin::Portlet-->
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						Add User
					</h3>
				</div>
			</div>
			<!--begin::Form-->
			{{--  <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" method="post"
                action="{{ url('user/store')}}" name="user_form" id="user_form" enctype="multipart/form-data">  --}}

                <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" method="post"
				action="" name="user_form" id="user_form" enctype="multipart/form-data">
			{{--  @csrf
			@method('post')  --}}
			<div class="kt-portlet__body">
					<div class="form-group row">
						<div class="col-lg-6">
							<label>Name :<span class="text-danger font-weight-bold">*</span> </label>
							<input type="text" class="form-control txtOnly" name="name" maxlength="25" placeholder="Enter name">
							<span class="kt-form__help error name"></span>
						</div>
						<div class="col-lg-6">
							<label>Email : <span class="text-danger font-weight-bold">*</span></label>
							<input type="text" maxlength='60' class="form-control" name="email" placeholder="Enter email">
							<span class="kt-form__help error email"></span>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-6">
							<label>Contact Number : </label>
							<!-- <div class="input-group-prepend"><span class="input-group-text"><i class="la la-phone"></i></span></div> -->
							<input type="text" class="form-control " name="contact_number" placeholder="Enter contact number">
							<span class="kt-form__help error contact_number"></span>
						</div>
						<div class="col-lg-6">
							<label>User Role : <span class="text-danger font-weight-bold">*</span></label>
							<select class="form-control kt-input" name="user_role_id">
								<option value="">Select User Role</option>
								{{--  @foreach($userrole as $val)  --}}
                                <option value=""></option>
                                {{--  <option value="{{$val['id']}}">{{$val['name']}}</option>  --}}
								{{--  @endforeach  --}}
							</select>
							<span class="kt-form__help error user_role_id"></span>
						</div>
						<!-- <div class="col-lg-6">
							<label class="">Date Of Birth : </label>
							<div class="input-group date">
									<input type="text" class="form-control" name="dob" readonly placeholder="Select date" id="kt_datepicker_1"/>
									<div class="input-group-append">
										<span class="input-group-text">
											<i class="fa fa-calendar"></i>
										</span>
									</div>
								</div>
								<span class="kt-form__help error dob"></span>
						</div>	 -->
					</div>
					<div class="form-group row">
						<div class="col-lg-6">
							<label>User Profile : <span class="text-danger font-weight-bold">*</span></label>
							<input id="file-1" type="file" name="image" class="file" data-overwrite-initial="false" data-min-file-count="1">
							<span class="kt-form__help error user_image"></span>
						</div>
					</div>

	        </div>
	            <div class="kt-portlet__foot">
					<div class="kt-form__actions">
						<div class="row">
							<div class="col-lg-6">
								<button type="submit" name="submit" id="submit"  class="btn btn-primary">Save</button>
								<!-- <button type="reset" class="btn btn-secondary">Cancel</button> -->
                                <a href="">
                                    {{--  <a href="{{url('user/index')}}">  --}}
                                    <button type="button" class="btn btn-secondary">Cancel</button>
                                </a>
							</div>
						</div>
					</div>
				</div>
			</form>
			<!--end::Form-->
		</div>
	</div>
</div>
</div>
@endsection
@push('scripts')
  <!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6/jquery.min.js" type="text/javascript"></script> -->
<script type="text/javascript" src="{{asset('js/UserFormValidation.js')}}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/js/fileinput.js" type="text/javascript"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/themes/fa/theme.js" type="text/javascript"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" type="text/javascript"></script>

<script src="{{asset('assets/js/pages/crud/forms/widgets/bootstrap-datepicker.js')}}" type="text/javascript"></script>
<!--
<script src="{{asset('plugins/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"  type="text/javascript"></script> -->
<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" type="text/javascript"></script> -->


<script type="text/javascript">

        $("#file-1").fileinput({
            theme: 'fa',
			showUpload:false,
			showRemove:false,
            uploadUrl: "/image-view",
            uploadExtraData: function() {
                return {
                    _token: $("input[name='_token']").val(),
                };
            },
            remove :false,
            allowedFileExtensions: ['jpg','jpeg', 'png', 'gif'],
            overwriteInitial: false,
            maxFileSize:2000,
            maxFilesNum: 1,
        });

        $(document).ready(function () {
		//called when key is pressed in textbox
			$(".contact_number").keypress(function (e) {
				//if the letter is not digit then display error and don't type anything
				if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
					//display error message
					$(".contact_number").html("Please Enter Digits Only").show().fadeOut(5000);
					return false;
				}
			});

            $('.txtOnly').bind('keyup blur',function(){
				    var node = $(this);
				    node.val(node.val().replace(/[^A-Za-z_\s]/,'') ); }
				);
        });
    </script>
@endpush
