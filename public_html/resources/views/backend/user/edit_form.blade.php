@extends('layouts_backend.masters',['title'=>'User Management'])
@section('content')

<style type="text/css">
	.file-drop-zone-title{
		display: none;
	}
	.fileinput-remove{
		display: none;
	}
	.file-footer-buttons{
		display: none;
	}
</style>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" media="all" rel="stylesheet" type="text/css"/>

<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
             <h3 class="kt-subheader__title">User Management</h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="{{ route('dashboard') }}" class="kt-subheader__breadcrumbs-home"><i class="fa fa-home"></i></a>
                                            <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="{{ route('user.index') }}" class="kt-subheader__breadcrumbs-link">User Management</a>
                         <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a class="kt-subheader__breadcrumbs-link">Edit</a>
                                        <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
                </div>
      </div>
    </div>
</div>
<!-- end:: Subheader -->

<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
		<div class="row">
	<div class="col-lg-12">
		<!--begin::Portlet-->
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						Edit User
					</h3>
				</div>
				<div class="kt-portlet__head-toolbar">
          		<div class="kt-portlet__head-actions">
                    {{--  <a href="{{ url('user/index') }}" class="btn btn-brand btn-elevate btn-icon-sm btn-bold">  --}}
                    <a href="" class="btn btn-brand btn-elevate btn-icon-sm btn-bold">
              		Back
            		</a>
          		</div>
        		</div>
			</div>
			<!--begin::Form-->
			{{--  <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" method="post"
                action="{{ url('user/update',$user->id)}}" name="user_form" id="user_form" enctype="multipart/form-data">  --}}

                <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" method="post"
				action="" name="user_form" id="user_form" enctype="multipart/form-data">
			{{--  @csrf
			@method('PATCH')  --}}
			<div class="kt-portlet__body">
					<div class="form-group row">
						<div class="col-lg-6">
							<label>Name : </label>
							<input type="text" class="form-control txtOnly" name="name" maxlength="25" placeholder="Enter name" value="{{$user['name']}}">
							<span class="kt-form__help error name"></span>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-6">
							<label>Email : </label>
							<input type="text" maxlength='60' class="form-control" name="email" placeholder="Enter email" value="{{$user['email']}}" readonly="">
							<span class="kt-form__help error email"></span>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-6">
							<label>Contact Number : </label>
							<input type="number" maxlength='10' class="form-control contact_number" name="contact_number" placeholder="Enter contact_number" value="{{$user['contact_number']}}" readonly="">
							<span class="kt-form__help error contact_number"></span>
						</div>


						<div class="col-lg-6">
								<label>Role</label>
								<select name="user_role_id" id='kt_select2_1' class="form-control kt-select2">
									<option hidden='' value="">Select user role</option>
										{{--  @foreach($userrole as $key=>$val)  --}}
										{{--  @if($val['id']==$user['user_role_id'])  --}}
										<option value="" selected></option>
										{{--  @else  --}}
										<option value=""></option>
										{{--  @endif
										@endforeach  --}}
								</select>
								<span class="m-form__help error user_role_id"></span>
							</div>
						<!-- <div class="col-lg-6">
							<label class="">Date Of Birth : </label>
							<div class="input-group date">
									<input type="text" class="form-control" name="dob" readonly placeholder="Select date" id="kt_datepicker_1"  value="{{$user['dob']}}"/>
									<div class="input-group-append">
										<span class="input-group-text">
											<i class="la la-calendar-check-o"></i>
										</span>
									</div>
								</div>
								<span class="m-form__help error dob"></span>
						</div> -->
					</div>
					<div class="form-group row">
						<div class="col-lg-6">
							<label>User Profile : </label>
							<input id="file-1" type="file" name="image" class="file" data-min-file-count="1"  value="{{url('/media/userimage',$user['image'])}}">
							<span class="kt-form__help error user_image"></span>
						</div>
					</div>

	        </div>
	            <div class="kt-portlet__foot">
					<div class="kt-form__actions">
						<div class="row">
							<div class="col-lg-6">
								<button type="submit" name="city_submit" id="city_submit"  class="btn btn-primary">Save</button>
								<!-- <button type="reset" class="btn btn-secondary">Cancel</button> -->
                                {{--  <a href="{{url('user/index')}}">  --}}
                                    <a href="">
                                    <button type="button" class="btn btn-secondary">Cancel</button>
                                </a>
							</div>
						</div>
					</div>
				</div>
			</form>
			<!--end::Form-->
		</div>
	</div>
</div>
</div>
@endsection
@push('scripts')
<script type="text/javascript" src="{{asset('js/UserFormValidation.js')}}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/js/fileinput.js" type="text/javascript"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/themes/fa/theme.js" type="text/javascript"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" type="text/javascript"></script>


<script type="text/javascript">
        $("#file-1").fileinput({
            theme: 'fa',
			showUpload:false,
			showRemove:false,
            uploadUrl: "/image-view",
            uploadExtraData: function() {
                return {
                    _token: $("input[name='_token']").val(),
                };
            },
            remove :false,
            allowedFileExtensions: ['jpg','jpeg', 'png', 'gif'],
            initialPreview:"{{url('media/userimage',$user->image)}}",
            initialPreviewAsData:true,
            overwriteInitial: true,
            maxFileSize:2000,
            maxFilesNum: 1,
        });
          $('.txtOnly').bind('keyup blur',function(){
				    var node = $(this);
				    node.val(node.val().replace(/[^A-Za-z_\s]/,'') ); }
				);
    </script>

<script src="{{asset('assets/js/pages/crud/forms/widgets/bootstrap-datepicker.js')}}" type="text/javascript"></script>

@endpush
