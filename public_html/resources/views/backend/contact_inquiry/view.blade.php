@extends('layouts_backend.masters', ['title'=>'Contact Inquiry'])
@section('content')
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
             <h3 class="kt-subheader__title">Contact Inquiry</h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="{{ route('dashboard') }}" class="kt-subheader__breadcrumbs-home"><i class="fa fa-home"></i></a>
                                            <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="{{ route('contact-inquiry.index') }}" class="kt-subheader__breadcrumbs-link">Contact Inquiry</a>
                        <a href="" class="kt-subheader__breadcrumbs-link">-</a>
                        <a class="kt-subheader__breadcrumbs-link">View</a>
                                        <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
                </div>
      </div>
    </div>
</div>
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

    <div class="row">
  <div class="col-lg-12">
    <!--begin::Portlet-->
    <div class="kt-portlet">
      <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
          <h3 class="kt-portlet__head-title">
            View Contact Inquiry
          </h3>
        </div>
        <div class="kt-portlet__head-toolbar">
          <div class="kt-portlet__head-actions">
            <a href="{{ url('contact-inquiry/index') }}" class="btn btn-brand btn-elevate btn-icon-sm btn-bold">
              Back
            </a>
          </div>
        </div>
      </div>
      <!--begin::Form-->
<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" method="post"
        action="{{ url('user/store')}}" name="cms_form" id="cms_form">
        @csrf
        <!-- <div class="m-portlet__body"> -->
          <div class="table-responsive table-all">
            <table class="table table-striped">
              <tr>
                <th style="width: 40%;">Name</th>
                <td></td>
              </tr>
              <tr>
                <th style="width: 40%;">Email</th>
                <td></td>
              </tr>
              <tr>
                <th style="width: 40%;">Contact Number</th>
                <td></td>
              </tr>
              <tr>
                <th style="width: 40%;">Queries</th>
                <td></td>
              </tr>
              <tr>
                <th style="width: 40%;">Message</th>
                <td></td>
              </tr>
            </table>
          </div>
          <!-- </div> -->
        </form>
      <!--end::Form-->
    </div>
  </div>
</div>

</div>
@stop
@push('scripts')
<script type="text/javascript" src="{{asset('js/UserFormValidation.js')}}"></script>
<script src="{{asset('vendors/jquery/dist/jquery.js')}}" type="text/javascript"></script>
<script src="{{asset('vendors/popper.js/dist/umd/popper.js')}}" type="text/javascript"></script>
<script src="{{asset('vendors/bootstrap/dist/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('vendors/js-cookie/src/js.cookie.js')}}" type="text/javascript"></script>
<script src="{{asset('vendors/moment/min/moment.min.js')}}" type="text/javascript"></script>
<script src="{{asset('vendors/tooltip.js/dist/umd/tooltip.min.js')}}" type="text/javascript"></script>
<script src="{{asset('vendors/perfect-scrollbar/dist/perfect-scrollbar.js')}}" type="text/javascript"></script>
<script src="{{asset('vendors/wnumb/wNumb.js')}}" type="text/javascript"></script>
<script src="{{asset('vendors/summernote/dist/summernote.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/demo/custom/crud/forms/widgets/summernote.js')}}" type="text/javascript"></script>
@endpush
