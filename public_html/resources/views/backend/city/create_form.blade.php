@extends('layouts_backend.masters',['title'=>'City Management'])
@section('content')
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
       <div class="kt-subheader__main">
             <h3 class="kt-subheader__title">City Management</h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="{{ route('dashboard') }}" class="kt-subheader__breadcrumbs-home"><i class="fa fa-home"></i></a>
                                            <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="{{ route('city.index') }}" class="kt-subheader__breadcrumbs-link">City Management</a>
                         <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a class="kt-subheader__breadcrumbs-link">Add</a>
                                        <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
                </div>
      </div>
    </div>
</div>
<!-- end:: Subheader -->

<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
		<div class="row">
	<div class="col-lg-12">
		<!--begin::Portlet-->
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						Add City
					</h3>
				</div>
			</div>
			<!--begin::Form-->
			<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" method="post"
				action="{{ url('city/store')}}" name="city_form" id="city_form">
				<input type="hidden" name="ajax_route" id="ajax_route" value="{{ROUTE('city/get-state-list','/')}}">
				@csrf
				@method('POST')
				<div class="kt-portlet__body">
					<div class="form-group row">
						<div class="col-lg-6">
							<label>Country : </label>
							<select class="form-control kt-input" name="country_id" id="country_id">
								<option value="">Select Country</option>
								@foreach($country as $val)
								<option value="{{$val['id']}}">{{$val['name']}}</option>
								@endforeach
							</select>
							<span class="kt-form__help error country_id"></span>

						</div>

						<div class="col-lg-6">
							<label>State : </label>
							<select class="form-control kt-input" name="state_id" id="state_id">
								<option value="" name="state">Select State</option>
							</select>
							<span class="kt-form__help error state_id"></span>

						</div>
					</div>

						<div class="form-group row">

						<div class="col-lg-12">
							<label>Name:</label>
							<input type="text" class="form-control" name="city_name" maxlength="65" placeholder="Enter city name">
							<span class="kt-form__help error city_name"></span>
						</div>
						</div>

	            </div>
	            <div class="kt-portlet__foot">
					<div class="kt-form__actions">
						<div class="row">
							<div class="col-lg-6">
								<button type="submit" name="city_submit" id="city_submit"  class="btn btn-primary">Save</button>
								<!-- <button type="reset" class="btn btn-secondary">Cancel</button> -->
								<a href="{{url('city/index')}}">
                                    <button type="button" class="btn btn-secondary">Cancel</button>
                                </a>
							</div>
						</div>
					</div>
				</div>
			</form>
			<!--end::Form-->
		</div>
	</div>
</div>
</div>
@endsection
@push('scripts')
<script type="text/javascript">
	$('#country_id').on('change',function(){
        var country=$('#country_id').val();
        $('#state_id').html("<option hidden value=''>Select State</option>");
        $.ajax({

            url  :$('#ajax_route').val()+'/'+country,
            type :'GET',
            dataType:'json',
            async:false,
            success : function ( data )
            {
                $.each(data, function (key, value) {
                    $('#state_id').append("<option value='"+value.id+"'>"+value.state_name+"</option>");
                });
            },
            error: function( json )
            {
                alert('error');
            }

        });
    });
</script>
<script type="text/javascript" src="{{asset('js/CityFormValidation.js')}}"></script>
@endpush
