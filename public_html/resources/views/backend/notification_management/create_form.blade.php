@extends('layouts_backend.masters',['title'=>'Notification Management'])
@section('content')

<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" media="all" rel="stylesheet" type="text/css"/>

<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
             <h3 class="kt-subheader__title">Notification Management</h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="{{ route('dashboard') }}" class="kt-subheader__breadcrumbs-home"><i class="fa fa-home"></i></a>
                                            <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="{{ route('notification-management.index') }}" class="kt-subheader__breadcrumbs-link">Notification Management</a>
                        <a href="" class="kt-subheader__breadcrumbs-link">-</a>
                        <a class="kt-subheader__breadcrumbs-link">Add</a>
                                        <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
                </div>
      </div>
    </div>
</div>
<!-- end:: Subheader -->

<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
		<div class="row">
	<div class="col-lg-12">
		<!--begin::Portlet-->
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						Add Notification
					</h3>
				</div>
			</div>
			<!--begin::Form-->
			<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" method="post" 
				action="{{ url('notification-management/store')}}" name="notification_form" id="notification_form">
			@csrf
			@method('post')
			<div class="kt-portlet__body">
					<div class="form-group row">
						<div class="col-lg-12">
							<label>Title : </label>
							<input type="text" class="form-control" name="title" maxlength="25" placeholder="Enter notification title">
							<span class="kt-form__help error title"></span>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-12">
							<label class="">Description :</label>
							<textarea class="form-control" name="description" maxlength="250" placeholder="Enter notification description"></textarea>
							<span class="kt-form__help error description"></span>
						</div>	
					</div>
					<div class="form-group m-form__group row">
						<div class="col-lg-12">
							<label for="">Notification Type</label>
							<select name="notification_type" id="m-input" class="form-control m-input" >
								<option value="">Select notification type</option>
								<option value="user_specific" name="user_specific">User Specific</option>
								<option value="user" name="user">User </option>
								<option value="all" name="all">All</option>
								
							</select>
						<!-- 0=user_specific,1=user,2=all -->
						<span class="kt-form__help error notification_type"></span>
					</div>
				</div>
				<div class="m-portlet__body">
					<div class="form-group m-form__group row">
						<div class="col-lg-12">
						<label>Notification To:</label>
						<select name="notification_to[]" id="m_select2_5" class="form-control m-input">
							<option value="">Select user name</option>
							@foreach($user as $val)
							<option value="{{$val['id']}}">{{$val['name']}}</option>
							@endforeach
						</select>
						<span class="kt-form__help error notification_to"></span>
					</div>
				</div>

	        </div>
	            <div class="kt-portlet__foot">
					<div class="kt-form__actions">
						<div class="row">
							<div class="col-lg-12">
								<button type="submit" name="notification_form" id="notification_form"  class="btn btn-primary">Save</button>
								<!-- <button type="reset" class="btn btn-secondary">Cancel</button> -->
								<a href="{{url('notification-management/index')}}">
                                    <button type="button" class="btn btn-secondary">Cancel</button>
                                </a>
							</div>
						</div>
					</div>
				</div>
			</form>
			<!--end::Form-->
		</div>
		
@endsection
@push('scripts')
<script type="text/javascript" src="{{asset('js/NotificationFormValidation.js')}}"></script>
<script src="https://code.jquery.com/jquery-latest.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/js/fileinput.js" type="text/javascript"></script>
<script src="{{asset('assets/js/pages/crud/forms/widgets/select2.js')}}" type="text/javascript"></script>

<script src="{{asset('assets/plugins/general/select2/dist/js/select2.full.js')}}" type="text/javascript"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/themes/fa/theme.js" type="text/javascript"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" type="text/javascript"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" type="text/javascript"></script>




@endpush