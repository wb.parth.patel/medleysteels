@extends('layouts_backend.masters',['title'=>'Module Management'])
@section('content')
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" media="all" rel="stylesheet" type="text/css"/>

<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">Module</h3>

				<span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                     <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="{{ route('dashboard') }}" class="kt-subheader__breadcrumbs-link">
                            Dashboard</a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="{{ ROUTE('module.index') }}" class="kt-subheader__breadcrumbs-link">Module Management</a>
                </div>
                    </div>
    </div>
</div>
<!-- end:: Subheader -->

<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
		<div class="row">
	<div class="col-lg-12">
		<!--begin::Portlet-->
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						Edit Module
					</h3>
				</div>
			</div>
				<!--begin::Form-->
				<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" method="post" 
				action="{{ url('module/update',$module_model->id)}}" name="module_form" id="module_form">
					@csrf
					@method('PATCH')
					<div class="kt-portlet__body">
					<div class="form-group row">
						<div class="col-lg-6">
								<label>Module Name:</label>
								<input type="text" class="form-control m-input" name="module_name" maxlength='25' placeholder="Enter module name" value="{{$module_model->module_name}}">
								<span class="kt-form__help error module_name"></span>
							</div>
							<div class="col-lg-6">
								<label>Menu Name:</label>
								<input type="text" class="form-control m-input" name="menu_name" maxlength='25' placeholder="Enter module name" value="{{$module_model->menu_name}}">
								<span class="kt-form__help error menu_name"></span>
							</div>
							<div class="col-lg-6">
								<label>Module Icon</label>
								<input type="text" class="form-control m-input" name="icon" maxlength='60' placeholder="Enter module icon" value="{{$module_model->icon}}">
								<span class="kt-form__help error icon"></span>
							</div>
						</div>
						<div class="kt-form__group form-group">
							<label for="">Action:</label>
							<div class="kt-checkbox-inline">
								
								@foreach($action as $key=>$val)
								<label class="kt-checkbox">

								@if(in_array($val->id,$id))
								<input type="checkbox" name='action[]' value="{{$val->id}}" checked>	
									{{$val->action_name}}
								 	<span></span>
								@else
								<input type="checkbox" name='action[]' value="{{$val->id}}">
									{{$val->action_name}}
								 	<span></span>
								@endif
								 
								 	
								 </label>
								@endforeach
							</div>
							<span class="kt-form__help error action"></span>
						</div>
					</div>
					 <div class="kt-portlet__foot">
					<div class="kt-form__actions">
							<div class="row">
								<div class="col-lg-6">
									<button type="submit" name="module_submit" id="module_submit" class="btn btn-primary">Save</button>
									<a href="{{url('module/index')}}">
									<button type="button" class="btn btn-secondary">Cancel</button>
									</a>
								</div>
							</div>
						</div>
					</div>
				</form>
				<!--end::Form-->
			</div>
		</div>
			<!--end::Portlet-->
	</div>
</div>
@endsection
@push('scripts')
<script type="text/javascript" src="{{asset('js/ModuleFormValidation.js')}}"></script>
@endpush