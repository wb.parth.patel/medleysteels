@extends('layouts_backend.masters',['title'=>'Module Management'])
@section('content')
<style type="text/css">
  .file-drop-zone-title{
    display: none;
  }
  .fileinput-remove{
    display: none;
  }
  .file-footer-buttons{
    display: none;
  }
</style>

<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" media="all" rel="stylesheet" type="text/css"/>

<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
             <h3 class="kt-subheader__title">Module Management</h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="{{ route('dashboard') }}" class="kt-subheader__breadcrumbs-home"><i class="fa fa-home"></i></a>
                                            <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="{{ route('module.index') }}" class="kt-subheader__breadcrumbs-link">Module Management</a>
                        <a href="" class="kt-subheader__breadcrumbs-link">-</a>
                        <a class="kt-subheader__breadcrumbs-link">Add</a>
                                        <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
                </div>
      </div>
    </div>
</div>
<!-- end:: Subheader -->

<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
		<div class="row">
	<div class="col-lg-12">
		<!--begin::Portlet-->
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						Add Module
					</h3>
				</div>
			</div>
			<!--begin::Form-->
			<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" method="post" 
			action="{{ url('module/store')}}" name="module_form" id="module_form">
			@csrf
				<div class="kt-portlet__body">
					<div class="form-group row">
						<div class="col-lg-6">
							<label>Module Name:</label>
							<input type="text" class="form-control" name="module_name" maxlength="55" placeholder="Enter module name">
							<span class="kt-form__help error module_name"></span>
						</div>
						<div class="col-lg-6">
							<label class="">Menu Name:</label>
							<input type="text" class="form-control" name="menu_name" placeholder="Enter menu name"max='55'>
							<span class="kt-form__help error menu_name"></span>
						</div>
						<div class="col-lg-6">
							<label class="">Module Icon:</label>
							<input type="text" class="form-control" name="icon" placeholder="Enter module icon"max='60'>
							<span class="kt-form__help error icon"></span>
						</div>
					</div>	 
					<div class="kt-form__group form-group">
						<label for="">Action:</label>
						<div class="kt-checkbox-inline">
								@foreach($action as $key=>$val)
								 <label class="kt-checkbox">
								 	<input type="checkbox" name='action[]' value="{{$val->id}}">{{$val->action_name}}
								 	<span></span>
								 </label>
								@endforeach

							</div>
							  <span class="kt-form__help error action"></span>
					</div>
	            </div>
	            <div class="kt-portlet__foot">
					<div class="kt-form__actions">
						<div class="row">
							<div class="col-lg-6">
								<button type="submit" name="submit" id="submit"  class="btn btn-primary">Save</button>
								<!-- <button type="reset" class="btn btn-secondary">Cancel</button> -->
								<a href="{{url('dashboard')}}">
                                    <button type="button" class="btn btn-secondary">Cancel</button>
                                </a>
							</div>
						</div>
					</div>
				</div>
			</form>
			<!--end::Form-->
		</div>
	</div>
</div>
</div>
@endsection
@push('scripts')
<script type="text/javascript" src="{{asset('js/ModuleFormValidation.js')}}"></script>
@endpush