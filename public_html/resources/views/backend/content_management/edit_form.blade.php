@extends('layouts_backend.masters',['title'=>'Content Management'])
@section('content')
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
             <h3 class="kt-subheader__title">Content Management</h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="{{ route('dashboard') }}" class="kt-subheader__breadcrumbs-home"><i class="fa fa-home"></i></a>
                                            <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="{{ route('content-management.index') }}" class="kt-subheader__breadcrumbs-link">Content Management</a>
                         <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a class="kt-subheader__breadcrumbs-link">Edit</a>
                                        <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
                </div>
      </div>
    </div>
</div>
<!-- end:: Subheader -->

<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
		<div class="row">
	<div class="col-lg-12">
		<!--begin::Portlet-->
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						Edit Content
					</h3>
				</div>
				<div class="kt-portlet__head-toolbar">
          <div class="kt-portlet__head-actions">
            <a href="{{ url('content-management/index') }}" class="btn btn-brand btn-elevate btn-icon-sm btn-bold">
              Back
            </a>
          </div>
        </div>
			</div>
			<!--begin::Form-->
			<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" method="post"
				action="" name="content_form" id="content_form">
				{{--  @csrf
				@method('PATCH')  --}}
				<div class="kt-portlet__body">
					<div class="form-group row">
						<div class="col-lg-6">
							<label>Enter Title:</label>
							<input type="text" class="form-control" name="title" maxlength="65" placeholder="Enter title name"  value="" readonly>
							<span class="kt-form__help error title"></span>
						</div>
						<div class="col-lg-12 col-md-9 col-sm-12">
							<label class="">Enter Content: </label>
							<textarea class="summernote" name="content"></textarea>
							<span class="kt-form__help error content"></span>
						</div>
					</div>
	            </div>
	            <div class="kt-portlet__foot">
					<div class="kt-form__actions">
						<div class="row">
							<div class="col-lg-6">
								<button type="submit" name="city_submit" id="city_submit"  class="btn btn-primary">Save</button>
								<!-- <button type="reset" class="btn btn-secondary">Cancel</button> -->
								<a href="{{url('content-management/index')}}">
                                    <button type="button" class="btn btn-secondary">Cancel</button>
                                </a>
							</div>
						</div>
					</div>
				</div>
			</form>
			<!--end::Form-->
		</div>
	</div>
</div>
</div>
@endsection
@push('scripts')
<script type="text/javascript" src="{{asset('js/ContentFormValidation.js')}}"></script>

<script src="{{asset('plugins/general/summernote/dist/summernote.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/js/pages/crud/forms/widgets/summernote.js')}}" type="text/javascript"></script>
@endpush
