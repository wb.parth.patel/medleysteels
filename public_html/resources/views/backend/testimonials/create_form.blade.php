@extends('layouts_backend.masters',['title'=>'Testimonials'])
@section('content')

<style type="text/css">
  .file-drop-zone-title{
    display: none;
  }
  .fileinput-remove{
    display: none;
  }
  .file-footer-buttons{
    display: none;
  }
  .was-validated .form-control:invalid, .form-control.is-invalid {
  	background-image: none;
  }
</style>

<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" media="all" rel="stylesheet" type="text/css"/>

<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-container  kt-container--fluid ">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">Testimonials</h3>
			<span class="kt-subheader__separator kt-hidden"></span>
			<div class="kt-subheader__breadcrumbs">
				<a href="{{ route('dashboard') }}" class="kt-subheader__breadcrumbs-home"><i class="fa fa-home"></i></a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="{{ route('testimonials.index') }}" class="kt-subheader__breadcrumbs-link">Testimonials</a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a class="kt-subheader__breadcrumbs-link">Add</a>
				<!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
			</div>
		</div>
	</div>
</div>
<!-- end:: Subheader -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
	<div class="row">
		<div class="col-lg-12">
			<!--begin::Portlet-->
			<div class="kt-portlet">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							Add Testimonials
						</h3>
					</div>
				</div>
				<!--begin::Form-->
				<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" method="post"
				action="" name="content_form" id="content_form" enctype="multipart/form-data">
				{{--  @csrf
				@method('POST')  --}}
				<div class="kt-portlet__body">
					<!-- <div class="form-group row">
						<input type="radio" name="tab" value="igotnone" onclick="show1();" checked="checked" /><b>Normal
						<input type="radio" name="tab" value="igottwo" onclick="show2();" /><b>Video
					</div> -->
					<div id="div1" class="hide">
						<div class="form-group row">
							<div class="col-lg-6">
								<label class="">Title:</label>
								<input type="text" class="form-control" name="title" placeholder="Enter Title" maxlength="30">
								<span class="kt-form__help error title"></span>
							</div>
							<div class="col-lg-6">
								<label class="">Description:</label>
								<textarea class="form-control" name="description"></textarea>
								<span class="kt-form__help error description"></span>
							</div>
						</div>
					<div class="form-group row">
						<div class="col-lg-6">
							<label class="">Author Name :</label>

							<input type="text" class="form-control" name="author_name" placeholder="Enter author name">
							<span class="kt-form__help error author_name"></span>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-6">
							<label>Testimonial Image : </label>
							<input id="file-1" type="file" name="image" class="file" data-overwrite-initial="false" data-min-file-count="1">
							<span class="kt-form__help error user_image"></span>
						</div>
					</div>
					</div>
					<!-- <div class="form-group row">
						<div class="col-lg-6">
							<label>Image : </label>
							<input id="file-1" type="file" name="image" class="file" data-overwrite-initial="false" data-min-file-count="1">
							<span class="kt-form__help error user_image"></span>
						</div>
					</div> -->
					<!-- <div class="form-group row">
						<div class="col-lg-12">
							<label class="">Image:</label>
							<input type="file" class="form-control" name="image">
							<span class="kt-form__help error image"></span>
						</div>
					</div> -->
					<!-- <div class="hide" id="div2">
						<div class="form-group row">
							<div class="col-lg-12">
								<label class="">Youtube Link</label>
								<input type="text" class="form-control" name="youtube_link" placeholder="Enter Youtube Link">
								<span class="kt-form__help error youtube_link"></span>
							</div>
						</div>
					</div> -->
					<br>
					<div class="form-group row">

					</div>
				</div>
				<div class="kt-portlet__foot">
					<div class="kt-form__actions">
						<div class="row">
							<div class="col-lg-6">
								<button type="submit" name="city_submit" id="city_submit"  class="btn btn-primary">Save</button>
								<!-- <button type="reset" class="btn btn-secondary">Cancel</button> -->
								<a href="">
									<button type="button" class="btn btn-secondary">Cancel</button>
								</a>
							</div>
						</div>
					</div>
				</div>
			</form>
			<!--end::Form-->
		</div>
	</div>
</div>
</div>
@endsection
@push('scripts')
<script type="text/javascript" src="{{asset('js/ContentFormValidation.js')}}"></script>

<script src="{{asset('plugins/general/summernote/dist/summernote.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/js/pages/crud/forms/widgets/summernote.js')}}" type="text/javascript"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/js/fileinput.js" type="text/javascript"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/themes/fa/theme.js" type="text/javascript"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" type="text/javascript"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" type="text/javascript"></script>

<script type="text/javascript" src="https://cdn.datatables.net/plug-ins/1.10.9/api/fnFilterClear.js"></script>

<script type="text/javascript">
    $("#file-1").fileinput({
        theme: 'fa',
		showUpload:false,
		showRemove:false,
        uploadUrl: "/image-view",
        uploadExtraData: function() {
            return {
                _token: $("input[name='_token']").val(),
            };
        },
        remove :false,
        allowedFileExtensions: ['jpg','jpeg', 'png', 'gif'],
        initialPreviewAsData:true,
        overwriteInitial: true,
        maxFileSize:2000,
        maxFilesNum: 1,
    });
</script>
 <script type="text/javascript">
 // 	document.getElementById('div2').style.display ='none';
 // 	function show1(){
 // 		document.getElementById('div1').style.display ='block';
 //  		document.getElementById('div2').style.display ='none';
	// }
	// function show2(){
 //  		document.getElementById('div2').style.display = 'block';
 //  		document.getElementById('div1').style.display ='none';
	// }
 </script>
@endpush
