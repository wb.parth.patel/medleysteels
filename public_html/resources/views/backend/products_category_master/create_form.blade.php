@extends('layouts_backend.masters',['title'=>'Product Category'])
@section('content')
<style type="text/css">
	.file-drop-zone-title{
		display: none;
	}
	.fileinput-remove{
		display: none;
	}
	.file-footer-buttons{
		display: none;
	}
	textarea {
		resize: none;
	}
	#count_message {
		background-color: smoke;
		margin-top: -20px;
		margin-right: 5px;
	}
</style>

<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" media="all" rel="stylesheet" type="text/css"/>

<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-container  kt-container--fluid ">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">Product Category</h3>
			<span class="kt-subheader__separator kt-hidden"></span>
	       	<div class="kt-subheader__breadcrumbs">
	       		<a href="{{ route('dashboard') }}" class="kt-subheader__breadcrumbs-home"><i class="fa fa-home"></i></a>
	       		<span class="kt-subheader__breadcrumbs-separator"></span>
	       		<a href="{{ route('product-category.index') }}" class="kt-subheader__breadcrumbs-link">Product Category</a>
	       		<span class="kt-subheader__breadcrumbs-separator"></span>
	       		<a class="kt-subheader__breadcrumbs-link">Add</a>
	       		<!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
	       	</div>
	       </div>
	   </div>
	</div>
<!-- end:: Subheader -->

<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
	<div class="row">
		<div class="col-lg-12">
			<!--begin::Portlet-->
			<div class="kt-portlet">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							Add Product Category
						</h3>
					</div>
				</div>
				<!--begin::Form-->
				<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="" name="content_form" id="content_form" method="post" enctype="multipart/form-data" >
					{{--  @csrf
					@method('POST')  --}}
					<div class="kt-portlet__body">
						<div class="form-group row">
							<div class="col-lg-6">
								<label>Product Category Name:</label>
								<input type="text" class="form-control" name="category_name" maxlength="65" placeholder="Enter Product Category Name">
								<span class="kt-form__help error category_name"></span>
							</div>
						</div>

						<div class="form-group row">
							<div class="col-lg-6">
								<label>Product Category Image : </label>
								<input  type="file" id="category_img" name="category_img" class="file" data-overwrite-initial="false" data-min-file-count="1" multiple="multiple" accept="image/*">
								<span class="kt-form__help error category_img"></span>

								{{-- <input id="file-1" type="file" name="image[]" class="file" data-overwrite-initial="false" data-min-file-count="1" multiple="multiple" accept="image/*">
 --}}
							</div>
						</div>
					</div>
					<div class="kt-portlet__foot">
						<div class="kt-form__actions">
							<div class="row">
								<div class="col-lg-6">
									<button type="submit" name="product_category_submit" id="product_category_submit"  class="btn btn-primary">Save</button>

									<a href="">
	                                    <button type="button" class="btn btn-secondary">Cancel</button>
	                                </a>
								</div>
							</div>
						</div>
					</div>
				</form>
				<!--end::Form-->
			</div>
		</div>
	</div>
</div>
@endsection
@push('scripts')
<script type="text/javascript" src="{{asset('js/ProductCategoryForm.js')}}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/js/fileinput.js" type="text/javascript"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/themes/fa/theme.js" type="text/javascript"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" type="text/javascript"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" type="text/javascript"></script>

<script type="text/javascript">

        $("#category_img").fileinput({
            theme: 'fa',
			showUpload:false,
			showRemove:false,
            uploadUrl: "/image-view",
            uploadExtraData: function() {
                return {
                    _token: $("input[name='_token']").val(),
                };
            },
            remove :false,
            allowedFileExtensions: ['jpg','jpeg', 'png'],
            overwriteInitial: false,
            maxFileSize:2000,
            maxFilesNum: 1,
        });



       	var text_max = 120;
		$('#count_message').html('0 / ' + text_max );

		$('#text').keyup(function() {
		var text_length = $('#text').val().length;
		var text_remaining = text_max - text_length;

		$('#count_message').html(text_length + ' / ' + text_max);
		});


    </script>
@endpush
