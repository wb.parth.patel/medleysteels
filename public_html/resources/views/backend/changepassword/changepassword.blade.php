@extends('layouts_backend.masters',['title'=>'Change Password'])
@section('content')

<div class="kt-subheader   kt-grid__item" id="kt_subheader">
  @if(session()->get('success'))
    <div class="col-5 pull-right">
    <div class="alert alert-success alert-dismissible fade show elementToFadeInAndOut" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    </button>
    {{ session()->get('success') }}
  </div>
  </div>
  @elseif(session()->get('error'))
  <div class="col-5 pull-right">
    <div class="alert alert-danger alert-dismissible fade show elementToFadeInAndOut" role="alert">

    {{ session()->get('error') }}
  </div>
  </div>
    @endif
   <div class="kt-container  kt-container--fluid ">
      <div class="kt-subheader__main">
             <h3 class="kt-subheader__title">Change Password</h3>
                <span class="kt-subheader__separator kt-hidden"></span>
                <div class="kt-subheader__breadcrumbs">
                    <a href="{{ route('dashboard') }}" class="kt-subheader__breadcrumbs-home"><i class="fa fa-home"></i></a>
                                            <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a class="kt-subheader__breadcrumbs-link">Change Password</a>
                                        <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
                </div>
      </div>
   </div>
</div>

<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
		<div class="row">
	<div class="col-lg-12">
		<!--begin::Portlet-->
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">
						Change Password
					</h3>
				</div>
			</div>
			<!--begin::Form-->
			<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" method="post"
                action="" name="changepassword_form" id="changepassword_form">
                    @csrf
                <input type="hidden" name="ajax_route" id='ajax_route' value="{{ROUTE('changePassword')}}">
				<div class="kt-portlet__body">
					<div class="form-group row">
						<div class="col-lg-6">
							<label>Current Password:</label>
							<input type="password" class="form-control" name="current_password" placeholder="Enter current password" id="current-password" maxlength="25">
							<span class="kt-form__help error current_password"></span>
						</div>
						<div class="col-lg-6">
							<label class="">New Password:</label>
							<input type="password" class="form-control" name="password" placeholder="Enter new password" id="new-password" maxlength="25">
							<span class="kt-form__help error password"></span>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-lg-6">
							<label>Confirm New Password:</label>
							<div class="kt-input-icon">
								<input type="password" class="form-control" name="password_confirmation" placeholder="Enter confirm new password" id="current-password" maxlength="25">
								<span class="kt-form__help error password_confirmation"></span>
							</div>
						</div>
					</div>
	            </div>
	            <div class="kt-portlet__foot">
					<div class="kt-form__actions">
						<div class="row">
							<div class="col-lg-6">
								<button type="submit" name="city_submit" id="city_submit"  class="btn btn-primary">Save</button>
								<!-- <button type="reset" class="btn btn-secondary">Cancel</button> -->
								{{--  <a href="{{url('dashboard')}}">  --}}
                                    <a href="">
                                    <button type="button" class="btn btn-secondary">Cancel</button>
                                </a>
							</div>
						</div>
					</div>
				</div>
			</form>
			<!--end::Form-->
		</div>
	</div>
</div>
</div>
@endsection
@push('scripts')
<script type="text/javascript" src="{{asset('js/ChangePasswordFormValidation.js')}}"></script>
@endpush
