<?php
namespace App\Http\Controllers;

use App\common_model\Dashboard;
use App\common_model\User;
use App\common_model\BlogManagement;
use Illuminate\Http\Request;
use Auth;
use DB;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$user_data = User::where(['is_active'=>'Y'])
                        ->where('id','!=',auth()->user()->id)
                        ->orderBy('id', 'DESC')
                        ->take(5)
                        ->get();

        $blog = BlogManagement::where(['is_active'=>'Y'])
                        ->orderBy('id', 'DESC')
                        ->take(5)
                        ->get();
        return view('backend.dashboard',['user_data'=>$user_data,'blog'=>$blog]);
    }

    public function all(){
        $user = User::where(['is_active'=>'Y'])
                        ->where('id','!=',auth()->user()->id)
                        ->count();

        $blog = BlogManagement::where(['is_active'=>'Y'])
                        ->count();

        $response=['total_user'=>$user,'total_blog'=>$blog,];
        return response()->json($response);

    }
    public function daily(){

        $user=User::where(DB::raw('date(created_date)'),'=',date('Y-m-d') )
                    ->where(['is_active'=>'Y'])
                    ->where('id','!=',auth()->user()->id)
                    ->count();

        $blog = BlogManagement::where(['is_active'=>'Y'])
                    ->where(DB::raw('date(created_date)'),'=',date('Y-m-d') )
                    ->count();

        $response=['total_user'=>$user,'total_blog'=>$blog,];
        return response()->json($response);

    }
    public function weekly(){

        $user=User::whereBetween(
                          DB::raw('date(created_date)'),
                          [DB::raw('DATE_ADD(CURDATE(),INTERVAL -7 DAY)'),DB::raw('CURDATE()')]
                         )
                    ->where(['is_active'=>'Y'])
                    ->where('id','!=',auth()->user()->id)
                    ->count();

        $blog = BlogManagement::where(['is_active'=>'Y'])
            ->whereBetween(
                          DB::raw('date(created_date)'),
                          [DB::raw('DATE_ADD(CURDATE(),INTERVAL -7 DAY)'),DB::raw('CURDATE()')]
                         )
            ->count();

        $response=['total_user'=>$user,'total_blog'=>$blog,];

        return response()->json($response);
    }
    public function monthly(){
        $user=User::whereBetween(
                          DB::raw('date(created_date)'),
                          [DB::raw('DATE_ADD(CURDATE(),INTERVAL -1 MONTH)'),DB::raw('CURDATE()')]
                         )
                    ->where(['is_active'=>'Y'])
                    ->where('id','!=',auth()->user()->id)
                    ->count();

        $blog = BlogManagement::where(['is_active'=>'Y'])
                ->count();

        $response=['total_user'=>$user,'total_blog'=>$blog,];

        return response()->json($response);
    }
    public function yearly(){
        $user=User::whereBetween(
                          DB::raw('date(created_date)'),
                          [DB::raw('DATE_ADD(CURDATE(),INTERVAL -1 YEAR)'),DB::raw('CURDATE()')]
                         )
                    ->where(['is_active'=>'Y'])
                    ->where('id','!=',auth()->user()->id)
                    ->count();

        $blog = BlogManagement::where(['is_active'=>'Y'])
            ->count();

        $response=['total_user'=>$user,'total_blog'=>$blog,];
        return response()->json($response);
    }
}
