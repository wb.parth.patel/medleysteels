<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Yajra\Datatables\Datatables;
use App\common_model\ServiceManagement;
use App\common_model\ContentManagement;
use App\common_model\MenuManagement;
use App\common_model\NewslatterMaster;
use App\common_model\SiteConfiguration;
use App\common_model\ContactInquiry;
use App\Http\Requests\ContactInquiryForm;
use App\Http\Requests\ContactUsFormFront;
use App\Http\Requests\NewslatterMasterForm;
 

use Mail;

 
use Response;

class ContactusController extends Controller
{
    public function index()
    {
    	$menu = MenuManagement::where(['is_active'=>'Y','type'=>'Website'])->get();
      $home = ContentManagement::where(['slug'=>'home'])->first();
      $contact_us = ContentManagement::where(['slug'=>'contact-us'])->first();
      /*Business hours (timing)*/
    //$Businesshours = SiteConfiguration::select('id', 'config_key','config_value')->where(['config_key' => 'MONDAY_TO_FRIDAY'])->first();
       $modayHours = SiteConfiguration::select('id', 'config_key','config_value')->where(['is_active' => 'Y'])->where(['config_key' => 'MONDAY_TO_FRIDAY'])->first();
    $saturdayHours= SiteConfiguration::select('id', 'config_key','config_value')->where(['is_active' => 'Y'])->where(['config_key' => 'SATURDAY'])->first();
     $sundayHours= SiteConfiguration::select('id', 'config_key','config_value')->where(['is_active' => 'Y'])->where(['config_key' => 'SUNDAY'])->first();
 
      $services = ServiceManagement::orderBy('id','desc')->where(['is_active'=>'Y'])->get();
      return view('frontend.contact',compact('services','menu','home','contact_us','modayHours','saturdayHours','sundayHours'));
  }

  


public function store(ContactUsFormFront $request)
{

  if ($request->ajax()) {
    return true;
}

$contact =new ContactInquiry();
  $contact->name=$request->name;
    $contact->email_id=$request->email_id;
    $contact->contact_number=$request->contact_number;
    $contact->queries=$request->queries;
    $contact->message=$request->message;
    $contact->created_date=date('Y-m-d H:i:s');
    $contact->updated_date=date('Y-m-d H:i:s');
   if($contact->save()){

  return redirect('/contact')->with('success','Thank you for your contact.');
}else{
    return redirect('/index')->with('error','Something Error: In data save!');
}
}


public function subscribe(NewslatterMasterForm $request)
{

     // $this->validate($request, [
     //        'email' => 'required|unique:newslatter_master,subscribe_email|regex:/^[\w-]+(?:\.[\w-]+)*@(?:[\w-]+\.)+[a-zA-Z]{2,7}$/',
     //    ],
     //    [
     //        'email.required' => 'Please enter email.',
     //        'email.unique' =>'This email address has already subscribed.',
     //    ]);

  if ($request->ajax()) {
    return true;
}

$subscribe =new NewslatterMaster();
$subscribe->subscribe_email=$request->subscribe_email;
$subscribe->created_date=date('Y-m-d H:i:s');

if($subscribe->save()){
 /*   $data = array('subscribe_email'=>$request->subscribe_email);
    Mail::send('subscribe_email', $data, function($message) use ($data){
      $message->from('smtp@xceltec.in','SUBSCRIBE');
      $message->subject("Everglades");
      $message->to($data['']);
  */

  return redirect('/index')->with('success','Thank you for your subscription.');
}else{
    return redirect('/index')->with('error','Something Error: In data save!');
}
}


} 