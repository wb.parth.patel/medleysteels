<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Yajra\Datatables\Datatables;
use App\common_model\ServiceManagement;
use App\common_model\ContentManagement;
use App\common_model\SiteConfiguration;
use App\common_model\MenuManagement;
use App\common_model\Testimonials;
use App\common_model\BlogCategoryMaster;
use Response;

class TestimonialController extends Controller
{
    public function index()
    {
    	$Testimonial = Testimonials::select('id', 'title', 'description', 'image','testimonial_type','author_name')->where(['is_active' => 'Y'])->orderBy('id', 'desc')->get();

        $services = ServiceManagement::orderBy('id','desc')->where(['is_active'=>'Y'])->get();
        $home = ContentManagement::where(['slug'=>'home'])->first();
        $modayHours = SiteConfiguration::select('id', 'config_key','config_value')->where(['is_active' => 'Y'])->where(['config_key' => 'MONDAY_TO_FRIDAY'])->first();
        $saturdayHours= SiteConfiguration::select('id', 'config_key','config_value')->where(['is_active' => 'Y'])->where(['config_key' => 'SATURDAY'])->first();
         $sundayHours= SiteConfiguration::select('id', 'config_key','config_value')->where(['is_active' => 'Y'])->where(['config_key' => 'SUNDAY'])->first();

        return view('frontend.testimonial',compact('services','Testimonial','home','modayHours','saturdayHours','sundayHours'));
    }
    public function details($id)
    {
        $menu = MenuManagement::where(['is_active'=>'Y','type'=>'Website'])->get();
        $blog = BlogManagement::select('blog_management.id','blog_management.blog_title','blog_management.blog_description','blog_management.image','blog_category_master.category_name')->orderBy('blog_management.id','desc')->leftjoin('blog_category_master','blog_category_master.id','blog_management.category_id')->where(['blog_management.id'=>$id])->first();
        $related_blog = BlogManagement::orderBy('id','desc')->where(['is_active'=>'Y'])->take('2')->get();
        $services = ServiceManagement::orderBy('id','desc')->where(['is_active'=>'Y'])->get();
        $home = ContentManagement::where(['slug'=>'home'])->first();
        $modayHours = SiteConfiguration::select('id', 'config_key','config_value')->where(['is_active' => 'Y'])->where(['config_key' => 'MONDAY_TO_FRIDAY'])->first();
        $saturdayHours= SiteConfiguration::select('id', 'config_key','config_value')->where(['is_active' => 'Y'])->where(['config_key' => 'SATURDAY'])->first();
        $sundayHours= SiteConfiguration::select('id', 'config_key','config_value')->where(['is_active' => 'Y'])->where(['config_key' => 'SUNDAY'])->first();

        return view('frontend.blog-details',compact('services','blog','home','menu','related_blog','modayHours','saturdayHours','sundayHours'));
    }


} 