<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Yajra\Datatables\Datatables;
use App\common_model\ServiceManagement;
use App\common_model\ContentManagement;
use App\common_model\MenuManagement;
use App\common_model\NewslatterMaster;
use App\common_model\SiteConfiguration;
use App\common_model\BlogManagement;
use App\common_model\BlogCategoryMaster;
use App\Http\Requests\NewslatterMasterForm;

class PrivacypolicyController extends Controller
{
  public function index()
  {
    $home = ContentManagement::where(['slug'=>'home'])->first();

    $privacypolicy = ContentManagement::select('id', 'title', 'content')->where(['slug' => 'privacy-policy'])->first();
    $services = ServiceManagement::orderBy('id','desc')->where(['is_active'=>'Y'])->get();
    $modayHours = SiteConfiguration::select('id', 'config_key','config_value')->where(['is_active' => 'Y'])->where(['config_key' => 'MONDAY_TO_FRIDAY'])->first();
    $saturdayHours= SiteConfiguration::select('id', 'config_key','config_value')->where(['is_active' => 'Y'])->where(['config_key' => 'SATURDAY'])->first();
    $sundayHours= SiteConfiguration::select('id', 'config_key','config_value')->where(['is_active' => 'Y'])->where(['config_key' => 'SUNDAY'])->first();
    
    
    return view('frontend.privacy-policy',compact('services','privacypolicy','home','modayHours','saturdayHours','sundayHours'));
  }
  
  public function careerdata()
  {
    $home = ContentManagement::where(['slug'=>'home'])->first();
    $careerdata = ContentManagement::select('id', 'title', 'content')->where(['slug' => 'career'])->first();
    $services = ServiceManagement::orderBy('id','desc')->where(['is_active'=>'Y'])->get();
    $modayHours = SiteConfiguration::select('id', 'config_key','config_value')->where(['is_active' => 'Y'])->where(['config_key' => 'MONDAY_TO_FRIDAY'])->first();
    $saturdayHours= SiteConfiguration::select('id', 'config_key','config_value')->where(['is_active' => 'Y'])->where(['config_key' => 'SATURDAY'])->first();
    $sundayHours= SiteConfiguration::select('id', 'config_key','config_value')->where(['is_active' => 'Y'])->where(['config_key' => 'SUNDAY'])->first();
    
    return view ('frontend.career',compact('careerdata','modayHours','saturdayHours','sundayHours'));
  } 
  
  public function affliatedata()
  {
   $home = ContentManagement::where(['slug'=>'home'])->first();
   $affliatedata = ContentManagement::select('id', 'title', 'content')->where(['slug' => 'affiliate'])->first();
   $services = ServiceManagement::orderBy('id','desc')->where(['is_active'=>'Y'])->get();
   $modayHours = SiteConfiguration::select('id', 'config_key','config_value')->where(['is_active' => 'Y'])->where(['config_key' => 'MONDAY_TO_FRIDAY'])->first();
   $saturdayHours= SiteConfiguration::select('id', 'config_key','config_value')->where(['is_active' => 'Y'])->where(['config_key' => 'SATURDAY'])->first();
   $sundayHours= SiteConfiguration::select('id', 'config_key','config_value')->where(['is_active' => 'Y'])->where(['config_key' => 'SUNDAY'])->first();
   return view ('frontend.affliate',compact('affliatedata','modayHours','saturdayHours','sundayHours'));
 } 

 public function disclaimerdata()
 {
   $home = ContentManagement::where(['slug'=>'home'])->first();
   $disclaimerdata = ContentManagement::select('id', 'title', 'content')->where(['slug' => 'disclaimer'])->first();
   $services = ServiceManagement::orderBy('id','desc')->where(['is_active'=>'Y'])->get();
   $modayHours = SiteConfiguration::select('id', 'config_key','config_value')->where(['is_active' => 'Y'])->where(['config_key' => 'MONDAY_TO_FRIDAY'])->first();
   $saturdayHours= SiteConfiguration::select('id', 'config_key','config_value')->where(['is_active' => 'Y'])->where(['config_key' => 'SATURDAY'])->first();
   $sundayHours= SiteConfiguration::select('id', 'config_key','config_value')->where(['is_active' => 'Y'])->where(['config_key' => 'SUNDAY'])->first();
   return view ('frontend.disclaimer',compact('disclaimerdata','modayHours','saturdayHours','sundayHours'));
 } 



}
