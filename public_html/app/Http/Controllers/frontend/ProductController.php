<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Yajra\Datatables\Datatables;
use App\common_model\ServiceManagement;
use App\common_model\ContentManagement;
use App\common_model\BlogManagement;
use App\common_model\SiteConfiguration;

use Response;

class ProductController extends Controller
{
    public function index()
    {
        $home = ContentManagement::where(['slug'=>'home'])->first();
    	$services = ServiceManagement::orderBy('id','desc')->where(['is_active'=>'Y'])->get();
          $modayHours = SiteConfiguration::select('id', 'config_key','config_value')->where(['is_active' => 'Y'])->where(['config_key' => 'MONDAY_TO_FRIDAY'])->first();
    $saturdayHours= SiteConfiguration::select('id', 'config_key','config_value')->where(['is_active' => 'Y'])->where(['config_key' => 'SATURDAY'])->first();
     $sundayHours= SiteConfiguration::select('id', 'config_key','config_value')->where(['is_active' => 'Y'])->where(['config_key' => 'SUNDAY'])->first();
 
    
        return view('frontend.product',compact('services','home','modayHours','saturdayHours','sundayHours'));
    }
     public function details()
    {
        $home = ContentManagement::where(['slug'=>'home'])->first();
        $services = ServiceManagement::orderBy('id','desc')->where(['is_active'=>'Y'])->get();
        return view('frontend.product-details',compact('services','home'));
    }

   
}