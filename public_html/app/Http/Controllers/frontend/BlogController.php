<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Yajra\Datatables\Datatables;
use App\common_model\ServiceManagement;
use App\common_model\ContentManagement;
use App\common_model\MenuManagement;
use App\common_model\NewslatterMaster;
use App\common_model\SiteConfiguration;
use App\common_model\BlogManagement;
use App\common_model\BlogCategoryMaster;
use App\Http\Requests\NewslatterMasterForm;
 
class BlogController extends Controller
{
  public function index()
    {
        $home = ContentManagement::where(['slug'=>'home'])->first();

        $blog = BlogManagement::orderBy('id','desc')->where(['is_active'=>'Y'])->get();
        $services = ServiceManagement::orderBy('id','desc')->where(['is_active'=>'Y'])->get();
       $modayHours = SiteConfiguration::select('id', 'config_key','config_value')->where(['is_active' => 'Y'])->where(['config_key' => 'MONDAY_TO_FRIDAY'])->first();
    $saturdayHours= SiteConfiguration::select('id', 'config_key','config_value')->where(['is_active' => 'Y'])->where(['config_key' => 'SATURDAY'])->first();
     $sundayHours= SiteConfiguration::select('id', 'config_key','config_value')->where(['is_active' => 'Y'])->where(['config_key' => 'SUNDAY'])->first();
 
    
        return view('frontend.blog',compact('services','blog','home','modayHours','saturdayHours','sundayHours'));
    }
     public function details($id)
    {
        $menu = MenuManagement::where(['is_active'=>'Y','type'=>'Website'])->get();
        $blog = BlogManagement::select('blog_management.id','blog_management.blog_title','blog_management.blog_description','blog_management.image','blog_category_master.category_name')->orderBy('blog_management.id','desc')->leftjoin('blog_category_master','blog_category_master.id','blog_management.category_id')->where(['blog_management.id'=>$id])->first();
        $related_blog = BlogManagement::orderBy('id','desc')->where(['is_active'=>'Y'])->take('2')->get();
        $services = ServiceManagement::orderBy('id','desc')->where(['is_active'=>'Y'])->get();
        $home = ContentManagement::where(['slug'=>'home'])->first();
        return view('frontend.blog-details',compact('services','blog','home','menu','related_blog'));
    }


    public function aboutus()
  {

        $aboutus = ContentManagement::where(['slug'=>'about-us'])->first();
       $modayHours = SiteConfiguration::select('id', 'config_key','config_value')->where(['is_active' => 'Y'])->where(['config_key' => 'MONDAY_TO_FRIDAY'])->first();
    $saturdayHours= SiteConfiguration::select('id', 'config_key','config_value')->where(['is_active' => 'Y'])->where(['config_key' => 'SATURDAY'])->first();
     $sundayHours= SiteConfiguration::select('id', 'config_key','config_value')->where(['is_active' => 'Y'])->where(['config_key' => 'SUNDAY'])->first();
 
        return view('frontend.about-us',compact('aboutus','modayHours','saturdayHours','sundayHours'));

  }

   
    //subscribe 
public function subscribe(NewslatterMasterForm $request)
{

     // $this->validate($request, [
     //        'email' => 'required|unique:newslatter_master,subscribe_email|regex:/^[\w-]+(?:\.[\w-]+)*@(?:[\w-]+\.)+[a-zA-Z]{2,7}$/',
     //    ],
     //    [
     //        'email.required' => 'Please enter email.',
     //        'email.unique' =>'This email address has already subscribed.',
     //    ]);

  if ($request->ajax()) {
    return true;
  }

  $subscribe =new NewslatterMaster();
  $subscribe->subscribe_email=$request->subscribe_email;
  $subscribe->created_date=date('Y-m-d H:i:s');

  if($subscribe->save()){
    /*$data = array('subscribe_email'=>$request->subscribe_email);
    Mail::send('subscribe_email', $data, function($message) use ($data){
      $message->from('smtp@xceltec.in','SUBSCRIBE');
      $message->subject("Everglades");
      $message->to($data['email']);
    */

    return redirect('/about-us')->with('success','Thank you for your subscription.');
  }else{
    return redirect('/about-us')->with('error','Something Error: In data save!');
  }
}

}