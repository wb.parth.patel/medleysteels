<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Yajra\Datatables\Datatables;
use App\common_model\ServiceManagement;
use App\common_model\ContentManagement;
use App\common_model\MenuManagement;
use App\common_model\SiteConfiguration;
use App\common_model\BlogManagement;
use App\common_model\BlogCategoryMaster;
use Response;

class ServiceController extends Controller
{
    public function index()
    {
        $home = ContentManagement::where(['slug'=>'home'])->first();
        
        $services = ServiceManagement::select('id', 'service_name', 'service_desctiprion', 'service_image')
        ->where(['is_active' => 'Y'])
        ->orderBy('id', "desc")
        ->get();
        //dd($services);
        
        $modayHours = SiteConfiguration::select('id', 'config_key','config_value')->where(['is_active' => 'Y'])->where(['config_key' => 'MONDAY_TO_FRIDAY'])->first();
        $saturdayHours= SiteConfiguration::select('id', 'config_key','config_value')->where(['is_active' => 'Y'])->where(['config_key' => 'SATURDAY'])->first();
        $sundayHours= SiteConfiguration::select('id', 'config_key','config_value')->where(['is_active' => 'Y'])->where(['config_key' => 'SUNDAY'])->first();
        return view('frontend.service',compact('services','home','modayHours','saturdayHours','sundayHours'));
    }
    public function detail($id)
    {
        $menu = MenuManagement::where(['is_active'=>'Y','type'=>'Website'])->get();
        
        $serviceDetails = ServiceManagement::select('id', 'service_name', 'service_desctiprion', 'service_image')->where(['id'=>$id])->first();
        // dd($services);

        $home = ContentManagement::where(['slug'=>'home'])->first();
        $modayHours = SiteConfiguration::select('id', 'config_key','config_value')->where(['is_active' => 'Y'])->where(['config_key' => 'MONDAY_TO_FRIDAY'])->first();
        $saturdayHours= SiteConfiguration::select('id', 'config_key','config_value')->where(['is_active' => 'Y'])->where(['config_key' => 'SATURDAY'])->first();
        $sundayHours= SiteConfiguration::select('id', 'config_key','config_value')->where(['is_active' => 'Y'])->where(['config_key' => 'SUNDAY'])->first();
        return view('frontend.servicedetail',compact('serviceDetails','home','menu','modayHours','saturdayHours','sundayHours'));
    }



}