<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Yajra\Datatables\Datatables;
use App\common_model\ServiceManagement;
use App\common_model\ContentManagement;
use App\common_model\BlogManagement;
use App\common_model\MenuManagement;
use App\common_model\NewslatterMaster;
use App\common_model\SliderManagement;
use App\common_model\SliderImage;
use App\common_model\Testimonials;
use App\common_model\SiteConfiguration;
use App\Http\Requests\NewslatterMasterForm;
use Response;
use Mail;  
use App\Http\Requests\SubsxriptionFrontForm;

class DashboardController extends Controller
{
  public function index()
  {
    /* Services Details */
    $services = ServiceManagement::select('id', 'service_name', 'service_desctiprion', 'service_icon')->where(['is_active' => 'Y'])->orderBy('id', 'desc')->take(4)->get();

    /* Our Company Details */
    $ourCompany = ContentManagement::select('id', 'title', 'content')->where(['slug' => 'our-company'])->first();
    
    /*We Are Service Details */
    $weAreService = ContentManagement::select('id', 'title', 'content')->where(['slug' => 'we-are-more-than-a-service-center'])->first();
    
    /*Take Advantage Details*/
    $takeAdvantage = ContentManagement::select('id', 'title', 'content')->where(['slug' => 'take-advantage-of-our-experience'])->first();
    
    /*Business hours (timing)*/
    //$Businesshours = SiteConfiguration::select('id', 'config_key','config_value')->where(['config_key' => 'MONDAY_TO_FRIDAY'])->first();
    //$Businesshours = SiteConfiguration::select('id', 'config_key','config_value')->where(['is_active' => 'Y'])->get();
    $modayHours = SiteConfiguration::select('id', 'config_key','config_value')->where(['is_active' => 'Y'])->where(['config_key' => 'MONDAY_TO_FRIDAY'])->first();
    $saturdayHours= SiteConfiguration::select('id', 'config_key','config_value')->where(['is_active' => 'Y'])->where(['config_key' => 'SATURDAY'])->first();
     $sundayHours= SiteConfiguration::select('id', 'config_key','config_value')->where(['is_active' => 'Y'])->where(['config_key' => 'SUNDAY'])->first();
    // $aboutus = ContentManagement::where(['slug'=>'about-us'])->first();
    // $home = ContentManagement::where(['slug'=>'home'])->first();
    // $exports = ContentManagement::where(['slug'=>'export'])->first();
    // $blog = BlogManagement::orderBy('id','desc')->where(['is_active'=>'Y'])->take(3)->get();
    // $expert_team = SliderManagement::select('slider_image.image','slider_management.title','slider_management.description','slider_management.url') ->leftjoin('slider_image','slider_image.slider_id','slider_management.id')->where(['slider_management.is_active'=>'Y'])->get();
    // $testimonial = Testimonials::orderBy('id','desc')->where(['is_active'=>'Y'])->get();
    // $menu = MenuManagement::where(['is_active'=>'Y','type'=>'Website'])->get();

    return view('frontend.index',['services' => $services, 'ourCompany' => $ourCompany, 'weAreService' => $weAreService , 'takeAdvantage' => $takeAdvantage,'modayHours' => $modayHours,'saturdayHours' => $saturdayHours,'sundayHours' => $sundayHours]);
  } 


  public function contact()
  {

    $services = ServiceManagement::orderBy('id','desc')->where(['is_active'=>'Y'])->take(6)->get(); 
    return view('frontend.contact');

  }

  public function about()
  {

    $services = ServiceManagement::orderBy('id','desc')->where(['is_active'=>'Y'])->take(6)->get(); 
    return view('frontend.about');

  }

 

    //subscribe 
public function subscribe(NewslatterMasterForm $request)
{

     // $this->validate($request, [
     //        'email' => 'required|unique:newslatter_master,subscribe_email|regex:/^[\w-]+(?:\.[\w-]+)*@(?:[\w-]+\.)+[a-zA-Z]{2,7}$/',
     //    ],
     //    [
     //        'email.required' => 'Please enter email.',
     //        'email.unique' =>'This email address has already subscribed.',
     //    ]);
 
  if ($request->ajax()) {
    return true;
  }

  $subscribe =new NewslatterMaster();
  $subscribe->subscribe_email=$request->subscribe_email;
  $subscribe->created_date=date('Y-m-d H:i:s');

  if($subscribe->save()){
   /* $data = array('subscribe_email'=>$request->subscribe_email);
    Mail::send('subscribe_email', $data, function($message) use ($data){
      $message->from('smtp@xceltec.in','SUBSCRIBE');
      $message->subject("Everglades");
      $message->to($data['email']);

 */    return redirect('/index')->with('success','Thank you for your subscription.');
  }else{
    return redirect('/index')->with('error','Something Error: In data save!');
  }
}


public function export()
{

 $services = ServiceManagement::orderBy('id','desc')->where(['is_active'=>'Y'])->get();
 $aboutus = ContentManagement::where(['slug'=>'about-us'])->first();
 $home = ContentManagement::where(['slug'=>'home'])->first();
 $exports = ContentManagement::where(['slug'=>'export'])->first();
 $exportsales = ContentManagement::where(['slug'=>'export-sales'])->first();
 $blog = BlogManagement::orderBy('id','desc')->where(['is_active'=>'Y'])->take(3)->get();


 return view('frontend.Exportsales',['blog'=>$blog,'services'=>$services,'aboutus'=>$aboutus,'home'=>$home,'exports'=>$exports,'exportsales'=>$exportsales]);
}

public function whoweare()
{

  $services = ServiceManagement::orderBy('id','desc')->where(['is_active'=>'Y'])->get();
  $aboutus = ContentManagement::where(['slug'=>'about-us'])->first();
  $home = ContentManagement::where(['slug'=>'home'])->first();
  $exports = ContentManagement::where(['slug'=>'export'])->first();

  $contact_us = ContentManagement::where(['slug'=>'contact-us'])->first();
  return view('frontend.whoweare',['services'=>$services,'aboutus'=>$aboutus,'home'=>$home,'exports'=>$exports,'contact_us'=>$contact_us]);
}


public function cofounder()
{

  $services = ServiceManagement::orderBy('id','desc')->where(['is_active'=>'Y'])->get();
  $home = ContentManagement::where(['slug'=>'home'])->first();
  $ourfounder = ContentManagement::where(['slug'=>'our-founder'])->first();

    $modayHours = SiteConfiguration::select('id', 'config_key','config_value')->where(['is_active' => 'Y'])->where(['config_key' => 'MONDAY_TO_FRIDAY'])->first();
    $saturdayHours= SiteConfiguration::select('id', 'config_key','config_value')->where(['is_active' => 'Y'])->where(['config_key' => 'SATURDAY'])->first();
     $sundayHours= SiteConfiguration::select('id', 'config_key','config_value')->where(['is_active' => 'Y'])->where(['config_key' => 'SUNDAY'])->first();
  return view('frontend.co-founder',['services'=>$services,'home'=>$home,'ourfounder'=>$ourfounder,'modayHours' => $modayHours,'saturdayHours' => $saturdayHours,'sundayHours' => $sundayHours]);
}



}