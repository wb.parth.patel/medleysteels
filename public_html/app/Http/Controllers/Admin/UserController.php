<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Hash;
use Mail;
use File;
use App\Http\Requests\UserForm;
use App\common_model\User;
use App\common_model\SubUserManagement;
use App\common_model\Userrole;
use Yajra\Datatables\Datatables;

class UserController extends Controller
{
    public function index()
    {
        return view('backend.user.index');
    }

    public function data(Request $request){
    //     if($request->filter_role){
    //         $user = User::
    //         where(['user_role_id'=>$request->filter_role])
    //         ->where('users.id','!=',Auth::user()->id)
    //         ->orderBy('users.id','desc')
    //         ->get();
    //     }
    //     else{
    //         $user = User::
    //         where('users.id','!=',Auth::user()->id)
    //         ->orderBy('users.id','desc')
    //         ->get();
    //     }
    //     return Datatables::of($user)
    //     ->addColumn('action', function ($user){
    //         if($user->is_active=='Y'){
    //             $class='fa fa-toggle-on';
    //         }else{
    //             $class='fa fa-toggle-off';
    //         }

    //         if($user->approved_member=='Y'){
    //             $data_icon = 'fa fa-times-circle';
    //             $deal_title = 'Block';
    //         }
    //         elseif($user->approved_member=='P'){
    //             $data_icon = 'fa fa-check-circle';
    //             $deal_title = 'Unblock';
    //         }else{
    //             $data_icon = 'fa fa-check-circle';
    //             $deal_title = 'Unblock';
    //         }

    //     if($user->is_active=='Y'){
    //         $status_title = 'Inactive';
    //     }else{
    //         $status_title = 'Active';
    //     }

    //     return '
    //     <a href="'.url('user/edit',$user->id).'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill"title="Edit"><i class="fa fa-edit"></i></a>

    //     <a href="'.url('user/status',$user->id).'" id="is_active" data-status="'.$user->is_active.'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="'.$status_title.'"><i class="'.$class.'"></i></a>

    //     <a href="'.url('user/view',$user->id).'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill"
    //     data-public_path="'.url('media/userimage').'" title="View">
    //     <i class="fa fa-eye"></i></a>

    //     <a href="'.url('user/delete',$user->id).'" id="delete_btn" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete"><i class="fa fa-trash"></i></a>';

    // })
    //     ->addColumn('status',function($user){

    //         if($user->is_active=='Y'){
    //             return '<span class="status_width"><span class="m-badge  m-badge--success m-badge--wide">Active</span></span>';
    //         }else{
    //             return '<span class="status_width"><span class="m-badge  m-badge  m-badge--danger m-badge--wide">Inactive</span></span>';
    //         }
    //     })->rawColumns(['status', 'action'])
    //     ->make(true);
    }

    public function create()
    {
        // $userrole=Userrole::select('id','name')->where(['is_active'=>'Y'])->get();
        // return view('backend.user.create_form',['userrole'=>$userrole]);
        return view('backend.user.create_form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserForm $request){
        // if($request->ajax()){
        //     return true;
        // }
        // if(isset(request()->image)){
        //     $new_name = time()."_".rand(1, 999).".".request()->image->getClientOriginalExtension();
        // }else{
        //     $new_name='';
        // }
        // $role=[2,3];
        // if(in_array($request->user_role,$role))
        // {
        //     $is_admin='N';
        // }else{
        //     $is_admin='Y';
        // }

        // $random = str_shuffle('abcdefghjklmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ234567890!$%^&!$%^&');
        // $password = substr($random, 0, 10);


        // $user_model=new User();
        // $user_model->user_role_id=$request->user_role_id;
        // $user_model->name=$request->name;
        // $user_model->contact_number=$request->contact_number;
        // $user_model->dob=date('Y-m-d', strtotime($request->dob));
        // $user_model->email=$request->email;
        // // $user_model->password=Hash::make($request->password);
        // $user_model->password = Hash::make($password);
        // $user_model->image=$new_name;
        // $user_model->is_admin=$is_admin;
        // $user_model->auth_key='';
        // $user_model->is_active='Y';
        // $user_model->ip_address=$request->ip();
        // $user_model->created_by=auth()->user()->id;
        // $user_model->created_date=date('Y-m-d H:i:s');
        // $user_model->updated_by=auth()->user()->id;
        // $user_model->updated_date=date('Y-m-d H:i:s');

        // $data = array('email'=>$request->email,'password'=>$password,'name'=>$request->name,'number'=>$request->contact_number,'url'=>url('/home'));
        // Mail::send('mail', $data, function($message) use ($data){

        //     $message->from('smtp@xceltec.in','New User');
        //     $message->subject("Medley Steel");
        //     $message->to($data['email']);
        // });

        // if($user_model->save()){
        //     // Upload image path
        //     $destinationPath = public_path('media/userimage');
        //     if(!file_exists($destinationPath)){
        //         File::makeDirectory($destinationPath, $mode = 0777, true, true);
        //     }
        //     if(isset(request()->image) && request()->image!=null){
        //         request()->image->move($destinationPath, $new_name);
        //     }
        //     return redirect('user/index')->with('success','User has been added successfully');
        // }else{
        //     return redirect('user/index')->with('error','Something Error: In data save!');
        // }
    }

     public function edit($id){
        // $user=User::where(['id'=>$id])->first();

        // $userrole = Userrole::where(['is_active'=>'Y'])->get();

        // if($user){
        //     return view('backend.user.edit_form',['user'=>$user,'userrole'=>$userrole]);
        // }else{
        //     return redirect('user/index')->with('error','Something Error: In data fetch!');
        // }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserForm $request, $id){
        // if($request->ajax()){
        //     return true;
        // }
        // $user_model=User::where(['id'=>$id])->first();

        // if(isset(request()->image)){
        //     $new_name = time()."_".rand(1, 999).".".request()->image->getClientOriginalExtension();
        // }else{
        //     $new_name=$user_model->image;
        // }


        // if(isset($_FILES['file']['name']) && ($_FILES['file']['name'])!=''){
        //     $ext=explode('/',request()->file->getMimeType());
        //     $imageName = date('YmdHis').'_'.rand(111,999).".".$ext[1];
        // }else{
        //     $imageName=$user_model->image;
        // }
        // $role=[2,3];
        // if(in_array($request->user_role,$role))
        // {
        //     $is_admin='N';
        // }else{
        //     $is_admin='Y';
        // }
        // if($user_model){
        //     $user_model->user_role_id=$request->user_role_id;
        //     $user_model->name=$request->name;
        //     $user_model->contact_number=$request->contact_number;
        //     $user_model->dob=date('Y-m-d', strtotime($request->dob));
        //     $user_model->email=$request->email;
        //     $user_model->is_admin=$is_admin;
        //     $user_model->image=$new_name;
        //     $user_model->ip_address=$request->ip();
        //     $user_model->updated_by=auth()->user()->id;
        //     $user_model->updated_date=date('Y-m-d H:i:s');
        //     $user_model->is_active='Y';

        //     if($user_model->save()){
        //         $destinationPath = public_path('media/userimage');
        //         if(!file_exists($destinationPath)){
        //             File::makeDirectory($destinationPath, $mode = 0777, true, true);
        //         }
        //         if(isset(request()->image) && request()->image!=null){
        //             request()->image->move($destinationPath, $new_name);
        //         }

                /*if(isset($_FILES['file']['name']) && ($_FILES['file']['name'])!=''){
                    request()->file->move(public_path('media/userimage'), $imageName);
                }*/
        //         return redirect('user/index')->with('success','Record has been updated successfully');
        //     }else{
        //         return redirect('user/index')->with('error','Something Error: In data save!');
        //     }
        // }else{
        //     return redirect('user/index')->with('error','Something Error: In data fetch!');
        // }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        // $user=User::where(['id'=>$id])->first();
        // if($user){
        //     $user->delete();
        //     return redirect('user/index')->with('success','Record has been deleted successfully');
        // }else{
        //     return redirect('user/index')->with('error','Something Error: In data fetch!');
        // }
    }

    //Update Status
    public function status($id){
        // $user=User::where(['id'=>$id])->first();
        // if($user){
        //     if($user->is_active=='Y'){
        //         $status='N';
        //     }else{
        //         $status='Y';
        //     }
        //     $user->is_active=$status;
        //     $user->updated_date=date('Y-m-d H:i:s');
        //     $user->updated_by=auth()->user()->id;
        //     if($user->save()){
        //         if($user->is_active == 'Y'){
        //             return redirect('user/index')->with('success','Record has been activated successfully');
        //         }
        //         else{
        //             return redirect('user/index')->with('success','Record has been inactivated successfully');
        //         }
        //     }else{
        //         return redirect('user/index')->with('success','Something Error: In data save!');
        //     }
        // }else{
        //     return redirect('user/index')->with('error','Something Error: In data fetch!');
        // }
    }

    //approve or dissapproved user
    public function approved(Request $request,$id){
        // $user=User::find($id);
        // if($user){
        //     if($user->is_block=='P'){
        //         $user->is_block='Y';
        //     }
        //     else if($user->is_block=='Y'){
        //         $user->is_block='N';
        //         $block_user = BlockUser::where(['blocked_by_user_id'=>auth()->user()->id,'blocked_user_id'=>$id])->first();
        //         if($block_user){
        //             $block_user->is_block = 'Y';
        //             $block_user->blocked_by_admin = 'Y';
        //             $block_user->updated_by = auth()->user()->id;
        //             $block_user->updated_date = date('Y-m-d H:i:s');
        //             $block_user->ip_address=$request->ip();
        //         }else{
        //             $block_user = new BlockUser();
        //             $block_user->user_device_id = '';
        //             $block_user->blocked_user_id = $id;
        //             $block_user->blocked_by_user_id = auth()->user()->id;
        //             $block_user->is_block = 'Y';
        //             $block_user->blocked_by_admin = 'Y';
        //             $block_user->created_by = auth()->user()->id;
        //             $block_user->created_date = date('Y-m-d H:i:s');
        //             $block_user->updated_by = auth()->user()->id;
        //             $block_user->updated_date = date('Y-m-d H:i:s');
        //             $block_user->ip_address=$request->ip();
        //         }
        //         $block_user->save();
        //     }else{
        //         $user->approved_member='Y';
        //         $block_user_data = BlockUser::where(['blocked_user_id'=>$id,'blocked_by_user_id'=>auth()->user()->id,
        //             'blocked_by_admin'=>'Y'])->first();
        //         $block_user_data->is_block = 'N';
        //         $block_user_data->blocked_by_admin = 'N';
        //         $block_user_data->updated_by = auth()->user()->id;
        //         $block_user_data->updated_date = date('Y-m-d H:i:s');
        //         $block_user_data->save();
        //     }
        //     $user->updated_date=date('Y-m-d H:i:s');
        //     $user->updated_by=auth()->user()->id;
        //     if($user->save()){
        //         if($user->approved_member=='N'){
        //             return redirect('user/index')->with('success','User has been blocked successfully');
        //         }else{
        //             return redirect('user/index')->with('success','User has been unblocked successfully');
        //         }
        //     }else{
        //         return redirect('user/index')->with('error','Something Error: In data save!');
        //     }
        // }else{
        //     return redirect('user/index')->with('error','Something Error!');
        // }
    }

    public function view($id)
    {
    //      $user=User::select('users.name','userrole.name as userrole_name','users.email','users.contact_number','users.dob','users.image')
    //                 ->leftjoin('userrole','users.user_role_id','userrole.id')
    //                 ->where(['users.id'=>$id])
    //                 ->first();
    //   if ($user) {
    //    return view('backend.user.view',compact('user'));
    //   } else {
    //     return redirect('backend.user.index')->with('error', 'Something Error!');
    //   }
    }

    public function subuser($id){
    //     $subuser = SubUserManagement::select('id','user_name','user_id')
    //                     ->where(['user_id'=>$id])
    //                     ->get();

    //     if ($subuser) {
    //         return view('backend.user.subuser',compact('subuser'));
    //     } else {
    //         return view('backend.user.subuser')->with('error', 'Something Error!');
    //   }
    }

}
