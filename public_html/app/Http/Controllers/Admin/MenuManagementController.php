<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Http\Requests\MenuManagementForm;
use App\common_model\MenuManagement;
use App\common_model\HotelManagement;
use App\common_model\BlogCategoryMaster;
use Yajra\Datatables\Datatables;
use Response;

class MenuManagementController extends Controller
{
    public function index()
    {
        return view('backend.menu_management.index');
    }
    public function data(Request $request)
    {
        if(isset($request->filter_data))
        {
            $site_config = MenuManagement::where(['is_active'=>$request->filter_data])
                            ->orderBy('id','desc')
                            ->get();
        }else{
            $site_config = MenuManagement::
                            orderBy('id','desc')
                            ->get();
        }
        return Datatables::of($site_config)
                ->addColumn('action', function ($site_config) {
                    if($site_config->is_active=='Y'){
                        $class='fa fa-toggle-on';
                    }else{
                        $class='fa fa-toggle-off';
                    }

                    if($site_config->is_active=='Y'){
                        $status_title = 'Inactive';
                    }else{
                        $status_title = 'Active';
                    }
                    return '<a href="'.url('menu-management/edit',$site_config->id).'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit"><i class="fa fa-edit"></i></a>

                        <a href="'.url('menu-management/status',$site_config->id).'" id="is_active" data-status="'.$site_config->is_active.'" class="btn-status m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="'.$status_title.'"><i class="'.$class.'"></i></a>

                        <a href="'.url('menu-management/delete',$site_config->id).'" id="delete_btn" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete"><i class="fa fa-trash"></i></a>
                        <a href="'.url('menu-management/show',$site_config->id).'"  class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="View"><i class="fa fa-eye"></i></a>'
                        ;
                })
                ->addColumn('status',function($site_config){
                    if($site_config->is_active=='Y'){
                        return '<span class="status_width"><span class="m-badge  m-badge--success m-badge--wide">Active</span></span>';
                    }else{
                        return '<span class="status_width"><span class="m-badge  m-badge  m-badge--danger m-badge--wide">Inactive</span></span>';
                    }
                })
                ->editColumn('type',function($site_config){
                    if($site_config->type== '' && $site_config->type== null){
                         return $type = 'Admin';
                    }
                    else{
                         return $site_config->type;
                    }
                   
                })
                ->rawColumns(['status', 'action','type'])                       
                ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend/menu_management/create_form');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MenuManagementForm $request)
    {
        if($request->ajax()){
            return true;
        }

        $menu=NEW MenuManagement();
        $menu->menuname=$request->menuname;
        $menu->type=$request->type;
        $menu->is_active='Y';
        $menu->is_delete='Y';        
        if($menu->save()){

            return redirect('menu-management/index')->with('success','Record has been added successfully');    
        }else{
            return redirect('menu-management/index')->with('error','Something Error: In data save!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $menu=MenuManagement::where(['id'=>$id])->first();

        if($menu){
            return view('backend/menu_management/view',['menu'=>$menu]);    
        }else{
            return redirect('menu-management/index')->with('error','Something Error!');
        }   
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $menu=MenuManagement::where(['id'=>$id])->first();
        if($menu){
            return view('backend/menu_management/edit_form',compact('menu'));    
        }else{
            return redirect('menu-management/index')->with('error','Something Error!');
        }   
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MenuManagementForm $request, $id)
    {
        $menu=MenuManagement::find($id);
        $menu->type=$request->type;
        $menu->menuname=$request->menuname;
        
        if($menu->save()){
            return redirect('menu-management/index')->with('success','Record has been updated successfully');
        }else{
            return redirect('menu-management/index')->with('error','Something Error: In data save!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $menu=MenuManagement::find($id);
        if($menu){
            $menu->delete();
            return redirect('menu-management/index')->with('success','Record has been deleted successfully');
        }else{
            return redirect('menu-management/index')->with('error','Something Error: In data save!');        
        }
    }

    public function status($id)
    {
        $menu=MenuManagement::where(['id'=>$id])->first();
        if($menu){
            if($menu->is_active=='Y'){
                $menu->is_active='N';
            }else{
                $menu->is_active='Y';
            }   
                  
            if($menu->save()){
                if($menu->is_active=='N'){
                    return redirect('menu-management/index')->with('success','Record has been inactivated successfully');      
                }else{
                    return redirect('menu-management/index')->with('success','Record has been activated successfully');            
                }
              
            }else{
                return redirect('menu-management/index')->with('error','Something Error: In data save!');      
            }
        }else{
            return redirect('menu-management/index')->with('error','Something Error!');    
        }
    }
}