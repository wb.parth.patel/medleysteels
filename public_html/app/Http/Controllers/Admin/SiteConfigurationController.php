<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Http\Requests\SiteConfigurationForm;
use App\common_model\SiteConfiguration;
use Yajra\Datatables\Datatables;
use Response;

class SiteConfigurationController extends Controller
{
    public function index()
    {
        return view('backend.site_configuration.index');
    }
    public function data(Request $request)
    {
        if(isset($request->filter_data))
        {
            $site_config = SiteConfiguration::where(['is_active'=>$request->filter_data])
                            ->orderBy('id','desc')
                            ->get();
        }else{
            $site_config = SiteConfiguration::
                            orderBy('id','desc')
                            ->get();
        }
        return Datatables::of($site_config)
                ->addColumn('action', function ($site_config) {
                    if($site_config->is_active=='Y'){
                        $class='fa fa-toggle-on';
                    }else{
                        $class='fa fa-toggle-off';
                    }

                    if($site_config->is_active=='Y'){
                        $status_title = 'Inactive';
                    }else{
                        $status_title = 'Active';
                    }
                    return '<a href="'.url('site-configuration/edit',$site_config->id).'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit"><i class="fa fa-edit"></i></a>

                        <a href="'.url('site-configuration/status',$site_config->id).'" id="is_active" data-status="'.$site_config->is_active.'" class="btn-status m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="'.$status_title.'"><i class="'.$class.'"></i></a>

                        <a href="'.url('site-configuration/delete',$site_config->id).'" id="delete_btn" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete"><i class="fa fa-trash"></i></a>';
                })
                ->addColumn('status',function($site_config){
                    if($site_config->is_active=='Y'){
                        return '<span class="status_width"><span class="m-badge  m-badge--success m-badge--wide">Active</span></span>';
                    }else{
                        return '<span class="status_width"><span class="m-badge  m-badge  m-badge--danger m-badge--wide">Inactive</span></span>';
                    }
                })->rawColumns(['status', 'action'])                       
                ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend/site_configuration/create_form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SiteConfigurationForm $request)
    {
        if($request->ajax()){
            return true;
        }

          
        if(isset(request()->image)){

            $new_name = time()."_".rand(1, 999).".".request()->image->getClientOriginalExtension();

            /*$ext=explode('/',request()->file->getMimeType());
            $imageName = date('YmdHis').'_'.rand(111,999).".".$ext[1]; */
        }else{
            $new_name='';
        }
        $array_=array(
        'config_key'=>str_replace(' ','_',strtoupper($request->config_key)),
        'config_value'=>$request->config_value,
        'created_date'=>date('Y-m-d H:i:s'),
        'updated_date'=>date('Y-m-d H:i:s'),
        'created_by'=>auth()->user()->id,
        'image'=>$new_name,
        'updated_by'=>auth()->user()->id,
        'ip_address'=>$request->ip(),
        'is_active'=>'Y',
        );

        $site_config=NEW SiteConfiguration($array_);
        if($site_config->save()){
            $destinationPath = public_path('media/site_config');
            if(!file_exists($destinationPath)){
                File::makeDirectory($destinationPath, $mode = 0777, true, true);
            }
            if(isset(request()->image) && request()->image!=null){
                request()->image->move($destinationPath, $new_name);        
            }
            return redirect('site-configuration/index')->with('success','Record has been added successfully');    
        }else{
            return redirect('site-configuration/index')->with('error','Something Error: In data save!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $siteconfig_data=SiteConfiguration::where(['id'=>$id])->first();
        if($siteconfig_data){
            return view('backend/site_configuration/edit_form',['siteconfig_data'=>$siteconfig_data]);    
        }else{
            return redirect('site-configuration/index')->with('error','Something Error!');
        }   
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $site_config=SiteConfiguration::where(['id'=>$id])->first();
        if($site_config){
            return view('backend/site_configuration/edit_form',['site_config'=>$site_config]);    
        }else{
            return redirect('site-configuration/index')->with('error','Something Error!');
        }   
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SiteConfigurationForm $request, $id)
    {

        if($request->ajax()){
            return true;
        }
        $siteconfig_model=SiteConfiguration::find($id);
         if(isset(request()->image)){
            $new_name = time()."_".rand(1, 999).".".request()->image->getClientOriginalExtension();
        }else{
            $new_name=$siteconfig_model->image;
        }
        //$siteconfig_model->config_key=$request->config_key;
        $siteconfig_model->config_value=$request->config_value;
        $siteconfig_model->image=$new_name;
        $siteconfig_model->updated_date=date('Y-m-d H:i:s');        
        $siteconfig_model->updated_by=auth()->user()->id;

        if($siteconfig_model->save()){
            $destinationPath = public_path('media/site_config');
            if(!file_exists($destinationPath)){
                File::makeDirectory($destinationPath, $mode = 0777, true, true);
            }
            if(isset(request()->image) && request()->image!=null){
                request()->image->move($destinationPath, $new_name);        
            }
              
            return redirect('site-configuration/index')->with('success','Record has been updated successfully');
        }else{
            return redirect('site-configuration/index')->with('error','Something Error: In data save!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cms_model=SiteConfiguration::find($id);
        if($cms_model){
            $cms_model->delete();
            return redirect('site-configuration/index')->with('success','Record has been deleted successfully');
        }else{
            return redirect('site-configuration/index')->with('error','Something Error: In data save!');        
        }
    }

    public function status($id)
    {
        $cms_model=SiteConfiguration::where(['id'=>$id])->first();
        if($cms_model){
            if($cms_model->is_active=='Y'){
                $cms_model->is_active='N';
            }else{
                $cms_model->is_active='Y';
            }   
            $cms_model->updated_date=date('Y-m-d H:i:s');        
            $cms_model->updated_by=auth()->user()->id;        
            if($cms_model->save()){
                if($cms_model->is_active=='N'){
                    return redirect('site-configuration/index')->with('success','Record has been inactivated successfully');      
                }else{
                    return redirect('site-configuration/index')->with('success','Record has been activated successfully');            
                }
              
            }else{
                return redirect('site-configuration/index')->with('error','Something Error: In data save!');      
            }
        }else{
            return redirect('site-configuration/index')->with('error','Something Error!');    
        }
    }
}