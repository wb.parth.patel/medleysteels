<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Http\Requests\BlogManagementForm;
use App\common_model\BlogManagement;
use App\common_model\HotelManagement;
use App\common_model\BlogCategoryMaster;
use Yajra\Datatables\Datatables;
use Response;

class BlogManagementController extends Controller
{
    public function index(){
        return view('backend.blog_management.index');
    }
    public function data(Request $request){

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $blog_category = BlogCategoryMaster::select('id','category_name')->where(['is_active'=>'Y'])->get();
        return view('backend/blog_management/create_form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BlogManagementForm $request){

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BlogManagementForm $request, $id){


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){

    }

    public function status($id){

    }
}
