<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Http\Requests\CountryForm;
use App\common_model\Country;
use Yajra\Datatables\Datatables;
use Response;

class CountryManagementController extends Controller
{
    public function index()
    {
        return view('backend.country.index');
    }
    public function data(Request $request)
    {
        if(isset($request->filter_data))
        {
            $category = Country::where(['is_active'=>'Y'])
                            ->orderBy('id','desc')
                            ->get();
        }else{
            $category = Country::orderBy('id','desc')
                            ->get();
        }
        return Datatables::of($category)
                ->addColumn('action', function ($category) {
                    if($category->is_active=='Y'){
                        $class='fa fa-toggle-on';
                    }else{
                        $class='fa fa-toggle-off';
                    }

                    if($category->is_active=='Y'){
                        $status_title = 'Inactive';
                    }else{
                        $status_title = 'Active';
                    }

                    return '<a href="'.url('country/edit',$category->id).'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit"><i class="fa fa-edit"></i></a>

                        <a href="'.url('country/status',$category->id).'" id="is_active" data-status="'.$category->is_active.'" class="btn-status m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="'.$status_title.'"><i class="glyphicon '.$class.'"></i></a>
                        
                        <a href="'.url('country/delete',$category->id).'" id="delete_btn" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete"><i class="fa fa-trash"></i></a>';
                })
                ->addColumn('status',function($category){
                    if($category->is_active=='Y'){
                        return '<span class="status_width"><span class="m-badge  m-badge--success m-badge--wide">Active</span></span>';
                    }else{
                        return '<span class="status_width"><span class="m-badge  m-badge  m-badge--danger m-badge--wide">Inactive</span></span>';
                    }
                })->rawColumns(['status', 'action'])                       
                ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend/country/create_form');
    }

    public function store(CountryForm $request)
    {
        if($request->ajax()){
            return true;
        }

        $module=NEW Country();
        $module->name=$request->name;
        $module->created_date=date('Y-m-d H:i:s');
        $module->updated_date=date('Y-m-d H:i:s');
        $module->created_by=auth()->user()->id;
        $module->updated_by=auth()->user()->id;           
        $module->is_active='Y';

        if($module->save()){
            return redirect('country/index')->with('success','Record has been added successfully');
        }else{
            return redirect('country/index')->with('error','Something Error: In data save!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category_data=Country::where(['id'=>$id])->first();

        if($category_data){
            return view('backend/country/edit_form',['category_data'=>$category_data]);    
        }else{
            return redirect('country/index')->with('error','Something Error!');
        }   
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CountryForm $request, $id)
    {

        if($request->ajax()){
            return true;
        }
        $category_model=Country::find($id);

        if($category_model){
            $category_model->name=$request->name;
            $category_model->is_active='Y';
            $category_model->updated_date=date('Y-m-d H:i:s');        
            $category_model->updated_by=auth()->user()->id;

            if($category_model->save()){
                return redirect('country/index')->with('success','Record has been updated successfully');
            }else{
                return redirect('country/index')->with('error','Something Error: In data save!');
            }    
        }else{
            return redirect('country/index')->with('error','Something Error: In data fetch!');
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category_model=Country::find($id);
        if($category_model){
            $category_model->delete();
            return redirect('country/index')->with('success','Record has been deleted successfully');
        }else{
            return redirect('country/index')->with('error','Something Error: In data fetch!');        
        }
    }

    public function status($id)
    {
        $category_model=Country::where(['id'=>$id])->first();
        if($category_model){
            if($category_model->is_active=='Y'){
                $category_model->is_active='N';
            }else{
                $category_model->is_active='Y';
            }   
            $category_model->updated_date=date('Y-m-d H:i:s');        
            $category_model->updated_by=auth()->user()->id;        
            if($category_model->save()){
                if($category_model->is_active=='N'){
                    return redirect('country/index')->with('success','Record has been inactivated successfully');
                }else{
                    return redirect('country/index')->with('success','Record has been activated successfully');
                }
              
            }else{
                return redirect('country/index')->with('error','Something Error: In data save!');      
            }
        }else{
            return redirect('country/index')->with('error','Something Error!');    
        }
    }

}