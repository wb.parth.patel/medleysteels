<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\MyProfileForm;
use App\Http\Requests\ChangePasswordForm;
use App\Http\Controllers\Controller;
use App\common_model\User;
use Illuminate\Support\Facades\Validator;
use App\common_model\Userrole;
use Auth;
use Session;
use Illuminate\Support\Facades\Hash;

class MyProfileController extends Controller
{
	public function index()
	{
		return view('backend/my_profile/index');
	}

	public function edit()
	{

        return view('backend.my_profile.edit_form');
	}

	public function update(MyProfileForm $request, $id)
    {

    }
}
