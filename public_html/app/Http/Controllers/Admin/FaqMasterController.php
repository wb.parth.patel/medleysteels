<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Http\Requests\FaqMasterForm;
use App\common_model\FaqMaster;
use Yajra\Datatables\Datatables;
use Response;

class FaqMasterController extends Controller
{
    public function index()
    {
        return view('backend.faq_master.index');
    }
    public function data(Request $request)
    {
        if(isset($request->filter_data))
        {
            $cms = FaqMaster::where(['is_active'=>$request->filter_data])
                            ->orderBy('id','desc')
                            ->get();
        }else{
            $cms = FaqMaster::
                            orderBy('id','desc')
                            ->get();
            
        }
        return Datatables::of($cms)
                ->addColumn('action', function ($cms) {
                    if($cms->is_active=='Y'){
                        $class='fa fa-toggle-on';
                    }else{
                        $class='fa fa-toggle-off';
                    }

                    if($cms->is_active=='Y'){
                        $status_title = 'Inactive';
                    }else{
                        $status_title = 'Active';
                    }
                    return '<a href="'.url('faq-master/edit',$cms->id).'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit"><i class="fa fa-edit"></i></a>

                        <a href="'.url('faq-master/status',$cms->id).'" id="is_active" data-status="'.$cms->is_active.'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="'.$status_title.'"><i class="glyphicon '.$class.'"></i></a>

                        <a href="'.url('faq-master/view', $cms->id) . '" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill"><i class="fa fa-eye" title="View"></i></a>

                        <a href="'.url('faq-master/delete',$cms->id).'" id="delete_btn" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete"><i class="fa fa-trash"></i></a>';
                })
                ->addColumn('status',function($cms){
                    if($cms->is_active=='Y'){
                        return '<span class="status_width"><span class="m-badge  m-badge--success m-badge--wide">Active</span></span>';
                    }else{
                        return '<span class="status_width"><span class="m-badge  m-badge  m-badge--danger m-badge--wide">Inactive</span></span>';
                    }
                })->rawColumns(['status', 'action'])                       
                ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend/faq_master/create_form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FaqMasterForm $request)
    {
        if($request->ajax()){
            return true;
        }
        $array_=array(
        'question'=>$request->question,
        'answer'=>$request->answer,
        'created_date'=>date('Y-m-d H:i:s'),
        'updated_date'=>date('Y-m-d H:i:s'),
        'created_by'=>auth()->user()->id,
        'updated_by'=>auth()->user()->id,
        'is_active'=>'Y',
        'ip_address'=>$request->ip()
        );

        $cms=NEW FaqMaster($array_);
        if($cms->save()){
            return redirect('faq-master/index')->with('success','Record has been added successfully');    
        }else{
            return redirect('faq-master/index')->with('error','Something Error: In data save!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function view($id)
    {
        $content=FaqMaster::
                    where(['id'=>$id])
                    ->first();
      if ($content) {
       return view('backend.faq_master.view',compact('content'));
      } else {
        return redirect('backend.faq_master.index')->with('error', 'Something Error!');
      }
    }
   

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cms_data=FaqMaster::where(['id'=>$id])->first();
        if($cms_data){
            return view('backend/faq_master/edit_form',['cms_data'=>$cms_data]);    
        }else{
            return redirect('faq-master/index')->with('error','Something Error!');
        }   
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FaqMasterForm $request, $id)
    {

        if($request->ajax()){
            return true;
        }
        $cms_model=FaqMaster::find($id);
        $cms_model->question=$request->question;
        $cms_model->answer=$request->answer;
        $cms_model->updated_date=date('Y-m-d H:i:s');        
        $cms_model->updated_by=auth()->user()->id;
        if($cms_model->save()){
            return redirect('faq-master/index')->with('success','Record has been updated successfully');
        }else{
            return redirect('faq-master/index')->with('error','Something Error: In data save!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cms_model=FaqMaster::find($id);
        if($cms_model){
            $cms_model->delete();
            return redirect('faq-master/index')->with('success','Record has been deleted successfully');
        }else{
            return redirect('faq-master/index')->with('error','Something Error: In data save!');        
        }
    }

    public function status($id)
    {
        $cms_model=FaqMaster::where(['id'=>$id])->first();
        if($cms_model){
            if($cms_model->is_active=='Y'){
                $cms_model->is_active='N';
            }else{
                $cms_model->is_active='Y';
            }   
            $cms_model->updated_date=date('Y-m-d H:i:s');        
            $cms_model->updated_by=auth()->user()->id;        
            if($cms_model->save()){
                if($cms_model->is_active=='N'){
                    return redirect('faq-master/index')->with('success','Record has been inactivated successfully');      
                }else{
                    return redirect('faq-master/index')->with('success','Record has been activated successfully');            
                }
              
            }else{
                return redirect('faq-master/index')->with('error','Something Error: In data save!');      
            }
        }else{
            return redirect('faq-master/index')->with('error','Something Error!');    
        }
    }
}