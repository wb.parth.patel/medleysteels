<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Http\Requests\ProductManagementForm;
use App\Http\Requests\ProductCategoryForm;
use App\common_model\ProductMaster;
use App\common_model\ProductCategory;
use Yajra\Datatables\Datatables;
use Response;


class ProductManagementController extends Controller
{
	public function index(){
        return view('backend.products_management.index');
    }

    public function data(Request $request){
       /* if(isset($request->filter_data))
        {
            $product = ProductMaster::where(['is_active'=>$request->filter_data])
            ->orderBy('id','desc')
            ->get();*/

    }

    public function create(){
        // $products_category=ProductCategory::select('id','category_name')->get();
        //dd($products_category);
        return view('backend/products_management/create_form');

    }

    public function store(ProductManagementForm $request){
       //dd($request);


    }

    public function show($id){

    }

    public function edit($id){


    }
    public function update(ProductManagementForm $request, $id){


    }

    public function status($id){

    }

            }
