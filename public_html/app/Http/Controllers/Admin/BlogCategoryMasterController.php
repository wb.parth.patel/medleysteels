<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Http\Requests\BlogCategoryMasterForm;
use App\common_model\BlogCategoryMaster;
use App\common_model\HotelManagement;
use Yajra\Datatables\Datatables;
use Response;

class BlogCategoryMasterController extends Controller
{
    public function index(){
        return view('backend.blog_category_master.index');
    }
    public function data(Request $request){

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend/blog_category_master/create_form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BlogCategoryMasterForm $request){

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BlogCategoryMasterForm $request, $id){


        //  if(isset(request()->image)){
        //     $new_name = time()."_".rand(1, 999).".".request()->image->getClientOriginalExtension();
        // }else{
        //     $new_name=$user_model->image;
        // }
        //$siteconfig_model->config_key=$request->config_key;

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
    }

    public function status($id){

    }
}
