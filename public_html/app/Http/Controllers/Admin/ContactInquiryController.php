<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Http\Requests\ContactInquiryForm;
use App\common_model\ContactInquiry;
use App\common_model\User;
use Yajra\Datatables\Datatables;
use Response;

class ContactInquiryController extends Controller
{
    public function index()
    {
        return view('backend.contact_inquiry.index');
    }
    public function data(Request $request){

    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){

    }

    public function status($id){

    }

    public function view($id){

    }
}
