<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Http\Requests\CityForm;
use App\common_model\Country;
use App\common_model\State;
use App\common_model\City;
use Yajra\Datatables\Datatables;
use Response;

class CityManagementController extends Controller
{
    public function index()
    {
        return view('backend.city.index');
    }
    public function data(Request $request)
    {
        if(isset($request->filter_data))
        {
            $city = City::select('city.id','city.city_name','country.name','city.is_active','state.state_name')
                            ->leftjoin('country','city.country_id','country.id')
                            ->leftjoin('state','city.state_id','state.id')
                            ->where(['city.is_active'=>'Y'])
                            ->get();            
        }else{
             $city = City::select('city.id','city.city_name','country.name','city.is_active','state.state_name')
                            ->leftjoin('country','city.country_id','country.id')
                            ->leftjoin('state','city.state_id','state.id')
                            ->where(['city.is_active'=>'Y'])
                            ->get(); 
        }
        return Datatables::of($city)
                ->addColumn('action', function ($city) {
                    if($city->is_active=='Y'){
                        $class='fa fa-toggle-on';
                    }else{
                        $class='fa fa-toggle-off';
                    }

                    if($city->is_active=='Y'){
                        $status_title = 'Inactive';
                    }else{
                        $status_title = 'Active';
                    }

                    return '<a href="'.url('city/edit',$city->id).'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit"><i class="fa fa-edit"></i></a>

                        <a href="'.url('city/status',$city->id).'" id="is_active" data-status="'.$city->is_active.'" class="btn-status m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="'.$status_title.'"><i class="glyphicon '.$class.'"></i></a>
                        
                        <a href="'.url('city/delete',$city->id).'" id="delete_btn" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete"><i class="fa fa-trash"></i></a>';
                })
                ->addColumn('status',function($city){
                    if($city->is_active=='Y'){
                        return '<span class="status_width"><span class="m-badge  m-badge--success m-badge--wide">Active</span></span>';
                    }else{
                        return '<span class="status_width"><span class="m-badge  m-badge  m-badge--danger m-badge--wide">Inactive</span></span>';
                    }
                })->rawColumns(['status', 'action'])                       
                ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $country=Country::where(['is_active'=>'Y'])->get();
        $state = State::where(['is_active'=>'Y'])->get();

        return view('backend/city/create_form',['country'=>$country,'state'=>$state]);
    }

    public function store(CityForm $request)
    {
        if($request->ajax()){
            return true;
        }

        $module=NEW City();
        $module->country_id=$request->country_id;
        $module->state_id=$request->state_id;
        $module->city_name=$request->city_name;
        $module->created_date=date('Y-m-d H:i:s');
        $module->updated_date=date('Y-m-d H:i:s');
        $module->created_by=auth()->user()->id;
        $module->updated_by=auth()->user()->id;           
        $module->is_active='Y';

        if($module->save()){
            return redirect('city/index')->with('success','Record has been added successfully');
        }else{
            return redirect('city/index')->with('error','Something Error: In data save!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $country=Country::where(['is_active'=>'Y'])->get();
        $state = State::where(['is_active'=>'Y'])->get();

        $city = City::where(['id'=>$id])->first();

        if($city){
            return view('backend/city/edit_form',['city'=>$city,'country'=>$country,'state'=>$state]);    
        }else{
            return redirect('city/index')->with('error','Something Error!');
        }   
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CityForm $request, $id)
    {

        if($request->ajax()){
            return true;
        }
        $city=City::find($id);

            $city->country_id=$request->country_id;
            $city->state_id=$request->state_id;
            $city->city_name=$request->city_name;
            $city->is_active='Y';
            $city->updated_date=date('Y-m-d H:i:s');        
            $city->updated_by=auth()->user()->id;

            if($city->save()){
                return redirect('city/index')->with('success','Record has been updated successfully');
        }else{
            return redirect('city/index')->with('error','Something Error: In data fetch!');
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $state=City::find($id);
        if($state){
            $state->delete();
            return redirect('city/index')->with('success','Record has been deleted successfully');
        }else{
            return redirect('city/index')->with('error','Something Error: In data fetch!');        
        }
    }

    public function status($id)
    {
        $state=State::where(['id'=>$id])->first();
        if($state){
            if($state->is_active=='Y'){
                $state->is_active='N';
            }else{
                $state->is_active='Y';
            }   
            $state->updated_date=date('Y-m-d H:i:s');        
            $state->updated_by=auth()->user()->id;        
            if($state->save()){
                if($state->is_active=='N'){
                    return redirect('city/index')->with('success','Record has been inactivated successfully');
                }else{
                    return redirect('city/index')->with('success','Record has been activated successfully');
                }
              
            }else{
                return redirect('city/index')->with('error','Something Error: In data save!');      
            }
        }else{
            return redirect('city/index')->with('error','Something Error!');    
        }
    }

}