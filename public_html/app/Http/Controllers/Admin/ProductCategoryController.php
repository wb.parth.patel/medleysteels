<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Http\Requests\ProductCategoryForm;
use App\common_model\ProductCategory;
use Yajra\Datatables\Datatables;
use Response;


class ProductCategoryController extends Controller
{
	public function index()
    {
        return view('backend.products_category_master.index');
    }

    public function data(Request $request){

    }

    public function create()
    {
        return view('backend/products_category_master/create_form');
    }

    public function store(ProductCategoryForm $request){
    	//dd($request);

    }


    public function show($id){

    }

    public function edit($id){

    }

    public function update(ProductCategoryForm $request, $id){


    }

    public function destroy($id){

    }

    public function status($id){

    }

}
