<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Http\Requests\SliderManagementForm;
use App\common_model\SliderManagement;
use App\common_model\SliderImage;
use Yajra\Datatables\Datatables;
use Response;

class SliderManagementController extends Controller
{
    public function index()
    {
        return view('backend.slider_management.index');
    }
    public function data(Request $request)
    {


    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend/slider_management/create_form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SliderManagementForm $request)
    {


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $service=SliderManagement::where(['id'=>$id])->first();
        $image=SliderImage::where(['slider_id'=>$id])->first();
        if($service){
            return view('backend/slider_management/view',['service'=>$service,'image'=>$image]);
        }else{
            return redirect('slider_management/index')->with('error','Something Error!');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function update(SliderManagementForm $request, $id)
    // {
    //     if ($request->ajax()) {
    //         return true;
    //     }

    //     $slider = SliderManagement::where(['id' => $id])->first();
    //     $destinationPath = public_path('media/slider');

    //     SliderImage::where(['slider_id'=>$id])->update(['is_active'=>'Y','is_delete'=>'Y']);
    //     if(isset(request()->image))
    //     {
    //         foreach ($request->image as $key => $value)
    //         {
    //              if (isset($value) && $value != NULL)
    //             {
    //                 $ext = $value->getClientOriginalExtension();
    //                 $new_img_name = time()."_".rand().".".$ext;
    //                 $value->move($destinationPath, $new_img_name);
    //             }
    //             else
    //             {
    //                 $new_img_name = '';
    //             }

    //             $images=new SliderImage();
    //             $images->created_date=date('Y-m-d H:i:s');
    //             $images->created_by=auth()->user()->id;
    //             $images->slider_id=$id;
    //             $images->image=$new_img_name;
    //             $images->is_active='Y';
    //             $images->is_delete='N';
    //             $images->updated_date=date('Y-m-d H:i:s');
    //             $images->updated_by=auth()->user()->id;
    //             $insert = $images->save();
    //         }
    //     }
    //     if ($request->image_id != null) {
    //         foreach ($request->image_id as $key => $value) {

    //         $priceperChield=SliderImage::where(['id'=>$value??''])->first();
    //         $priceperChield->is_active='Y';
    //         $priceperChield->is_delete='N';
    //         $priceperChield->updated_date=date('Y-m-d H:i:s');
    //         $priceperChield->updated_by=auth()->user()->id;
    //         $insert = $priceperChield->save();

    //         }

    //     }


    //     $slider->title = $request->title;
    //     $slider->description = $request->description;
    //     $slider->updated_by = auth()->user()->id;
    //     $slider->updated_date = date('Y-m-d H:i:s');
    //     $slider->ip_address = $request->ip();

    //     if ($slider->update()) {
    //         return redirect('slider-management/index')->with('success', 'Record has been updated successfully');
    //     } else {
    //         return redirect('slider-management/index')->with('error', 'Something Error: In data save!');
    //     }
    // }

    public function update(SliderManagementForm $request, $id)
    {


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }

    public function status($id)
    {

    }
}
