<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Http\Requests\NotificationForm;
use App\common_model\NotificationManagement;
use App\common_model\HotelManagement;
use Yajra\Datatables\Datatables;
use App\common_model\User;
use Response;

class NotificationManagementController extends Controller
{
	public function index()
	{
		return view('backend.notification_management.index');
	}

	public function data(Request $request)
	{
		 if(isset($request->filter_data))
        {
            $notification = NotificationManagement::where(['is_active'=>$request->filter_data])
                            ->orderBy('id','desc')
                            ->get();
        }else{
            $notification = NotificationManagement::
                            orderBy('id','desc')
                            ->get();
        }
        return Datatables::of($notification)
                ->addColumn('action', function ($notification) {
                    if($notification->is_active=='Y'){
                        $class='glyphicon-remove-circle';
                    }else{
                        $class='glyphicon-ok-circle';
                    }

                    if($notification->is_active=='Y'){
                        $status_title = 'Inactive';
                    }else{
                        $status_title = 'Active';
                    }
                    return '
                        <a href="'.url('notification-management/view', $notification->id) . '" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill"><i class="fa fa-eye" title="View"></i></a>

                        <a href="'.url('notification-management/delete',$notification->id).'" id="delete_btn" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete"><i class="fa fa-trash"></i></a>';
                })
                ->rawColumns(['status', 'action'])                       
                ->make(true);
	}

    public function create()
    {
        $user=User::where(['is_active'=>'Y'])->where('user_role_id','=','1')->get();
        return view('backend.notification_management.create_form', compact('user'));
    }

    public function store(NotificationForm $request)
    {
        if($request->ajax()){
            return true;
        }

        $notification = new NotificationManagement();
        $notification->title=$request->title; 
        $notification->description=$request->description;
        $notification->notification_type=$request->notification_type;
        $notification->notification_to=implode(',',(array)$request->notification_to)??'1';
        $notification->created_date=date('Y-m-d H:i:s');
        $notification->updated_date=date('Y-m-d H:i:s');
        $notification->created_by=auth()->user()->id;
        $notification->updated_by=auth()->user()->id;           
        $notification->is_active='1';

        if($notification->save()){

            return redirect('notification-management/index')->with('success','Record has been added successfully');    
        }else{
            return redirect('notification-management/index')->with('error','Something Error: In data save!');
        }
    }

     public function destroy($id)
    {
        $notification=NotificationManagement::find($id);
        if($notification){
            $notification->delete();
            return redirect('notification-management/index')->with('success','Record has been deleted successfully');
        }else{
            return redirect('notification-management/index')->with('error','Something Error: In data save!');        
        }
    }

    public function view($id)
    {
        $notification = NotificationManagement::select('id','title','description','notification_type')->where(['id'=>$id])->first();

        if($notification)
        {
            return view('backend.notification_management.view',compact('notification'));
        }else{
            return redirect('notification-management/index')->with('error','Something Error: In data save!');
        }
    }


}