<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Http\Requests\StateForm;
use App\common_model\Country;
use App\common_model\State;
use Yajra\Datatables\Datatables;
use Response;

class StateManagementController extends Controller
{
    public function index()
    {
        return view('backend.state.index');
    }
    public function data(Request $request)
    {
        if(isset($request->filter_data))
        {
            $state = State::select('state.id','state.name','country.name','state.is_active')
                            ->leftjoin('country','state.country_id','country.id')
                            ->where(['state.is_active'=>'Y'])
                            ->orderBy('id','desc')
                            ->get();            
        }else{
             $state = State::select('state.id','state.state_name','country.name','state.is_active')
                            ->leftjoin('country','state.country_id','country.id')
                            
                            ->get();
            
        }
        return Datatables::of($state)
                ->addColumn('action', function ($state) {
                    if($state->is_active=='Y'){
                        $class='fa fa-toggle-on';
                    }else{
                        $class='fa fa-toggle-off';
                    }

                    if($state->is_active=='Y'){
                        $status_title = 'Inactive';
                    }else{
                        $status_title = 'Active';
                    }

                    return '<a href="'.url('state/edit',$state->id).'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit"><i class="fa fa-edit"></i></a>

                        <a href="'.url('state/status',$state->id).'" id="is_active" data-status="'.$state->is_active.'" class="btn-status m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="'.$status_title.'"><i class="glyphicon '.$class.'"></i></a>
                        
                        <a href="'.url('state/delete',$state->id).'" id="delete_btn" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete"><i class="fa fa-trash"></i></a>';
                })
                ->addColumn('status',function($state){
                    if($state->is_active=='Y'){
                        return '<span class="status_width"><span class="m-badge  m-badge--success m-badge--wide">Active</span></span>';
                    }else{
                        return '<span class="status_width"><span class="m-badge  m-badge  m-badge--danger m-badge--wide">Inactive</span></span>';
                    }
                })->rawColumns(['status', 'action'])                       
                ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $country = Country::select('id','name','is_active')->where(['is_active'=>'Y'])->get();

        return view('backend/state/create_form',['country'=>$country]);
    }

    public function store(StateForm $request)
    {
        if($request->ajax()){
            return true;
        }

        $module=NEW State();
        $module->country_id=$request->country_id;
        $module->state_name=$request->state_name;
        $module->created_date=date('Y-m-d H:i:s');
        $module->updated_date=date('Y-m-d H:i:s');
        $module->created_by=auth()->user()->id;
        $module->updated_by=auth()->user()->id;           
        $module->is_active='Y';

        if($module->save()){
            return redirect('state/index')->with('success','Record has been added successfully');
        }else{
            return redirect('state/index')->with('error','Something Error: In data save!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $country=Country::where(['is_active'=>'Y'])->get();
        $state = State::where(['id'=>$id])->first();

        if($state){
            return view('backend/state/edit_form',['state'=>$state,'country'=>$country]);    
        }else{
            return redirect('state/index')->with('error','Something Error!');
        }   
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StateForm $request, $id)
    {

        if($request->ajax()){
            return true;
        }
        $state=State::find($id);

            $state->country_id=$request->country_id;
            $state->state_name=$request->state_name;
            $state->is_active='Y';
            $state->updated_date=date('Y-m-d H:i:s');        
            $state->updated_by=auth()->user()->id;

            if($state->save()){
                return redirect('state/index')->with('success','Record has been updated successfully');
        }else{
            return redirect('state/index')->with('error','Something Error: In data fetch!');
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $state=State::find($id);
        if($state){
            $state->delete();
            return redirect('state/index')->with('success','Record has been deleted successfully');
        }else{
            return redirect('state/index')->with('error','Something Error: In data fetch!');        
        }
    }

    public function status($id)
    {
        $state=State::where(['id'=>$id])->first();
        if($state){
            if($state->is_active=='Y'){
                $state->is_active='N';
            }else{
                $state->is_active='Y';
            }   
            $state->updated_date=date('Y-m-d H:i:s');        
            $state->updated_by=auth()->user()->id;        
            if($state->save()){
                if($state->is_active=='N'){
                    return redirect('state/index')->with('success','Record has been inactivated successfully');
                }else{
                    return redirect('state/index')->with('success','Record has been activated successfully');
                }
              
            }else{
                return redirect('state/index')->with('error','Something Error: In data save!');      
            }
        }else{
            return redirect('state/index')->with('error','Something Error!');    
        }
    }

}