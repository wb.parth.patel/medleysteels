<?php

    namespace App\Http\Controllers;

    use Illuminate\Foundation\Bus\DispatchesJobs;
    use Illuminate\Routing\Controller as BaseController;
    use Illuminate\Foundation\Validation\ValidatesRequests;
    use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
    use App\common_model\SiteConfiguration;
    use App\common_model\UserDevice;
    use App\common_model\User;
    use App\common_model\NotificationManagement;
    use Twilio\Rest\Client;
    use Mail;

    class Controller extends BaseController{

        use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
        
        public static function getSiteconfigValueByKey($key){
        	$model = SiteConfiguration::where(['config_key' => $key, 'is_active' => 'Y'])->first();
        	if(!empty($model)){
                $data = array(
                  "value" => $model->config_value,
                  "image" => url('media/site_config',$model->image),
                );
        		return $data;
        	}else{
        		return 'Undefine Key';
        	}
        }

        function SendSms($phone_number, $body){
            try{
                $sid  = $this->getSiteconfigValueByKey('TWILIO_SID');
                $token = $this->getSiteconfigValueByKey('TWILIO_TOKEN');
                $number = $this->getSiteconfigValueByKey('TWILIO_NUMBER');

                $client = new Client($sid, $token);            
           
                $sms_response = $client->messages->create($phone_number, ['from' => $number, 'body' => $body]);
                return $sms_response;
            }catch(\Services_Twilio_RestException $e){
                return false;
            }catch(Exception $ex){
                return false;
            }
            return true;
        }

        function UserAccess($user_id, $user_device_id, $access_token){
            try{
                $user_device = UserDevice::where(['user_id' => $user_id, 'user_device_id' => $user_device_id,'user_device_token' => $access_token,'is_login' => 
                            'Y'])->where('user_device_token', '!=', '')->count();

                $user = User::where(['id' => $user_id, 'is_active' => 'Y'])->count();
                if($user_device > 0 && $user > 0){
                    return true;
                }else{
                    return false;
                }
            }catch(Exception $ex){
                return false;
            }
        }

        public static function checkUserRole($role_name){    
            $userrole = Userrole::where(['name' => $role_name, 'is_active' => 'Y'])->first();
            if($userrole){
                return $userrole->id;
            }else{
                return false;
            }
        }

        public function sendMultiplePushNotificationvideocall($fields){
            $url = 'https://fcm.googleapis.com/fcm/send';
            $headers = array(
                'Authorization: key=AAAAzHFr5GI:APA91bHQTjkW5VE6puJ2xD3KanekdiOdh4-2pw0ED30GXWQlfSXQRqN2sAmA-IcOenOOIfRj6lkwuoZ8bJ9_P0ocgRSlmARcc-ZP_YY7daYiK_bP6g_hrNne3QGGyueosnZza0yQogEI',
                'Content-Type: application/json'
            );

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

            $result = curl_exec($ch);

            if($result === FALSE){
                die('Curl Failed: ' . curl_error($ch));
            }

            curl_close($ch);
            $res = json_decode($result);

            if(isset($res->success) && $res->success){
                return true;
            }else{
                return false;
            }
        }

        function action($route,$site_config)
        {
            if($site_config->is_active=='Y'){
                        $class='fa fa-toggle-on';
                    }else{
                        $class='fa fa-toggle-off';
                    }

                    if($site_config->is_active=='Y'){
                        $status_title = 'Inactive';
                    }else{
                        $status_title = 'Active';
                    }

            return '<a href="'.url($route.'/edit',$site_config->id).'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit"><i class="fa fa-edit"></i></a>

                        <a href="'.url($route.'/status',$site_config->id).'" id="is_active" data-status="'.$site_config->is_active.'" class="btn-status m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="'.$status_title.'"><i class="'.$class.'"></i></a>

                        <a href="'.url($route.'/delete',$site_config->id).'" id="delete_btn" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete"><i class="fa fa-trash"></i></a>

                        <a href="'.url($route.'/view',$site_config->id).'"  class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="View"><i class="fa fa-eye"></i></a>
                        ';
        }

        function sendNotitficationvideocall($title, $message, $type, $user_id, $activity, $name, $room_name, $participate_id, $id = ''){

            $fcmtoken = UserDevice::select('fcm_token')->where(['user_id' => $participate_id])->where('user_device_token','!=','')->get();

            $fcm_token = array();
            if(!empty($fcmtoken) && count((array) $fcmtoken) > 0){
                foreach($fcmtoken as $key => $val){
                    $fcm_token[] = $val->fcm_token;
                }

            $postdata['type'] = $type;
            if($id != ''){
                $postdata['id'] = $participate_id;
            }

            $notificationData = [
                'user_id' => $user_id,
                'title' => $title,
                'message' => $message,
                'body' =>  $message,
                'type' => $type,
                'id' => $participate_id,
                'click_action' => $activity,
                'name' => $name,
                'roomname' => $room_name
            ];

            $arr_fields = array(
                'registration_ids' => $fcm_token,
                'notification' => [
                    'title' => $title,
                    'message' => $message,
                    'body' =>  $message,
                    'type' => $type,
                    'click_action' => $activity,
                    'name' => $name,
                    'roomname' => $room_name
                ],
                'data' => $notificationData, 
                'click_action' => $activity,
            );

            $sendnotification = $this->sendMultiplePushNotificationvideocall($arr_fields);

                if($sendnotification == true){
                    $notify = new NotificationManagement(); 
                    $notify->notification_to = $user_id;
                    $notify->title = $title;
                    $notify->description = $message;
                    $notify->notification_type = $type;
                    $notify->notificaiton_from = $participate_id;
                    $notify->created_by = $user_id;
                    $notify->created_date = date('Y-m-d H:i:s');
                    $notify->updated_by = $user_id;
                    $notify->updated_date = date('Y-m-d H:i:s'); 
                    $notify->is_active = 'Y';
                    $notify->is_delete = 'N';
                    $notify->save();
                }
            }
        }
    }