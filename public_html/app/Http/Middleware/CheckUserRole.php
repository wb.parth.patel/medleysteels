<?php

namespace App\Http\Middleware;
use App\common_model\User;
use Auth;

use Closure;

class CheckUserRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next){

        if(\Auth::user()->is_active!='Y'){
            \Auth::logout();
            redirect('/logout');
        }

        $allowaction=array('data');
        $allowcontroller=array('home','dashboard','changePassword');
        $action=explode('/',$request->getPathInfo());

        $action=explode('/',$request->getPathInfo());

        if(in_array(($action[1]??''), $allowcontroller)==false && in_array(($action[2]??''), $allowaction)==false){
            if(($action[2]??'')=='store'){
                $action[2]='create';
            }

            else if(($action[2]??'')=='update'){
                $action[2]='edit';

            }else if(($action[2]??'')=='index'){
                $action[2]='list';
            }

            $user_id=\Auth::user()->id;
            $role=User::SELECT('module_name','action_name')
            ->join('userrole','users.user_role_id','userrole.id')
            ->join('role_action','userrole.id','role_action.role_id')
            ->join('module','role_action.module_id','module.id')
            ->join('action','role_action.action_id','action.id')
            ->where(['userrole.is_active'=>'Y','userrole.is_deleted'=>'N','role_action.is_active'=>'Y','module_name'=>str_replace("-", " ",$action[1]),'action_name'=>$action[2],'users.id'=>$user_id])
            ->count();

            if($role>0 || (\Auth::user()->is_admin=='Y' && \Auth::user()->user_role_id==1)){

                $response = $next($request);
                return $response->header('Cache-Control','nocache, no-store, max-age=0, must-revalidate')
                    ->header('Pragma','no-cache')
                    ->header('Expires','Sat, 26 Jul 1997 05:00:00 GMT');
            }else{
                return redirect()->back()->with('error','You are not authorized to perform this operation');
            }
        }else{
        $response = $next($request);
                return $response->header('Cache-Control','nocache, no-store, max-age=0, must-revalidate')
                    ->header('Pragma','no-cache')
                    ->header('Expires','Sat, 26 Jul 1997 05:00:00 GMT');
    }
}
}
