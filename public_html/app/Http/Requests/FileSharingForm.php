<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FileSharingForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        if($this->method()=='PATCH'){

            return [
            'sub_user_id'=>'required',
            'name'=>'required',
            'documents'=>'required'
            ];    
        }else{
            return [
            'sub_user_id'=>'required',
            'name'=>'required',
            'documents'=>'required'
            ];
        }
    }
    public function messages(){
        return [
            'sub_user_id.required'=>'Please Enter User Name',
            'name.required'=>'Please Enter Name',
            'documents.required'=>'Please Select Document'
        ];
    }
}
