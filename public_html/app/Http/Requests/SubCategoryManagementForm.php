<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SubCategoryManagementForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        if($this->method()=='PATCH'){

            return [
                'sub_name'=>'required'
            ];    
        }else{
            return [
            'category_id'=>'required',
            'sub_name'=>'required|unique:sub_category_management',
            'level_id'=>'required',
            ];
        }
    }
    public function messages(){
        return [
            'category_id.required'=>'Please select category',
            'sub_name.unique'=>'Sub Category already exist',
            'sub_name.required'=>'Please enter sub category',
            'description.required'=>'Please enter description',
            'level_id.required'=>'Please select levels',
        ];
    }
}
