<?php
namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

class BlackOutDateForm extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        if($this->method()=='PATCH'){
            return [
            'from_date'=>"required|date|date_format:Y-m-d",
            'to_date'=>"required|date|date_format:Y-m-d|after:from_date",
            ];    
        }else{
            return [
            'from_date'=>"required|date|date_format:Y-m-d",
            'to_date'=>"required|date|date_format:Y-m-d|after:from_date",
            ];    
        }
        
    }
    
    public function messages(){
        return [
            'from_date.required'=>'From date is required',        
            'to_date.required'=>'To date is required',
        ];
    }
}
