<?php
namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

class HotelManagementForm extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        if($this->method()=='PATCH'){
            return [
            'hotel_name'=>"required|unique:hotel_management,hotel_name",
            'hotel_contact'=>"required",
            'hotel_address'=>"required",
            'hotel_email'=>"required",
            ];    
        }else{
            return [
            'hotel_name'=>'required|max:60|unique:hotel_management,hotel_name',
            'hotel_email'=>"required",
            ];    
        }
        
    }
    
    public function messages(){
        return [
            'hotel_name.required'=>'Hotel Name is required',        
            'hotel_contact.required'=>'Hotel Contact Number is required',
            'hotel_address.required'=>'Hotel Address is required',
            'hotel_email.required'=>'Hotel Email is required',
            'hotel_logo.required'=>'Hotel Logo is required',
            'country_id.required'=>'Country is required',
            'state_id.required'=>'State is required',
            'city_id.required'=>'City is required',
            'hotel_lat.required'=>'Hotel Latitude is required',
            'hotel_long.required'=>'Hotel Longitude is required',
        ];
    }
}
