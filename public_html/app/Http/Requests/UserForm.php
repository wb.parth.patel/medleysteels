<?php
namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;
class UserForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    
    public function rules()
    {
        if($this->method()=='PATCH'){
            return [
                    'name'=>'required|max:60',
                   
                    'email'=>'required|email|unique:users,email,'.$this->id,
                   ];
        }else{
            return [
                    'user_role_id'=>'required',
                    'name'=>'required|max:60',
                    'contact_number'=>'required|numeric|digits_between:6,13|unique:users,contact_number',
                    'email'=>'required|email|unique:users,email',
                    // 'password'=>'required|min:8'
                   ];
        }
    }

    public function messages(){
        return[
                'user_role_id.required'=>'Please select user role ',
                'name.required'=>'Please enter name',                
                'contact_number.required'=>'Please enter contact number',
                'email.required'=>'Please enter email',
                'email.email'=>'Please enter valid email address',
                'password.required'=>'Please enter password',
                'password.min' => 'The password must be at least 8 characters.',
                'dob.required' => 'Please select Date of birth',
                'image.required'=>'Please select image'
              ];
    }
}