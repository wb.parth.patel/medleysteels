<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class WishlistMasterForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->method()=='PATCH'){
            return [
                'user_id'=>'required',
                'deal_id'=>'required',
                //'hotel_id'=>'required',
            ];
        }else{
            return [
                'user_id'=>'required',
                'deal_id'=>'required',
                //'hotel_id'=>'required',
            ];
        }
    }

    public function messages(){
        return[ 
            'user_id.required'=>'Please select user',
            'deal_id.required'=>'Please select deal',
            //'hotel_id.required'=>'Please select hotel',
        ];
    }
}
