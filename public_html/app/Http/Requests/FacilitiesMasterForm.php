<?php
namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

class FacilitiesMasterForm extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        if($this->method()=='PATCH'){
            return [
            'facility_name'=>"required",
            'description'=>"required",
            ];    
        }else{
            return [
            'facility_name'=>'required|max:60|unique:facilities_master,facility_name',
            'description'=>"required",
            ];    
        }
        
    }
    
    public function messages(){
        return [
            'facility_name.required'=>'Please enter facilitiy name.',        
            'description.required'=>'Please enter facility description.',
        ];
    }
}
