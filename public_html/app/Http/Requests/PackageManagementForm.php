<?php
namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

class PackageManagementForm extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        if($this->method()=='PATCH'){
            return [
            'room_name'=>"required",
            'number_of_nights'=>"required|digits_between:1,2",
            'maximum_number_of_rooms_per_booking'=>"required|digits_between:1,2",
            'description'=>"required",
            'video'=>"required",
            'from_date_surcharge'=>'required',
            'to_date_surcharge'=>'required|after_or_equal:from_date_surcharge',
            'actual_price'=>"required|digits_between:1,6",
            'discounted_price'=>"required|digits_between:1,6",
            'hotel_price'=>"required|digits_between:1,6",
            'number_of_adults_allowed'=>"required|digits_between:1,3",
            'number_of_childs_allowed'=>"required|digits_between:1,3",
            'price_per_extra_adult'=>"required|digits_between:1,6",
            ];    
        }else{
            return [
            'deal_id'=>"required",
            'room_name'=>"required",
            'number_of_nights'=>"required|digits_between:1,2",
            'maximum_number_of_rooms_per_booking'=>"required|digits_between:1,2",
            'description'=>"required",
            'video'=>"required",
            'from_date_surcharge'=>'required',
            'to_date_surcharge'=>'required|after_or_equal:from_date_surcharge',
            'actual_price'=>"required|digits_between:1,6",
            'discounted_price'=>"required|digits_between:1,6",
            'hotel_price'=>"required|digits_between:1,6",
            'number_of_adults_allowed'=>"required|digits_between:1,3",
            'number_of_childs_allowed'=>"required|digits_between:1,3",
            'price_per_extra_adult'=>"required|digits_between:1,6",
            'surcharge_rate'=>"required|digits_between:1,4",
            // 'price_per_extra_child'=>"required|digits_between:1,6",
            ];    
        }
        
    }
    
    public function messages(){
        return [
            'hotel_user_id.required'=>'Please enter Hotel User',
            'hotel_id.required'=>'The Hotel is required',        
            /*'room_type.required'=>'Please enter room type',*/
            'description.required'=>'Please enter description',
        ];
    }
}
