<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class IntroManagementForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        if($this->method()=='PATCH'){

            return [
            'title'=>'required',
            'description'=>'required',
            ];    
        }else{
            return [
            'title'=>'required|unique:intro_management,title',
            'description'=>'required'
            ];
        }
    }
    public function messages(){
        return [
            'title.required'=>'Please Enter title',
            'description.required'=>'Please Enter description'
        ];
    }
}
