<?php
namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

class ProductManagementForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    
    public function rules()
    {
        if($this->method()=='PATCH'){
            return [
                'product_name'=>'required',
                'product_description'=>'required',
                    
            ];
        }else{
            return [
                     'category_id'=>'required',
                    // 'name'=>'required|max:60',
                    // 'contact_number'=>'required|numeric|digits_between:6,13|unique:users,contact_number',
                'product_name'=>'required',
                'product_description'=>'required',
                    // 'password'=>'required|min:8'
            ];
        }
    }

    public function messages(){
        return[
                'category_id.required'=>'Please select category',
                
                'product_name.required'=>'Product name is required',
                'product_description.required'=>'Product description is required',

            ];
        }
    }