<?php
namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

class BlogManagementForm extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        if($this->method()=='PATCH'){
            return [
            'category_id'=>'required',
            'blog_title'=>"required",
            'blog_description'=>"required",
            
            ];    
        }else{
            return [
                 'category_id'=>'required',
            'blog_title'=>'required|max:60|',
            'blog_description'=>"required",
            ];    
        }
        
    }
    
    public function messages(){
        return [
            'hotel_user_id.required'=>'Hotel user Id is required',        
            'hotel_id.required'=>'Hotel name is required',
            'category_id.required'=>'Category name is required',
            'blog_title.required'=>'Blog title is required',
            'blog_description.required'=>'Blog description is required',
            'category_id.required'=>'Please select category',
        ];
    }
}
