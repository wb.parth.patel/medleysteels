<?php
namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

class SubsxriptionFrontForm extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        if($this->method()=='POST'){  
            return [
                'email' => 'required|unique:newslatter_master,subscribe_email|regex:/^[\w-]+(?:\.[\w-]+)*@(?:[\w-]+\.)+[a-zA-Z]{2,7}$/',
            ];    
        }
        
    }
    
    public function messages(){
        return [
            'email.required' => 'Please enter email.',
            'email.unique' =>'This email address has already subscribed.',
        ];
    }
}
