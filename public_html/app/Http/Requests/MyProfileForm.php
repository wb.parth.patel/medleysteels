<?php
namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;
class MyProfileForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    
    public function rules()
    {
        if($this->method()=='PATCH'){
            return [
               'name'=>'required|max:60',
                    'contact_number'=>'required|numeric|digits_between:6,13|unique:users,contact_number,'.$this->id,
                   ];
        }else{
            return [
                    // 'user_role'=>'required',
                    'name'=>'required|max:60',
                    'contact_number'=>'required|numeric|unique:users,contact_number',
                    // 'user_role'=>'required',
                    // 'dob'=>'required|date',
                    'email'=>'required|email|unique:users,email',
                    // 'password'=>'required',
                    // 'file' => 'mimes:jpeg,jpg,png',
                    /*'file'=>[function ($attribute, $value, $fail) { 
                        if(!$attribute){
                          return $fail(__('Please select file'));
                        }
                    }],*/
                    //'image' => 'required|mimes:jpeg,jpg,png',
                    // 'is_admin'=>'required'
                   ];
        }
    }

    public function messages(){
        return[
                'user_role.required'=>'Please select user role ',
                'user_name.required'=>'Please enter user name',
                'contact_number.required'=>'Please enter contact number',
                'email.required'=>'Please enter email',
                'email.email'=>'Please enter valid email address',
                'password'=>'Please enter password',
                'file.mimes'=>'The user profile image must be a file of type: jpeg, jpg, png.',
              ];
    }
}