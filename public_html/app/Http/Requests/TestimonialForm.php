<?php
namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

class TestimonialForm extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        if($this->method()=='PATCH'){
            return [
            ];    
        }else{
            return [
            ];    
        }
        
    }
    
    public function messages(){
        return [
            'title.required'=>'Title is required',        
            'description.required'=>'Description is required',
            'youtube_link.required'=>'Youtube link is required',
        ];
    }
}
