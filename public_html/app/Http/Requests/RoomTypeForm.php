<?php
namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

class RoomTypeForm extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        if($this->method()=='PATCH'){
            return [
            'room_type'=>"required",
            'description'=>"required",
            ];    
        }else{
            return [
            'room_type'=>'required|max:60|',
            'description'=>"required",
            ];    
        }
        
    }
    
    public function messages(){
        return [
            'hotel_user_id.required'=>'Please enter Hotel User',
            'hotel_id.required'=>'The Hotel is required',        
            'room_type.required'=>'Please enter room type',
            'description.required'=>'Please enter description',
        ];
    }
}
