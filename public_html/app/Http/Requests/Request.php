<?php
namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

class Request extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        if($this->method()=='PATCH'){
            return [

                'category_id'=>'required',
                'product_name'=>'required|max:60',
                  // 'product_description'=>'required
                //'category_id'=>'required',
            ];    
        }else{
            return [

                'category_id'=>'required',
                'product_name'=>'required|unique:products_management',
                'product_description'=>'required',
            ];    
        }
    }
    
    public function messages(){
        return [

            'category_id.required'=>'Please select category',
            'product_name.required'=>'Please enter product name',
            'product_description'=>'Enter Product Description',
        ];
    }
}
