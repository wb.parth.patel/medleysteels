<?php
namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

class NewslatterMasterForm extends FormRequest
{


    public function rules()
    {
        if($this->method()=='PATCH'){
            return [
            'subscribe_email'=>"required",
           
            ];    
        }else{
            return [
            'subscribe_email'=>'required|max:60|',

            ];    
        }
        
    }
    
    public function messages(){
        return [
            'subscribe_email.required'=>'Please enter email address',       
        ];
    }
}
