<?php
namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

class ProductCategoryForm extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        if($this->method()=='PATCH'){
            return [
                'category_name'=>'required',
                
            ];    
        }else{
            return [
                'category_name'=>'required|unique:products_category_master',
                'category_img'=>'required',

            ];    
        }
        
    }
    
    public function messages(){
        return [
            'category_name.required'=>'Category name is required',
            'category_name.unique'=>'Category name is already exists',
            'category_img.required'=>'Category image is required',
        ];
    }
}
