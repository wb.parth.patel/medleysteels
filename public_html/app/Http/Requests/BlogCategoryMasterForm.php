<?php
namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

class BlogCategoryMasterForm extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        if($this->method()=='PATCH'){
            return [
            // 'hotel_user_id'=>"required",
            // 'hotel_id'=>"required",
            'category_name'=>"required",
            'category_image'=>'required',
            // 'image'=>"required",
            ];    
        }else{
            return [
            'category_name'=>'required|max:60',
            'category_image'=>'required',
            ];    
        }
        
    }
    
    public function messages(){
        return [
            'hotel_id.required'=>'Hotel name is required',        
            'category_name.required'=>'Category name is required',
            'category_image.required'=>'Category image is required',
        ];
    }
}
