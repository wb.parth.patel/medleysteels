<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FeatureManagementForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        if($this->method()=='PATCH'){

            return [
            ];    
        }else{
            return [
            'feature_name'=>'required',
            'category_id'=>'required',
            'feature_text'=>'required',
            'level_id'=>'required',
            'sub_category_id'=>'required'
            ];
        }
    }
    public function messages(){
        return [
            'feature_name.required'=>'Please Enter Feature Name',
            'category_id.required'=>'Please Select Category',
            'sub_category_id.required'=>'Please Select Sub Category',
            'feature_text.required'=>'Please Enter Feature Text',
            'level_id.required'=>'Please Select Level'
        ];
    }
}
