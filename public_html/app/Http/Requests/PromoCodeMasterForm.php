<?php
namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

class PromoCodeMasterForm extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        if($this->method()=='PATCH'){
            return [
            'deal_id'=>"required",
            'amount'=>"required",
            'promo_code_name'=>"required",
            'code_start_date'=>"required",
            'code_end_date'=>"required",
            'user_specific'=>"required",
            'use_limit'=>"required",
            'user_id'=>"required",
            'allow_credit'=>"required",
            ];    
        }else{
            return [
            'deal_id'=>'required',
            'amount'=>'required',
            'promo_code_name'=>'required|max:60|',
            'code_start_date'=>'required|date',
            'code_end_date'=> 'required|date|after_or_equal:code_start_date',
            'allow_credit'=>'required',
            'user_id'=>"required",
            ];    
        }
        
    }
    
    public function messages(){
        return [
            'deal_id.required'=>'Deal Name is required',        
            'amount.required'=>'Amount is required',
            'promo_code_name.required'=>'Promo Code Name is required',
            'code_start_date.required'=>'Start Date is required',
            'code_end_date.required'=>'End Date is required',
            'user_specific.required'=>'User Specific is required',
            'use_limit.required'=>'Limit is required',
            'user_id.required'=>'Select user',
            'allow_credit.required'=>'Allow Credit is required',
        ];
    }
}
