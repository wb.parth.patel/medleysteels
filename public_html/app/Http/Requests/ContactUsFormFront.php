<?php
namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

class ContactUsFormFront extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    
    public function rules()
    {
        if($this->method()=='PATCH'){
            return [
                'name'=>'required|max:35',
               //'email_id'=>'required',
           ];
        }else{
            return [
                'name'=>'required|max:35',
          //   'email_id'=>'required',
          // 'contact_number' => 'required|numeric|digits_between:6,13',
          //       'queries' => 'max:130',
          //        'message' => 'max:130',

            ];
        }
    } 

    public function messages(){
        return[
            'name.required' => 'Please enter name.',
            'name.max' => ' The name may not be greater than 35 characters.',
            // 'email_id.required' => 'Please enter email.',
            // 'email_id.unique' =>'This email address has already subscribed.',
            // 'email_id.max' =>'The email may not be greater than 51 characters.',
            // 'contact_number.required' => 'Please enter phone number.',
        //    'email_id.required'=>'Please enter email.',
        //     'email_id.email_id'=>'Please enter valid email address.',
        //     'queries.required'=>'The subject may not be greater than 130 characters.',
        //     'message.required'=>'The subject may not be greater than 130 characters.',
              ];
    } 
}







