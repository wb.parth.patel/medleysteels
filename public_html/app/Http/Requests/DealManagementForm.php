<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DealManagementForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        if($this->method()=='PATCH'){

            return [
                'deal_name'=>'required',
                'deal_url'=>'required',
                'main_description'=>'required',
                'highlights'=>'required',
                'fine_print'=>'required',
                'services'=>'required',
                'deal_start_date'=>'required',
                'deal_end_date'=>'required',
                'featured_inclusion'=>'required',    
                'popularity_rank'=>'required|digits_between:1,1',
                'tripadvisor_rating'=>'required',
                'deal_price'=>'required',
                'deal_upto_price'=>'required',
                'deal_save_upto'=>'required',
                'allow_point_redemption'=>'required',
                'deal_status'=>'required',
                'featured_deal'=>'required',
                'what_you_will_love'=>'required',
                'safety_standards'=>'required',
                'how_to_get_there'=>'required',
                'contact_us'=>'required',
                'property_name'=>'required',
                'meta_title'=>'required',
                'meta_description'=>'required',
                'notes_from_hotel'=>'required',
                'travel_end_date'=>'required',
                'deal_cashback'=>'required',
                'meta_keywords'=>'required',
                'deal_video'=>'required',
                'headline'=>'required',
                'about_hotel'=>'required',
                'inclusion'=>'required',
                'location_google'=>'required',
                'theme'=>'required',
                'part_payment'=>'required',
                'early_purchase'=>'required',
                'latest_addition'=>'required',
                'usage_of_multiple_packages'=>'required',
                'purchase_multiple_deal'=>'required',
            ];    
        }else{
            return [
                'deal_name'=>'required|unique:deal_management,deal_name',
                'deal_url'=>'required',
                /*'image'=>'required',*/
                'main_description'=>'required',
                'highlights'=>'required',
                'fine_print'=>'required',
                'services'=>'required',
                'hotel_facilities'=>'required',
                'deal_start_date'=>'required',
                'deal_end_date'=>'required|after_or_equal:deal_start_date',
                'featured_inclusion'=>'required',
                'popularity_rank'=>'required|digits_between:1,2',
                'tripadvisor_rating'=>'required|digits_between:1,2',
                'deal_price'=>'required|digits_between:1,10',
                'deal_upto_price'=>'required|digits_between:1,10',
                'deal_save_upto'=>'required|digits_between:1,10',
                'allow_point_redemption'=>'required',
                'deal_status'=>'required',
                'featured_deal'=>'required',
                'what_you_will_love'=>'required',
                'safety_standards'=>'required',
                'how_to_get_there'=>'required',
                'contact_us'=>'required',
                'property_name'=>'required',
                'meta_title'=>'required',
                'meta_description'=>'required',
                'notes_from_hotel'=>'required',
                'travel_end_date'=>'required',
                'deal_cashback'=>'required',
                'meta_keywords'=>'required',
                'deal_video'=>'required',
                'headline'=>'required',
                'about_hotel'=>'required',
                'inclusion'=>'required',
                'location_google'=>'required',
                'theme'=>'required',
                'part_payment'=>'required',
                'early_purchase'=>'required',
                'latest_addition'=>'required',
                'usage_of_multiple_packages'=>'required',
                'purchase_multiple_deal'=>'required',
            ];
        }
    }
    public function messages(){
        return [
            'name.required'=>'Please enter country name',
            'name.unique'=>'Country already exist',
            'deal_name.required'=>'Please enter deal name',
            'deal_url.required'=>'Please enter deal url',
            'highlights.required'=>'Please enter highlights',
            'fine_print.required'=>'Please enter fine prints',
            'services.required'=>'Please enter services',
            'hotel_facilities.required'=>'Please select hotel facilities',
            'deal_start_date.required'=>'Please select deal start date',
            'deal_end_date.required'=>'Please select deal end date',
            'featured_inclusion.required'=>'Please enter featured inclusion',
            'cancelation_policy.required'=>'Please enter cancelation policy',
            'popularity_rank.required'=>'Please enter popularity rank',
            'tripadvisor_rating.required'=>'Please enter tripadvisor rating',
            'deal_price.required'=>'Please enter deal price',
            'deal_upto_price.required'=>'Please enter deal upto price',
            'deal_save_upto.required'=>'Please enter deal save upto price',
            'allow_point_redemption.required'=>'Please select allow point redemption status',
            'deal_status.required'=>'Please enter deal status',
            'featured_deal.required'=>'Please enter featured deal status',
            'what_you_will_love.required'=>'Please enter what you will love',
            'safety_standards.required'=>'Please enter safety standards',
            'how_to_get_there.required'=>'Please enter how to get there',
            'contact_us.required'=>'Please enter contact us',
            'property_name.required'=>'Please enter property name',
            'meta_title.required'=>'Please enter meta title',
            'meta_description.required'=>'Please enter meta description',
            'notes_from_hotel.required'=>'Please enter notes from hotel',
            'fabcredit_limit.required'=>'Please enter fabcredit limit',
            'travel_end_date.required'=>'Please enter travel end date',
            'deal_cashback.required'=>'Please enter deal cashback',
            'meta_keywords.required'=>'Please enter meta keywords',
            'deal_video.required'=>'Please enter deal video',
            'max_fab_redemption.required'=>'Please enter max fab redemption',
            'headline.required'=>'Please enter headline',
            'about_hotel.required'=>'Please enter about hotel',
            'inclusion.required'=>'Please enter inclusion',
            'location_google.required'=>'Please enter location google',
            'tag.required'=>'Please enter tag',
            'theme.required'=>'Please enter theme',
            'part_payment.required'=>'Please enter part payment status',
            'early_purchase.required'=>'Please enter early purchase',
            'latest_addition.required'=>'Please enter latest addition status',
            'usage_of_multiple_packages.required'=>'Please enter usage of multiple packages status',
            'purchase_multiple_deal.required'=>'Please enter purchase multiple deal status',
        ];
    }
}
