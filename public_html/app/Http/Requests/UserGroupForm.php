<?php
namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

class UserGroupForm extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        if($this->method()=='PATCH'){
            return [
            'group_name'=>"required",
            'user_id'=>"required",
            ];    
        }else{
            return [
            'group_id'=>"required|unique:user_group,group_id",
            'user_id'=>"required",
            ];    
        }
        
    }
    
    public function messages(){
        return [
            'group_name.required'=>'Group name is required',        
            'user_id.required'=>'User name is required',
        ];
    }
}
