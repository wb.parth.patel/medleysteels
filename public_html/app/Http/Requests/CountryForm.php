<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CountryForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        if($this->method()=='PATCH'){

            return [
            'name'=>"required|max:60|unique:country,name,".$this->id,
            ];    
        }else{
            return [
            'name'=>'required|max:60|unique:country,name'
            ];
        }
    }
    public function messages(){
        return [
            'name.required'=>'Please enter country name',
            'name.unique'=>'Country already exist',
        ];
    }
}
