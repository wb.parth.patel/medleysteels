<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GroupManagementForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        if($this->method()=='PATCH'){

            return [
            'group_name'=>'required',
            ];    
        }else{
            return [
            'group_name'=>'required',
            ];
        }
    }
    public function messages(){
        return [
            'group_name.required'=>'Please enter Group Name',
            'group_name.unique'=>'Group Name already exist',
        ];
    }
}
