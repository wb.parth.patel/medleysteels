<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FaqMasterForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        if($this->method()=='PATCH'){

            return [
            'question'=>'required',
            'answer'=>'required',
            ];    
        }else{
            return [
            'question'=>'required',
            'answer'=>'required',
            ];
        }
    }
    public function messages(){
        return [
            'question.required'=>'Please enter Question',
            'question.unique'=>'Question already exist',
            'answer.required'=>'Answer is Required',
        ];
    }
}
