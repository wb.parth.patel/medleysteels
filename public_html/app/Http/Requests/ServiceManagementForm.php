<?php
namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

class ServiceManagementForm extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        if($this->method()=='PATCH'){
            return [
            // 'hotel_user_id'=>"required",
            // 'hotel_id'=>"required",
            'service_desctiprion'=>'required',
            'service_name'=>'required|alpha_num',
            'service_detail'=>'required',

                        // 'image'=>"required",
            ];    
        }else{
            return [
            'service_desctiprion'=>'required',
            'service_name'=>'required|alpha_num',
            'service_detail'=>'required',
            ];    
        }
        
    }
    
    public function messages(){
        return [
            'hotel_id.required'=>'Hotel name is required',        
            'service_desctiprion.required'=>'Service description is required',
            'service_name.required'=>'Service name is required.',
            'service_detail.required'=>'Service short description is required',
            'category_image.required'=>'Category image is required',
        ];
    }
}
