<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderManagementForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->method()=='PATCH'){
            return [
                'name'=>'required',
                'email'=>'required',
                'contact_number'=>'required',
                'travel_start_date'=>'required',
                'travel_end_date'=>'required',
                'number_of_adults'=>'required',
                'number_of_childs'=>'required',
                'booking_date'=>'required',
                'hotel_email'=>'required',
                'price'=>'required',
                'fabcredits_used'=>'required',
                'deal_interest'=>'required',
                'package_interest'=>'required',
                'booking_status'=>'required',    
            ];
        }else{
            return [
                'name'=>'required',
                'email'=>'required',
                'contact_number'=>'required',
                'travel_start_date'=>'required',
                'travel_end_date'=>'required',
                'number_of_adults'=>'required',
                'number_of_childs'=>'required',
                'booking_date'=>'required',
                'hotel_email'=>'required',
                'price'=>'required',
                'fabcredits_used'=>'required',
                'deal_interest'=>'required',
                'package_interest'=>'required',
                'booking_status'=>'required',
            ];
        }
    }

    public function messages(){
        return[ 
            'name.required'=>'Please enter Name ',
            'email.required'=>'Please enter Email',
            'contact_number.required'=>'Please enter Contact Number',
            'travel_start_date.required'=>'Please enter Travel start date',
            'travel_end_date.required'=>'Please enter travel end date',
            'number_of_adults.required'=>'Please enter number of adult',
            'number_of_childs.required'=>'Please enter number of child',
            'booking_date.required'=>'Please enter booking date',
            'hotel_email.required'=>'Please enter hotel email',
            'price.required'=>'Please enter price',
            'fabcredits_used.required'=>'Please enter fab Credit Use or Not',
            'deal_interest.required'=>'Please enter Deal',
            'package_interest.required'=>'Please enter package interest',
            'booking_status.required'=>'Please enter booking status',            
        ];
    }
}
