<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CityForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        if($this->method()=='PATCH'){

            return [
            'city_name'=>"required|max:60|unique:city,city_name,".$this->id,
            'country_id'=>'required',
            'state_id'=>'required'
            ];    
        }else{
            return [
            'city_name'=>'required|max:60|unique:city,city_name',
            'country_id'=>'required',
            'state_id'=>'required'
            ];
        }
    }
    public function messages(){
        return [
            'city_name.required'=>'Please enter city name',
            'city_name.unique'=>'City already exist',
        ];
    }
}
