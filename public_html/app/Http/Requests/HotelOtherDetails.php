<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HotelOtherDetailsForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        if($this->method()=='PATCH'){

            return [
            
            ];    
        }else{
            return [
            'hotel_id'=>'required',
            'highlights'=>'required',
            'what_you_love'=>'required',
            'fine_print'=>'required',
            'services_facilities'=>'required',
            'get_there'=>'required',
            'contact_us'=>'required',
            'cancellation_policy'=>'required',
            ];
        }
    }
    public function messages(){
        return [
            'name.required'=>'Please enter country name',
            'name.unique'=>'Country already exist',
        ];
    }
}
