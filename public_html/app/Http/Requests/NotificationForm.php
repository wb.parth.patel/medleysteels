<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NotificationForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->method()=='PATCH'){
            return [
                'title'=>'required',
                'description'=>'required',
                'notification_type'=>'required',
                'notification_to'=>'required'
                
            ];
        }else{
            return [
                'title'=>'required',
                'description'=>'required',
                'notification_type'=>'required',
                'notification_to'=>'required'
            ];
        }
    }

    public function messages(){
        return[ 
            'title.required'=>'Please enter notification title ',
            'description.required'=>'Please enter notification description',
            'notification_type.required'=>'Please select notification type',
            'notification_to.required'=>'Please Select notification to'
            
        ];
    }
}
