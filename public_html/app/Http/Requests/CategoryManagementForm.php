<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryManagementForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        if($this->method()=='PATCH'){

            return [
            'category_name'=>"required|max:60|unique:category_management,category_name,".$this->id,
            // 'feature_name'=>'required',
            'description'=>'required'
            ];    
        }else{
            return [
            'category_name'=>'required|max:60|unique:category_management',
            // 'age'=>'required',
            // 'feature_name'=>'required',
            'description'=>'required'
            ];
        }
    }
    public function messages(){
        return [
            'category_name.required'=>'Please enter category name',
            'category_name.unique'=>'Category already exist',
            'feature_name.required'=>'Please enter feature name',
            'description.required'=>'Please enter description',
        ];
    }
}
