<?php

namespace App\Imports;

use App\common_model\PromoCodeMaster;
use App\common_model\User;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class PromoCodeImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new PromoCodeMaster([
            'deal_id'     => $row['deal_id'],
            'amount'    => $row['amount'],
            'promo_code_name'    => $row['promo_code_name'], 
            'code_start_date'    => $row['code_start_date'],
            'code_end_date'    => $row['code_end_date'],
            'user_specific'    => $row['user_specific'],
            'use_limit'    => $row['use_limit'],
            'user_id'    => $row['user_id'],
            'allow_credit'    => $row['allow_credit'],
            //'password' => \Hash::make($row['password']),
        ]);
    }
}
