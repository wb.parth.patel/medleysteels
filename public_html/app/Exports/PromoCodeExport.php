<?php

namespace App\Exports;

use App\common_model\User;
use App\common_model\PromoCodeMaster;
use Maatwebsite\Excel\Concerns\FromCollection;

class PromoCodeExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return PromoCodeMaster::all();
    }
}
