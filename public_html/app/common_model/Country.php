<?php

namespace App\common_model;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table='country';
    public $timestamps = false;
    protected $fillable = [
			    			'id',
			    			'name',
			    			'created_by',
			    			'created_date',
			    			'updated_by',
			    			'updated_date',
			    			'is_active',
			    			'ip_address',
			  			  ];
}
