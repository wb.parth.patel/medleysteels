<?php

namespace App\common_model;

use Illuminate\Database\Eloquent\Model;

class SliderImage extends Model
{
    protected $table='slider_image';
    public $timestamps = false;
    protected $fillable = [
			    			'id',
			    			'slider_id',
			    			'image',
			    			'created_date',
			    			'is_active',
			    			'created_by',
			  			  ];



   
}
