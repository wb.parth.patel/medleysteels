<?php

namespace App\common_model;

use Illuminate\Database\Eloquent\Model;

class NotificationManagement extends Model
{
    protected $table='notification_management';
    public $timestamps = false;
    protected $fillable = [
    					'id',
    					'hotel_id',
		    			'title',
		    			'description',
		    			'notification_type',
		    			'notification_to',
		    			'created_by',
		    			'created_date',
		    			'updated_by',
		    			'updated_date',
		    			'is_active',
		    			'ip_address',
		  			  ];
	public function Deal()
    {
        return $this->belongsTo('App\common_model\HotelManagement','hotel_id','id');
    }
}