<?php

namespace App\common_model;

use Illuminate\Database\Eloquent\Model;

class BlogManagement extends Model
{
    protected $table='blog_management';
    public $timestamps = false;
    protected $fillable = [
			    			'id',
			    			'hotel_user_id',
			    			'hotel_id',
			    			'category_id',
			    			'blog_title',
			    			'image',
			    			'blog_description',
			    			'created_date',
			    			'updated_by',
			    			'updated_date',
			    			'is_active',
			    			'ip_address',
			  			  ];
}
