<?php

namespace App\common_model;

use Illuminate\Database\Eloquent\Model;

class BlogCategoryMaster extends Model
{
    protected $table='blog_category_master';
    public $timestamps = false;
    protected $fillable = [
			    			'id',
			    			'user_id',
			    			'category_name',
			    			'category_image',
			    			'created_date',
			    			'updated_date',
			    			'is_active',
			    			'ip_address',
			  			  ];
}
