<?php

namespace App\common_model;

use Illuminate\Database\Eloquent\Model;

class UserReferance extends Model
{
    protected $table='user_referance_master';
    public $timestamps = false;
    protected $fillable = [
			    			'id',
			    			'user_id',
			    			'referance_name',
			    			'referance_contact_number',
			    			'deal_id',
			    			'credit_point',
			    			'created_by',
			    			'created_date',
			    			'updated_by',
			    			'updated_date',
			    			'is_active',
			    			'ip_address',
			  			  ];
}
