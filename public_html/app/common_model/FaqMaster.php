<?php

namespace App\common_model;

use Illuminate\Database\Eloquent\Model;

class FaqMaster extends Model
{
    protected $table='faq_master';
    public $timestamps = false;
    protected $fillable = [
			    			'id',
			    			'question',
			    			'answer',
			    			'created_by',
			    			'created_date',
			    			'updated_by',
			    			'updated_date',
			    			'is_active',
			    			'ip_address',
			  			  ];
}
