<?php

namespace App\common_model;

use Illuminate\Database\Eloquent\Model;

class Testimonials extends Model
{
    protected $table='testimonials';
    public $timestamps = false;
    protected $fillable = [
    					'id',
    					'title',
		    			'description',
		    			'image',
		    			'youtube_link',
		    			'created_by',
		    			'created_date',
		    			'updated_by',
		    			'updated_date',
		    			'is_active',
		    			'ip_address',
		  			  ];
}