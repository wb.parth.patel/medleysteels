<?php

namespace App\common_model;

use Illuminate\Database\Eloquent\Model;

class MenuManagement extends Model
{
    protected $table='menu';
    public $timestamps = false;
  
}
