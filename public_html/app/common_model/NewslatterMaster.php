<?php

namespace App\common_model;

use Illuminate\Database\Eloquent\Model;

class NewslatterMaster extends Model
{
    protected $table='newslatter_master';
    public $timestamps = false;
    protected $fillable = [
			    			'id',
			    			'subscribe_email',
			    			'created_date',
			    			'ip_address',
			  			  ];
}
