<?php

namespace App\common_model;

use Illuminate\Database\Eloquent\Model;

class ContactInquiry extends Model
{
    protected $table='contact_inquiry';
    public $timestamps = false;
    protected $fillable = [
			    			'id',
			    			'user_id',
			    			'name',
			    			'email_id',
			    			'contact_number',
			    			'queries',
			    			'message',
			    			'created_by',
			    			'created_date',
			    			'updated_by',
			    			'updated_date',
			    			'is_active',
			    			'ip_address',
			  			  ];
}
