<?php

namespace App\common_model;

use Illuminate\Database\Eloquent\Model;

class ServiceManagement extends Model
{
    protected $table='service_management';
    public $timestamps = false;
    protected $fillable = [
			    			'id',
			    			'service_name',
			    			'service_desctiprion',
			    			'service_image',
			    			'service_detail',
			    			'description',
			    			'created_date',
			    			'updated_by',
			    			'updated_date',
			    			'is_active',
			    			'ip_address',
			  			  ];
}
