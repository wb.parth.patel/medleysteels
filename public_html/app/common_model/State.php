<?php

namespace App\common_model;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $table='state';
    public $timestamps = false;
    protected $fillable = [
			    			'id',
			    			'country_id',
			    			'state_name',
			    			'created_by',
			    			'created_date',
			    			'updated_by',
			    			'updated_date',
			    			'is_active',
			    			'ip_address',
			  			  ];
}
