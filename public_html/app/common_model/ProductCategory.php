<?php

namespace App\common_model;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
	protected $table='products_category_master';
    public $timestamps = false;
    protected $fillable = [
			    			'id',
			    			'category_name',
			    			'category_img',
			    			'created_by',
			    			'created_date',
			    			'updated_by',
			    			'updated_date',
			    			'is_active',
			    			'ip_address',
			  			  ];
}