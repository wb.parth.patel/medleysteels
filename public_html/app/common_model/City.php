<?php

namespace App\common_model;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table='city';
    public $timestamps = false;
    protected $fillable = [
			    			'id',
			    			'state_id',
			    			'country_id',
			    			'city_name',
			    			'created_by',
			    			'created_date',
			    			'updated_by',
			    			'updated_date',
			    			'is_active',
			    			'ip_address',
			  			  ];
}
