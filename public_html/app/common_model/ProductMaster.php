<?php

namespace App\common_model;

use Illuminate\Database\Eloquent\Model;

class ProductMaster extends Model
{
	protected $table='products_management';
    public $timestamps = false;
    protected $fillable = [
			    			'id',
			    			'category_id',
			    			'product_name',
			    			'product_description',
			    			'created_by',
			    			'created_date',
			    			'updated_by',
			    			'updated_date',
			    			'is_active',
			    			'ip_address',

			  			  ];
}